#!/bin/bash
sudo apt-get install build-essential -y
sudo apt-get install libboost-all-dev libeigen3-dev -y
sudo apt-get install qt5-default -y

#FlyCapture 
sudo apt-get install libglade2-0 libglademm-2.4-1v5 libgtkmm-2.4-1v5 -y
sudo apt-get install libatkmm-1.6-dev libcairomm-1.0-dev libglade2-dev libglademm-2.4-dev libglibmm-2.4-dev libgtkglext1-dev libgtkglextmm-x11-1.2-0v5 libgtkglextmm-x11-1.2-dev libgtkmm-2.4-dev libpangomm-1.4-dev libpangox-1.0-dev libsigc++-2.0-dev -y

sudo apt install aptitude -y
sudo aptitude install libogre-1.9-dev ogre-1.9-dev -y
sudo aptitude install libois-dev -y

# Line for default install of protobuff. The end of the file install in another manner
# sudo apt-get install libois-dev libogre-1.9-dev libprotobuf-dev protobuf-compiler -y

# Downloading programs
cd ~/Downloads
if [ -a ode-0.15.2.tar.gz ]
	then
		echo "ode already exists"
else
	wget "https://bitbucket.org/odedevs/ode/downloads/ode-0.15.2.tar.gz" -O ode-0.15.2.tar.gz
fi

#Unpacking files
if [ -a ode-0.15.2 ]
	then
		echo "ode already unpack"
else
	tar -zxvf ode-0.15.2.tar.gz
fi

#Installing ode
cd ode-0.15.2
./configure --enable-double-precision --with-demos CXXFLAGS="-fpic"
make -j4
sudo make install


pushd $HOME/Documents
git clone https://github.com/jtsiomb/kdtree.git
cd kdtree
./configure
make -j 4
sudo make install
sudo ldconfig
popd

pushd /tmp/
wget -c -N https://github.com/Itseez/opencv/archive/2.4.11.zip -O opencv-2.4.11.zip
unzip opencv-2.4.11.zip

# not really sure about these next 2 lines, but it's necessary to
# install opencv dependencies before going further (@Thiago in 29/03/18)
sudo apt-get install build-essential -y
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y

pushd opencv-2.4.11
mkdir build
cd build
cmake -G "Unix Makefiles" -D CMAKE_CXX_COMPILER=/usr/bin/g++ CMAKE_C_COMPILER=/usr/bin/gcc -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=OFF -D WITH_OPENGL=ON -D BUILD_FAT_JAVA_LIB=ON -D INSTALL_TO_MANGLED_PATHS=ON -D INSTALL_CREATE_DISTRIB=ON -D INSTALL_TESTS=ON -D ENABLE_FAST_MATH=ON -D WITH_IMAGEIO=ON -D BUILD_SHARED_LIBS=OFF -D WITH_GSTREAMER=ON ..
make all -j 4
sudo make install
popd
popd


# Installing protobuff with the original version
pushd $HOME
wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-cpp-2.6.1.tar.gz
tar -xvf protobuf-cpp-2.6.1.tar.gz
cd protobuf-2.6.1
./configure
make -j 4
sudo make install
sudo ldconfig
popd

pushd $HOME
wget https://cmake.org/files/v3.5/cmake-3.5.2.tar.gz
tar -xvf cmake-3.5.2.tar.gz
cd cmake-3.5.2
./configure
make -j 4
sudo make install
sudo ldconfig
popd