#!/bin/bash
# Downloading programs
cd /tmp/
if [ -a ode-0.15.2.tar.gz ]
	then
		echo "ode already exists"
else
	wget "https://bitbucket.org/odedevs/ode/downloads/ode-0.15.2.tar.gz" -O ode-0.15.2.tar.gz
fi

#Unpacking files
if [ -a ode-0.15.2 ]
	then
		echo "ode already unpack"
else
	tar -zxvf ode-0.15.2.tar.gz
fi

#Installing correct gcc and g++ versions
sudo apt-get install gcc-5 -y
sudo apt-get install g++-5 -y

#Installing ode
cd ode-0.15.2
./configure --enable-double-precision --with-demos CXXFLAGS="-fpic"
make CC=gcc-5 CPP=g++-5 CXX=g++-5 LD=g++-5 -j4
sudo make install
