#!/bin/bash
pushd /tmp/
git clone https://github.com/jtsiomb/kdtree.git
cd kdtree
./configure
make -j 4
sudo make install
sudo ldconfig
popd
