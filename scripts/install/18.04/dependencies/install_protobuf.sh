#!/usr/bin/env bash
pushd /tmp/
wget https://github.com/google/protobuf/releases/download/v3.5.1/protobuf-cpp-3.5.1.tar.gz
tar -xvf protobuf-cpp-3.5.1.tar.gz
cd protobuf-3.5.1
./configure
make -j 4
sudo make install
sudo ldconfig
popd