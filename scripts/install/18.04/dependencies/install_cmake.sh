#!/bin/bash
pushd /tmp/
wget https://cmake.org/files/v3.5/cmake-3.5.2.tar.gz
tar -xvf cmake-3.5.2.tar.gz
cd cmake-3.5.2
./configure
make -j 4
sudo make install
sudo ldconfig
popd
