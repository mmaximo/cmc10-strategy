#!/bin/bash
chmod +x ./dependencies/*.sh
sudo apt-get update
clear && echo "### Installing qt5, boost and eigen ###" && sleep 3
./dependencies/install_qt5_boost_eigen.sh
clear && echo "### Installing flycapture dependencies ###" && sleep 3
./dependencies/install_flycapture.sh
clear && echo "### Installing ODE ###" && sleep 3
./dependencies/install_ode.sh
clear && echo "### Installing kdtree ###" && sleep 3
./dependencies/install_kdtree.sh
clear && echo "### Installing protobuf ###" && sleep 3
./dependencies/install_protobuf.sh
clear && echo "### Installing opencv ###" && sleep 3
./dependencies/install_opencv.sh
clear && echo "### Installing cmake ###" && sleep 3
./dependencies/install_cmake.sh
clear && echo "### Installation finished, now intall flycapture using the installation instuctions ###"
sleep 5
clear && echo "Oppening next installation instructions..."
sleep 3
nano ./manual_instalacao/step3-flycapture.txt

