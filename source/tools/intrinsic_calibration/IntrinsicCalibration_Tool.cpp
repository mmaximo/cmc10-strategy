//
// Created by igor on 01/03/17.
//

#include <iostream>
#include <sstream>

#include "core/communication/camera/PointGreyFrameGrabber.h"
#include "core/modeling/vision/CameraProjection.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace core::communication;
using namespace cv;
using namespace std;

void help() {
    cout << "Press:" << endl << "c: to start calibration" << endl
         << "u: to change between distorted and undistorted image" << endl << "esc: to exit the program" << endl;
}

class Settings {
public:
    Settings() : goodInput(false) {}

    enum Pattern {
        NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID
    };
    enum InputType {
        INVALID, WEBCAM, PT_GREY_CAMERA
    };

    int read(int argc, char *argv[])                          //Read serialization for this class
    {
//        if (argc < 3) {
//            std::cerr << "You need to pass 2 arguments:" << std::endl
//                      << "1) Either the /dev/videoX number for a webcam or \"PT_GREY_CAMERA\" for point grey cameras."
//                      << std::endl << "2) Output filename" << std::endl;
//            return -1;
//        }
//        if (argv[1][0] >= '0' && argv[1][0] <= '9') {
//            input = argv[1][0];
//            inputType = WEBCAM;
//        } else if (strcmp(argv[1], "PT_GREY_CAMERA") == 0) {
//            input = "PT_GREY_CAMERA";
//            inputType = PT_GREY_CAMERA;
//        } else {
//            std::cerr
//                    << "Wrong argument!" << std::endl << "You need to pass 2 arguments:" << std::endl
//                    << "1) Either the /dev/videoX number for a webcam or \"PT_GREY_CAMERA\" for point grey cameras."
//                    << std::endl << "2) Output path with filename" << std::endl;
//            return -1;
//        }
        input = "PT_GREY_CAMERA";
        inputType = PT_GREY_CAMERA;
        boardSize.width = 4;
        boardSize.height = 11;
        patternToUse = "ASYMMETRIC_CIRCLES_GRID";
        squareSize = 0.5 * 3.734e-2;
//        squareSize = 2.6e-2; // 3.7335
        nrFrames = 50;
        aspectRatio = 1;
        outputFileName = "../configs/intrinsicParameters.txt";
        calibZeroTangentDist = 0;
        calibFixPrincipalPoint = 0;
        flipVertical = 0;
        showUndistorted = 1;
        delay = 300;
        interpret();

        return 0;
    }

    void interpret() {
        goodInput = true;
        if (boardSize.width <= 0 || boardSize.height <= 0) {
            cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
            goodInput = false;
        }
        if (squareSize <= 10e-6) {
            cerr << "Invalid square size " << squareSize << endl;
            goodInput = false;
        }
        if (nrFrames <= 0) {
            cerr << "Invalid number of frames " << nrFrames << endl;
            goodInput = false;
        }

        if (input.empty())      // Check for valid input
            inputType = INVALID;
        else {
            if (input[0] >= '0' && input[0] <= '9') {
                stringstream ss(input);
                ss >> cameraID;
                inputType = WEBCAM;
            } else if (input.compare("PT_GREY_CAMERA") == 0)
                inputType = PT_GREY_CAMERA;

            if (inputType == WEBCAM)
                inputCapture.open(cameraID);
            else if (inputType == PT_GREY_CAMERA) {
                frameGrabber = new PointGreyFrameGrabber();
                frameGrabber->setPixelFormat(core::modeling::PixelFormat::RGB);
                frameGrabber->setFrameRate(60);
                frameGrabber->startCapturing();
            } else {
                inputType = INVALID;
                cerr << " Inexistent input: " << input;
                goodInput = false;
            }
        }

        flag = 0;
        if (calibFixPrincipalPoint) flag |= CV_CALIB_FIX_PRINCIPAL_POINT;
        if (calibZeroTangentDist) flag |= CV_CALIB_ZERO_TANGENT_DIST;
        if (aspectRatio) flag |= CV_CALIB_FIX_ASPECT_RATIO;


        calibrationPattern = NOT_EXISTING;
        if (!patternToUse.compare("CHESSBOARD")) calibrationPattern = CHESSBOARD;
        if (!patternToUse.compare("CIRCLES_GRID")) calibrationPattern = CIRCLES_GRID;
        if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID")) calibrationPattern = ASYMMETRIC_CIRCLES_GRID;
        if (calibrationPattern == NOT_EXISTING) {
            cerr << " Inexistent camera calibration mode: " << patternToUse << endl;
            goodInput = false;
        }
    }

    Mat nextImage() {
        Mat result;
        if (inputCapture.isOpened()) {
            Mat view0;
            inputCapture >> view0;
            view0.copyTo(result);
        } else if (inputType == PT_GREY_CAMERA) {
            unsigned char *data = frameGrabber->grabFrame();
            Mat view0(frameGrabber->getImageHeight(), frameGrabber->getImageWidth(), CV_8UC3, data);
            cvtColor(view0, view0, CV_RGB2BGR);
            view0.copyTo(result);
        }

        return result;
    }

public:
    Size boardSize;            // The size of the board -> Number of items by width and height
    Pattern calibrationPattern;// One of the Chessboard, circles, or asymmetric circle pattern
    float squareSize;          // The size of a square in your defined unit (point, millimeter,etc).
    int nrFrames;              // The number of frames to use from the input for calibration
    float aspectRatio;         // The aspect ratio
    int delay;                 // In case of a video input
    bool calibZeroTangentDist; // Assume zero tangential distortion
    bool calibFixPrincipalPoint;// Fix the principal point at the center
    bool flipVertical;          // Flip the captured images around the horizontal axis
    string outputFileName;      // The name of the file where to write
    bool showUndistorted;       // Show undistorted images after calibration
    string input;               // The input ->
    int cameraID;
    VideoCapture inputCapture;
    PointGreyFrameGrabber *frameGrabber;
    InputType inputType;
    bool goodInput;
    int flag;
    string patternToUse;
};

enum {
    DETECTION = 0, CAPTURING = 1, CALIBRATED = 2
};

bool runCalibrationAndSave(Settings &s, Size imageSize, Mat &cameraMatrix, Mat &distCoeffs,
                           vector<vector<Point2f> > imagePoints);

int main(int argc, char *argv[]) {
    Settings s;
    if (s.read(argc, argv) == -1)
        return -1;
    help();
    vector<vector<Point2f> > imagePoints;
    Mat cameraMatrix, distCoeffs;
    Size imageSize;
    int mode = DETECTION;
    clock_t prevTimestamp = 0;
    const Scalar RED(0, 0, 255), GREEN(0, 255, 0);
    const char ESC_KEY = 27;

    for (int i = 0;; ++i) {
        Mat view;

        view = s.nextImage();

        //-----  If no more image, or got enough, then stop calibration and show result -------------
        if (mode == CAPTURING && imagePoints.size() >= (unsigned) s.nrFrames) {
            if (runCalibrationAndSave(s, imageSize, cameraMatrix, distCoeffs, imagePoints))
                mode = CALIBRATED;
            else
                mode = DETECTION;
        }
        if (view.empty())          // If no more images then run calibration, save and stop loop.
        {
            if (imagePoints.size() > 0)
                runCalibrationAndSave(s, imageSize, cameraMatrix, distCoeffs, imagePoints);
            break;
        }


        imageSize = view.size();  // Format input image.
        if (s.flipVertical) flip(view, view, 0);

        vector<Point2f> pointBuf;

        bool found;
        switch (s.calibrationPattern) // Find feature points on the input format
        {
            case Settings::CHESSBOARD:
                found = findChessboardCorners(view, s.boardSize, pointBuf,
                                              CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK |
                                              CV_CALIB_CB_NORMALIZE_IMAGE);
                break;
            case Settings::CIRCLES_GRID:
                found = findCirclesGrid(view, s.boardSize, pointBuf);
                break;
            case Settings::ASYMMETRIC_CIRCLES_GRID:
                found = findCirclesGrid(view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID);
                break;
            default:
                found = false;
                break;
        }

        if (found)                // If done with success,
        {
            // improve the found corners' coordinate accuracy for chessboard
            if (s.calibrationPattern == Settings::CHESSBOARD) {
                Mat viewGray;
                cvtColor(view, viewGray, COLOR_BGR2GRAY);
                cornerSubPix(viewGray, pointBuf, Size(11, 11),
                             Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
            }

            if (mode == CAPTURING &&  // For camera only take new samples after delay time
                (clock() - prevTimestamp > s.delay * 1e-3 * CLOCKS_PER_SEC)) {
                imagePoints.push_back(pointBuf);
                prevTimestamp = clock();
            }

            // Draw the corners.
            drawChessboardCorners(view, s.boardSize, Mat(pointBuf), found);
        }

        //----------------------------- Output Text ------------------------------------------------
        string msg = (mode == CAPTURING) ? "100/100" :
                     mode == CALIBRATED ? "Calibrated" : "Press 'c' to start";
        int baseLine = 0;
        Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
        Point textOrigin(view.cols - 2 * textSize.width - 10, view.rows - 2 * baseLine - 10);

        if (mode == CAPTURING) {
            if (s.showUndistorted)
                msg = format("%d/%d Undist", (int) imagePoints.size(), s.nrFrames);
            else
                msg = format("%d/%d", (int) imagePoints.size(), s.nrFrames);
        }

        putText(view, msg, textOrigin, 1, 1, mode == CALIBRATED ? GREEN : RED);

        //------------------------- Video capture  output  undistorted ------------------------------
        if (mode == CALIBRATED && s.showUndistorted) {
            Mat temp = view.clone();
            undistort(temp, view, cameraMatrix, distCoeffs);
        }

        //------------------------------ Show image and check for input commands -------------------
        namedWindow("Image View", WINDOW_NORMAL);
        imshow("Image View", view);
        char key = (char) waitKey(50);

        if (key == ESC_KEY)
            break;

        if (key == 'u' && mode == CALIBRATED)
            s.showUndistorted = !s.showUndistorted;

        if (key == 'c') {
            mode = CAPTURING;
            imagePoints.clear();
        }
    }
    return 0;
}

static double computeReprojectionErrors(const vector<vector<Point3f> > &objectPoints,
                                        const vector<vector<Point2f> > &imagePoints,
                                        const vector<Mat> &rvecs, const vector<Mat> &tvecs,
                                        const Mat &cameraMatrix, const Mat &distCoeffs,
                                        vector<float> &perViewErrors) {
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for (i = 0; i < (int) objectPoints.size(); ++i) {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                      distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);

        int n = (int) objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err * err / n);
        totalErr += err * err;
        totalPoints += n;
    }

    return std::sqrt(totalErr / totalPoints);
}

static void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f> &corners,
                                     Settings::Pattern patternType /*= Settings::CHESSBOARD*/) {
    corners.clear();

    switch (patternType) {
        case Settings::CHESSBOARD:
        case Settings::CIRCLES_GRID:
            for (int i = 0; i < boardSize.height; ++i)
                for (int j = 0; j < boardSize.width; ++j)
                    corners.push_back(Point3f(float(j * squareSize), float(i * squareSize), 0));
            break;

        case Settings::ASYMMETRIC_CIRCLES_GRID:
            for (int i = 0; i < boardSize.height; i++)
                for (int j = 0; j < boardSize.width; j++)
                    corners.push_back(Point3f(float((2 * j + i % 2) * squareSize), float(i * squareSize), 0));
            break;
        default:
            break;
    }
}

static bool runCalibration(Settings &s, Size &imageSize, Mat &cameraMatrix, Mat &distCoeffs,
                           vector<vector<Point2f> > imagePoints, vector<Mat> &rvecs, vector<Mat> &tvecs,
                           vector<float> &reprojErrs, double &totalAvgErr) {

    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if (s.flag & CV_CALIB_FIX_ASPECT_RATIO)
        cameraMatrix.at<double>(0, 0) = 1.0;

    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

    objectPoints.resize(imagePoints.size(), objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
    double rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,
                                 distCoeffs, rvecs, tvecs, s.flag | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5);

    cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                            rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

    return ok;
}

// Print camera parameters to the output file
static void saveCameraParams(Settings &s, Size &imageSize, Mat &cameraMatrix, Mat &distCoeffs) {
    double cx = cameraMatrix.at<double>(0, 2);
    double cy = cameraMatrix.at<double>(1, 2);
    double fx = cameraMatrix.at<double>(0, 0);
    double fy = cameraMatrix.at<double>(1, 1);
    double k1 = distCoeffs.at<double>(0);
    double k2 = distCoeffs.at<double>(1);
    double p1 = distCoeffs.at<double>(2);
    double p2 = distCoeffs.at<double>(3);
    double k3 = distCoeffs.at<double>(4);
    core::modeling::CameraProjection::saveIntrinsicParams(s.outputFileName.data(), fx, fy, cx, cy, k1, k2, p1,
                                                                  p2, k3);
}

bool runCalibrationAndSave(Settings &s, Size imageSize, Mat &cameraMatrix, Mat &distCoeffs,
                           vector<vector<Point2f> > imagePoints) {
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(s, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs,
                             reprojErrs, totalAvgErr);
    cout << (ok ? "Calibration succeeded" : "Calibration failed")
         << ". avg re projection error = " << totalAvgErr << endl;

    if (ok)
        saveCameraParams(s, imageSize, cameraMatrix, distCoeffs);
    return ok;
}