//
// Created by igor on 02/03/17.
//

#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "core/communication/camera/PointGreyFrameGrabber.h"
#include "core/communication/camera/V4L2FrameGrabber.h"
#include "modeling/vision/CameraProjection.h"
#include "representations/Field.h"

using namespace std;
using namespace cv;
using namespace core;
using namespace core::communication;
using namespace core::representations;

const int ESC_KEY = 27;
const int TRAINING_POINTS = 10;
#define WINDOW_NAME "Extrinsic calibration"

std::vector<Point2f> imagePoints;
std::vector<Point3f> objectPoints;
bool calibrationDone = false;
int mapper[TRAINING_POINTS];
const char *intrinsicParamsFile = "../configs/intrinsicParameters.txt";
const char *extrinsicParamsFile = "../configs/extrinsicParameters.txt";
Point fieldPoints[14];

void saveExtrinsicParameters(const char *extrinsicParamsFile, Mat &rvec, Mat &tvec);

void readIntrinsicParameters(const char *intrinsicParamsFile, Mat &cameraMatrix, Mat &distCoeffs);

void initializeObjectPoints();

void initializeFieldPoints(PointGreyFrameGrabber &frameGrabber);

void drawField(Mat img, unsigned long i);

static void mouseClick(int event, int u, int v, int, void *imagePtr);

int main(int argc, char *argv[]) {
    Mat cameraMatrix(3, 3, cv::DataType<double>::type);
    Mat distCoeffs(5, 1, cv::DataType<double>::type);
    Mat rvec(3, 1, cv::DataType<double>::type);
    Mat tvec(3, 1, cv::DataType<double>::type);
    PointGreyFrameGrabber pointGreyFrameGrabber;
    int cols = 0;
    int rows = 0;
    unsigned char *data;
    unsigned char *rgbData;

    readIntrinsicParameters(intrinsicParamsFile, cameraMatrix, distCoeffs);
    initializeObjectPoints();
    initializeFieldPoints(pointGreyFrameGrabber);
    cols = pointGreyFrameGrabber.getImageWidth();
    rows = pointGreyFrameGrabber.getImageHeight();
    pointGreyFrameGrabber.startCapturing();

    namedWindow(WINDOW_NAME, WINDOW_NORMAL);

    Mat image(rows, cols, CV_8UC3);
    rgbData = new unsigned char[3 * cols * rows];
    setMouseCallback(WINDOW_NAME, mouseClick, &image);

    std::cout << "Calibration started. Double click on the correspondent red balls." << std::endl;

    while (true) {
        data = pointGreyFrameGrabber.grabFrame();
        core::modeling::Image::yuv422ToRgb(data, rgbData, cols, rows);
        image.data = rgbData;
        cvtColor(image, image, CV_RGB2BGR);
        drawField(image, imagePoints.size());
        imshow("Extrinsic calibration", image);
        if (imagePoints.size() == TRAINING_POINTS && !calibrationDone) {
            solvePnP(objectPoints, imagePoints, cameraMatrix, distCoeffs, rvec, tvec);
            saveExtrinsicParameters(extrinsicParamsFile, rvec, tvec);
            calibrationDone = true;
            std::cout << "Calibration done!" << std::endl;
        }

        int key = waitKey(1) & 255; // bit mask with 11111111b = 255d
        if (key == ESC_KEY || getWindowProperty(WINDOW_NAME, 0) == -1)
            return 0;
    }
}

void saveExtrinsicParameters(const char *extrinsicParamsFile, Mat &rvec, Mat &tvec) {
    Mat rotationMatrix(3, 3, cv::DataType<double>::type);
    Rodrigues(rvec, rotationMatrix);
    core::modeling::CameraProjection::saveExtrinsicParams(extrinsicParamsFile, rotationMatrix.at<double>(0, 0),
                                                  rotationMatrix.at<double>(0, 1), rotationMatrix.at<double>(0, 2),
                                                  rotationMatrix.at<double>(1, 0), rotationMatrix.at<double>(1, 1),
                                                  rotationMatrix.at<double>(1, 2), rotationMatrix.at<double>(2, 0),
                                                  rotationMatrix.at<double>(2, 1), rotationMatrix.at<double>(2, 2),
                                                  tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2));
}

void readIntrinsicParameters(const char *intrinsicParamsFile, Mat &cameraMatrix, Mat &distCoeffs) {
    double fx, fy, cx, cy, k1, k2, k3, p1, p2;
    std::ifstream file(intrinsicParamsFile);
    std::string line;
    char dummy;

    std::getline(file, line);
    file >> fx >> dummy >> fy >> dummy >> cx >> dummy >> cy >> dummy;

    std::getline(file, line);
    file >> k1 >> dummy >> k2 >> dummy >> p1 >> dummy >> p2 >> dummy >> k3;

    file.close();

    distCoeffs.at<double>(0) = k1;
    distCoeffs.at<double>(1) = k2;
    distCoeffs.at<double>(2) = p1;
    distCoeffs.at<double>(3) = p2;
    distCoeffs.at<double>(4) = k3;

    cameraMatrix.at<double>(0, 0) = fx;
    cameraMatrix.at<double>(0, 1) = 0;
    cameraMatrix.at<double>(0, 2) = cx;
    cameraMatrix.at<double>(1, 0) = 0;
    cameraMatrix.at<double>(1, 1) = fy;
    cameraMatrix.at<double>(1, 2) = cy;
    cameraMatrix.at<double>(2, 0) = 0;
    cameraMatrix.at<double>(2, 1) = 0;
    cameraMatrix.at<double>(2, 2) = 1;
}

//      ______________(1)_______________
//     |               |                |
//     |               |                |
//(9)_(10)             |               (2)_(3)
// |                   |                    |
// |    LEFT SIDE      |      RIGHT SIDE    |
// | _                 |                  _ |
//(8) (7)              |               (5) (4)
//     |               |                |
//     |______________(6)_______________|
//

void initializeObjectPoints() {
    // RIGHT SIDE
    objectPoints.push_back(Point3f((float) 0.0,               // 1
                                   (float) Field::TOP_WALL_Y, 0)); //

    objectPoints.push_back(Point3f((float) Field::GOAL_RIGHT_X,         // 2
                                   (float) Field::GOAL_LENGTH_Y / 2, 0)); //

    objectPoints.push_back(Point3f((float) (Field::GOAL_RIGHT_X + Field::GOAL_LENGTH_X), // 3
                                   (float) Field::GOAL_LENGTH_Y / 2, 0));                  //

    objectPoints.push_back(Point3f((float) (Field::GOAL_RIGHT_X + Field::GOAL_LENGTH_X), // 4
                                   (float) -Field::GOAL_LENGTH_Y / 2, 0));                 //

    objectPoints.push_back(Point3f((float) (Field::GOAL_RIGHT_X),        // 5
                                   (float) -Field::GOAL_LENGTH_Y / 2, 0)); //

    // LEFT SIDE
    objectPoints.push_back(Point3f((float) 0.0,                // 6
                                   (float) -Field::TOP_WALL_Y, 0)); //

    objectPoints.push_back(Point3f((float) (Field::GOAL_LEFT_X),         // 7
                                   (float) -Field::GOAL_LENGTH_Y / 2, 0)); //

    objectPoints.push_back(Point3f((float) (Field::GOAL_LEFT_X - Field::GOAL_LENGTH_X), // 8
                                   (float) -Field::GOAL_LENGTH_Y / 2, 0));                //

    objectPoints.push_back(Point3f((float) (Field::GOAL_LEFT_X - Field::GOAL_LENGTH_X), // 9
                                   (float) Field::GOAL_LENGTH_Y / 2, 0));                 //

    objectPoints.push_back(Point3f((float) Field::GOAL_LEFT_X,          // 10
                                   (float) Field::GOAL_LENGTH_Y / 2, 0)); //
}


static void mouseClick(int event, int u, int v, int, void *imagePtr) {
    if (event == CV_EVENT_LBUTTONDBLCLK) {
        if (imagePoints.size() < TRAINING_POINTS) {
            imagePoints.push_back(Point2f(u, v));
            printf("Pixel clicked: u = %4d, v = %4d\n", u, v);
        }
    }
}

void drawField(Mat img, unsigned long j) {
    for (int i = 0; i < 13; i++)
        line(img, fieldPoints[i], fieldPoints[i + 1], Scalar(0, 0, 255), 2, CV_AA);

    line(img, fieldPoints[13], fieldPoints[0], Scalar(0, 0, 255), 2, CV_AA);

    if (j < TRAINING_POINTS)
        circle(img, fieldPoints[mapper[j]], 5, Scalar(0, 0, 255), 5, CV_AA);
}

void initializeFieldPoints(PointGreyFrameGrabber &frameGrabber) {
    int PIXEL_HEIGHT = int(frameGrabber.getImageHeight() * 0.5);
    int PIXEL_WIDTH = int(frameGrabber.getImageWidth() * 0.5);

    double heightScale = PIXEL_HEIGHT / Field::FIELD_LENGTH_Y;
    double widthScale = PIXEL_WIDTH / Field::FIELD_LENGTH_X;

    int PIXEL_TOP_Y = int(-Field::TOP_WALL_Y * heightScale) + PIXEL_HEIGHT;
    int PIXEL_BOTTOM_Y = int(-Field::BOTTOM_WALL_Y * heightScale) + PIXEL_HEIGHT;
    int PIXEL_GOAL_TOP_Y = int(-Field::GOAL_LENGTH_Y * heightScale / 2) + PIXEL_HEIGHT;
    int PIXEL_GOAL_BOTTOM_Y = int(Field::GOAL_LENGTH_Y * heightScale / 2) + PIXEL_HEIGHT;
    int PIXEL_GOAL_LEFT_X = int(-Field::FIELD_LENGTH_X * widthScale / 2) + PIXEL_WIDTH;
    int PIXEL_GOAL_RIGHT_X = int(Field::FIELD_LENGTH_X * widthScale / 2) + PIXEL_WIDTH;
    int PIXEL_GOAL_INSIDE_RIGHT_X = PIXEL_GOAL_RIGHT_X + int(Field::GOAL_LENGTH_X * widthScale);
    int PIXEL_GOAL_INSIDE_LEFT_X = PIXEL_GOAL_LEFT_X - int(Field::GOAL_LENGTH_X * widthScale);

    mapper[0] = 0; // field top mid (1)
    mapper[1] = 2;
    mapper[2] = 3;
    mapper[3] = 4;
    mapper[4] = 5; // goal right (2 ~ 5)

    mapper[5] = 7; // field bottom mid (6)
    mapper[6] = 9;
    mapper[7] = 10;
    mapper[8] = 11;
    mapper[9] = 12; // goal left (7 ~ 10)

    fieldPoints[0] = Point(PIXEL_WIDTH, PIXEL_TOP_Y);           // field top mid   (1)
    fieldPoints[1] = Point(PIXEL_GOAL_RIGHT_X, PIXEL_TOP_Y);    // field top right

    fieldPoints[2] = Point(PIXEL_GOAL_RIGHT_X, PIXEL_GOAL_TOP_Y);           // right goal, top left     (2)
    fieldPoints[3] = Point(PIXEL_GOAL_INSIDE_RIGHT_X, PIXEL_GOAL_TOP_Y);    // right goal, top right    (3)
    fieldPoints[4] = Point(PIXEL_GOAL_INSIDE_RIGHT_X, PIXEL_GOAL_BOTTOM_Y); // right goal, bottom right (4)
    fieldPoints[5] = Point(PIXEL_GOAL_RIGHT_X, PIXEL_GOAL_BOTTOM_Y);        // right goal, bottom left  (5)

    fieldPoints[6] = Point(PIXEL_GOAL_RIGHT_X, PIXEL_BOTTOM_Y); // field bottom right
    fieldPoints[7] = Point(PIXEL_WIDTH, PIXEL_BOTTOM_Y);        // field bottom mid   (6)
    fieldPoints[8] = Point(PIXEL_GOAL_LEFT_X, PIXEL_BOTTOM_Y);  // field bottom left

    fieldPoints[9] = Point(PIXEL_GOAL_LEFT_X, PIXEL_GOAL_BOTTOM_Y);         // left goal, bottom right (7)
    fieldPoints[10] = Point(PIXEL_GOAL_INSIDE_LEFT_X, PIXEL_GOAL_BOTTOM_Y); // left goal, bottom left  (8)
    fieldPoints[11] = Point(PIXEL_GOAL_INSIDE_LEFT_X, PIXEL_GOAL_TOP_Y);    // left goal, top left     (9)
    fieldPoints[12] = Point(PIXEL_GOAL_LEFT_X, PIXEL_GOAL_TOP_Y);           // left goal, top right    (10)

    fieldPoints[13] = Point(PIXEL_GOAL_LEFT_X, PIXEL_TOP_Y);    // field top left
}
