%
Reading simulator
data
        data = load('simulatorDynamics_AgentTest_8234');

x = data(
:,1);
y = data(
:,2);
omega = unwrap(data(
:,3));

%
Constants
        t = 30e-3;
L = 0.0331;
R = 0.03;
T = 1 / 30;

%
Generating vector
time
        time = zeros(length(x), 1);

for
i = 1
:(
length(time)
)
time(i) = 1 / 30 * (i - 1);
i = i + 1;
end

%
Generating vector
vx(t)
vx = zeros(length(x), 1);

i = length(x);
while i > 1
vx(i) = (x(i) - x(i - 1)) / t;

i = i - 1;

end

vx(

1) = vx(2);

%
Generating vector
vy(t)
vy = zeros(length(y), 1);

i = length(y);
while i > 1
vy(i) = (y(i) - y(i - 1)) / t;

i = i - 1;

end

vy(

1) = vy(2);

%
Generating vector
v(t)
v = zeros(length(x), 1);

for
i = 1
:(
length(v)
)
v(i) = sqrt((vx(i) ^ 2) + (vy(i) ^ 2));
i = i + 1;
end

%
Plotting v
x t
%figure(1);
%
plot(time, v
);
%title('v x t');
%xlabel('t (s)');
%ylabel('v (m/s)');

%
Generating vector
w(t)
w = zeros(length(omega), 1);

i = length(omega);
while i > 1
w(i) = (omega(i) - omega(i - 1)) / t;

i = i - 1;

end

w(

1) = w(2);

%
Plotting w
x t
%figure(2);
%
plot(time, w
);
%title('w x t');
%xlabel('t (s)');
%ylabel('w (rad/s)');

%
Generating vector
wr(t)
wl_sim = zeros(length(omega), 1);

for
i = 1
:(
length(wr_sim)
)

wl_sim(i) = (v(i) + (w(i) * L)) / R;

i = i + 1;
end

%

Vector wl(t)
        wr_sim = zeros(length(omega), 1);

for
i = 1
:(
length(wr_sim)
)

wr_sim(i) = (v(i) - (w(i) * L)) / R;

i = i + 1;
end


        timeReal = simout.Time;
dataReal = simout.Data;

wr_real = dataReal(
:,1);
wl_real = dataReal(
:,2);


%
Ploting w_real
and w_simulated
figure(1);
plot(timeReal, wr_real,
'r-');
hold on
plot(time, wr_sim,
'b-');
legend('\omega_{r,real}','\omega_{r,sim}');

figure(2);
plot(timeReal, wl_real,
'r-');
hold on
plot(time, wl_sim,
'b-');
legend('\omega_{l,real}','\omega_{l,sim}');


