//
// Created by Thiago on 25/04/18.
//

#include "manager/GameManager.h"

#include <iostream>
using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace manager {

using tools::simulator::server::utils::SIDE;

GameManager::GameManager() : GameManager(GameType::NORMAL) {}

GameManager::GameManager(GameType gameType) :
             GameManager(Player::PLAYERS_PER_SIDE, Player::PLAYERS_PER_SIDE, gameType) {}

GameManager::GameManager(int numberOfLeftPlayers,
                         int numberOfRightPlayers,
                         GameType gameType) :
             gameStatistics(statistics::GameStatistics::getInstance()),
                 leftTeam(physics::Team(SIDE::LEFT)),
             rightTeam(physics::Team(SIDE::RIGHT)),
             judge(this->leftTeam,
                   this->rightTeam,
                   this->ball),
             simWorld(physics::SimulationWorld::getInstance()),
             odeWorld(physics::ODEWorld::getInstance()),
             communicationClient1(DEFAULT_PORT_1),
             communicationClient2(DEFAULT_PORT_2),
             messageParser(this->leftTeam,
                           this->rightTeam,
                           this->ball) {

    this->setPreference(SIDE::LEFT);

    this->numberOfLeftPlayers  = numberOfLeftPlayers;
    this->numberOfRightPlayers = numberOfRightPlayers;

    this->simWorld.addToSimulation(&this->ball);
    this->gameStatistics.setGameType(gameType);

    this->stopWheels();
}

GameManager::~GameManager() {
//    this->communicationClient1.join();
//    this->communicationClient2.join();
}

void GameManager::setPreference(SIDE side) {
    if(side == SIDE::LEFT) {
        this->leftPreference  = true;
        this->rightPreference = false;
    }
    else if(side == SIDE::RIGHT) {
        this->leftPreference  = false;
        this->rightPreference = true;
    }
}

void GameManager::switchPreference() {
    if(this->leftPreference)
        this->setPreference(SIDE::RIGHT);
    else
        this->setPreference(SIDE::LEFT);
}

bool GameManager::isLeftPreference() {
    return this->leftPreference;
}

bool GameManager::isRightPreference() {
    return this->rightPreference;
}

int GameManager::getNumberOfLeftPlayers() {
    return this->numberOfLeftPlayers;
}

int GameManager::getNumberOfRightPlayers() {
    return this->numberOfRightPlayers;
}

void GameManager::simulationStep() {
    this->simWorld.updateODEWorld();
    this->odeWorld.update();
}

void GameManager::finish() {
    this->gameStatistics.endSimulation();
}

void GameManager::finishPenalty(int numberOfPenalties) {
    this->gameStatistics.logPenalty(numberOfPenalties);
}

void GameManager::addLeftScore() {
    this->gameStatistics.addLeftScore();
}

void GameManager::addRightScore() {
    this->gameStatistics.addRightScore();
}
void GameManager::addPenaltyScore(){
    this->gameStatistics.addPenaltyScore();
}
int GameManager::getRightScore() {
    return this->gameStatistics.getRightScore();
}

int GameManager::getLeftScore() {
    return this->gameStatistics.getLeftScore();
}

void GameManager::restartMatch() {
    this->gameStatistics.restartMatch();
}

void GameManager::update() {
    for (int i = 0; i < this->numberOfLeftPlayers; i++) {
        this->desiredWheelSpeeds[0][i].right = this->getCommandInfo()[0].playersWheelSpeed[i].right;
        this->desiredWheelSpeeds[0][i].left  = this->getCommandInfo()[0].playersWheelSpeed[i].left;
    }

    for (int i = 0; i < this->numberOfRightPlayers; i++) {
        this->desiredWheelSpeeds[1][i].right = this->getCommandInfo()[1].playersWheelSpeed[i].right;
        this->desiredWheelSpeeds[1][i].left  = this->getCommandInfo()[1].playersWheelSpeed[i].left;
    }

    this->leftTeam.setWheelSpeeds(this->desiredWheelSpeeds[0]);
    this->rightTeam.setWheelSpeeds(this->desiredWheelSpeeds[1]);

    // Cartas para fazer os players ficarem em pe
//    for (int i = 0; i < 3; i++) {
//        double r = ((double) rand() / (RAND_MAX));
//        if (r > 0.99) {
//            this->leftTeam->getRobot(i)->keepUp();
//            this->rightTeam->getRobot(i)->keepUp();
//        }
//    }

    // checks if a foul has been made
    this->judge.update();

    if(this->judge.getWasRightGoal())
        this->setPreference(SIDE::LEFT);

    else if(this->judge.getWasLeftGoal())
        this->setPreference(SIDE::RIGHT);

    if(this->ball.getPosition()[0] > 0) // checks x coordinate of ball
        this->gameStatistics.addTimeBallOnRightSide();
    else
        this->gameStatistics.addTimeBallOnLeftSide();
}

bool GameManager::getGameStarted() {
    return this->judge.getGameStarted();
}

bool GameManager::getPlayAgain() {
    return this->judge.getPlayAgain();
}

bool GameManager::getGameEnded() {
    return this->judge.getGameEnded();
}

bool GameManager::getWasGoal() {
    return this->judge.getWasGoal();
}

bool GameManager::getWasFreeBall() {
    return this->judge.getWasFreeBall();
}

bool GameManager::getWasFreeKick() {
    return this->judge.getWasFreeKick();
}

bool GameManager::getWasGoalKick() {
    return this->judge.getWasGoalKick();
}

bool GameManager::getWasPenaltyKick() {
    return this->judge.getWasPenaltyKick();
}

void GameManager::stopWheels() {
    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        this->desiredWheelSpeeds[0][i].left  = 0.0;
        this->desiredWheelSpeeds[0][i].right = 0.0;

        this->desiredWheelSpeeds[1][i].left  = 0.0;
        this->desiredWheelSpeeds[1][i].right = 0.0;
    }
}

void GameManager::exchangeInfo() {
    this->communicationClient1.sendMessage(this->messageParser.parseOut(false));
    this->communicationClient2.sendMessage(this->messageParser.parseOut(true));

    this->commandInfo[0] = this->messageParser.parseIn(this->communicationClient1.getLastMessage());
    this->commandInfo[1] = this->messageParser.parseIn(this->communicationClient2.getLastMessage());
}

physics::Ball &GameManager::getBall() {
    return this->ball;
}

physics::Team &GameManager::getLeftTeam() {
    return this->leftTeam;
}

physics::Team &GameManager::getRightTeam() {
    return this->rightTeam;
}

core::control::ControlInterface *GameManager::getCommandInfo() {
    return this->commandInfo;
}

void GameManager::setUpCommunication() {
    std::cout << "Connecting to simulatorMain 1\n";
    if (!this->communicationClient1.establishConnection())
        fprintf(stderr, "Error accepting %d\n", errno);
    std::cout << "Connected to simulatorMain 1\n\n";
    this->communicationClient1.start();

    std::cout << "Connecting to simulatorMain 2\n";
    if (!this->communicationClient2.establishConnection())
        fprintf(stderr, "Error accepting %d\n", errno);
    std::cout << "Connected to simulatorMain2\n\n";
    this->communicationClient2.start();
}

} // manager
} // server
} // simulator
} // tools
