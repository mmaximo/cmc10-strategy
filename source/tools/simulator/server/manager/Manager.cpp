//
// Created by Muzio on 31/03/15.
//

#include "manager/Manager.h"
//#include "source/tools/simulator/server/statistics/GameStatistics.h"
//#include "manager/GameManager.h"
//#define DEBUG_COMM

#include <iostream>
using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace manager {

using core::representations::Player;
using core::representations::Field;

Manager::Manager() :
         Manager(Player::PLAYERS_PER_SIDE,
                 Player::PLAYERS_PER_SIDE) {}

Manager::Manager(GameType gameType) :
         Manager(Player::PLAYERS_PER_SIDE,
                 Player::PLAYERS_PER_SIDE,
                 gameType) {}

Manager::Manager(int numberOfLeftPlayers,
                 int numberOfRightPlayers,
                 GameType gameType) : gameManager(numberOfLeftPlayers,
                                                  numberOfRightPlayers,
                                                  gameType),
                                      stateManager(this->gameManager),
                                      ogreManager(gui::OgreManager::getInstance()),
                                      simWorld(physics::SimulationWorld::getInstance()) {
//    this->debugStepCounter = 0;
    this->running = true;
}

Manager::~Manager() {}

//main method
void Manager::runAsync() {
    this->setUp();

    //std::thread guiThread(&Manager::startGUI,this);
    int numberOfMessages = 50000;
    while (numberOfMessages-- > 0) {
        this->runSimulator();
        this->sleepForRealTimeSpeed();
    }

    std::cout << "Ending simulation...\n";
    this->running = false;
}

void Manager::runAccelerated(int numberOfMatches) {
    this->setUp();

    //std::thread guiThread(&Manager::startGUI,this);
    int matchDuration = 60 * 60 * 10; // FPS * seconds per minute * minutes

    for (int i = 1; i <= numberOfMatches; i++) {
        this->gameManager.setPreference(this->coinToss());
        std::cout << "Started new match #" << i << std::endl;
        this->runHalfMatch(matchDuration/2);

        this->gameManager.switchPreference();
        std::cout << "Half Time of match #" << i << std::endl;
        this->runHalfMatch(matchDuration/2);
        this->gameManager.restartMatch();
    }

    this->running = false;
    this->gameManager.finish();
}

void Manager::setUp() {
    //Connect to simulatorMain
    this->gameManager.setUpCommunication();

    this->fps = 0;
    frameStartTimeToFPS = utils::Timer::getCurrentTime();
}

void Manager::startGUI() {
    double time = Clock::getTime();
    while (this->running) {
        if (Clock::getTime() - time > 1.0/30) {
            time = Clock::getTime();
            this->simWorld.updateOgreWorld();
            this->ogreManager.updateFrame();
        }
    }
}

void Manager::runSimulator() {
    this->frameStartTime = utils::Timer::getCurrentTime();
    this->gameManager.exchangeInfo();

    for (int cont = 1; cont <= SIMULATION_STEPS_IN_ONE_COMMUNICATION_STEP; cont++) {
        //sleep(2);
        this->stateManager.runState();
        this->gameManager.update();
        this->stateManager.loadState();
    }

    this->fps++;

    if (this->frameStartTimeToFPS + 1 < utils::Timer::getCurrentTime()) {
//        std::cout << "[server/.../Manager.cpp] #" << debugStepCounter++ << " FPS=" << fps << std::endl;
        this->fps = 0;
        this->frameStartTimeToFPS = utils::Timer::getCurrentTime();
    }
}

void Manager::runPenalty(int numberOfPenalties) {
    this->setUp();

    int matchDuration = 60 * 3; // FPS * seconds

    std::cout << "Started new Penalty Test #" << std::endl;
    for (int i = 1; i <= numberOfPenalties; i++) {
        int counter = 0;
        while((counter < matchDuration) and
              (this->gameManager.getRightScore() == 0) and
              (this->gameManager.getLeftScore()  == 0)) {

            this->runSimulator();
            this->sleepForRealTimeSpeed();

            counter++;
        }

        if (this->gameManager.getLeftScore() != 0) {
            std::cout << "It was a penalty goal!" << std::endl;
            this->gameManager.addPenaltyScore();
        }
        else
            std::cout << "Try again..." << std::endl;

        std::cout << "Time to exit match " << counter / 60.0 << " seconds" << std::endl;

        this->resetStatePenalty();
        this->gameManager.restartMatch();
    }

    this->gameManager.finishPenalty(numberOfPenalties);
    this->running = false;
}

void Manager::runPenaltyAccelerated(int numberOfPenalties) {
    this->setUp();

    int matchDuration = 60 * 3; // FPS * seconds

    for (int i = 1; i <= numberOfPenalties; i++) {
        std::cout << std::endl << "Started new Penalty Test #" << i << std::endl;

        int counter = 0;
        while((counter++ < matchDuration) and
              (this->gameManager.getRightScore() == 0) and
              (this->gameManager.getLeftScore()  == 0)) {

            usleep(20);
            this->runSimulator();
        }

        if (this->gameManager.getLeftScore() != 0) {
            std::cout << "It was a penalty goal!" << std::endl;
            this->gameManager.addPenaltyScore();
        }
        else
            std::cout << "Try again..." << std::endl;

        std::cout << "Time to exit match " << counter / 60.0 << " seconds" << std::endl;

        this->resetStatePenalty();
        this->gameManager.restartMatch();
    }

    this->gameManager.finishPenalty(numberOfPenalties);
    this->running = false;
}

void Manager::runShotOfGoal(int numberOfShots) {
    this->setUp();

    int matchDuration = 60 * 10; // FPS * seconds per minute

    std::cout << "Started new Shots of Goal Test #" << std::endl;
    for (int i = 1; i <= numberOfShots; i++) {
        for (int k = 1; k <= matchDuration; k++) {
            this->runSimulator();
            this->sleepForRealTimeSpeed();
        }

        this->resetState();
        this->gameManager.restartMatch();
    }

    this->gameManager.finish();
    this->running = false;
}

void Manager::runFreeBall(int numberOfFreeBalls) {
    this->setUp();

    int matchDuration = 60 * 5; // FPS * seconds per minute
    std::cout << "Started new Shots of Goal Test #" << std::endl;

    for (int i = 0; i < numberOfFreeBalls; i++) {
        for (int k = 0; k < matchDuration; k++) {
            this->runSimulator();
            this->sleepForRealTimeSpeed();
        }

        this->resetState();
        this->gameManager.restartMatch();
    }

    this->gameManager.finish();
    this->running = false;
}

SIDE Manager::coinToss() {
    if(utils::getRandom() < 0.5)
        return SIDE::LEFT;
    return SIDE::RIGHT;
}

void Manager::sleepForRealTimeSpeed() {
    stepDelta = utils::Timer::getCurrentTime() - frameStartTime;

    while (stepDelta > COMMUNICATION_TIME_STEP) {
//        std::cout << "Skipped frame" << std::endl;
        stepDelta = 0;
    }

    std::this_thread::sleep_for(std::chrono::microseconds(
            (int64_t) (1000000 * (COMMUNICATION_TIME_STEP - stepDelta))));
}

void Manager::resetState() {
    this->stateManager.setState(gamestate::STARTING);
    this->stateManager.loadState();
}
void Manager::resetStatePenalty(){
    this->stateManager.setState(gamestate::FOUL_PENALTYKICK);
    this->stateManager.loadState();
}
void Manager::runHalfMatch(int duration, int step_delay, int match_delay) {
    this->resetState();

    for (int k = 1; k <= duration; k++) {
        this->runSimulator();
        usleep(step_delay);
    }

    usleep(match_delay);
}

void Manager::runUnivectorOptimization() {
    this->setUp();

    //std::thread guiThread(&Manager::startGUI,this);
    int maxTime = 60 * 60 * 3; // FPS * seconds per minute * minutes
    struct Track{
        Vector2<double> ballPos;
        Vector2<double> playerPos;
    };

    vector<Track> tracks;

    Track aux;
    const int tam = 2;
    int jLimit = tam+3;
    for(int i = -tam; i <= tam+1; i ++) {
        //if (i == 0) jLimit ++;
        for(int j = -jLimit; j <= jLimit; j ++) {
            aux.ballPos = Vector2<double>(Field::RIGHT_WALL_X * i/(tam+2),
                                          Field::TOP_WALL_Y * j/(tam+2));
            aux.playerPos = Vector2<double>(aux.ballPos.x + 0.1, aux.ballPos.y + 0.1);
            tracks.push_back(aux);
        }
    }

//    for(auto &track : tracks) {
//        cout << "ball.x = " << track.ballPos.x << endl;
//        cout << "ball.y = " << track.ballPos.y << endl;
//    }

    int counter = 0;

    std::ofstream nameFileStartPositions,
            startPositions;
    std::string nameFile = "univector-optimization.conf";

    nameFileStartPositions.open("../configs/simulator_predefined_positions/lastModifiedFile.txt");
    nameFileStartPositions << nameFile;
    nameFileStartPositions.close();

    int tracksDone = 0;

    for(auto &track : tracks) {
        startPositions.open("../configs/simulator_predefined_positions/" + nameFile);
        startPositions << track.ballPos.x << " " << track.ballPos.y << endl << endl;
        startPositions << 3 << endl;
        startPositions << track.playerPos.x << " " << track.playerPos.y << " " << 0 << endl;
        startPositions << -0.71 << " " << 0 << " " << 1.5708 << endl;
        startPositions << 0.65 << " " << -0.1 << " " << 1.5708 << endl << endl;
        startPositions << 1 << endl;
        startPositions << 0.65 << " " << 0.1 << " " << 1.5708 << endl << endl;
        startPositions.close();
        cout << "1" << endl;
        this->gameManager.setPreference(SIDE::LEFT);
        this->resetState();

//        cout << "New match" << endl;
//        cout << "ball.x = " << track.ballPos.x << endl;
//        cout << "ball.y = " << track.ballPos.y << endl;

        int counterBeforeStep = counter;
        while(this->gameManager.getLeftScore()  == 0 and counter < maxTime) {
            this->runSimulator();
            this->sleepForRealTimeSpeed();

            counter ++;
            if(this->gameManager.getRightScore() > 0) counter = maxTime; // We made a goal to the wrong side!
            if(counter - counterBeforeStep > maxTime / (2.0*tam*tam)) counter = maxTime;
        }
        if(counter >= maxTime) break;

        this->gameManager.restartMatch();
        tracksDone ++;
    }

    double deltaTime = ((double) counter) / 60.0; // In seconds

    std::ofstream outputFile;
    outputFile.open("../source/tools/parameter-optimization/univector/logger/simulator_output.txt");
    outputFile << deltaTime << " " << tracksDone << endl;

    cout << deltaTime << " " << tracksDone << endl;

    this->running = false;
    this->gameManager.finish();
}

void Manager::runUnivectorOptimizationAllyCollision() {
    this->setUp();

    //std::thread guiThread(&Manager::startGUI,this);
    int maxTime = 60 * 60 * 3; // FPS * seconds per minute * minutes
    struct Track {
        Vector2<double> ballPos;
        Vector2<double> playerPos;
    };

    vector<Track> tracks;

    Track aux;
    const int tam = 2;
    int jLimit = tam+3;
    for(int i = -tam; i <= tam+1; i ++) {
        //if (i == 0) jLimit ++;
        for(int j = -jLimit; j <= jLimit; j ++) {
            aux.ballPos = Vector2<double>(Field::RIGHT_WALL_X * i/(tam+2),
                                          Field::TOP_WALL_Y * j/(jLimit+1));
            aux.playerPos = Vector2<double>(aux.ballPos.x + 0.1, aux.ballPos.y + 0.1);
            tracks.push_back(aux);
        }
    }

//    for(auto &track : tracks) {
//        cout << "ball.x = " << track.ballPos.x << endl;
//        cout << "ball.y = " << track.ballPos.y << endl;
//    }

    int counter = 0;

    std::ofstream nameFileStartPositions,
            startPositions;
    std::string nameFile = "univector-optimization.conf";

    nameFileStartPositions.open("../configs/simulator_predefined_positions/lastModifiedFile.txt");
    nameFileStartPositions << nameFile;
    nameFileStartPositions.close();

    int tracksDone = 0;

    for(auto &track : tracks) {
        startPositions.open("../configs/simulator_predefined_positions/" + nameFile);
        startPositions << track.ballPos.x << " " << track.ballPos.y << endl << endl;
        startPositions << 3 << endl;
        startPositions << -0.71 << " " << 0.1 << " " << 1.5708 << endl;
        startPositions << track.playerPos.x << " " << track.playerPos.y << " " << 0 << endl;
        startPositions << 0.65 << " " << -0.1 << " " << 1.5708 << endl << endl;
        startPositions << 0 << endl;
        startPositions << 0.65 << " " << 0.1 << " " << 1.5708 << endl << endl;
        startPositions.close();

        this->gameManager.setPreference(SIDE::LEFT);
        this->resetState();

//        cout << "New match" << endl;
//        cout << "ball.x = " << track.ballPos.x << endl;
//        cout << "ball.y = " << track.ballPos.y << endl;

        int counterBeforeStep = counter;
        while(this->gameManager.getLeftScore()  == 0 and counter < maxTime) {
            this->runSimulator();
            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
//            this->sleepForRealTimeSpeed();
            counter ++;
            if(this->gameManager.getRightScore() > 0) counter = maxTime; // We made a goal to the wrong side!
            if(counter - counterBeforeStep > maxTime / (2.0*tam*tam)) counter = maxTime;
        }
        if(counter >= maxTime) break;

        this->gameManager.restartMatch();
        tracksDone ++;
    }

    double deltaTime = ((double) counter) / 60.0; // In seconds

    std::ofstream outputFile;
    outputFile.open("../source/tools/parameter-optimization/univector_obstacles/logger/simulator_output.txt");
    outputFile << deltaTime << " " << tracksDone << endl;

    cout << deltaTime << " " << tracksDone << endl;

    this->running = false;
    this->gameManager.finish();    
}

} // manager
} // server
} // simulator
} // tools
