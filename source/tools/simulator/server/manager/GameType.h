//
// Created by Igor on 04/04/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_MANAGER_GAMETYPE_H
#define TOOLS_SIMULATOR_SERVER_MANAGER_GAMETYPE_H

namespace tools {
namespace simulator {
namespace server {
namespace manager {

// Behavior types stores all possible types a behaviors can have

enum class GameType {
    NORMAL,
    ACCELERATED,
    PENALTY,
    FREE_BALL,
    GOAL_SHOOT
};

} // manager
} // server
} // simulator
} // tools

#endif
