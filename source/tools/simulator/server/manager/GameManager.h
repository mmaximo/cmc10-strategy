//
// Created by Thiago on 25/04/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_MANAGER_GAMEMANAGER_H
#define TOOLS_SIMULATOR_SERVER_MANAGER_GAMEMANAGER_H

#include "communication/Communication.h"
#include "communication/MessageParser.h"
#include "control/ControlInterface.h"
#include "gamestate/Judge.h"
#include "gui/OgreManager.h"
#include "statistics/GameStatistics.h"
#include "manager/GameType.h"
#include "physics/ball/Ball.h"
#include "physics/group/SimulatorField.h"
#include "physics/group/Team.h"
#include "physics/ode/ODEWorld.h"
#include "physics/SimulationWorld.h"
#include "utils/SimulatorUtils.h"

namespace tools {
namespace simulator {
namespace server {
namespace manager {

using core::representations::Player;
using tools::simulator::server::utils::SIDE;

class GameManager {
public:
    GameManager();
    explicit GameManager(GameType gameType);
    GameManager(int numberOfLeftPlayers,
                int numberOfRightPlayers,
                GameType gameType = GameType::NORMAL);
    ~GameManager();

    int getNumberOfLeftPlayers();
    int getNumberOfRightPlayers();
    int getLeftScore();
    int getRightScore();

    void setPreference(SIDE side);
    void switchPreference();
    bool isLeftPreference();
    bool isRightPreference();

    void addLeftScore();
    void addRightScore();
    void addPenaltyScore();
    void exchangeInfo();
    void finish();
    void finishPenalty(int numberOfPenalties);
    void restartMatch();
    void setUpCommunication();
    void simulationStep();
    void stopWheels();
    void update();

    bool getGameStarted();
    bool getPlayAgain();
    bool getGameEnded();
    bool getWasGoal();
    bool getWasFreeBall();
    bool getWasFreeKick();
    bool getWasGoalKick();
    bool getWasPenaltyKick();

    physics::Ball &getBall();
    physics::Team &getLeftTeam();
    physics::Team &getRightTeam();
    core::control::ControlInterface *getCommandInfo(); // TODO: remove this

    statistics::GameStatistics &gameStatistics;

protected:
    bool leftPreference;
    bool rightPreference;
    int numberOfLeftPlayers;
    int numberOfRightPlayers;

    physics::Ball ball;
    physics::Team leftTeam;
    physics::Team rightTeam;
    tools::simulator::server::physics::SimulatorField field;
    judge::Judge judge;
    physics::SimulationWorld &simWorld;
    physics::ODEWorld &odeWorld;
    communication::Communication communicationClient1;
    communication::Communication communicationClient2;
    communication::MessageParser messageParser;
    core::control::ControlInterface commandInfo[2];
    core::representations::WheelSpeed desiredWheelSpeeds[2][Player::PLAYERS_PER_SIDE];
};

} // manager
} // server
} // simulator
} // tools

#endif
