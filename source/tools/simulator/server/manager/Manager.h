//
// Created by Muzio on 31/03/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_MANAGER_MANAGER_H
#define TOOLS_SIMULATOR_SERVER_MANAGER_MANAGER_H

#include <atomic>
#include <chrono>
#include <thread>

#include "gamestate/StateManager.h"
#include "gui/OgreManager.h"
#include "manager/GameManager.h"
#include "physics/SimulationWorld.h"
#include "utils/Timer.h"
#include "utils/Clock.h"

#include <iostream>
#include <stdio.h>
//#include "../monitor/render/DrawableObjectFactory.h"

#define COMMUNICATION_TIME_STEP 1.0/60
#define SIMULATION_STEPS_IN_ONE_COMMUNICATION_STEP 4

namespace tools {
namespace simulator {
namespace server {
namespace manager {

using core::utils::Clock;

class Manager {
public:
    Manager();
    explicit Manager(GameType gameType);
    Manager(int numberOfLeftPlayers,
            int numberOfRightPlayers,
            GameType gameType = GameType::NORMAL);
    ~Manager();

    void startGUI();
    void runAsync();
    void runAccelerated(int numberOfMatches = 0);
    void runPenalty(int numberOfPenalties);
    void runPenaltyAccelerated(int numberOfPenalties);
    void runShotOfGoal(int numberOfShots);
    void runFreeBall(int numberOfFreeBalls);
    void runUnivectorOptimization();
    void runUnivectorOptimizationAllyCollision();
private:
    SIDE coinToss();
    void setUp();
    void runSimulator();
    void sleepForRealTimeSpeed();
    void resetState();
    void resetStatePenalty();
    void runHalfMatch(int duration,
                      int step_delay = 200,
                      int match_delay = 50000);

    int fps;
//    int debugStepCounter;

    double frameStartTime,
           frameStartTimeToFPS,
           stepDelta;

    GameManager gameManager;
    StateManager stateManager;
    gui::OgreManager &ogreManager;
    physics::SimulationWorld &simWorld;
    std::atomic<bool> running;
};

} // manager
} // server
} // simulator
} // tools

#endif
