//
// Created by Muzio on 04/12/14.
//

#include <iostream>
#include <ode/ode.h>
#include "manager/Manager.h"

void ODEMessageHandler(int errnum, const char *msg, va_list ap) {
    // Overload function, to ignore ODE warning messages poluting the terminal :(
}

void showUsage() {
    std::cout << "usage: server" << std::endl;
    std::cout << "It will run the server at normal speed with 3x3 players" << std::endl << std::endl;
    std::cout << "usage: server $numberOfLeftPlayer $numberOfRightPlayers" << std::endl;
    std::cout << "$numberOfXPlayer should be changed for an int" << std::endl << std::endl;
    std::cout << "Accelerated usage: server $numberOfLeftPlayer $numberOfRightPlayers -accelerated $n or server -accelerated $n" << std::endl;
    std::cout << "    -accelerated $n: runs n matches in accelerated mode" << std::endl << std::endl;
}

bool validateNumberOfPlayer(int leftPlayers, int rightPlayers) {
    bool isValid = true;

    if (leftPlayers > core::representations::Player::PLAYERS_PER_SIDE or leftPlayers < 0) {
        std::cout << "Number of left players should be between 1 and PLAYERS_PER_SIDE" << std::endl;
        isValid = false;
    }

    if (rightPlayers > core::representations::Player::PLAYERS_PER_SIDE or rightPlayers < 0) {
        std::cout << "Number of right players should be between 1 and PLAYERS_PER_SIDE" << std::endl;
        isValid = false;
    }

    return isValid;
}

using namespace tools::simulator::server::manager;

int main(int argc, char **argv) {
    std::string acceleratedFlag = "-accelerated";

    std::ofstream nameFileStartPositions;
    nameFileStartPositions.open("../configs/simulator_predefined_positions/lastModifiedFile.txt");
    nameFileStartPositions << "default.conf";
    nameFileStartPositions.close();

    // sets ODE message handler, to avoid "LCP internal error" message on the terminal :(
    dSetMessageHandler(ODEMessageHandler);

    if (argc != 1 && argc != 3 && argc != 5) {
        showUsage();
        return 1;
    }

    if (argc == 1) {
        {
            printf("Initializing server on real time mode.\n");
            Manager manager;
            std::thread simulationThread(&Manager::runAsync, &manager);
            manager.startGUI();
            simulationThread.join();
        }
//        quick_exit(0);
    }
    else {
        if(argv[1] == acceleratedFlag) {
            {
                int numberOfMatches = std::stoi(argv[2]);
                printf("Initializing server on accelerated time mode.\n");
                Manager manager{GameType::ACCELERATED};
                std::thread simulationThread(&Manager::runAccelerated, &manager, numberOfMatches);
                manager.startGUI();
                simulationThread.join();
            }
//            quick_exit(0);
    }

        int leftPlayers  = std::stoi(argv[1]);
        int rightPlayers = std::stoi(argv[2]);

        if (!validateNumberOfPlayer(leftPlayers, rightPlayers))
            return 1;

        if(argc == 5) {
            if (argv[3] == acceleratedFlag) {
                int numberOfMatches = std::stoi(argv[4]);
                printf("Initializing server on accelerated time mode.\n");
                Manager manager{leftPlayers, rightPlayers, GameType::ACCELERATED};
                std::thread simulationThread(&Manager::runAccelerated, &manager, numberOfMatches);
                manager.startGUI();
                simulationThread.join();
            }
//            quick_exit(0);
        }
        {
            printf("Initializing server on real time mode with %d players on the left side "
                   "and %d on the right.\n", leftPlayers, rightPlayers);
            Manager manager{leftPlayers, rightPlayers};
            std::thread simulationThread(&Manager::runAsync, &manager);
            manager.startGUI();
            simulationThread.join();
        }

//        quick_exit(0);
    }
}
