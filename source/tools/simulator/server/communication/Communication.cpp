//
// Created by Alexandre on 04/12/14.
//

#include "communication/Communication.h"
#include <iostream>

namespace tools {
namespace simulator {
namespace server {
namespace communication {

using namespace tools::simulator::server::utils;

Communication::Communication(int port) {
    this->running = true;
    this->testId = 0;
    this->portNumber = port;
    
    int success;
    struct addrinfo hints, *res;
    
    memset(&hints,0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    success = getaddrinfo(NULL,std::to_string(port).c_str(),&hints,&res);
    if(success != 0){
	errorOut(gai_strerror(success));
    }
    
    socketFd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);

    //removes binding issues
    int optval = 1;
    setsockopt(socketFd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));

    if (bind(socketFd,res->ai_addr,res->ai_addrlen) < 0) {
        errorOut("ERROR on binding");
    }

    listen(socketFd, 5);  //max number of connections
    clientAddressLength = sizeof(clientAddress);
}

Communication::~Communication() { }

void Communication::sendMessage(::google::protobuf::Message *message) {
    google::protobuf::uint32 message_length = message->ByteSize();
    int header_length = sizeof(message_length);
    int buffer_length = header_length + message_length;

    google::protobuf::uint8 buffer[buffer_length];

    google::protobuf::io::ArrayOutputStream aos(buffer, buffer_length);
    google::protobuf::io::CodedOutputStream coded_output(&aos);

    coded_output.WriteLittleEndian32(message_length);
    message->SerializeToCodedStream(&coded_output);

    sendData(newSocketFd,buffer,buffer_length);

    delete message;
    //std::cout << "Succesfully sent packet" <<std::endl;
}

void Communication::communicationExecute() {
    //check connection is open ...
    while (this->running)
        receiveMessage();
}

void Communication::receiveMessage() {
    google::protobuf::uint32 message_length;

    int header_length = sizeof(message_length);
    google::protobuf::uint8 header[header_length];
    
    recvData(newSocketFd,header,header_length);

    google::protobuf::io::CodedInputStream::ReadLittleEndian32FromArray(header,
                                                                        &message_length);

    google::protobuf::uint8 buffer[message_length];
    
    recvData(newSocketFd,buffer,message_length);

    google::protobuf::io::ArrayInputStream array_input(buffer, message_length);
    google::protobuf::io::CodedInputStream coded_input(&array_input);

    comMutex.lock();
    if (!message.ParseFromCodedStream(&coded_input)) {
        comMutex.unlock();
        std::cout << "Could not parse message" << std::endl;
    }
    comMutex.unlock();

//    std::cout << "Message received: " << commandMessage.wheelspeeds(0).left() << std::endl;
}

bool Communication::establishConnection() {
    //establising the connection
    newSocketFd = accept(socketFd, (struct sockaddr *) &clientAddress,
                         &clientAddressLength);
    if (newSocketFd < 0)
        return false;
    std::cout << "Connection Established" << std::endl;
    return true;
}

CommandInfoMessage Communication::getLastMessage() {
    boost::lock_guard<boost::mutex> guard(comMutex);
    return message;
}

bool Communication::receivedMessage() {
    boost::lock_guard<boost::mutex> guard(comMutex);
    return gotFirstMessage;
}

//method that runs the thread
void Communication::start() {
    comThread = boost::thread(&Communication::communicationExecute, this);
}

void Communication::join() {
    this->running = false;
    comThread.join();
}

/** 
 * @param sockID: socketId from socket from which data will be recovered
 * @param buffer: pointer to pre-allocated array with size equal to the 
 * number of bytes expected to be recovered 
 * @param bufferLength: number of bytes expected to be recovered
 */
void Communication::recvData(int sockID, void* buffer, int bufferLength) {
    int recvBytes = 0;
    recvBytes = recv(sockID,buffer,bufferLength,0);
    while (recvBytes < bufferLength){std::cout << "Communication::recvData\n";
	if(recvBytes <= 0)
	    errorOut("COULD NOT READ DATA");
	bufferLength -= recvBytes;
	buffer = buffer + recvBytes;
	recvBytes = recv(sockID,buffer,bufferLength,0);
    }
}

/** 
 * @param sockID: socketId from socket from which data will be sent
 * @param buffer: pointer to pre-allocated array with size equal to the 
 * number of bytes expected to be sent 
 * @param bufferLength: number of bytes expected to be sent
 */
void Communication::sendData(int sockID, void* buffer, int bufferLength) {
    int sentBytes = 0;
    sentBytes = send(sockID,buffer,bufferLength,0);
    while(sentBytes < bufferLength) {std::cout << "Communication::sendData\n";
	if(sentBytes <= 0)
	    errorOut("COULD NOT SEND DATA");
	bufferLength -= sentBytes;
	buffer = buffer + sentBytes;
	sentBytes = send(sockID,buffer,bufferLength,0);
    }
}



} // communication
} // server
} // simulator
} // tools
