//
// Created by Muzio on 06/04/15.
//

#include "communication/MessageParser.h"

namespace tools {
namespace simulator {
namespace server {
namespace communication {

using tools::simulator::server::utils::SIDE;

MessageParser::MessageParser(physics::Team &leftTeam,
                             physics::Team &rightTeam,
                             physics::Ball &ball) :
               leftTeam(leftTeam),
               rightTeam(rightTeam),
               ball(ball) {}

MessageParser::~MessageParser() {}

FieldInfoMessage *MessageParser::parseOut(bool invertSides) {
    physics::Team *outputLeftTeam,
                  *outputRightTeam;

    if(invertSides) {
        this->invert = -1;
        outputLeftTeam  = &this->rightTeam;
        outputRightTeam = &this->leftTeam;
    }
    else {
        this->invert = 1;
        outputLeftTeam  = &this->leftTeam;
        outputRightTeam = &this->rightTeam;
    }

    // Protocol buffer object
    fieldInfoMessage = new FieldInfoMessage();
    fieldInfoMessage->set_id(1);

    for (int i = 0; i < core::representations::Player::PLAYERS_PER_SIDE; i++) {
        this->parseOutPlayer(outputLeftTeam->getRobot(i), SIDE::LEFT);
        this->parseOutPlayer(outputRightTeam->getRobot(i), SIDE::RIGHT);
    }

    this->parseOutBall();
    return fieldInfoMessage;
}

void MessageParser::parseOutPlayer(physics::Robot *robot, SIDE player_side) {
    this->robot = robot;
    if (player_side == SIDE::LEFT)
        this->playerInfoMessage = this->fieldInfoMessage->add_leftplayer();
    else
        this->playerInfoMessage = this->fieldInfoMessage->add_rightplayer();

    this->parseOutBody();
    this->parseOutWheel(SIDE::LEFT);
    this->parseOutWheel(SIDE::RIGHT);
}

void MessageParser::parseOutBody() {
    body = playerInfoMessage->mutable_body();
    position = body->mutable_position();
    position->set_x(robot->getPosition()[0] * this->invert);
    position->set_y(robot->getPosition()[1] * this->invert);
    position->set_z(robot->getPosition()[2]);

    rotation = body->mutable_rotation();
    rotation->set_a(robot->getQuaternion()[0]);
    rotation->set_b(robot->getQuaternion()[1]);
    rotation->set_c(robot->getQuaternion()[2]);
    rotation->set_d(robot->getQuaternion()[3]);

    velocity = body->mutable_velocity();
    velocity->set_x(robot->getVelocity()[0] * this->invert);
    velocity->set_y(robot->getVelocity()[1] * this->invert);

    body->set_angularspeed(robot->getAVelocity()[2]);
    body->set_angle(robot->getAngle() + M_PI * (1 - this->invert) / 2);
}

void MessageParser::parseOutWheel(SIDE wheel_side) {
    if (wheel_side == SIDE::LEFT) {
        wheel = playerInfoMessage->mutable_leftwheel();
        position = wheel->mutable_position();
        position->set_x(robot->getLWheelPosition()[0] * this->invert);
        position->set_y(robot->getLWheelPosition()[1] * this->invert);
        position->set_z(robot->getLWheelPosition()[2]);
        rotation = wheel->mutable_rotation();
        rotation->set_a(robot->getLWheelQuaternion()[0]);
        rotation->set_b(robot->getLWheelPosition()[1]);
        rotation->set_c(robot->getLWheelPosition()[2]);
        rotation->set_d(robot->getLWheelPosition()[3]);
    } else {
        wheel = playerInfoMessage->mutable_rightwheel();
        position = wheel->mutable_position();
        position->set_x(robot->getRWheelPosition()[0] * this->invert);
        position->set_y(robot->getRWheelPosition()[1] * this->invert);
        position->set_z(robot->getRWheelPosition()[2]);
        rotation = wheel->mutable_rotation();
        rotation->set_a(robot->getRWheelQuaternion()[0]);
        rotation->set_b(robot->getRWheelPosition()[1]);
        rotation->set_c(robot->getRWheelPosition()[2]);
        rotation->set_d(robot->getRWheelPosition()[3]);
    }
}

void MessageParser::parseOutBall() {
    ball_message = fieldInfoMessage->mutable_ball();
    position = ball_message->mutable_position();
    position->set_x(ball.getPosition()[0] * this->invert);
    position->set_y(ball.getPosition()[1] * this->invert);
    position->set_z(ball.getPosition()[2]);
    velocity = ball_message->mutable_velocity();
    velocity->set_x(ball.getVelocity()[0] * this->invert);
    velocity->set_y(ball.getVelocity()[1] * this->invert);
}

core::control::ControlInterface MessageParser::parseIn(CommandInfoMessage message) {
    core::control::ControlInterface commandInfo;
    // Initializing object
    for (int i = 0; i < core::representations::Player::PLAYERS_PER_SIDE; i++) {
        commandInfo.playersWheelSpeed[i].left  = 0;
        commandInfo.playersWheelSpeed[i].right = 0;
    }

    for (int i = 0; i < message.wheelspeeds_size(); i++) {
        commandInfo.playersWheelSpeed[i].left  = message.wheelspeeds(i).left();
        commandInfo.playersWheelSpeed[i].right = message.wheelspeeds(i).right();
    }
    return commandInfo;
}

} // communication
} // server
} // simulator
} // tools
