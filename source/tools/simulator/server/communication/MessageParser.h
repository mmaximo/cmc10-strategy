//
// Created by Muzio on 06/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_COMMUNICATION_MESSAGEPARSER_H
#define TOOLS_SIMULATOR_SERVER_COMMUNICATION_MESSAGEPARSER_H

#include <math.h>

#include "control/ControlInterface.h"
#include "messages.pb.h" // generated automatically by the protocol buffer compiler
#include "physics/ball/Ball.h"
#include "physics/group/Team.h"
#include "representations/Player.h"
#include "utils/SimulatorUtils.h"
#include "utils/SimulatorUtils.h"

namespace tools {
namespace simulator {
namespace server {
namespace communication {

using tools::simulator::server::utils::SIDE;

class MessageParser {
public:
    MessageParser(physics::Team &leftTeam,
                  physics::Team &rightTeam,
                  physics::Ball &ball);
    virtual ~MessageParser();
    FieldInfoMessage *parseOut(bool invertSides);
    core::control::ControlInterface parseIn(CommandInfoMessage message);

private:
    void parseOutPlayer(physics::Robot *robot, SIDE player_side);
    void parseOutBody();
    void parseOutWheel(SIDE wheel_side);
    void parseOutBall();

    FieldInfoMessage *fieldInfoMessage;
    FieldInfoMessage_PlayerInfo *playerInfoMessage;
    Representations_Body *body;
    Representations_Wheel *wheel;
    Representations_Vector3 *position;
    Representations_Vector4 *rotation;
    Representations_Vector2 *velocity;
    FieldInfoMessage_BallInfo *ball_message;
    dxBody *physicsBody;
    dxBody *physicsWheel;
    physics::Team &leftTeam;
    physics::Team &rightTeam;
    physics::Ball &ball;
    physics::Robot *robot;
    double angle;
    int invert;
};

} // communication
} // server
} // simulator
} // tools

#endif
