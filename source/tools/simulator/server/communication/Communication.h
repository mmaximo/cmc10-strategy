//
// Created by Alexandre on 04/12/14.
//

#ifndef TOOLS_SIMULATOR_SERVER_COMMUNICATION_COMMUNICATIONOUT_H
#define TOOLS_SIMULATOR_SERVER_COMMUNICATION_COMMUNICATIONOUT_H

#include <boost/thread.hpp>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/coded_stream.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <atomic>
#include <string>

#include "messages.pb.h" // generated automatically by the protocol buffer compiler

#include "utils/SimulatorUtils.h"

#define DEFAULT_PORT_1 8234
#define DEFAULT_PORT_2 8235

namespace tools {
namespace simulator {
namespace server {
namespace communication {

class Communication {
public:
    explicit Communication(int port);

    virtual ~Communication();

    bool establishConnection();
    void join();
    bool receivedMessage();
    void receiveMessage();
    void sendMessage(::google::protobuf::Message *message);
    //void setMessage(FieldInfoMessage commandMessage);
    void start();
    CommandInfoMessage getLastMessage();
    //thread related methods

    int socketFd{},
        newSocketFd{},
        portNumber{};

    struct sockaddr_storage clientAddress;

    socklen_t clientAddressLength;

private:
    void communicationExecute();
//	monitorcommunication::Message createMessage(monitorcommunication::Message::MessageType msg);
//	void sendMessage(const monitorcommunication::Message& message);

    CommandInfoMessage message;
    boost::thread comThread;
    boost::mutex comMutex;
    int testId{};
    bool gotFirstMessage{};
    std::atomic<bool> running;
    
    void recvData(int sockID, void* buffer, int bufferLength);
    void sendData(int sockID, void* buffer, int bufferLength);
};

} // communication
} // server
} // simulator
} // tools

#endif
