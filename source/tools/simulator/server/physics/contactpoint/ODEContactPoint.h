//
// Created by Muzio on 17/03/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_ODECONTACTPOINT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_ODECONTACTPOINT_H

#include <ode/ode.h>
#include "physics/base/ODEObject.h"
#include "physics/ode/ODEData.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEContactPoint : public ODEObject {
public:
    ODEContactPoint();
    virtual ~ODEContactPoint();
    void createObject();
    const dReal *getVelocity();
    const dReal *getAVelocity();
};

} // physics
} // server
} // simulator
} // tools

#endif
