//
// Created by Bandeira on 25/03/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_OGRECONTACTPOINT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_OGRECONTACTPOINT_H

#include "OGRE/OgreSceneManager.h"
#include "OGRE/OgreEntity.h"
#include "physics/base/OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class OgreContactPoint : public OgreObject {
public:
    OgreContactPoint();
    ~OgreContactPoint();
    void loadObject(Ogre::SceneManager *manager);
};

} // physics
} // server
} // simulator
} // tools

#endif
