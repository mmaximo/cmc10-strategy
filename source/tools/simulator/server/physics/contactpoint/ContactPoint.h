#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_CONTACTPOINT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_CONTACTPOINT_CONTACTPOINT_H

#include "gui/OgreManager.h"
#include "physics/base/AbstractObject.h"
#include "physics/contactpoint/OgreContactPoint.h"
#include "physics/contactpoint/ODEContactPoint.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ContactPoint : public AbstractObject {
public:
    ContactPoint();
    const dReal *getVelocity();
    const dReal *getAVelocity();
    void reset();
    ~ContactPoint();

protected:
    void createODEObject() override;
    void createOgreObject() override;
};

} // physics
} // server
} // simulator
} // tools

#endif
