 #include "physics/contactpoint/OgreContactPoint.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreContactPoint::OgreContactPoint() { }

OgreContactPoint::~OgreContactPoint() {
    this->mainNode->removeAndDestroyAllChildren();
}

void OgreContactPoint::loadObject(Ogre::SceneManager *manager) {
    Ogre::Entity *ballEntity = manager->createEntity("ball.mesh");
    ballEntity->setCastShadows(true);
    this->mainNode->attachObject(ballEntity);
    this->mainNode->setScale(0.01, 0.01, 0.01);
}

} // physics
} // server
} // simulator
} // tools
