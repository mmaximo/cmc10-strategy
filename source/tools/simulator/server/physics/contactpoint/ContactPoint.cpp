#include "physics/contactpoint/ContactPoint.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

ContactPoint::ContactPoint() {
    this->type = "temporary";
    this->createObject();
}

ContactPoint::~ContactPoint() {
    delete odeObject;
    delete ogreObject;
}

void ContactPoint::createODEObject() {
    odeObject = new ODEContactPoint();
    odeObject->createObject();
}

void ContactPoint::createOgreObject() {
    ogreObject = new OgreContactPoint();
    ogreObject->addToScene();
}

const dReal *ContactPoint::getVelocity() {
    auto *ball = dynamic_cast<ODEContactPoint *>(odeObject);
    return ball->getVelocity();
}

const dReal *ContactPoint::getAVelocity() {
    auto *ball = dynamic_cast<ODEContactPoint *>(odeObject);
    return ball->getAVelocity();
}

void ContactPoint::reset() {
    this->setPosition(5, 5, 5);
}

} // physics
} // server
} // simulator
} // tools
