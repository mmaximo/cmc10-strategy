//
// Created by Muzio on 17/03/15.
//

#include "physics/contactpoint/ODEContactPoint.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

ODEContactPoint::ODEContactPoint() { }

ODEContactPoint::~ODEContactPoint() { }

void ODEContactPoint::createObject() {
    ODEData &data = ODEData::getInstance();
    this->mainBody = dBodyCreate(data.getWorldID());

    //Fixing body
    dBodySetKinematic(this->mainBody);
}

const dReal *ODEContactPoint::getVelocity() {
    return dBodyGetLinearVel(this->mainBody);
}

const dReal *ODEContactPoint::getAVelocity() {
    return dBodyGetAngularVel(this->mainBody);
}

} // physics
} // server
} // simulator
} // tools
