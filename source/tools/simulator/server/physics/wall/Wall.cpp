#include "physics/wall/Wall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

Wall::Wall(dVector3 scale) {
    this->type = "wall";
    this->createObject(scale);
}

Wall::~Wall() {
    delete odeObject;
    delete ogreObject;
}

void Wall::createObject(dVector3 scale) {
    this->createODEObject(scale);
    this->createOgreObject(Ogre::Vector3(scale[0], scale[1], scale[2]));
    this->update();
}

void Wall::createODEObject(dVector3 scale) {
    this->odeObject = new ODEWall();
    ODEWall *wall = static_cast<ODEWall *>(odeObject);
    wall->createObject(scale);
}

void Wall::createOgreObject(Ogre::Vector3 scale) {
    ogreObject = new OgreWall();
    ogreObject->addToScene();
    ogreObject->setScale(scale);
}

}
}
}
}
