#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_OGREWALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_OGREWALL_H

#include "OGRE/OgreEntity.h"
#include "OGRE/OgreSceneManager.h"
#include "physics/base/OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class OgreWall : public OgreObject {
public:
    OgreWall();
    ~OgreWall();
    void loadObject(Ogre::SceneManager *manager);
};

}
}
}
}

#endif