#include "physics/wall/OgreWall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreWall::OgreWall() {
}

OgreWall::~OgreWall() {
    mainNode->removeAndDestroyAllChildren();
}

void OgreWall::loadObject(Ogre::SceneManager *manager) {
    Ogre::Entity *wallEntity = manager->createEntity("wall.mesh");
    wallEntity->setCastShadows(true);
    mainNode->attachObject(wallEntity);
}

}
}
}
}