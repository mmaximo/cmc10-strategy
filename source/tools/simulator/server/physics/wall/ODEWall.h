#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_ODEWALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_ODEWALL_H

#include <ode/ode.h>
#include "physics/base/ODEObject.h"
#include "physics/ode/ODEData.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEWall : public ODEObject {
public:
    ODEWall();
    virtual ~ODEWall();
    void createObject(dVector3 scale);
    void createObject();
};

} // physics
} // server
} // simulator
} // tools

#endif