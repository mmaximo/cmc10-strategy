#include "physics/wall/ODEWall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

ODEWall::ODEWall() { }

ODEWall::~ODEWall() { }

void ODEWall::createObject(dVector3 scale) {
    ODEData &data = ODEData::getInstance();

    mainBody = dBodyCreate(data.getWorldID());
    //Fixing body
    dBodySetKinematic(mainBody);
    //Setting collision surfaces
    dGeomID geom = dCreateBox(data.getSpaceID(), scale[0], scale[1], scale[2]);
    dGeomSetCategoryBits(geom, ODEData::cWall);
    dGeomSetBody(geom, mainBody);
}

void ODEWall::createObject() { }

} // physics
} // server
} // simulator
} // tools
