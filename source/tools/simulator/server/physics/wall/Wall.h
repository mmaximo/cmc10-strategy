#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_WALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_WALL_WALL_H

#include "physics/base/AbstractObject.h"
#include "physics/wall/OgreWall.h"
#include "physics/wall/ODEWall.h"
#include "gui/OgreManager.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class Wall : public AbstractObject {
public:
    Wall(dVector3 scale);
    ~Wall();
    void createObject(dVector3 scale) override;

private:
    void createODEObject(dVector3 scale) override;
    void createOgreObject(Ogre::Vector3 scale) override;
};

}
}
}
}

#endif
