#include "physics/motor/MotorModel.h"
#include "representations/Player.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

MotorModel::MotorModel():Vsat(7.93),
			 gearRatio(51.45),
			 gearEfficiency(0.7805),
			 motorResistence(4.3),
			 Kt(0.0017),
			 viscousFriction(5.8734e-8),
			 encoderResolution(617.4),
			 sampleTime(1.0/60/4/1){
			 prevEncoderPos = 0;
			 angularVel = 0;
}

MotorModel::~MotorModel() { }

double MotorModel::getTorque(double omegaReal){
    double backEMF = Kt*gearRatio*omegaReal;
    double vfTorque = gearEfficiency*gearRatio*gearRatio*viscousFriction*omegaReal;
    double torque = gearEfficiency*gearRatio*Kt*(voltage-backEMF)/motorResistence;
    
    return torque - vfTorque;
}

double MotorModel::getVoltage() {
    return voltage;
}

void MotorModel::updateVoltage(double omegaReference, double omegaReal) {
    voltage = controller.getControl(omegaReference,omegaReal);
}

void MotorModel::updateEncoder(double rotation) {
    while(rotation - prevEncoderPos > M_PI)
	rotation -= 2*M_PI;
    while(rotation - prevEncoderPos < -M_PI)
	rotation += 2*M_PI;
    pulses = (int) ((rotation - prevEncoderPos)*gearRatio/0.5236);
    if(abs(pulses)>0){
	prevEncoderPos = rotation;
    }
    angularVel = 2*M_PI*pulses/(encoderResolution*sampleTime);
}

double MotorModel::getVelocity(){
    return angularVel;
}
int MotorModel::getPulses(){
    return pulses;
}

}
}
}
}
