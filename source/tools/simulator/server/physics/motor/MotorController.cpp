#include "physics/motor/MotorController.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

MotorController::MotorController() : stdUnitToRPM(60.0/(2*M_PI)),
                                     pwmToStdUnit(7.93/255){
    Kp = .5;//1.2;// Kp(teensy) * ( [V] / [PWM]) * ([RPM] / [RAD/S] )
    Ki = 0.5;// Kp(teensy) * ( [V] / [PWM]) * ([RPM] / [RAD/S] )
    Kd = 0;// Kp(teensy) * ( [V] / [PWM]) * ([RPM] / [RAD/S] )
    integrative = 0;
}

MotorController::~MotorController() { }

double MotorController::getControl(double omegaReference, double omegaReal) {
    error = (omegaReference - omegaReal)*stdUnitToRPM;
    integrative += error; //Just copied from teensy code
    control = ((int)(Kp*error+Kd*(error-previousError)+Ki*integrative))*pwmToStdUnit;
    previousError = error;
    
    if(control > 7.93 || control < -7.93)//Anti-windup
	integrative -= error;
    
    if(control > 7.93)
	control = 7.93;
    if(control < -7.93)
	control = -7.93;
    
    return control;
}

}
}
}
}