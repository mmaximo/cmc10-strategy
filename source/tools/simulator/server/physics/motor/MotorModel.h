#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_MOTOR_MOTORMODEL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_MOTOR_MOTORMODEL_H

#include "physics/motor/MotorController.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class MotorModel {
public:
  MotorModel();
  ~MotorModel();
  double getTorque(double omegaReal);
  double getVoltage();
  void updateVoltage(double omegaReference,double omegaReal);
  void updateEncoder(double rotation);
  double getVelocity();
  int getPulses();
  
private:
    MotorController controller;
    
    double prevEncoderPos;
    int pulses;
    double angularVel;
    
    double voltage;
    
    //TODO: Create a representation class with motor parameters
    const double Vsat;
    const double gearRatio;
    const double gearEfficiency;
    const double motorResistence;
    const double Kt;
    const double viscousFriction;
    const double encoderResolution;
    const double sampleTime;
};

}
}
}
}

#endif