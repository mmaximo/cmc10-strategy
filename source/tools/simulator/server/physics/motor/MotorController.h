#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_MOTOR_MOTORCONTROLLER_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_MOTOR_MOTORCONTROLLER_H

#include <math/Vector2.h>

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class MotorController {
public:
    MotorController();
    ~MotorController();
    double getControl(double omegaReference, double omegaReal);

private:
    //Parameters:
    double Kp;
    double Ki;
    double Kd;

    double error;
    double previousError;
    double integrative;
    double control;
    
    const double stdUnitToRPM;
    const double pwmToStdUnit;
};

}
}
}
}
#endif
