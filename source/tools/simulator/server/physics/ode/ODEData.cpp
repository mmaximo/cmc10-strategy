//
// Created by Thiago on 24/05/18.
//

#include "physics/ode/ODEData.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
ODEData &ODEData::getInstance() {
    static ODEData instance;
    return instance;
}

ODEData::ODEData() {
    this->communicationTimeStep = 1.0/60;
    this->simulationStepsInOneCommunicationStep = 4*1;

    this->worldID = dWorldCreate();
    this->spaceID = dHashSpaceCreate(0);
    this->groupID = dJointGroupCreate(0);

    this->timeStep = this->communicationTimeStep/this->simulationStepsInOneCommunicationStep;
}

ODEData::~ODEData() {
    dJointGroupDestroy(this->groupID);
    dSpaceDestroy(this->spaceID);
    dWorldDestroy(this->worldID);
    dCloseODE();
}

dWorldID ODEData::getWorldID() {
    return this->worldID;
}

dSpaceID ODEData::getSpaceID() {
    return this->spaceID;
}

dJointGroupID ODEData::getGroupID() {
    return this->groupID;
}

double ODEData::getTimeStep() {
    return this->timeStep;
}

} // physics
} // server
} // simulator
} // tools
