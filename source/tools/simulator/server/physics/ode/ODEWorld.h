#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ODEWORLD
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ODEWORLD

#include <set>
#include <ode/ode.h>

#include "physics/ode/ODEData.h"
#include "physics/SimulationWorld.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEWorld {
public:
    static ODEWorld &getInstance();

    void update();
    static void collision(void* data, dGeomID o1, dGeomID o2);
    double getTime();

private:
    ODEWorld(); // Prevent construction
    ODEWorld(const ODEWorld&); // Prevent construction by copying
    ODEWorld& operator=(const ODEWorld&); // Prevent assignment
    ~ODEWorld(); // Prevent unwanted destruction

    enum CollisionType {
        Robot2Field = 0,
        Wheel2Field,
        Ball2Field,
        Robot2Ball,
        Robot2Robot,
        RobotChassi2RobotWheel,
        Frictionless
    };

    ODEData &odeData;
    dWorldID worldID;
    dSpaceID spaceID;
    dJointGroupID groupID;

    double timeStep;
    double simulatedTime;
};

} // physics
} // server
} // simulator
} // tools

#endif
