//
// Created by Thiago on 24/05/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ODEDATA
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ODEDATA

#include <ode/ode.h>

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEData {
public:
    static ODEData &getInstance();

    dWorldID getWorldID();
    dSpaceID getSpaceID();
    dJointGroupID getGroupID();
    double getTimeStep();

    enum category {
        cBall,
        cField,
        cRobotChassi,
        cRobotWheel,
        cRobotCastor,
        cWall
    };

private:
    ODEData(); // Prevent construction
    ODEData(const ODEData&); // Prevent construction by copying
    ODEData& operator = (const ODEData&); // Prevent assignment
    ~ODEData(); // Prevent unwanted destruction

    dWorldID worldID;
    dSpaceID spaceID;
    dJointGroupID groupID;

    int simulationStepsInOneCommunicationStep;
    double communicationTimeStep;
    double timeStep;
};

} // physics
} // server
} // simulator
} // tools

#endif
