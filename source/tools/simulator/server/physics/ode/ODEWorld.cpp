#include "physics/ode/ODEWorld.h"

#define COLLISION_DEBUG

namespace tools {
namespace simulator {
namespace server {
namespace physics {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
ODEWorld &ODEWorld::getInstance() {
    static ODEWorld instance;
    return instance;
}

ODEWorld::ODEWorld() :
          odeData(ODEData::getInstance()) {
    dInitODE();

    this->worldID  = this->odeData.getWorldID();
    this->spaceID  = this->odeData.getSpaceID();
    this->groupID  = this->odeData.getGroupID();
    this->timeStep = this->odeData.getTimeStep();

    dWorldSetGravity(this->worldID, 0, 0, -9.81);
    dWorldSetERP(this->worldID, 0.7);
    dWorldSetCFM(this->worldID, 1e-4);

    this->simulatedTime = 0.0;
}

ODEWorld::~ODEWorld() = default;

double ODEWorld::getTime() {
    return this->simulatedTime;
}

void ODEWorld::update() {
    this->simulatedTime += this->timeStep;

    dSpaceCollide(this->spaceID, this, &collision);
    dWorldStep(this->worldID, this->timeStep);
    dJointGroupEmpty(this->groupID);
}

void ODEWorld::collision(void *data, dGeomID o1, dGeomID o2) {
    ODEWorld *self = (ODEWorld *) data;
    dBodyID b1 = dGeomGetBody(o1);
    dBodyID b2 = dGeomGetBody(o2);
    unsigned long category1 = dGeomGetCategoryBits(o1);
    unsigned long category2 = dGeomGetCategoryBits(o2);

    int n;
    //no collision if connected
    if (b1 && b2 && dAreConnectedExcluding(b1, b2, dJointTypeContact))
        return;

    const int N = 10;//Max. number of contact points.
    dContact contact[N];
    n = dCollide(o1, o2, N, &contact[0].geom, sizeof(dContact));

    CollisionType collisionType;
    if ((category1 == ODEData::cRobotWheel and category2 == ODEData::cField) or
        (category2 == ODEData::cRobotWheel and category1 == ODEData::cField))
        collisionType = CollisionType::Wheel2Field;

    else if ((category1 == ODEData::cBall and category2 == ODEData::cField) or
             (category2 == ODEData::cBall and category1 == ODEData::cField))
        collisionType = CollisionType::Ball2Field;

    else if (((category1 == ODEData::cRobotChassi or category1 == ODEData::cRobotWheel) and category2 == ODEData::cBall) or
             ((category2 == ODEData::cRobotChassi or category2 == ODEData::cRobotWheel) and category1 == ODEData::cBall))
        collisionType = CollisionType::Robot2Ball;

    else
        collisionType = CollisionType::Frictionless;

    for (int i = 0; i < n; ++i) {
        switch (collisionType) {
            case Wheel2Field:
                contact[i].surface.mode = dContactApprox1 | dContactBounce /*| dContactSlip1 | dContactSlip2*/;
                contact[i].surface.mu = 2;//1;
                contact[i].surface.bounce = 0;
                contact[i].surface.bounce_vel = dInfinity;
                //contact[i].surface.slip1 = 0.1;
                //contact[i].surface.slip2 = 0.1;
                break;

            case Ball2Field:
                contact[i].surface.mode = dContactApprox1 | dContactBounce;
                contact[i].surface.mu = 0.8;
                contact[i].surface.bounce = 0.5;
                contact[i].surface.bounce_vel = 0.1;
                break;

            case Robot2Ball:
                contact[i].surface.mode = dContactApprox1 | dContactBounce;
                contact[i].surface.mu = 0.3;
                contact[i].surface.bounce = 0.4;
                contact[i].surface.bounce_vel = 0.1;
                break;

            default://Frictionless
                contact[i].surface.mode = dContactApprox1 | dContactBounce;
                contact[i].surface.mu = 0;
                contact[i].surface.bounce = 0;
                contact[i].surface.bounce_vel = dInfinity;
                break;
        }

        dJointID c = dJointCreateContact(self->worldID,
                                         self->groupID,
                                         &contact[i]);

        dJointAttach(c, dGeomGetBody(contact[i].geom.g1),
                        dGeomGetBody(contact[i].geom.g2));

        #ifdef COLLISION_DEBUG
            SimulationWorld::getInstance().addContact(contact[i].geom.pos[0],
                                                      contact[i].geom.pos[1],
                                                      contact[i].geom.pos[2]);
        #endif
    }
}

} // physics
} // server
} // simulator
} // tools
