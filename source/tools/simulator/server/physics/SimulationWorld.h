//
// Created by Bandeira on 04/02/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_SIMULATIONWORLD
#define TOOLS_SIMULATOR_SERVER_PHYSICS_SIMULATIONWORLD

#include "physics/base/AbstractObject.h"
#include "physics/ball/Ball.h"
#include "physics/contactpoint/ContactPoint.h"
#include "physics/robot/Robot.h"
#include "physics/wall/Wall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class SimulationWorld {
public:
    static SimulationWorld &getInstance();

    void updateODEWorld();
    void updateOgreWorld();
    void addToSimulation(AbstractObject *object);
    void addContact(double x, double y, double z);

private:
    SimulationWorld(); // Prevent construction
    SimulationWorld(const SimulationWorld&); // Prevent construction by copying
    SimulationWorld& operator=(const SimulationWorld&); // Prevent assignment
    ~SimulationWorld(); // Prevent unwanted destruction

    void hideContacts();

    int nContacts;
    int nVisibleContacts;
    int maxContacts;
    std::set<AbstractObject *> objects;
    std::set<Ball *> balls;
    std::set<Robot *> robots;
    std::set<Wall *> walls;
    std::vector<ContactPoint> contacts;
};

} // physics
} // server
} // simulator
} // tools

#endif
