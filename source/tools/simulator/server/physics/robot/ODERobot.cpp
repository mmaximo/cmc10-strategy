//
// Created by Muzio on 31/03/15.
//

#include "physics/robot/ODERobot.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

using core::representations::Player;
using core::utils::Logger;
using core::utils::LogSystem;

ODERobot::ODERobot(int robotID) : id(robotID) { }

void ODERobot::createObject () {
    ODEData &data = ODEData::getInstance();

    mainAngle = 0;//Robot angle in radians
    //robot body
    mainBody = dBodyCreate(data.getWorldID());
    //robot mass
    dMass m;
    dMassSetBoxTotal ( &m, Player::TOTAL_MASS, Player::EFFECTIVE_X, Player::EFFECTIVE_Y, Player::EFFECTIVE_Z );
    dBodySetMass ( mainBody, &m );
    //collision surface
    dGeomID geom = dCreateBox ( data.getSpaceID(), Player::WIDTH, Player::LENGTH, Player::HEIGHT );
    dGeomSetCategoryBits ( geom, ODEData::cRobotChassi ); //Definindo a categoria da geometria
    //wheels: create only after creating main body
    dGeomSetBody ( geom, mainBody );
    dGeomSetOffsetPosition(geom,0,0,Player::CENTER_MASS_TO_CENTER_GEOMETRIC);
    createWheel(); //left wheel
    createWheel(); //right wheel OBS:This order is defined by adjustBodies() implementation.
}

void ODERobot::createWheel() {
    ODEData &data = ODEData::getInstance();

    dBodyID wheel = dBodyCreate(data.getWorldID());
    //dBodySetFiniteRotationMode(wheel,1);//Fix some inaccurate calculation bugs
    dMass m;
    double Imotor = 2.1321e-8;
    double Iwheel = 9.97e-6;
//    double efficiency = 0.7805;
    double gearRatio = 51.45;
    double radius = sqrt(2*(Iwheel+Imotor*gearRatio*gearRatio)/Player::WHEEL_MASS);
    double thickness = Player::WHEEL_THICKNESS*Player::WHEEL_RADIUS*Player::WHEEL_RADIUS/radius/radius;
    dMassSetCylinderTotal ( &m, Player::WHEEL_MASS, 3, radius, thickness );
    dBodySetMass ( wheel,&m );
    dGeomID geom = dCreateCylinder ( data.getSpaceID(),Player::WHEEL_RADIUS, Player::WHEEL_THICKNESS );
    dGeomSetCategoryBits ( geom,ODEData::cRobotWheel );
    dGeomSetBody ( geom,wheel );
    childBodies.insert ( wheel );
    createJoint();
}

void ODERobot::createJoint () {
    ODEData &data = ODEData::getInstance();

    dJointID joint = dJointCreateHinge(data.getWorldID(), 0);
    dJointAttach(joint,mainBody, 0);
    // set the maximum force that can be applied to achieve the desired velocity
    //dJointSetHingeParam ( joint,dParamFMax, Player::HINGE_FORCE_MAX );
    // set the velocity
    dJointSetHingeParam(joint, dParamVel, 0.0);
    //parameter that ensures thae smooth transition from stop stages to transition statges of a joint
    //dJointSetHingeParam ( joint, dParamFudgeFactor, Player::HINGE_ACCELERATION );
    joints.insert ( joint );
}

void ODERobot::adjustBodies() {
    std::set<dBodyID>::iterator it1;
    std::set<dJointID>::iterator it2;
    dReal angle = M_PI / 2;
    for (it1 = childBodies.begin(), it2 = joints.begin(); (it1 != childBodies.end() &&
                                                           it2 != joints.end()); it1++, it2++) {
        if (it1 == childBodies.end() || it2 == joints.end())
            dError(-1, "Number of child bodies and joints not matching\n");
        adjustWheel(*it1, angle);
        adjustJoint(*it2, angle);
        angle += M_PI;
    }
}

void ODERobot::adjustWheel(dBodyID wheel, dReal angle) {
    dQuaternion wheelSetup, wheelOrientation, wheelQuaternion;
    dQSetIdentity(wheelQuaternion);
    dQFromAxisAndAngle ( wheelSetup,1,0,0,M_PI/2.0 ); //rotate 90ª around x axis.
    dQFromAxisAndAngle ( wheelOrientation,0,0,1, mainAngle );
    dQMultiply0 ( wheelQuaternion,wheelOrientation,wheelSetup );
    dBodySetQuaternion ( wheel,wheelQuaternion );
    double wheelDist = Player::WHEELS_TO_CENTER;//+Player::WHEEL_THICKNESS/2;
    dVector3 wheelPos;
    dBodyGetRelPointPos(mainBody,wheelDist*cos(angle),wheelDist*sin(angle),
			1*(Player::WHEEL_RADIUS-Player::HEIGHT/2-0.001*Player::DISTANCE_FROM_GROUND+Player::CENTER_MASS_TO_CENTER_GEOMETRIC),
			wheelPos);
    dBodySetPosition ( wheel,wheelPos[0],wheelPos[1],wheelPos[2] );
}

void ODERobot::adjustJoint ( dJointID joint, dReal angle ) {
    double jointDist = Player::WHEELS_TO_CENTER;
    dVector3 jointPos;
    dBodyGetRelPointPos(mainBody,jointDist*cos(angle),jointDist*sin(angle),
			1*(Player::WHEEL_RADIUS-Player::HEIGHT/2-0.001*Player::DISTANCE_FROM_GROUND+Player::CENTER_MASS_TO_CENTER_GEOMETRIC),
			jointPos);
    dJointSetHingeAnchor ( joint,jointPos[0],jointPos[1],jointPos[2] );
    dJointSetHingeAxis ( joint,cos(angle+mainAngle),sin(angle+mainAngle),0);
}

void ODERobot::connectJoints() {
    std::set<dBodyID>::iterator it1;
    std::set<dJointID>::iterator it2;
    for (it1 = childBodies.begin(), it2 = joints.begin(); (it1 != childBodies.end() &&
                                                           it2 != joints.end()); it1++, it2++) {
        if (it1 == childBodies.end() || it2 == joints.end())
            dError(-1, "Number of child bodies and joints not matching\n");
        dJointAttach(*it2, mainBody, *it1);
    }
}

void ODERobot::disconnectJoints() {
    std::set<dJointID>::iterator it;
    for (it = joints.begin(); it != joints.end(); it++) {
        dJointAttach(*it, mainBody, 0);
    }
}


void ODERobot::setPosition(dVector3 position) {
    disconnectJoints();
    dBodySetPosition(mainBody, position[0], position[1], position[2] + Player::CENTER_GEOMETRIC_TO_CENTER_MASS);
    adjustBodies();
    connectJoints();
    setOrientation(0);
}

void ODERobot::setPosition(dReal x, dReal y, dReal z) {
    disconnectJoints();
    dBodySetPosition(mainBody, x, y, z + Player::CENTER_GEOMETRIC_TO_CENTER_MASS);
    adjustBodies();
    connectJoints();
    setOrientation(0);
}


void ODERobot::setOrientation(dReal angle) {
    disconnectJoints();
    mainAngle = angle;
    dQuaternion orientation;
    dQSetIdentity(orientation);
    dQFromAxisAndAngle(orientation, 0, 0, 1, mainAngle);
    dBodySetQuaternion(mainBody, orientation);
    adjustBodies();
    connectJoints();
}

void ODERobot::setChassiKinematic() {
    dBodySetKinematic(mainBody);
}

const dReal* ODERobot::getRobotPosition() {
    const dReal* cMassPosition = dBodyGetPosition ( mainBody );
    position[0] = cMassPosition[0];
    position[1] = cMassPosition[1];
    position[2] = cMassPosition[2] + Player::CENTER_MASS_TO_CENTER_GEOMETRIC;
    return position;
}

const dReal* ODERobot::getLWheelPosition() {
    auto it = childBodies.begin();
    return dBodyGetPosition ( *it );
}

const dReal* ODERobot::getRWheelPosition() {
    auto it = childBodies.rbegin();
    return dBodyGetPosition ( *it );
}

const dReal *ODERobot::getRobotQuaternion() {
    return dBodyGetQuaternion(mainBody);
}

const dReal* ODERobot::getLWheelQuaternion() {
    auto it = childBodies.begin();
    return dBodyGetQuaternion ( *it );
}

const dReal* ODERobot::getRWheelQuaternion() {
    auto it = childBodies.rbegin();
    return dBodyGetQuaternion ( *it );
}

const dReal* ODERobot::getLWheelASpeed() {
    auto it = childBodies.begin();
    const dReal* vel = dBodyGetAngularVel(*it);
    dBodyVectorFromWorld(*it,vel[0],vel[1],vel[2],leftSpeed);
    return leftSpeed;
}

const dReal* ODERobot::getRWheelASpeed() {
    auto it = childBodies.rbegin();
    const dReal* vel = dBodyGetAngularVel(*it);
    dBodyVectorFromWorld(*it,vel[0],vel[1],vel[2],rightSpeed);
    return rightSpeed;
}

double ODERobot::getLWheelRotation() {
    auto it = childBodies.begin();
    dVector3 referenceX, referenceY;
    dVector3 result;
    double rotation;
    dBodyVectorToWorld(mainBody,1,0,0,referenceX);
    dBodyVectorToWorld(mainBody,0,0,1,referenceY);
    dBodyVectorFromWorld(*it,referenceX[0],referenceX[1],referenceX[2],result);
    rotation = atan2(result[0]*referenceY[0]+result[1]*referenceY[2]-result[2]*referenceY[1],
		     result[0]*referenceX[0]+result[1]*referenceX[2]-result[2]*referenceX[1]);
    
    return rotation;
}

double ODERobot::getRWheelRotation() {
    auto it = childBodies.rbegin();
    dVector3 referenceX, referenceY;
    dVector3 result;
    double rotation;
    dBodyVectorToWorld(mainBody,1,0,0,referenceX);
    dBodyVectorToWorld(mainBody,0,0,1,referenceY);
    dBodyVectorFromWorld(*it,referenceX[0],referenceX[1],referenceX[2],result);
    rotation = atan2(result[0]*referenceY[0]+result[1]*referenceY[2]-result[2]*referenceY[1],
		     result[0]*referenceX[0]+result[1]*referenceX[2]-result[2]*referenceX[1]);
    
    return rotation;
}

const dReal* ODERobot::getVelocity() {
    return dBodyGetLinearVel(mainBody);
}

const dReal *ODERobot::getAVelocity() {
    return dBodyGetAngularVel(mainBody);
}

dReal ODERobot::getAngle() {
    if (childBodies.size() != 2)
        dError(-1, "Wrong number of wheels\n");
    std::set<dBodyID>::iterator itLeft = childBodies.begin();
    std::set<dBodyID>::reverse_iterator itRight = childBodies.rbegin();

    return atan2(
            dBodyGetPosition(*itRight)[0] - dBodyGetPosition(*itLeft)[0],
            dBodyGetPosition(*itLeft)[1] - dBodyGetPosition(*itRight)[1]);
}

void ODERobot::updateRobot () {
    if ( childBodies.size() !=2 )
        dError ( -1,"Wrong number of joints\n" );
    
    auto leftJoint = joints.begin();
    double lSpeed = -getLWheelASpeed()[2];
    leftMotor.updateEncoder(getLWheelRotation());
    double lTorque = -leftMotor.getTorque(lSpeed);
    dJointAddHingeTorque ( *leftJoint, lTorque );
    
    auto rightJoint = joints.rbegin();
    double rSpeed = -getRWheelASpeed()[2];
    rightMotor.updateEncoder(getRWheelRotation());
    double rTorque = rightMotor.getTorque(rSpeed);
    dJointAddHingeTorque ( *rightJoint, rTorque );
    /*
    if(id == 0){
	Logger& timeLogger = LogSystem::getInstance().getLogger("simulated_time");
	timeLogger << data.getTime() << "\n";
	timeLogger.flush();
	
	Logger& leftWheelRotationLogger = LogSystem::getInstance().getLogger("l_wheel_rotation");
	leftWheelRotationLogger << getLWheelRotation() << "\n";
	leftWheelRotationLogger.flush();
	
	Logger& leftWheelSpeedLogger = LogSystem::getInstance().getLogger("l_wheel_speed");
	leftWheelSpeedLogger << lSpeed << "\n";
	leftWheelSpeedLogger.flush();

	Logger& leftWheelTorqueLogger = LogSystem::getInstance().getLogger("l_wheel_torque");
	leftWheelTorqueLogger << -lTorque << "\n";
	leftWheelTorqueLogger.flush();
	
	Logger& leftWheelVoltageLogger = LogSystem::getInstance().getLogger("l_wheel_voltage");
	leftWheelVoltageLogger << leftMotor.getVoltage() << "\n";
	leftWheelVoltageLogger.flush();
	
	Logger& leftWheelVelocityLogger = LogSystem::getInstance().getLogger("l_wheel_velocity");
	leftWheelVelocityLogger << leftMotor.getVelocity() << "\n";
	leftWheelVelocityLogger.flush();
	
	Logger& leftWheelPulsesLogger = LogSystem::getInstance().getLogger("l_wheel_pulses");
	leftWheelPulsesLogger << leftMotor.getPulses() << "\n";
	leftWheelPulsesLogger.flush();
    }*/
}

void ODERobot::commandRobotSpeed(double left, double right) {
//    auto leftJoint = joints.begin();
    double lSpeed = -getLWheelASpeed()[2];
    leftMotor.updateVoltage(left,lSpeed);
    
//    auto rightJoint = joints.rbegin();
    double rSpeed = -getRWheelASpeed()[2];
    rightMotor.updateVoltage(right,rSpeed);
}

void ODERobot::setFiniteAxis() {
    std::set<dBodyID>::iterator it1;
    std::set<dJointID>::iterator it2;
    dVector3 axis;
    for (it1 = childBodies.begin(), it2 = joints.begin(); (it1 != childBodies.end() &&
                                                           it2 != joints.end()); it1++, it2++) {
        if (it1 == childBodies.end() || it2 == joints.end())
            dError(-1, "Number of child bodies and joints not matching\n");
        dJointGetHingeAxis(*it2, axis);
        dBodySetFiniteRotationAxis(*it1, axis[0], axis[1], axis[2]);
    }
}

//TODO It forces the robot to be perpendicular to xy plane.(shoud never be used!!)
void ODERobot::keepUp() {
    setOrientation(getAngle());
}

void ODERobot::stop() {
    tools::simulator::server::physics::ODEObject::stop();
    for (std::set<dBodyID>::iterator it = childBodies.begin(); (it != childBodies.end()); it++) {
        dBodySetLinearVel(*it, 0, 0, 0);
        dBodySetAngularVel(*it, 0, 0, 0);
    }

    for (std::set<dJointID>::iterator it = joints.begin(); (it != joints.end()); it++)
        dJointSetHingeParam(*it, dParamVel, 0);
}

ODERobot::~ODERobot() { }

} // physics
} // server
} // simulator
} // tools
