#include "physics/robot/OgreRobot.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreRobot::OgreRobot(int team, int id) {
    this->id = id;
    this->team = team;
}

OgreRobot::~OgreRobot() {
    mainNode->removeAndDestroyAllChildren();
}

void OgreRobot::loadObject(Ogre::SceneManager *manager) {
    std::stringstream ss;
    ss << "vssRobotT" << team << "P" << id << ".mesh";
    std::string entityName = ss.str();
    Ogre::Entity *robotEntity = manager->createEntity(entityName);
    robotEntity->setCastShadows(true);
    mainNode->attachObject(robotEntity);
    mainNode->setScale(0.08, 0.08, 0.08);
    mainNode->roll(Ogre::Radian(M_PI / 2));
}

void OgreRobot::fixOrientation() {
    mainNode->pitch(Ogre::Radian(M_PI / 2));
}

}
}
}
}