#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_OGREROBOT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_OGREROBOT_H

#include "OGRE/OgreEntity.h"
#include "OGRE/OgreSceneManager.h"
#include "physics/base/OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class OgreRobot : public OgreObject {
public:
    OgreRobot(int team, int id);
    ~OgreRobot();
    void loadObject(Ogre::SceneManager *manager);
    void fixOrientation();

private:
    int id;
    int team;
};

} // physics
} // server
} // simulator
} // tools

#endif