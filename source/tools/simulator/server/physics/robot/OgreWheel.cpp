#include "physics/robot/OgreWheel.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreWheel::OgreWheel() {
}

OgreWheel::~OgreWheel() {
    mainNode->removeAndDestroyAllChildren();
}

void OgreWheel::loadObject(Ogre::SceneManager *manager) {
    Ogre::Entity *wheelEntity = manager->createEntity("wheel.mesh");
    wheelEntity->setCastShadows(true);
    mainNode->attachObject(wheelEntity);
    mainNode->setScale(2 * core::representations::Player::WHEEL_RADIUS,
                       2 * core::representations::Player::WHEEL_RADIUS,
                       core::representations::Player::WHEEL_THICKNESS);
}

}
}
}
}