//
// Created by Muzio on 31/03/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_ODEROBOT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_ODEROBOT_H

#include <set>
#include <sstream>
#include <ode/ode.h>

#include "physics/motor/MotorModel.h"
#include "physics/base/ODEObject.h"
#include "physics/ode/ODEData.h"
#include "representations/Player.h"
#include "utils/Logger.h"
#include "utils/LogSystem.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODERobot : public ODEObject {
public:
    ODERobot(int robotID);
    virtual ~ODERobot();
    virtual void createObject();
    void commandRobotSpeed(double left, double right);
    void updateRobot();
    void setFiniteAxis();
    void keepUp();
    void stop();
    const dReal* getRobotPosition();
    const dReal* getLWheelPosition();
    const dReal* getRWheelPosition();
    const dReal* getRobotQuaternion();
    const dReal* getLWheelQuaternion();
    const dReal* getRWheelQuaternion();
    const dReal* getLWheelASpeed();
    const dReal* getRWheelASpeed();
    double getLWheelRotation();
    double getRWheelRotation();
    const dReal* getVelocity();
    const dReal* getAVelocity();
    dReal getAngle();
    void setPosition(dVector3 position);
    void setPosition(dReal x, dReal y, dReal z);
    void setOrientation(dReal angle);
    void setChassiKinematic(); //Used only in tests

protected:
    const int id;
    dReal mainAngle;
    dReal position[3];
    void createWheel();
    void createJoint();
    void adjustBodies();
    void adjustWheel(dBodyID wheel, dReal angle);
    void adjustJoint(dJointID joint, dReal angle);
    void connectJoints();
    void disconnectJoints();
    std::set<dBodyID> childBodies;
    std::set<dJointID> joints;
    
    dVector3 leftSpeed; //Acochambration
    dVector3 rightSpeed;
    MotorModel leftMotor;
    MotorModel rightMotor;
};

} // physics
} // server
} // simulator
} // tools

#endif
