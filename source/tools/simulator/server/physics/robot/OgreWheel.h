#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_OGREWHEEL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_OGREWHEEL_H

#include "OGRE/OgreEntity.h"
#include "OGRE/OgreSceneManager.h"
#include "physics/base/OgreObject.h"
#include "representations/Player.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class OgreWheel : public OgreObject {
public:
    OgreWheel();
    ~OgreWheel();
    void loadObject(Ogre::SceneManager *manager);
};

} // physics
} // server
} // simulator
} // tools

#endif