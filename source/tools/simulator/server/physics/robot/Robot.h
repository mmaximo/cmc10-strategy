#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_ROBOT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ROBOT_ROBOT_H

#include "gui/OgreManager.h"
#include "physics/base/AbstractObject.h"
#include "physics/robot/OgreWheel.h"
#include "physics/robot/ODERobot.h"
#include "physics/robot/OgreRobot.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class Robot : public AbstractObject {
public:
    Robot(int id, int team);
    void update();
    void commandRobotSpeed(double left, double right);
    void updateRobot();
    void setFiniteAxis();
    void keepUp();
    const dReal* getRobotPosition();
    const dReal* getLWheelPosition();
    const dReal* getRWheelPosition();
    const dReal* getRobotQuaternion();
    const dReal* getLWheelQuaternion();
    const dReal* getRWheelQuaternion();
    const dReal* getVelocity();
    const dReal* getAVelocity();
    const dReal* getLWheelASpeed();
    const dReal* getRWheelASpeed();
    dReal getAngle();
    ~Robot();

protected:
    void createODEObject() override;
    void createOgreObject() override;

    int id, team;

private:
    OgreWheel *ogreLWheel;
    OgreWheel *ogreRWheel;
};

} // physics
} // server
} // simulator
} // tools

#endif