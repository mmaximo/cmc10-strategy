#include "physics/robot/Robot.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

Robot::Robot(int team, int id) {
    this->type = "robot";
    this->id = id;
    this->team = team;
    this->createObject();
}

Robot::~Robot() {
    delete this->odeObject;
    delete this->ogreObject;
}

void Robot::createODEObject() {
  this->odeObject = new ODERobot(3*this->team + this->id);
  this->odeObject->createObject();
}

void Robot::createOgreObject() {
    this->ogreObject = new OgreRobot(this->team, this->id);
    this->ogreLWheel = new OgreWheel();
    this->ogreRWheel = new OgreWheel();
    this->ogreObject->addToScene();
    this->ogreLWheel->addToScene();
    this->ogreRWheel->addToScene();
}

void Robot::update() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    const dReal *position = robot->getRobotPosition();
    const dReal *orientation = robot->getRobotQuaternion();

    OgreRobot *ogreRobot = static_cast<OgreRobot *>(ogreObject);
    ogreRobot->update(position, orientation);
    ogreRobot->fixOrientation();

    position = robot->getLWheelPosition();
    orientation = robot->getLWheelQuaternion();
    ogreLWheel->update(position, orientation);
    position = robot->getRWheelPosition();
    orientation = robot->getRWheelQuaternion();
    ogreRWheel->update(position, orientation);
}

void Robot::commandRobotSpeed(double left, double right) {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    robot->commandRobotSpeed(left, right);
}

void Robot::updateRobot() {
    ODERobot* robot = static_cast<ODERobot*>(odeObject);
    robot->updateRobot();
}

void Robot::setFiniteAxis() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    robot->setFiniteAxis();//Needed to avoid inaccurate calculation errors
}

void Robot::keepUp() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    robot->keepUp();
}

const dReal *Robot::getRobotPosition() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getRobotPosition();
}

const dReal *Robot::getLWheelPosition() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getLWheelPosition();
}

const dReal *Robot::getRWheelPosition() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getRWheelPosition();
}

const dReal *Robot::getRobotQuaternion() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getRobotQuaternion();
}

const dReal *Robot::getLWheelQuaternion() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getLWheelQuaternion();
}

const dReal *Robot::getRWheelQuaternion() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getRWheelQuaternion();
}

const dReal *Robot::getVelocity() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getVelocity();
}

const dReal *Robot::getAVelocity() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getAVelocity();
}

const dReal* Robot::getLWheelASpeed() {
    ODERobot* robot = static_cast<ODERobot*>(odeObject);
    return robot->getLWheelASpeed();
}

const dReal* Robot::getRWheelASpeed() {
    ODERobot* robot = static_cast<ODERobot*>(odeObject);
    return robot->getRWheelASpeed();
}

dReal Robot::getAngle() {
    ODERobot *robot = static_cast<ODERobot *>(odeObject);
    return robot->getAngle();
}

} // physics
} // server
} // simulator
} // tools
