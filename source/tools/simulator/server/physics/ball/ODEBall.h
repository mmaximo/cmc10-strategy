//
// Created by Muzio on 17/03/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_ODEBALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_ODEBALL_H

#include <ode/ode.h>
#include "physics/base/ODEObject.h"
#include "physics/ode/ODEData.h"

const double BDIAMETER = 0.0427; // ball diameter
const double BMASS = 0.046;      // ball mass

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEBall : public ODEObject {
public:
    ODEBall();
    virtual ~ODEBall();
    void createObject();
    const dReal *getVelocity();
    const dReal *getAVelocity();
};

} // physics
} // server
} // simulator
} // tools

#endif
