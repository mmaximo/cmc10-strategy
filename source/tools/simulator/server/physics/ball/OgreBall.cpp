#include "physics/ball/OgreBall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreBall::OgreBall() { }

OgreBall::~OgreBall() {
//    this->mainNode->removeAndDestroyAllChildren();
}

void OgreBall::loadObject(Ogre::SceneManager *manager) {
    double diameter = core::representations::Ball::DIAMETER;

    Ogre::Entity *ballEntity = manager->createEntity("ball.mesh");
    ballEntity->setCastShadows(true);
    this->mainNode->attachObject(ballEntity);
    this->mainNode->setScale(diameter,
                             diameter,
                             diameter);
}

}
}
}
}
