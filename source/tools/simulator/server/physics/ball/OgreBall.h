//
// Created by Bandeira on 04/02/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_OGREBALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_OGREBALL_H

#include "OGRE/OgreSceneManager.h"
#include "OGRE/OgreEntity.h"
#include "core/representations/Ball.h"
#include "physics/base/OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class OgreBall : public OgreObject {
public:
    OgreBall();
    ~OgreBall();
    void loadObject(Ogre::SceneManager *manager);
};

} // physics
} // server
} // simulator
} // tools

#endif
