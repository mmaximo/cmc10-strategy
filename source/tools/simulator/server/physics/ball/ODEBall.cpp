//
// Created by Muzio on 17/03/15.
//

#include "physics/ball/ODEBall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

ODEBall::ODEBall() { }

ODEBall::~ODEBall() { }

void ODEBall::createObject() {
    ODEData &data = ODEData::getInstance();
    mainBody = dBodyCreate(data.getWorldID());

    //Setting mass
    dMass m;
    dMassSetSphereTotal(&m, BMASS, BDIAMETER / 2.0);
    dBodySetMass(mainBody, &m);

    //Setting collision surface
    dGeomID geom = dCreateSphere(data.getSpaceID(), BDIAMETER / 2.0);
    dGeomSetCategoryBits(geom, ODEData::cBall);
    dGeomSetBody(geom, mainBody);

    dBodySetAngularDamping(mainBody, 0.01);
}

const dReal *ODEBall::getVelocity() {
    return dBodyGetLinearVel(mainBody);
}

const dReal *ODEBall::getAVelocity() {
    return dBodyGetAngularVel(mainBody);
}

} // physics
} // server
} // simulator
} // tools
