//
// Created by Bandeira on 04/02/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_BALL_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_BALL_BALL_H

#include <ode/ode.h>
#include "physics/base/AbstractObject.h"
#include "physics/ball/ODEBall.h"
#include "physics/ball/OgreBall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class Ball : public AbstractObject {
public:
    Ball();
    ~Ball();
    const dReal *getVelocity();
    const dReal *getAVelocity();

protected:
    void createODEObject()  override;
    void createOgreObject() override;
};

} // physics
} // server
} // simulator
} // tools

#endif
