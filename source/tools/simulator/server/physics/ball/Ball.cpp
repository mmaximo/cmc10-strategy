#include "physics/ball/Ball.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

Ball::Ball() {
    this->type = "ball";
    this->createObject();
}

Ball::~Ball() {
    delete odeObject;
    delete ogreObject;
}

void Ball::createODEObject() {
    this->odeObject = new ODEBall();
    this->odeObject->createObject();
}

void Ball::createOgreObject() {
    this->ogreObject = new OgreBall();
    this->ogreObject->addToScene();
}

const dReal *Ball::getVelocity() {
    ODEBall *ball = static_cast<ODEBall *>(odeObject);
    return ball->getVelocity();
}

const dReal *Ball::getAVelocity() {
    ODEBall *ball = static_cast<ODEBall *>(odeObject);
    return ball->getAVelocity();
}

} // physics
} // server
} // simulator
} // tools
