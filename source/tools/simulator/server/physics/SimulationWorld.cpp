#include "physics/SimulationWorld.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
SimulationWorld &SimulationWorld::getInstance() {
    static SimulationWorld instance;
    return instance;
}

SimulationWorld::SimulationWorld() {
    this->nContacts = 0;
    this->nVisibleContacts = 0;
    this->maxContacts = 250;

    this->contacts.resize(static_cast<unsigned long>(this->maxContacts));

    for (int index = 0; index < this->maxContacts; index++) {
        this->contacts[index].reset();
        this->contacts[index].update();

        this->addToSimulation(&this->contacts[index]);
    }
}

SimulationWorld::~SimulationWorld() = default;

void SimulationWorld::addToSimulation(AbstractObject *object) {
    if (object->type != "temporary")
        objects.insert(object);

    if (object->type == "ball") {
        auto *ball = dynamic_cast<Ball *>(object);
        balls.insert(ball);
    }

    else if (object->type == "robot") {
        auto *robot = dynamic_cast<Robot *>(object);
        robots.insert(robot);
    }

    else if (object->type == "wall") {
        auto *wall = dynamic_cast<Wall *>(object);
        walls.insert(wall);
    }
}

void SimulationWorld::updateODEWorld() {
    for(auto& robot : robots){
        robot->updateRobot();
        //robot->setFiniteAxis();
    }

    this->nContacts = 0;
}

void SimulationWorld::updateOgreWorld() {
    for (auto &object : objects)
        object->update();

    for (int index = 0; index < this->nContacts; index++)
        this->contacts[index].update();

    this->hideContacts();
}

void SimulationWorld::addContact(double x, double y, double z) {
    if (this->nContacts == this->maxContacts)
        return;

    this->contacts[this->nContacts].setPosition(x, y, z);
    this->nContacts++;
}

void SimulationWorld::hideContacts() {
    for (int index = this->nContacts; index < this->nVisibleContacts; index++)
        this->contacts[index].reset();
}

} // physics
} // server
} // simulator
} // tools
