#include "OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

OgreObject::OgreObject() : nodeAttached(false) { }

OgreObject::~OgreObject() { }

void OgreObject::addToScene() {
    Ogre::SceneManager *sceneManager = gui::OgreManager::getInstance().getSceneManager();
    Ogre::SceneNode *root = sceneManager->getRootSceneNode();

    this->mainNode = root->createChildSceneNode();
    this->nodeAttached = true;
    this->loadObject(sceneManager);
}

void OgreObject::setPosition(Ogre::Vector3 position) {
    if (this->nodeAttached)
        this->mainNode->setPosition(position.x,
                                    position.y,
                                    position.z);
}

void OgreObject::setOrientation(Ogre::Quaternion orientation) {
    if (this->nodeAttached)
        this->mainNode->setOrientation(orientation.w,
                                       orientation.x,
                                       orientation.y,
                                       orientation.z);
}

void OgreObject::setScale(Ogre::Vector3 scale) {
    if (this->nodeAttached)
        this->mainNode->setScale(scale.x,
                                 scale.y,
                                 scale.z);
}

void OgreObject::update(const dReal *position, const dReal *orientation) {
    this->setPosition(Ogre::Vector3(position[0],
                                    position[1],
                                    position[2]));
    this->setOrientation(Ogre::Quaternion(orientation[0],
                                          orientation[1],
                                          orientation[2],
                                          orientation[3]));
}

} // physics
} // server
} // simulator
} // tools
