#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_OGREOBJECT_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_OGREOBJECT_H

#include <ode/ode.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>
//#include <OgreNode.h>
#include "physics/base/ODEObject.h"
#include "gui/OgreManager.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

//using Ogre::Node;

class OgreObject {
public:
    OgreObject();
    virtual ~OgreObject();
    virtual void addToScene();
    virtual void setPosition(Ogre::Vector3 position);
    virtual void setOrientation(Ogre::Quaternion orientation);
    virtual void setScale(Ogre::Vector3 scale);
    virtual void update(const dReal *position, const dReal *orientation);
    virtual void loadObject(Ogre::SceneManager *sceneManager) = 0;

protected:
    bool nodeAttached;
    Ogre::SceneNode *mainNode;
};

} // physics
} // server
} // simulator
} // tools

#endif
