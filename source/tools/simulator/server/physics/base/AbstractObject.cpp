#include "AbstractObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

AbstractObject::AbstractObject() { }

AbstractObject::~AbstractObject() { }

void AbstractObject::createObject() {
    this->createODEObject();
    this->createOgreObject();
}

void AbstractObject::createObject(dVector3 scale) {
    this->createODEObject(scale);
    this->createOgreObject(Ogre::Vector3(scale[0],
                                         scale[1],
                                         scale[2]));
}

//Updates only ogre info.
void AbstractObject::update() {
    const dReal *position = this->odeObject->getPosition();
    const dReal *orientation = this->odeObject->getOrientation();
    this->ogreObject->update(position, orientation);
}

void AbstractObject::setPosition(double x, double y, double z) {
    this->odeObject->stop();
    this->odeObject->setPosition(x, y, z);
}

void AbstractObject::setRotation(double theta) {
    this->odeObject->stop();
    this->odeObject->setOrientation(theta);
}

const dReal *AbstractObject::getPosition() {
    return this->odeObject->getPosition();
}

const dReal *AbstractObject::getQuaternion() {
    return this->odeObject->getOrientation();
}

void AbstractObject::createODEObject() { }

void AbstractObject::createOgreObject() { }

void AbstractObject::createODEObject(dVector3 scale) { }

void AbstractObject::createOgreObject(Ogre::Vector3 scale) { }

}
}
}
}
