#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ODEOBJECT
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ODEOBJECT

#include <ode/ode.h>

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class ODEObject {
public:
    ODEObject();

    virtual ~ODEObject();
    const dReal *getPosition();
    const dReal *getOrientation();
    virtual void setPosition(dReal x, dReal y, dReal z);
    virtual void setOrientation(dReal angle);
    virtual void stop();
    virtual void createObject() = 0;

protected:
    dBodyID mainBody;
};

} // tools
} // simulator
} // server
} // tools

#endif
