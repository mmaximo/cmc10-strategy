#include "ODEObject.h"

#include <iostream>
using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace physics {

ODEObject::ODEObject() { }

ODEObject::~ODEObject() { }

const dReal *ODEObject::getPosition() {
    return dBodyGetPosition(mainBody);
}

const dReal *ODEObject::getOrientation() {
    return dBodyGetQuaternion(mainBody);
}

void ODEObject::setPosition(dReal x, dReal y, dReal z) {
    dBodySetPosition(mainBody, x, y, z);
}

void ODEObject::setOrientation(dReal angle) {
    dQuaternion orientation;
    dQSetIdentity(orientation);
    dQFromAxisAndAngle(orientation, 0, 0, 1, angle);
    dBodySetQuaternion(mainBody, orientation);
}

void ODEObject::stop() {
    dBodySetLinearVel(mainBody, 0, 0, 0);
    dBodySetAngularVel(mainBody, 0, 0, 0);
}

}
}
}
}
