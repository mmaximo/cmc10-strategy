#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_ABSTRACTOBJECT
#define TOOLS_SIMULATOR_SERVER_PHYSICS_ABSTRACTOBJECT

#include <string.h>

#include <ode/ode.h>
#include <OGRE/Ogre.h>

#include "physics/base/ODEObject.h"
#include "physics/base/OgreObject.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class AbstractObject {
public:
    AbstractObject();
    virtual ~AbstractObject();
    virtual void createObject();
    virtual void createObject(dVector3 scale);
    virtual void update();
    virtual void setPosition(double x, double y, double z);
    virtual void setRotation(double theta);
    const dReal *getPosition();
    const dReal *getQuaternion();

    std::string type;

protected:
    virtual void createODEObject();
    virtual void createOgreObject();
    virtual void createODEObject(dVector3 scale);
    virtual void createOgreObject(Ogre::Vector3 scale);

    ODEObject *odeObject;
    OgreObject *ogreObject;
};

} // physics
} // server
} // simulator
} // tools

#endif
