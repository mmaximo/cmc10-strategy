#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS
#define TOOLS_SIMULATOR_SERVER_PHYSICS

#include "ode/ode.h"
#include "core/representations/Field.h"
#include "physics/ode/ODEData.h"
#include "physics/SimulationWorld.h"
#include "physics/wall/Wall.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

class SimulatorField {
public:
    SimulatorField();
    ~SimulatorField();

    void createField();

private:
    dVector3 pos;
    dVector3 scale;
    SimulationWorld &simWorld;

    void createGround();
    void createHorizontalWall(bool top);
    void createVerticalWall(bool top, bool right);
    void createHorizontalGoalWall(bool top, bool right);
    void createVerticalGoalWall(bool right);
    void createDiagonalWall(bool top, bool right);
};

} // physics
} // server
} // simulator
} // tools

#endif