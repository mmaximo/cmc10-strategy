//
// Created by Zero Um on 16/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_PHYSICS_TEAM_H
#define TOOLS_SIMULATOR_SERVER_PHYSICS_TEAM_H

#include "physics/SimulationWorld.h"
#include "representations/Player.h"
#include "physics/robot/Robot.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

using core::representations::WheelSpeed;
using core::representations::Player;

class Team {
public:
    Team(int teamId);
    virtual ~Team();
    Robot *getRobot(int n);
    const dReal* getRobotPosition(int robotId);
    void setWheelSpeeds(WheelSpeed wheelSpeeds[]);
    void setRobotPosition(int robotId, double x, double y, double z);
    void setRobotRotation(int robotId, double rotation);

private:
    Robot *team[Player::PLAYERS_PER_SIDE];
};

} // physics
} // server
} // simulator
} // tools

#endif
