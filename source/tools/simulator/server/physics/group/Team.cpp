//
// Created by Zero Um on 16/04/15.
//

#include "physics/group/Team.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

Team::Team(int teamId) { //TODO: Fix this...
    SimulationWorld &simWorld = SimulationWorld::getInstance();

    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        this->team[i] = new Robot(teamId, i);
        simWorld.addToSimulation(team[i]);

        //team[i]->setPosition(0,0,0);
        //team[i]->setRotation(0);
    }
}

Robot *Team::getRobot(int n) {
    if (n < 0 || n >= Player::PLAYERS_PER_SIDE)
        return nullptr;

    return team[n];
}

const dReal* Team::getRobotPosition(int robotId) {
    Robot *robot = this->getRobot(robotId);

    if(robot == nullptr)
        return nullptr;

    return robot->getPosition();
}

void Team::setWheelSpeeds(WheelSpeed wheelSpeeds[]) {
    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++)
        team[i]->commandRobotSpeed(wheelSpeeds[i].left, wheelSpeeds[i].right);
}

void Team::setRobotPosition(int robotId, double x, double y, double z) {
    Robot *robot = this->getRobot(robotId);

    if(robot == nullptr)
        return;

    robot->setPosition(x, y, z);
}

void Team::setRobotRotation(int robotId, double rotation) {
    Robot *robot = this->getRobot(robotId);

    if(robot == nullptr)
        return;

    robot->setRotation(rotation);
}

Team::~Team() {
    // TODO: delete robots
}

} // physics
} // server
} // simulator
} // tools
