#include "physics/group/SimulatorField.h"

namespace tools {
namespace simulator {
namespace server {
namespace physics {

SimulatorField::SimulatorField() :
                simWorld(SimulationWorld::getInstance()){
    this->createField();
}

void SimulatorField::createField() {
    this->createGround();

    this->createHorizontalWall(true);
    this->createHorizontalWall(false);

    this->createVerticalWall(true, true);
    this->createVerticalWall(true, false);
    this->createVerticalWall(false, true);
    this->createVerticalWall(false, false);

    this->createHorizontalGoalWall(true, true);
    this->createHorizontalGoalWall(true, false);
    this->createHorizontalGoalWall(false, true);
    this->createHorizontalGoalWall(false, false);

    this->createDiagonalWall(true, true);
    this->createDiagonalWall(true, false);
    this->createDiagonalWall(false, true);
    this->createDiagonalWall(false, false);

    this->createVerticalGoalWall(true);
    this->createVerticalGoalWall(false);
}

SimulatorField::~SimulatorField() {

}

void SimulatorField::createGround() { //TODO:Remove this lazy implementation
    //ODE
    dGeomID ground = dCreatePlane(ODEData::getInstance().getSpaceID(), 0.0, 0.0, 1.0, 0.0);
    dGeomSetCategoryBits(ground, ODEData::cField);

    //Ogre
    Ogre::Entity *groundEntity = gui::OgreManager::getInstance().getSceneManager()->createEntity("Plane.mesh");
    groundEntity->setCastShadows(true);
    Ogre::SceneNode *node = gui::OgreManager::getInstance().getSceneManager()->getRootSceneNode()->createChildSceneNode();
    node->attachObject(groundEntity);
    node->rotate(Ogre::Quaternion(sqrtf(0.5), sqrtf(0.5), 0, 0));
}

void SimulatorField::createHorizontalWall(bool top) {
    scale[0] = core::representations::Field::FIELD_LENGTH_X;
    scale[1] = core::representations::Field::WALL_THICKNESS;
    scale[2] = core::representations::Field::WALL_HEIGHT;
    int signal = top ? 1 : -1;
    pos[0] = 0.0;
    pos[1] = signal * (core::representations::Field::FIELD_LENGTH_Y + core::representations::Field::WALL_THICKNESS) / 2.0;
    pos[2] = core::representations::Field::WALL_HEIGHT / 2.0;

    auto pWall = new physics::Wall(scale);
    pWall->setPosition(pos[0], pos[1], pos[2]);
    pWall->setRotation(0);

    simWorld.addToSimulation(pWall);
}

void SimulatorField::createVerticalWall(bool top, bool right) {
    scale[0] = core::representations::Field::WALL_THICKNESS;
    scale[1] = core::representations::Field::FIELD_LENGTH_Y / 2.0 + core::representations::Field::WALL_THICKNESS
               - core::representations::Field::GOAL_LENGTH_Y / 2.0;
    scale[2] = core::representations::Field::WALL_HEIGHT;
    int signal1 = right ? 1 : -1;
    int signal2 = top ? 1 : -1;
    pos[0] = signal1 * (core::representations::Field::FIELD_LENGTH_X + core::representations::Field::WALL_THICKNESS) / 2.0;
    pos[1] = signal2 * (core::representations::Field::FIELD_LENGTH_Y + 2 * core::representations::Field::WALL_THICKNESS
                        + core::representations::Field::GOAL_LENGTH_Y) / 4.0;
    pos[2] = core::representations::Field::WALL_HEIGHT / 2.0;

    auto *pWall = new physics::Wall(scale);
    pWall->setPosition(pos[0], pos[1], pos[2]);
    pWall->setRotation(0);

    simWorld.addToSimulation(pWall);
}

void SimulatorField::createHorizontalGoalWall(bool top, bool right) {
    scale[0] = core::representations::Field::GOAL_LENGTH_X - core::representations::Field::WALL_THICKNESS;
    scale[1] = core::representations::Field::WALL_THICKNESS;
    scale[2] = core::representations::Field::WALL_HEIGHT;
    int signal1 = right ? 1 : -1;
    int signal2 = top ? 1 : -1;
    pos[0] = signal1 * (core::representations::Field::GOAL_LENGTH_X + core::representations::Field::FIELD_LENGTH_X +
                        core::representations::Field::WALL_THICKNESS) / 2.0;
    pos[1] = signal2 * (core::representations::Field::GOAL_LENGTH_Y + core::representations::Field::WALL_THICKNESS) / 2.0;
    pos[2] = core::representations::Field::WALL_HEIGHT / 2.0;

    auto pWall = new physics::Wall(scale);
    pWall->setPosition(pos[0], pos[1], pos[2]);
    pWall->setRotation(0);

    simWorld.addToSimulation(pWall);
}

void SimulatorField::createVerticalGoalWall(bool right) {
    scale[0] = core::representations::Field::WALL_THICKNESS;
    scale[1] = core::representations::Field::GOAL_LENGTH_Y + 2 * core::representations::Field::WALL_THICKNESS;
    scale[2] = core::representations::Field::WALL_HEIGHT;
    int signal = right ? 1 : -1;
    pos[0] = signal * (core::representations::Field::FIELD_LENGTH_X + 2 * core::representations::Field::GOAL_LENGTH_X
                       + core::representations::Field::WALL_THICKNESS) / 2.0;
    pos[1] = 0.0;
    pos[2] = core::representations::Field::WALL_HEIGHT / 2.0;

    auto *pWall = new physics::Wall(scale);
    pWall->setPosition(pos[0], pos[1], pos[2]);
    pWall->setRotation(0);

    simWorld.addToSimulation(pWall);
}

void SimulatorField::createDiagonalWall(bool top, bool right) {
    scale[0] = sqrt(2) * core::representations::Field::CORNER_TRIANGLE_SIDE_SIZE;
    scale[1] = core::representations::Field::WALL_THICKNESS;
    scale[2] = core::representations::Field::WALL_HEIGHT;
    int signal1 = right ? 1 : -1;
    int signal2 = top ? 1 : -1;
    pos[0] = signal1 * (core::representations::Field::FIELD_LENGTH_X - core::representations::Field::CORNER_TRIANGLE_SIDE_SIZE +
                        core::representations::Field::WALL_THICKNESS / sqrt(2.0)) / 2.0;
    pos[1] = signal2 * (core::representations::Field::FIELD_LENGTH_Y - core::representations::Field::CORNER_TRIANGLE_SIDE_SIZE +
                        core::representations::Field::WALL_THICKNESS / sqrt(2.0)) / 2.0;
    pos[2] = core::representations::Field::WALL_HEIGHT / 2.0;

    auto *pWall = new physics::Wall(scale);
    pWall->setPosition(pos[0], pos[1], pos[2]);
    pWall->setRotation(-signal1 * signal2 * M_PI / 4);

    simWorld.addToSimulation(pWall);
}

} // physics
} // server
} // simulator
} // tools
