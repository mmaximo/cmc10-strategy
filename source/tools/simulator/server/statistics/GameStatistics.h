//
// Created by Igor on 04/04/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_STATISTICS_GAMESTATISTICS_H
#define TOOLS_SIMULATOR_SERVER_STATISTICS_GAMESTATISTICS_H

#include "statistics/Logger.h"

namespace tools {
namespace simulator {
namespace server {
namespace statistics {

class GameStatistics {
public:
    static GameStatistics &getInstance();

    void restartMatch();
    void endSimulation();
    void updateGameStatisticsWins();
    int getLeftScore();
    int getRightScore();
    void addLeftScore();
    void addRightScore();
    void addPenaltyScore();
    void addLeftFoul();
    void addRightFoul();
    void addTimeBallOnLeftSide();
    void addTimeBallOnRightSide();
    void logPenalty(int numberOfPenalties);
    void setGameType(manager::GameType gameType);

private:
    GameStatistics(); // Prevent construction
    GameStatistics(const GameStatistics&); // Prevent construction by copying
    GameStatistics& operator = (const GameStatistics&); // Prevent assignment
    ~GameStatistics(); // Prevent unwanted destruction

    bool gameTypeSet;
    int numberOfPenaltiesGoals,
        leftFouls,
        rightFouls,
        leftScore,
        rightScore,
        totalLeftScore,
        totalRightScore,
        leftSideWins,
        rightSideWins,
        ties;
    unsigned long long int totalTimeBallOnRightSide,
                           totalTimeBallOnLeftSide,
                           timeBallOnRightSide,
                           timeBallOnLeftSide;

    Logger *logger;
};

} // statistics
} // server
} // simulator
} // tools

#endif
