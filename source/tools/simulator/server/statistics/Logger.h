//
// Created by Matheus on 31/08/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_STATISTICS_LOGGER_H
#define TOOLS_SIMULATOR_SERVER_STATISTICS_LOGGER_H

#include <fstream>
#include <iostream>

#include "manager/GameType.h"

namespace tools {
namespace simulator {
namespace server {
namespace statistics {

class Logger {
public:
    Logger(manager::GameType gameType);
    ~Logger();

    void logMatchScore(const int leftScore, const int rightScore, const int matchNumber);

    void logPercentageWin(const int numberOfMatches,
                          const int leftSideWins,
                          const int rightSideWins,
                          const int ties);

    void logPenaltyStatistics(const int numberOfPenalties,
                              const int numberOfPenaltyGoals);

    void logFouls(const int leftFouls, const int rightFouls);

    void logPercentageBallIsOnEachSide(const double timeBallOnLeftSide,
                                       const double timeBallOnRightSide);

    void logTotalPercentageBallIsOnEachSide(double totalTimeBallOnLeftSide,
                                            double totalTimeBallOnRightSide);

    void logTotalGoals(int totalLeftScore, int totalRightScore);

private:
    std::ofstream mainWinsFile;
    std::ofstream penaltyStatisticsFile;
    manager::GameType gameType;
};

} // statistics
} // server
} // simulator
} // tools

#endif
