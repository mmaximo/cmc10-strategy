//
// Created by Igor on 04/04/18.
//

#include "statistics/GameStatistics.h"

#include <iostream>
using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace statistics {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
GameStatistics &GameStatistics::getInstance() {
    static GameStatistics instance;
    return instance;
}

GameStatistics::GameStatistics() {
    this->logger = nullptr;

    this->gameTypeSet = false;
    this->leftScore = 0;
    this->rightScore = 0;
    this->totalLeftScore = 0;
    this->totalRightScore = 0;
    this->leftSideWins = 0;
    this->rightSideWins = 0;
    this->ties = 0;
    this->numberOfPenaltiesGoals = 0;
    this->leftFouls = 0;
    this->rightFouls = 0;
    this->totalTimeBallOnRightSide = 0;
    this->totalTimeBallOnLeftSide = 0;
    this->timeBallOnRightSide = 0;
    this->timeBallOnLeftSide = 0;
}

GameStatistics::~GameStatistics() {
    delete this->logger;
}

void GameStatistics::restartMatch() {
    this->updateGameStatisticsWins();
    this->leftScore = 0;
    this->rightScore = 0;
    this->leftFouls = 0;
    this->rightFouls = 0;
    this->timeBallOnRightSide = 0;
    this->timeBallOnLeftSide = 0;
}

void GameStatistics::endSimulation() {
    if(!this->logger)
        return;

    int numberOfMatches = this->leftSideWins + this->rightSideWins + this->ties;

    this->logger->logPercentageWin(numberOfMatches,
                                  this->leftSideWins,
                                  this->rightSideWins,
                                  this->ties);

    this->logger->logTotalGoals(this->totalLeftScore, this->totalRightScore);

    this->logger->logTotalPercentageBallIsOnEachSide(this->totalTimeBallOnLeftSide,
                                                     this->totalTimeBallOnRightSide);

    this->totalTimeBallOnLeftSide  = 0;
    this->totalTimeBallOnRightSide = 0;
}

int GameStatistics::getLeftScore() {
    return this->leftScore;
}

int GameStatistics::getRightScore() {
    return this->rightScore;
}

void GameStatistics::addLeftScore() {
    this->leftScore++;
    this->totalLeftScore++;

//    this->addPenaltyScore();
    cout << "Goal [LEFT] : " << this->leftScore << " x " << this->rightScore << endl;
}

void GameStatistics::addRightScore() {
    this->rightScore++;
    this->totalRightScore++;

    cout << "Goal [RIGHT]: " << this->leftScore << " x " << this->rightScore << endl;
}

void GameStatistics::addPenaltyScore() {
    this->numberOfPenaltiesGoals++;
}

void GameStatistics::addRightFoul() {
    this->rightFouls++;
}

void GameStatistics::addLeftFoul() {
    this->leftFouls++;
}

void GameStatistics::addTimeBallOnLeftSide() {
    this->timeBallOnLeftSide ++;
    this->totalTimeBallOnLeftSide++;
}

void GameStatistics::addTimeBallOnRightSide() {
    this->timeBallOnRightSide ++;
    this->totalTimeBallOnRightSide++;
}

void GameStatistics::updateGameStatisticsWins() {
    if(this->leftScore > this->rightScore)
        this->leftSideWins++;
    else if (this->leftScore < this->rightScore)
        this->rightSideWins++;
    else
        this->ties++;

    if(!this->logger)
        return;

    this->logger->logMatchScore(this->leftScore, this->rightScore,
                                this->leftSideWins + this->rightSideWins + this->ties);
    this->logger->logFouls(this->leftFouls, this->rightFouls);
    this->logger->logPercentageBallIsOnEachSide(this->timeBallOnLeftSide, this->timeBallOnRightSide);
}

void GameStatistics::logPenalty(int numberOfPenalties) {
    if(!this->logger)
        return;

    this->logger->logPenaltyStatistics(numberOfPenalties, this->numberOfPenaltiesGoals);
}

void GameStatistics::setGameType(manager::GameType gameType) {
    if(this->gameTypeSet)
        return;

    this->logger = new Logger(gameType);
    this->gameTypeSet = true;
}

} // statistics
} // server
} // simulator
} // tools
