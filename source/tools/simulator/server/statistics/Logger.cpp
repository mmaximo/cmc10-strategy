//
// Created by Matheus on 31/08/16.
//

#include "statistics/Logger.h"

#include <iostream>
#include <iomanip>

using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace statistics {

Logger::Logger(manager::GameType gameType) {
    this->gameType = gameType;

    if (gameType == manager::GameType::ACCELERATED or gameType == manager::GameType::NORMAL or
        gameType == manager::GameType::FREE_BALL   or gameType == manager::GameType::GOAL_SHOOT) {
        this->mainWinsFile.open("../logging/simulator/simulator_logger.txt");
        this->mainWinsFile << "          LeftGoals  RightGoals  LeftFouls  RightFouls  TimeBallOnLeftSide  TimeBallOnRightSide" << std::endl;
    }

    if (gameType == manager::GameType::PENALTY)
        this->penaltyStatisticsFile.open("../logging/simulator/penalty_statistics.txt", std::ios::out | std::ios::app);
}

Logger::~Logger() {
    if (gameType == manager::GameType::ACCELERATED) {
        this->mainWinsFile.close();
    }

    if (gameType == manager::GameType::PENALTY)
        this->penaltyStatisticsFile.close();
}

void Logger::logMatchScore(const int leftScore, const int rightScore, const int matchNumber) {
    this->mainWinsFile << "Match " << std::setw(3) << matchNumber << ":     " << std::setw(2)
                       << leftScore << "        " << std::setw(2) << rightScore <<"        ";
    std::cout << "Left score = " << leftScore << " and right score = " << rightScore << std::endl;
    std::ofstream optimizationFile;
    optimizationFile.open("../source/tools/parameter-optimization/logger/univector/simulator_win_logger.txt");
    optimizationFile << leftScore<< " "<< rightScore << " " << endl;
    optimizationFile.close();
}

void Logger::logPercentageWin(const int numberOfMatches,
                              const int leftSideWins,
                              const int rightSideWins,
                              const int ties) {
    double percentageWinLeft = leftSideWins * 100.0 / numberOfMatches;
    double percentageWinRight = 1.0 * rightSideWins * 100.0 / numberOfMatches;
    double percentageTies = ties * 100.0 / numberOfMatches;

    this->mainWinsFile << std::endl << "Left Wins: " << leftSideWins << std::endl;
    this->mainWinsFile << "Right Wins: " << rightSideWins << std::endl;
    this->mainWinsFile << "Ties: " << ties << std::endl;
    this->mainWinsFile << "Number of Matches: " << numberOfMatches << std::endl;

    this->mainWinsFile << std::endl << "Percentage of wins by left: " << percentageWinLeft << "%" << std::endl;
    this->mainWinsFile << "Percentage of win by right: " << percentageWinRight << "%" << std::endl;
    this->mainWinsFile << "Percentage of ties: " << percentageTies << "%" << std::endl << std::endl;

    std::ofstream optimizationFile;
    optimizationFile.open("../source/tools/parameter-optimization/logger/univector/simulator_win_logger.txt", std::ios::out | std::ios::app);
    optimizationFile << leftSideWins << " "<< rightSideWins << " " << ties << endl;
    optimizationFile.close();
}

void Logger::logPenaltyStatistics(const int numberOfPenalties, const int numberOfPenaltyGoals) {
    std::cout << "Number of penalties: " << numberOfPenalties << std::endl;
    std::cout << "Number of goals: " << numberOfPenaltyGoals << std::endl;
    std::cout << "Percentage of goals: " << ((double) numberOfPenaltyGoals / (double) numberOfPenalties) * 100.0
              << "%" << std::endl;

    this->penaltyStatisticsFile << "Number of penalties: " << numberOfPenalties << std::endl;
    this->penaltyStatisticsFile << "Number of goals: " << numberOfPenaltyGoals << std::endl;
    this->penaltyStatisticsFile << "Percentage of goals: " << ((double) numberOfPenaltyGoals / (double) numberOfPenalties) * 100.0 << "%" << std::endl;
}

void Logger::logPercentageBallIsOnEachSide(const double timeBallOnLeftSide,
                                           const double timeBallOnRightSide) {
    double totalTime = timeBallOnLeftSide + timeBallOnRightSide;
    double leftSidePercentual  = (timeBallOnLeftSide/totalTime) * 100.0;
    double rightSidePercentual = (timeBallOnRightSide/totalTime) * 100.0;

    this->mainWinsFile << std::fixed << std::setprecision(2) << leftSidePercentual << "%            " <<
            std::fixed << std::setprecision(2) << rightSidePercentual << "%" << std::endl;

    std::cout << "TimeBallOnLeftSide: " << std::fixed << std::setprecision(2) << leftSidePercentual << "% and timeBallOnRightSide = "
            << std::fixed << std::setprecision(2) << rightSidePercentual << "%" << std::endl;
    std::cout << std::endl;
}

void Logger::logFouls(const int leftFouls, const int rightFouls) {
    this->mainWinsFile << std::setw(3) << leftFouls << "         " << std::setw(3) << rightFouls << "            ";

    std::cout << "Left fouls = " << leftFouls << " and rightFouls = " << rightFouls << std::endl;
}

void Logger::logTotalPercentageBallIsOnEachSide(double totalTimeBallOnLeftSide, double totalTimeBallOnRightSide) {
    double totalTime = totalTimeBallOnLeftSide + totalTimeBallOnRightSide,
           rightSidePercentual = (totalTimeBallOnRightSide/totalTime) * 100.0,
           leftSidePercentual  = (totalTimeBallOnLeftSide/totalTime) * 100.0;

    this->mainWinsFile << "Percentage time ball on the right side: " << rightSidePercentual << "%" << std::endl;
    this->mainWinsFile << "Percentage time ball on the left side: " << leftSidePercentual << "%" << std::endl << std::endl;

    std::ofstream optimizationFile;
    optimizationFile.open("../source/tools/parameter-optimization/logger/simulator_ball_logger.txt");
    optimizationFile << leftSidePercentual << " " << rightSidePercentual << endl;
    optimizationFile.close();
}

void Logger::logTotalGoals(int totalLeftScore, int totalRightScore) {
    this->mainWinsFile << "Total left goals: " << totalLeftScore << std::endl;
    this->mainWinsFile << "Total right goals: " << totalRightScore << std::endl;
}

} // statistics
} // server
} // simulator
} // tools
