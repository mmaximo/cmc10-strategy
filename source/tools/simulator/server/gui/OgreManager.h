#ifndef TOOLS_SIMULATOR_SERVER_GUI_OGREMANAGER
#define TOOLS_SIMULATOR_SERVER_GUI_OGREMANAGER

#include <Ogre.h>

// Simulator widgets constants - ----- ----- ----- ----- -----
const int SIM_WIDTH  = 640; // default width
const int SIM_HEIGHT = 480; // default height
const double SIM_AR  = (((double) SIM_WIDTH)/SIM_HEIGHT); // aspect ratio

const int SIM_MIN_WIDTH  = 100; // minimum width
const int SIM_MIN_HEIGHT = 100; // minimum height
// ----- ----- ----- ----- ----- ----- ----- ----- ----- -----

namespace tools {
namespace simulator {
namespace server {
namespace gui {

class OgreManager {
public:
    static OgreManager &getInstance();

    bool updateFrame();
    void initialise();
    void setupCamera();
    void setupLight();
    void setupResources();
    void setupSceneManager();
    Ogre::SceneManager *getSceneManager();

protected:
    void attachToRootNode(Ogre::MovableObject *newObject);
    Ogre::Light *createLight(Ogre::Vector3 direction, Ogre::Vector3 position);

    Ogre::Camera *camera;
    Ogre::Light *light1;
    Ogre::Light *light2;
    Ogre::RenderWindow *renderWindow;
    Ogre::SceneManager *sceneManager;
    Ogre::SceneNode *rootSceneNode;
    Ogre::Root *root;

private:
    OgreManager(); // Prevent construction
    OgreManager(const OgreManager&); // Prevent construction by copying
    OgreManager& operator = (const OgreManager&); // Prevent assignment
    ~OgreManager(); // Prevent unwanted destruction
};

} // gui
} // server
} // simulator
} // tools

#endif
