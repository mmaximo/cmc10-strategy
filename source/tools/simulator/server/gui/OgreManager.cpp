#include "gui/OgreManager.h"

namespace tools {
namespace simulator {
namespace server {
namespace gui {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
OgreManager &OgreManager::getInstance() {
    static OgreManager instance;
    return instance;
}

OgreManager::OgreManager() {
    initialise();
    setupSceneManager();
    setupCamera();
    setupLight();
    setupResources();
}

OgreManager::~OgreManager() {
    sceneManager->destroyAllCameras();
    sceneManager->destroyAllManualObjects();
    sceneManager->destroyAllEntities();
    sceneManager->getRootSceneNode()->removeAndDestroyAllChildren();
    Ogre::ResourceGroupManager::getSingleton().destroyResourceGroup("ResGroup");
    renderWindow->removeAllViewports();
    root->shutdown();
    delete(renderWindow);
}

bool OgreManager::updateFrame() {
    // If it should close the widgets, closes
    if (renderWindow->isClosed())
        return false;

    // Maintains aspect ratio while resizing --- ----- ----- -----
    int act_width  = renderWindow->getWidth(),  // actual width
        act_height = renderWindow->getHeight(); // actual height

    if(act_width < SIM_MIN_WIDTH)    // prevents 'zero' sizes
        act_width = SIM_MIN_WIDTH;   // and 'zero' divisions
    if(act_height < SIM_MIN_HEIGHT)  // when calculating aspect
        act_height = SIM_MIN_HEIGHT; // ratio

    double act_ar = ((double) act_width)/act_height; // actual aspect ratio

    if(act_ar < SIM_AR)                                   //
        act_width = (int) (SIM_AR*((double) act_height)); // fix aspect ratio
    else                                                  //       :)
        act_height = (int) (((double) act_width)/SIM_AR); //

    renderWindow->resize(act_width, act_height); // fix dimensions
    // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----

    renderWindow->update(false); // swapBuffers = false, doesn't swap automatically
    renderWindow->swapBuffers(); // manual buffer swap
    root->renderOneFrame();

    //Handling mouse and keyboard events
//    Ogre::WindowEventUtilities::messagePump();
    return true;
}

//Setup OGRE Window
void OgreManager::initialise() {
    this->root = new Ogre::Root("../resources/plugins.cfg", // load plugins info
                                "../resources/ogre.cfg",    // load configurations
                                "simulator_gui.log");       // logfile to be created

    if (!this->root->restoreConfig() &&  // restoreConfig: checks if there is valid saved config of previous run
        !this->root->showConfigDialog()) // showConfigDialog: displays widgets asking user to choose system settings,
        return;                          // if no saved config is found

    this->root->initialise(false); // autoCreateWindow = false, doesn't create rendering widgets automatically

    this->renderWindow = this->root->createRenderWindow("VSS Simulator Monitor", // widgets name
                                                        SIM_WIDTH,               // width
                                                        SIM_HEIGHT,              // height
                                                        false);                  // fullScreen

    this->renderWindow->setActive(true);       // state = true
    this->renderWindow->setAutoUpdated(false); // autoupdate = false
}

void OgreManager::setupCamera() {
    this->camera = this->sceneManager->createCamera("MainCamera");
    this->attachToRootNode(camera);

    // ViewPort: A rectangle into which rendering output is sent

    //Constants for viewport creation
    float viewportWidth  = 1.0f;
    float viewportHeight = 1.0f;
    float viewportLeft = (1.0f - viewportWidth)  * 0.5f;
    float viewportTop  = (1.0f - viewportHeight) * 0.5f;
    int mainViewportZOrder = 100; // relative order to other viewports in the same target,
                                  // higher Z-orders are on top of lower orders

    //Creating the viewport
    Ogre::Viewport *vp = this->renderWindow->addViewport(this->camera,  mainViewportZOrder,
                                                         viewportLeft,  viewportTop,        // top-left coordinates
                                                         viewportWidth, viewportHeight);    // dimensions

    //Setting up the background color
    vp->setAutoUpdated(true); // autoupdate = true
    vp->setBackgroundColour(Ogre::ColourValue(0.0f, 0.0f, 0.0f));

    //Setups the camera aspect ratio
    int width  = vp->getActualWidth(),
        height = vp->getActualHeight();
    double ratio = ((double) width)/height;
    this->camera->setAspectRatio(ratio);

    //Setups the camera to have a top down configuration
    this->camera->setNearClipDistance(.5f);   // clip distance to clipping planes, for constructing
    this->camera->setFarClipDistance(3000.0f); // the camera field of view (or frustum)
    this->camera->setPosition(0.0f, 0.0f, 2.0f);
    this->camera->lookAt(0.0f, 0.0f, 0.0f);
}

//Setups ambient light
void OgreManager::setupLight() {
    this->sceneManager->setAmbientLight(Ogre::ColourValue(0.3f, 0.3f, 0.3f));
    this->sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    this->light1 = this->createLight(Ogre::Vector3(0, 0, -1),           // direction
                                     Ogre::Vector3( 0.8f, 0.0f, 3.0f)); // position
    this->attachToRootNode(light1);

    this->light2 = this->createLight(Ogre::Vector3(0, 0, -1),           // direction
                                     Ogre::Vector3(-0.8f, 0.0f, 3.0f)); // position
    this->attachToRootNode(light2);
}

void OgreManager::setupResources() {
    Ogre::ResourceGroupManager *resources = Ogre::ResourceGroupManager::getSingletonPtr();

    resources->createResourceGroup("ResGroup");

    resources->addResourceLocation("../resources/textures", // path
                                   "FileSystem",            // type
                                   "ResGroup",              // group
                                   false);                  // recursive = false

    resources->addResourceLocation("../resources/meshes", // path
                                   "FileSystem",          // type
                                   "ResGroup",            // group
                                   false);                // recursive = false

    resources->initialiseResourceGroup("ResGroup");
    resources->loadResourceGroup("ResGroup");
}

void OgreManager::setupSceneManager() {
    this->sceneManager = root->createSceneManager(Ogre::ST_GENERIC);
    this->rootSceneNode = this->sceneManager->getRootSceneNode();
}

Ogre::SceneManager *OgreManager::getSceneManager() {
    return this->sceneManager;
}

void OgreManager::attachToRootNode(Ogre::MovableObject *newObject) {
    Ogre::SceneNode *newNode = this->rootSceneNode->createChildSceneNode();  // create new child node
    newNode->attachObject(newObject);                                        // attach object to created node
}

Ogre::Light *OgreManager::createLight(Ogre::Vector3 direction, Ogre::Vector3 position) {
    Ogre::Light *light = this->sceneManager->createLight();

    light->setType(Ogre::Light::LT_POINT);      // current default
    light->setDiffuseColour(0.7f, 0.7f, 0.7f);  //     values
    light->setSpecularColour(0.0f, 0.0f, 0.0f); //

    light->setDirection(direction); // customizable
    light->setPosition(position);   //    values

    return light;
}
    
} // gui
} // server
} // simulator
} // tools
