//
// Created by Muzio on 07/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_GAMESTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_GAMESTATE_H

#include "core/representations/Ball.h"
#include "core/representations/Field.h"
#include "core/representations/Team.h"
#include "physics/ball/Ball.h"
#include "physics/group/Team.h"
#include "gamestate/States.h"
#include "manager/GameManager.h"
#include "statistics/GameStatistics.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

class GameState {
public:
    GameState(manager::GameManager &gm);
    virtual ~GameState();
    virtual State run() = 0;
    virtual void enterState() = 0;

protected:
    physics::Ball &ball;
    physics::Team &leftTeam;
    physics::Team &rightTeam;
    manager::GameManager &gm;
};

} // gamestate
} // server
} // simulator
} // tools

#endif
