//
// Created by Muzio on 17/03/15.
//

#include "gamestate/Judge.h"

#include <iostream>
using namespace std;

namespace tools {
namespace simulator {
namespace server {
namespace judge {

using namespace core;

Judge::Judge(physics::Team &leftTeam,
             physics::Team &rightTeam,
             physics::Ball &ball) :
       leftTeam(leftTeam),
       rightTeam(rightTeam),
       ball(ball) {

    this->gameStarted = false;
    this->gameEnded   = false;
    this->playAgain   = false;
    this->wasGoal     = false;
    this->wasFoul     = false;

    // For the fouls
    currentFrame = 0;
    this->penaltyKickStateGameCycle = currentFrame;
    this->goalKickStateGameCycle    = currentFrame;
    this->ballInStalemateGameCycle  = currentFrame;
    this->lastBallPosInStalemate = core::math::Vector2<double>(ball.getPosition()[0],
                                                                         ball.getPosition()[1]);

    this->wasFreeBall = false;
    this->wasFreeKick = false;
    this->wasGoalKick = false;
    this->wasPenaltyKick = false;
}

Judge::~Judge() { }



bool Judge::update() {
    this->gameStarted = true;
    this->wasGoal = false;
    this->wasFoul = false;

    this->wasGoal = this->checkIfGoal();

    // For the fouls
    currentFrame++;
    this->wasFreeBall = this->checkIfFreeBall();
    this->wasFreeKick = this->checkIfFreeKick();
    this->wasGoalKick = this->checkIfGoalKick();
    this->wasPenaltyKick = this->checkIfPenaltyKick();

    if(this->wasPenaltyKick)
        cout << "penalty!" << endl;
    if(this->wasFreeBall)
        cout << "Free ball!" << endl;
    if(this->wasGoalKick)
        cout << "Goal kick!" << endl;
    if(this->wasFreeKick)
        cout << "Free kick!" << endl;


    return this->wasFreeBall or
           this->wasFreeKick or
           this->wasGoalKick or
           this->wasPenaltyKick;
}

bool Judge::checkIfGoal() {
    this->wasRightGoal = (ball.getPosition()[0] <= representations::Field::LEFT_WALL_X - representations::Ball::DIAMETER);
    this->wasLeftGoal  = (ball.getPosition()[0] >= representations::Field::RIGHT_WALL_X + representations::Ball::DIAMETER);

    return this->wasRightGoal or this->wasLeftGoal;
}

bool Judge::checkIfAllyGoal() {
    return ball.getPosition()[0] >= representations::Field::RIGHT_WALL_X + representations::Ball::DIAMETER;
}

bool Judge::checkIfOpponentGoal() {
    return ball.getPosition()[0] <= representations::Field::LEFT_WALL_X - representations::Ball::DIAMETER;
}

bool Judge::checkIfFreeBall() {
    double currentStep = currentFrame;
    bool isFoul = false;
    const dReal *ballPos = this->ball.getPosition();

    if (ballPos[0] == 0.0 and ballPos[1] == 0) {
        ballInStalemateGameCycle = currentStep;
        return false;
    }
    if (ballPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X and
        ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
        ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y)
        return false;

    if (ballPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X and
        ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
        ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y)
        return false;
    // std::cout << "X = " << ball.getPositionX() << "Y = " << ball.getPositionY() << std::endl;
    //std::cout << "LastPos.x = " << lastBallPosInStalemate.x << " e y = " << lastBallPosInStalemate.y <<std::endl;
    if (fabs(ballPos[0] - lastBallPosInStalemate.x) > distanceMinToStalemate or
        fabs(ballPos[1] - lastBallPosInStalemate.y) > distanceMinToStalemate) {
        ballInStalemateGameCycle = currentStep;
        lastBallPosInStalemate = core::math::Vector2<double>(ballPos[0], ballPos[1]);
        //std::cout << "Entrou no if" << std::endl;
    }
    if (currentStep - ballInStalemateGameCycle > frameInStaleMate) {
        ballInStalemateGameCycle = currentStep;
        isFoul = true;
    }

    return isFoul;
}

bool Judge::checkIfFreeKick() {
    // TODO: complete this funciton
    return false;
}

bool Judge::checkIfGoalKick() {
    int allyPlayersInOpponentGoalieArea = 0;
    int opponentPlayersInAllyGoalieArea = 0;
    double currentStep = currentFrame;
    bool isFoul = false;
    const dReal *ballPos = this->ball.getPosition();
    const dReal *robotPos = nullptr;

    if (ballPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X   and
        ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
        ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y){

        for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
            robotPos = this->rightTeam.getRobotPosition(i);
            if (robotPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X and
                robotPos[1] < representations::Field::GOALIE_AREA_TOP_Y  and
                robotPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y) {
                opponentPlayersInAllyGoalieArea++;
            }
        }

    }

    if(ballPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X  and
       ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
       ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y) {

        for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
            robotPos = this->leftTeam.getRobotPosition(i);
            if (robotPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X and
                robotPos[1] < representations::Field::GOALIE_AREA_TOP_Y   and
                robotPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y) {
                allyPlayersInOpponentGoalieArea++;
            }
        }

    }

    if ((ballPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X   and
         ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
         ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y)   or
        (ballPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X  and
         ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
         ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y)) {
        if (currentStep - ballInStalemateGameCycle > frameInStaleMate) {
            ballInStalemateGameCycle = currentStep;
            isFoul = true;
        }
    }

    if (allyPlayersInOpponentGoalieArea > 1 || opponentPlayersInAllyGoalieArea > 1)
        isFoul = true;

    return isFoul;
}


bool Judge::checkIfPenaltyKick() {
    bool isFoul = false;
    int allyPlayersInAllyGoalieArea = 0;
    int opponentPlayersInOpponentGoalieArea = 0;
    double currentStep = currentFrame;
    const dReal *ballPos = this->ball.getPosition();
    const dReal *robotPos = nullptr;

    if (ballPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X   and
        ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
        ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y){
        for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
            robotPos = this->leftTeam.getRobotPosition(i);
            if (robotPos[0] < representations::Field::LEFT_GOALIE_AREA_FRONT_LINE_X and
                robotPos[1] < representations::Field::GOALIE_AREA_TOP_Y  and
                robotPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y) {
                allyPlayersInAllyGoalieArea++;
            }
        }
    }

    if(ballPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X  and
       ballPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y and
       ballPos[1] < representations::Field::GOALIE_AREA_TOP_Y) {
        for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
            robotPos = this->rightTeam.getRobotPosition(i);
            if (robotPos[0] > representations::Field::RIGHT_GOALIE_AREA_FRONT_LINE_X and
                robotPos[1] < representations::Field::GOALIE_AREA_TOP_Y   and
                robotPos[1] > representations::Field::GOALIE_AREA_BOTTOM_Y) {
                opponentPlayersInOpponentGoalieArea++;
            }
        }
    }

    if (allyPlayersInAllyGoalieArea > 1) {
        if (currentStep - penaltyKickStateGameCycle > frameToPenaltyKick) {
            if(!checkIfOpponentGoal())
                isFoul = true;
            penaltyKickStateGameCycle = currentStep;
        }
    } else
        penaltyKickStateGameCycle = currentStep;

    if (opponentPlayersInOpponentGoalieArea > 1) {
        if (currentStep - penaltyKickStateGameCycle > frameToPenaltyKick) {
            if(!checkIfAllyGoal())
                isFoul = true;
            penaltyKickStateGameCycle = currentStep;
        }
    } else
        penaltyKickStateGameCycle = currentStep;

    return isFoul;
}

} // judge
} // server
} // simulator
} // tools
