//
// Created by Thiago on 24/04/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_STATES_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_STATES_H

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

enum State {
    STARTING,
    PLAYING,
    GOAL,
    ENDING,
    FOUL_FREEBALL,
    FOUL_FREEKICK,
    FOUL_GOALKICK,
    FOUL_PENALTYKICK,
    UNDEF_STATE
};

} // gamestate
} // server
} // simulator
} // tools

#endif
