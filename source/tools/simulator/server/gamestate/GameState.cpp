//
// Created by Thiago on 16/07/18.
//

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

GameState::GameState(manager::GameManager &gm) :
           ball(gm.getBall()),
           leftTeam(gm.getLeftTeam()),
           rightTeam(gm.getRightTeam()),
           gm(gm) { }

GameState::~GameState() { }

} // gamestate
} // server
} // simulator
} // tools