//
// Created by Muzio on 17/03/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_JUDGE_JUDGE_H
#define TOOLS_SIMULATOR_SERVER_JUDGE_JUDGE_H

#include "core/representations/Field.h"
#include "physics/group/Team.h"
#include "physics/ball/Ball.h"
#include "core/representations/Ball.h"
#include "utils/Timer.h"

namespace tools {
namespace simulator {
namespace server {
namespace judge {

class Judge {
public:
    Judge(physics::Team &leftTeam,
          physics::Team &rightTeam,
          physics::Ball &ball);

    virtual ~Judge();

    bool getGameStarted() { return this->gameStarted; }
    bool getGameEnded()   { return this->gameEnded;   }
    bool getPlayAgain()   { return this->playAgain;   }

    bool getWasGoal() { return this->wasGoal; }
    bool getWasLeftGoal()  { return this->wasLeftGoal;  };
    bool getWasRightGoal() { return this->wasRightGoal; };

    bool getWasFoul()     { return this->wasFoul;     }
    bool getWasFreeBall() { return this->wasFreeBall; }
    bool getWasFreeKick() { return this->wasFreeKick; }
    bool getWasGoalKick() { return this->wasGoalKick; }
    bool getWasPenaltyKick() { return this->wasPenaltyKick; };

    bool update(); // returns true if a foul happened

private:
    bool gameStarted;
    bool gameEnded;
    bool playAgain;
    bool wasGoal;
    bool wasFoul;
    bool wasRightGoal;
    bool wasLeftGoal;
    physics::Team &leftTeam;
    physics::Team &rightTeam;
    physics::Ball &ball;

    bool checkIfGoal();
    bool checkIfAllyGoal();
    bool checkIfOpponentGoal();

    // For the fouls
    unsigned long int currentFrame;
    const double frameInStaleMate = 2200;

    core::math::Vector2<double> lastBallPosInStalemate;
    const double distanceMinToStalemate = 0.01;
    double ballInStalemateGameCycle;

    bool wasFreeBall;
    bool checkIfFreeBall();

    bool wasFreeKick;
    bool checkIfFreeKick();

    bool wasGoalKick;
    bool checkIfGoalKick();

    const double frameToGoalKick = 730;
    double goalKickStateGameCycle;

    // Penalty Kick Foul
    bool wasPenaltyKick;
    bool checkIfPenaltyKick();

    const double frameToPenaltyKick = 730;
    double penaltyKickStateGameCycle;
};

} // judge
} // server
} // simulator
} // tools

#endif
