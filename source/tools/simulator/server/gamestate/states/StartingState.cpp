//
// Created by Muzio on 07/04/15.
//

#include "gamestate/states/StartingState.h"

#include <iostream>

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

using namespace std;

using core::representations::Player;
using core::representations::Field;
using core::representations::Ball;

StartingState::StartingState(manager::GameManager &gm) :
               GameState(gm) { }

StartingState::~StartingState() { }

State StartingState::run() {
    //run simulation loop
    if (this->gm.getGameStarted())
        return PLAYING;

    return UNDEF_STATE;
}

void StartingState::initialPositions(double ballPosition[2],
                                     double leftPlayersPositions[Player::PLAYERS_PER_SIDE][3],
                                     double rightPlayersPositions[Player::PLAYERS_PER_SIDE][3]) {
    std::string nameFile,
                dummy;

    std::ifstream nameFileStartPositions,
                  startPositions;

    nameFileStartPositions.open("../configs/simulator_predefined_positions/lastModifiedFile.txt");
    nameFileStartPositions >> nameFile;
    nameFileStartPositions.close();

    startPositions.open("../configs/simulator_predefined_positions/" + nameFile);

    for (int i = 0; i < 2; i++)
        startPositions >> ballPosition[i];

    int leftPlayers, rightPlayers;
    startPositions >> leftPlayers;

    if(gm.isLeftPreference()) {
        for (int i = 0; i < leftPlayers; i++)
            for (int j = 0; j < 3; j++) {
                startPositions >> leftPlayersPositions[i][j];
               // cout << leftPlayersPositions[i][j] << " " << endl;
            }

        startPositions >> rightPlayers;

        for (int i = 0; i < rightPlayers; i++)
            for (int j = 0; j < 3; j++)
                startPositions >> rightPlayersPositions[i][j];
    }

    else if(this->gm.isRightPreference()) {
        double aux;

        for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
            startPositions >> aux;
            rightPlayersPositions[i][0] = -aux;

            for (int j = 1; j < 3; j++)
                startPositions >> rightPlayersPositions[i][j];
        }

        startPositions >> dummy;

        for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
            startPositions >> aux;
            leftPlayersPositions[i][0] = -aux;

            for (int j = 1; j < 3; j++)
                startPositions >> leftPlayersPositions[i][j];
        }
    }

    startPositions.close();
}

void StartingState::positionPlayersToStart() {
    double default_x = 1e6,
           default_y = Field::TOP_WALL_Y,
           default_z = Player::HEIGHT/2 + Player::DISTANCE_FROM_GROUND,
           default_angle = 0.0;

    //Putting them to the infinite
    for(int i = 0; i < Player::PLAYERS_PER_SIDE; i ++) {
        int y_offset = 2*(i+1);

        leftTeam.setRobotPosition(i, default_x,
                                     default_y + y_offset,
                                     default_z);
        leftTeam.setRobotRotation(i, default_angle);

        rightTeam.setRobotPosition(i, default_x,
                                      default_y + y_offset,
                                      default_z);
        rightTeam.setRobotRotation(i, default_angle);
    }

    double ballPosition[2],
           leftPlayersPositions[Player::PLAYERS_PER_SIDE][3],
           rightPlayersPositions[Player::PLAYERS_PER_SIDE][3];

    this->initialPositions(ballPosition,
                           leftPlayersPositions,
                           rightPlayersPositions);

//    cout << "left" << endl;
    for (int i = 0; i < this->gm.getNumberOfLeftPlayers(); i++) {
//        cout << leftPlayersPositions[i][0] << " " << leftPlayersPositions[i][1] << endl;
        leftTeam.setRobotPosition(i, leftPlayersPositions[i][0],  // x
                                     leftPlayersPositions[i][1],  // y
                                     default_z);                  // z
        leftTeam.setRobotRotation(i, leftPlayersPositions[i][2]);
    }

//    cout << "right" << endl;
    for (int i = 0; i < this->gm.getNumberOfRightPlayers(); i++) {
//        cout << rightPlayersPositions[i][0] << " " << rightPlayersPositions[i][1] << endl;
        rightTeam.setRobotPosition(i, rightPlayersPositions[i][0],  // x
                                      rightPlayersPositions[i][1],  // y
                                      default_z);                   // z
        rightTeam.setRobotRotation(i, rightPlayersPositions[i][2]);
    }
//    cout << endl;

    this->ball.setPosition(ballPosition[0],   // x
                           ballPosition[1],   // y
                           Ball::DIAMETER/2); // z
}

void StartingState::enterState() {
    this->positionPlayersToStart();

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < Player::PLAYERS_PER_SIDE; j++) {
            this->gm.getCommandInfo()[i].playersWheelSpeed[j].left = 0;
            this->gm.getCommandInfo()[i].playersWheelSpeed[j].right = 0;
        }
    }
}

} // gamestate
} // server
} // simulator
} // tools
