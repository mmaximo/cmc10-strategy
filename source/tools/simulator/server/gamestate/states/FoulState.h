//
// Created by Thiago on 17/07/18.
//

#ifndef ITANDROIDS_VSS_FOULSTATE_H
#define ITANDROIDS_VSS_FOULSTATE_H

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

using core::representations::Field;

class FoulState : public GameState {
public:
    FoulState(manager::GameManager &gm);
    virtual ~FoulState();
    virtual State run() override = 0;
    virtual void enterState() override = 0;

    bool isInAllyGoalieArea(const dReal *pos);
    bool isInOpponentGoalieArea(const dReal *pos);
};

} // gamestate
} // server
} // simulator
} // tools

#endif
