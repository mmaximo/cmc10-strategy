//
// Created by Muzio on 07/04/15.
//

#include "gamestate/states/EndingState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

EndingState::EndingState(manager::GameManager &gm) :
             GameState(gm) {

}

EndingState::~EndingState() { }

State EndingState::run() {
    if (this->gm.getPlayAgain())
        return STARTING;

    return UNDEF_STATE;
}

void EndingState::enterState() {

}

} // gamestate
} // server
} // simulator
} // tools
