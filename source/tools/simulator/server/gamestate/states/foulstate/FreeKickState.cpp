//
// Created by Igor Ribeiro on 06/08/16.
//

#include "gamestate/states/foulstate/FreeKickState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

FreeKickState::FreeKickState(manager::GameManager &gm) :
               FoulState(gm),
               gameStatistics(statistics::GameStatistics::getInstance())  { }

FreeKickState::~FreeKickState() {
    // TODO Auto-generated destructor stub
}

State FreeKickState::run() {
    //run simulation loop
    if (gm.getGameStarted())
        return PLAYING;

    return UNDEF_STATE;
}

void FreeKickState::enterState() {
    _whoseFoul = whoseFoul();

    if(this->_whoseFoul == Team::ALLY) {
        std::cout << "Left Foul: Free Kick" << std::endl;
        this->gameStatistics.addLeftFoul();
    } else {
        std::cout << "Right Foul: Free Kick" << std::endl;
        this->gameStatistics.addRightFoul();
    }

    positionPlayers();
}

void FreeKickState::positionPlayers() {

}

Team FreeKickState::whoseFoul() {
    return Team::ALLY;
}

} // foulstate
} // gamestate
} // server
} // simulator
} // tools
