//
// Created by Igor Ribeiro on 06/08/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_GOALKICKSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_GOALKICKSTATE_H

#include "gamestate/states/FoulState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using core::representations::Team;

class GoalKickState : public FoulState {
public:
    GoalKickState(manager::GameManager &gm);
    ~GoalKickState();
    State run() override;
    void enterState() override;

private:
    void positionPlayers();
    Team whoseFoul();

    Team _whoseFoul;
    statistics::GameStatistics &gameStatistics;
};

} // foulstate
} // gamestate
} // server
} // simulator
} // tools

#endif
