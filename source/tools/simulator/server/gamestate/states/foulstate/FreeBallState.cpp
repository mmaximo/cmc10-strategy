//
// Created by Igor Ribeiro on 06/08/16.
//

#include "gamestate/states/foulstate/FreeBallState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

FreeBallState::FreeBallState(manager::GameManager &gm) :
               FoulState(gm),
               gameStatistics(statistics::GameStatistics::getInstance()) { }

FreeBallState::~FreeBallState() { }

State FreeBallState::run() {
    //run simulation loop
    if (this->gm.getGameStarted())
        return PLAYING;

    return UNDEF_STATE;
}

void FreeBallState::enterState() {
    this->_whereFoul = this->whereFoul();
    this->positionPlayers();
}

void FreeBallState::positionPlayers() {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < Player::PLAYERS_PER_SIDE; j++) {
            gm.getCommandInfo()[i].playersWheelSpeed[j].left  = 0;
            gm.getCommandInfo()[i].playersWheelSpeed[j].right = 0;
        }
    }

    if (_whereFoul == 1) {
        this->ball.setPosition(FREE_BALL_POSITION_RIGHT_X,
                               FREE_BALL_POSITION_TOP_Y,
                               Ball::DIAMETER / 2);

        this->leftTeam.setRobotPosition(1, FREE_BALL_POSITION_RIGHT_X - FREE_BALL_DISTANCE_TO_BALL,
                                           FREE_BALL_POSITION_TOP_Y,
                                           Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        this->rightTeam.setRobotPosition(1, FREE_BALL_POSITION_RIGHT_X + FREE_BALL_DISTANCE_TO_BALL,
                                            FREE_BALL_POSITION_TOP_Y,
                                            Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(this->gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -.2,
                                               -.5,
                                               Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .2,
                                                -.5,
                                                Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

    } else if (_whereFoul == 2) {
        this->ball.setPosition(FREE_BALL_POSITION_LEFT_X,
                               FREE_BALL_POSITION_TOP_Y,
                               Ball::DIAMETER / 2);

        this->leftTeam.setRobotPosition(1, FREE_BALL_POSITION_LEFT_X - FREE_BALL_DISTANCE_TO_BALL,
                                           FREE_BALL_POSITION_TOP_Y,
                                           Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        this->rightTeam.setRobotPosition(1, FREE_BALL_POSITION_LEFT_X + FREE_BALL_DISTANCE_TO_BALL,
                                            FREE_BALL_POSITION_TOP_Y,
                                            Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -.2,
                                               -.5,
                                               Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .2,
                                                -.5,
                                                Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

    } else if (_whereFoul == 3) {
        this->ball.setPosition(FREE_BALL_POSITION_LEFT_X,
                               FREE_BALL_POSITION_BOTTOM_Y,
                               Ball::DIAMETER / 2);

        this->leftTeam.setRobotPosition(1, FREE_BALL_POSITION_LEFT_X - FREE_BALL_DISTANCE_TO_BALL,
                                           FREE_BALL_POSITION_BOTTOM_Y,
                                           Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        this->rightTeam.setRobotPosition(1, FREE_BALL_POSITION_LEFT_X + FREE_BALL_DISTANCE_TO_BALL,
                                            FREE_BALL_POSITION_BOTTOM_Y,
                                            Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -.2,
                                               .5,
                                               Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .2,
                                                .5,
                                                Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

    } else {
        this->ball.setPosition(FREE_BALL_POSITION_RIGHT_X,
                               FREE_BALL_POSITION_BOTTOM_Y,
                               Ball::DIAMETER / 2);

        this->leftTeam.setRobotPosition(1, FREE_BALL_POSITION_RIGHT_X - FREE_BALL_DISTANCE_TO_BALL,
                                           FREE_BALL_POSITION_BOTTOM_Y,
                                           Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        this->rightTeam.setRobotPosition(1, FREE_BALL_POSITION_RIGHT_X + FREE_BALL_DISTANCE_TO_BALL,
                                            FREE_BALL_POSITION_BOTTOM_Y,
                                            Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -.2,
                                               .5,
                                               Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .2,
                                                .5,
                                                Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);
    }

    if(gm.getNumberOfLeftPlayers() > 1)
        this->leftTeam.setRobotPosition(0, -Field::GOALIE_AREA_MIDDLE_X,
                                           0,
                                           Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

    if(gm.getNumberOfRightPlayers() > 1)
        this->rightTeam.setRobotPosition(0, Field::GOALIE_AREA_MIDDLE_X,
                                            0,
                                            Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND);

    for(int i = 0; i < Player::PLAYERS_PER_SIDE; i++)
        this->rightTeam.setRobotRotation(i, M_PI);
}

int FreeBallState::whereFoul() {
    const dReal *ballPos = this->ball.getPosition();

    if (ballPos[0] >= 0 && ballPos[1] >= 0) // top right
        return 1;

    else if (ballPos[0] <= 0 && ballPos[1] >= 0) // top left
        return 2;

    else if (ballPos[0] <= 0 && ballPos[1] <= 0) // bottom left
        return 3;

    return 4; // bottom right
}

} // foulstate
} // gamestate
} // server
} // simulator
} // tools
