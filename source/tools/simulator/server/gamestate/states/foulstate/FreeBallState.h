//
// Created by Igor Ribeiro on 06/08/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_FREEBALLSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_FREEBALLSTATE_H

#include "gamestate/states/FoulState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using core::representations::Field;
using core::representations::Player;
using core::representations::Ball;

class FreeBallState : public FoulState {
public:
    FreeBallState(manager::GameManager &gm);
    ~FreeBallState();
    State run() override;
    void enterState() override;

private:
    int whereFoul();
    void positionPlayers();

    int _whereFoul;
    statistics::GameStatistics &gameStatistics;

    const double FREE_BALL_POSITION_RIGHT_X = Field::RIGHT_WALL_X / 2;
    const double FREE_BALL_POSITION_LEFT_X = Field::LEFT_WALL_X / 2;
    const double FREE_BALL_POSITION_TOP_Y = 0.40;
    const double FREE_BALL_POSITION_BOTTOM_Y = -0.40;
    const double FREE_BALL_DISTANCE_TO_BALL = 0.20;
};

} // foulstate
} // gamestate
} // server
} // simulator
} // tools

#endif
