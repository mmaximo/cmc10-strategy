//
// Created by Igor Ribeiro on 06/08/16.
//

#include "gamestate/states/foulstate/GoalKickState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using core::representations::Player;

GoalKickState::GoalKickState(manager::GameManager &gm) :
               FoulState(gm),
               gameStatistics(statistics::GameStatistics::getInstance()) { }

GoalKickState::~GoalKickState() { }

State GoalKickState::run() {
    //run simulation loop
    if (gm.getGameStarted())
        return PLAYING;

    return UNDEF_STATE;
}

void GoalKickState::enterState() {
    _whoseFoul = whoseFoul();

    if(this->_whoseFoul == Team::ALLY) {
        std::cout << "Foul [LEFT]:  Goal Kick" << std::endl;
        this->gameStatistics.addLeftFoul();
    } else {
        std::cout << "Foul [RIGHT]: Goal Kick" << std::endl;
        this->gameStatistics.addRightFoul();
    }

    positionPlayers();
    std::cout << "Goal Kick: Players Positioned" << std::endl;
}

Team GoalKickState::whoseFoul() {
    int allyPlayersInOpponentGoalieArea = 0;
    int opponentPlayersInAllyGoalieArea = 0;
    const dReal *robotPos = nullptr;

    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        robotPos = this->leftTeam.getRobotPosition(i);
        if (this->isInOpponentGoalieArea(robotPos))
            allyPlayersInOpponentGoalieArea++;

        robotPos = this->rightTeam.getRobotPosition(i);
        if (this->isInAllyGoalieArea(robotPos))
            opponentPlayersInAllyGoalieArea++;
    }

    if (allyPlayersInOpponentGoalieArea > 1)
        return Team::OPPONENT;

    if (opponentPlayersInAllyGoalieArea > 1)
        return Team::ALLY;

    if (this->ball.getPosition()[0] > 0.0)
       return Team::OPPONENT;

    return Team::ALLY;
}

void GoalKickState::positionPlayers() {
    double default_height = Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < Player::PLAYERS_PER_SIDE; j++) {
            gm.getCommandInfo()[i].playersWheelSpeed[j].left = 0;
            gm.getCommandInfo()[i].playersWheelSpeed[j].right = 0;
        }
    }


    if (_whoseFoul == Team::ALLY) {
        this->ball.setPosition(Field::LEFT_GOALIE_AREA_FRONT_LINE_X + 0.015,
                               0,
                               0.05);

        this->leftTeam.setRobotPosition(0, -Field::GOALIE_AREA_MIDDLE_X,
                                           0,
                                           default_height);

        this->rightTeam.setRobotPosition(1, Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                            Field::GOAL_LENGTH_Y,
                                            default_height);

        if(gm.getNumberOfRightPlayers() > 1)
            this->rightTeam.setRobotPosition(0, Field::GOALIE_AREA_MIDDLE_X,
                                                0,
                                                default_height);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .4,
                                                -.5,
                                                default_height);

        if(gm.getNumberOfLeftPlayers() > 1)
            this->leftTeam.setRobotPosition(1, Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                               -Field::GOAL_LENGTH_Y,
                                               default_height);

        if(gm.getNumberOfRightPlayers() > 2)
            this->leftTeam.setRobotPosition(2, .2,
                                               .5,
                                               default_height);
    } else {
        this->ball.setPosition(Field::RIGHT_GOALIE_AREA_FRONT_LINE_X + 0.015,
                               0,
                               0.05);

        this->rightTeam.setRobotPosition(0, Field::GOALIE_AREA_MIDDLE_X,
                                            0,
                                            default_height);

         this->leftTeam.setRobotPosition(1, Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                            Field::GOAL_LENGTH_Y,
                                            default_height);

        if(gm.getNumberOfLeftPlayers() > 1)
            this->leftTeam.setRobotPosition(0, -Field::GOALIE_AREA_MIDDLE_X,
                                               0,
                                               default_height);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -.4,
                                               -.5,
                                               default_height);

        if(gm.getNumberOfRightPlayers() > 1)
            this->rightTeam.setRobotPosition(1, Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                                -Field::GOAL_LENGTH_Y,
                                                default_height);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, -.2,
                                                -.5,
                                                default_height);
    }

    for(int i = 0; i < Player::PLAYERS_PER_SIDE; i++)
        this->rightTeam.setRobotRotation(i, M_PI);
}

} // foulstate
} // gamestate
} // server
} // simulator
} // tools
