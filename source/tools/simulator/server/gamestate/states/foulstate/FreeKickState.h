//
// Created by Igor Ribeiro on 06/08/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_FREEKICKSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_FREEKICKSTATE_H

#include "gamestate/states/FoulState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using core::representations::Team;

class FreeKickState : public FoulState {
public:
    FreeKickState(manager::GameManager &gm);
    ~FreeKickState();
    State run() override;
    void enterState() override;

private:
    Team whoseFoul();
    void positionPlayers();

    Team _whoseFoul;
    statistics::GameStatistics &gameStatistics;

    const double FREE_KICK_POSITION_RIGHT_X = Field::RIGHT_WALL_X / 2;
    const double FREE_KICK_POSITION_LEFT_X = Field::LEFT_WALL_X / 2;
    const double FREE_KICK_POSITION_Y = 0;
};

} // foulstate
} // gamestate
} // server
} // simulator
} // tools

#endif
