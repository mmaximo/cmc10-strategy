//
// Created by Igor Ribeiro on 06/08/16.
//

#include "gamestate/states/foulstate/PenaltyKickState.h"

#include <iostream>

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using namespace std;
using core::representations::Player;
using core::representations::Ball;

PenaltyKickState::PenaltyKickState(manager::GameManager &gm) :
                  FoulState(gm),
                  gameStatistics(statistics::GameStatistics::getInstance()) { }

PenaltyKickState::~PenaltyKickState() { }

State PenaltyKickState::run() {
    //run simulation loop
    if (this->gm.getGameStarted())
        return PLAYING;

    return UNDEF_STATE;
}

void PenaltyKickState::enterState() {
    this->_whoseFoul = this->whoseFoul();

    if(this->_whoseFoul == Team::ALLY) {
        std::cout << "Left Foul: Penalty Kick" << std::endl;
        this->gameStatistics.addLeftFoul();
    } else {
        std::cout << "Right Foul: Penalty Kick" << std::endl;
        this->gameStatistics.addRightFoul();
    }

    this->positionPlayers();
    cout << "players positioned" << endl;
}

Team PenaltyKickState::whoseFoul() {
    int allyPlayersInAllyGoalieArea = 0;
    int opponentPlayersInOpponentGoalieArea = 0;
    const dReal *robotPos = nullptr;

    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        robotPos = this->leftTeam.getRobotPosition(i);
        if (this->isInAllyGoalieArea(robotPos))
            allyPlayersInAllyGoalieArea++;

        robotPos = this->rightTeam.getRobotPosition(i);
        if (this->isInOpponentGoalieArea(robotPos))
            opponentPlayersInOpponentGoalieArea++;
    }

    if (allyPlayersInAllyGoalieArea > 1)
        return Team::OPPONENT;
//    else if (opponentPlayersInOpponentGoalieArea > 1)
//        return Team::ALLY;

    return Team::ALLY;
}

void PenaltyKickState::positionPlayers() {
    double default_z = Player::HEIGHT / 2 + Player::DISTANCE_FROM_GROUND;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < Player::PLAYERS_PER_SIDE; j++) {
            this->gm.getCommandInfo()[i].playersWheelSpeed[j].left = 0;
            this->gm.getCommandInfo()[i].playersWheelSpeed[j].right = 0;
        }
    }

    if (_whoseFoul == Team::ALLY) {
        this->ball.setPosition(PENALTY_POSITION_RIGHT_X,
                               PENALTY_POSITION_Y,
                               Ball::DIAMETER / 2);

        this->leftTeam.setRobotPosition(1, PENALTY_POSITION_RIGHT_X - PENALTY_DISTANCE_TO_BALL,
                                           PENALTY_POSITION_Y,
                                           default_z);

        this->rightTeam.setRobotPosition(0, Field::GOALIE_AREA_MIDDLE_X,
                                            0,
                                            default_z);

        if(gm.getNumberOfLeftPlayers() > 1)
            this->leftTeam.setRobotPosition(0, -Field::GOALIE_AREA_MIDDLE_X,
                                               0,
                                               default_z);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, -0.3,
                                               0,
                                               default_z);

        if(gm.getNumberOfRightPlayers() > 1)
            this->rightTeam.setRobotPosition(1, 0,
                                                -.5,
                                                default_z);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, 0,
                                                .5,
                                                default_z);

    } else {
        this->ball.setPosition(PENALTY_POSITION_LEFT_X,
                               PENALTY_POSITION_Y,
                               Ball::DIAMETER / 2);

        this->rightTeam.setRobotPosition(1, PENALTY_POSITION_LEFT_X + PENALTY_DISTANCE_TO_BALL,
                                            PENALTY_POSITION_Y,
                                            default_z);

        this->leftTeam.setRobotPosition(0, -Field::GOALIE_AREA_MIDDLE_X,
                                           0,
                                           default_z);

        if(gm.getNumberOfLeftPlayers() > 1)
            this->leftTeam.setRobotPosition(1, 0,
                                               .5,
                                               default_z);

        if(gm.getNumberOfLeftPlayers() > 2)
            this->leftTeam.setRobotPosition(2, 0,
                                               -.5,
                                               default_z);

        if(gm.getNumberOfRightPlayers() > 1)
            this->rightTeam.setRobotPosition(0, Field::GOALIE_AREA_MIDDLE_X,
                                                0,
                                                default_z);

        if(gm.getNumberOfRightPlayers() > 2)
            this->rightTeam.setRobotPosition(2, .3,
                                                0,
                                                default_z);

    }

    for(int i = 0; i < Player::PLAYERS_PER_SIDE; i++)
        this->rightTeam.setRobotRotation(i, M_PI);
}

} // foulstate
} // gamestate
} // server
} // simulator
} // tools
