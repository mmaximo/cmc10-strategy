//
// Created by Igor Ribeiro on 06/08/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_PENALTYKICKSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_FOULSTATE_PENALTYKICKSTATE_H

#include "gamestate/states/FoulState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {
namespace foulstate {

using core::representations::Team;

class PenaltyKickState : public FoulState {
public:
    PenaltyKickState(manager::GameManager &gm);
    ~PenaltyKickState();
    State run() override;
    void enterState() override;

private:
    Team whoseFoul();
    void positionPlayers();

    Team _whoseFoul;
    statistics::GameStatistics &gameStatistics;

    const double PENALTY_POSITION_RIGHT_X = Field::RIGHT_WALL_X / 2;
    const double PENALTY_POSITION_LEFT_X = Field::LEFT_WALL_X / 2;
    const double PENALTY_POSITION_Y = 0;
    const double PENALTY_DISTANCE_TO_BALL = 0.20;
};

} // foulstate
} // gamestate
} // server
} // simulator
} // tools

#endif
