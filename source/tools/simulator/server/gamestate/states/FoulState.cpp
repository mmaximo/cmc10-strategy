//
// Created by Thiago on 17/07/18.
//

#include "gamestate/states/FoulState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

FoulState::FoulState(manager::GameManager &gm) :
           GameState(gm) { }

FoulState::~FoulState() { }

bool FoulState::isInAllyGoalieArea(const dReal *pos) {
    return pos[0] < Field::LEFT_GOALIE_AREA_FRONT_LINE_X and
           pos[1] < Field::GOALIE_AREA_TOP_Y  and
           pos[1] > Field::GOALIE_AREA_BOTTOM_Y;
}

bool FoulState::isInOpponentGoalieArea(const dReal *pos) {
    return pos[0] > Field::RIGHT_GOALIE_AREA_FRONT_LINE_X and
           pos[1] < Field::GOALIE_AREA_TOP_Y   and
           pos[1] > Field::GOALIE_AREA_BOTTOM_Y;
}

} // gamestate
} // server
} // simulator
} // tools
