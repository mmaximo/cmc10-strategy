//
// Created by Muzio on 07/04/15.
//

#include "GoalState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

GoalState::GoalState(manager::GameManager &gm) :
           GameState(gm) {

}

GoalState::~GoalState() { }

State GoalState::run() {
    //run simulation loop
    if (this->gm.getGameStarted())
        return STARTING;

    return UNDEF_STATE;
}

void GoalState::enterState() {
    if (this->allyGoal())
        gm.addLeftScore();
    else
        gm.addRightScore();
}

bool GoalState::allyGoal() {
    return this->ball.getPosition()[0] >= core::representations::Field::FIELD_LENGTH_X / 2;
}

} // gamestate
} // server
} // simulator
} // tools
