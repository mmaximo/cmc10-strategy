//
// Created by Muzio on 07/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_GOALSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_GOALSTATE_H

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

class GoalState : public GameState {
public:
    GoalState(manager::GameManager &gm);
    ~GoalState();
    State run();
    void enterState();

private:
    bool allyGoal();
};

} // gamestate
} // server
} // simulator
} // tools

#endif
