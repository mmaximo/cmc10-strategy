//
// Created by Muzio on 07/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_ENDINGSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_ENDINGSTATE_H

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

class EndingState : public GameState {
public:
    EndingState(manager::GameManager &gm);
    ~EndingState();

    State run();
    void enterState();
};

} // gamestate
} // server
} // simulator
} // tools

#endif
