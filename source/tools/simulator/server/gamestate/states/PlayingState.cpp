//
// Created by Muzio on 07/04/15.
//

#include "PlayingState.h"

#include <iostream>

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

using namespace std;

PlayingState::PlayingState(manager::GameManager &gm) :
              GameState(gm) { }

PlayingState::~PlayingState() { }

State PlayingState::run() {
    //run simulation loop
    if (this->gm.getGameEnded())
        return ENDING;

    if (this->gm.getWasGoal())
        return GOAL;

    if (this->gm.getWasFreeBall())
        return FOUL_FREEBALL;

    if (this->gm.getWasFreeKick())
        return FOUL_FREEKICK;

    if (this->gm.getWasGoalKick())
        return FOUL_GOALKICK;

    if (this->gm.getWasPenaltyKick())
        return FOUL_PENALTYKICK;

    this->gm.simulationStep();

    return PLAYING;
}

void PlayingState::enterState() { }

} // gamestate
} // server
} // simulator
} // tools
