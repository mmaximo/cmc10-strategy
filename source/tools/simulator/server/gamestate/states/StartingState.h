//
// Created by Muzio on 07/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_STARTINGSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_STARTINGSTATE_H

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

class StartingState : public GameState {
public:
    StartingState(manager::GameManager &gm);
    ~StartingState();
    State run();
    void enterState();

private:
    void positionPlayersToStart();
    void initialPositions(double ballPosition[2],
                          double leftPlayersPositions[3][3],
                          double rightPlayersPositions[3][3]);
};

} // gamestate
} // server
} // simulator
} // tools

#endif
