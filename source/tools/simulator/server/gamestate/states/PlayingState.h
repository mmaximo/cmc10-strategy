//
// Created by Muzio on 07/04/15.
//

#ifndef TOOLS_SIMULATOR_SERVER_GAMESTATE_PLAYINGSTATE_H
#define TOOLS_SIMULATOR_SERVER_GAMESTATE_PLAYINGSTATE_H

#include "gamestate/GameState.h"

namespace tools {
namespace simulator {
namespace server {
namespace gamestate {

class PlayingState : public GameState {
public:
    PlayingState(manager::GameManager &gm);
    ~PlayingState();
    State run();
    void enterState();
};

} // gamestate
} // server
} // simulator
} // tools

#endif

