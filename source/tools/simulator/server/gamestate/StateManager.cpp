//
// Created by Thiago on 25/04/18.
//

#include "gamestate/StateManager.h"

namespace tools {
namespace simulator {
namespace server {
namespace manager {

StateManager::StateManager(GameManager &gameManager, gamestate::State firstState) :
        gameManager(gameManager) {
    this->reloadState = firstState;
    this->setState(firstState);
    this->loadState();
}

StateManager::~StateManager() { }

void StateManager::loadState() {
    this->state = (std::shared_ptr < gamestate::GameState >) this->getState(this->currentState);

    if(this->isValidState())
        this->state.get()->enterState();
    else
        this->currentState = this->reloadState;
}

void StateManager::runState() {
    if(this->isValidState())
        this->currentState = this->state->run();
}

void StateManager::setState(gamestate::State select) {
    this->currentState = select;
}

bool StateManager::isValidState() {
    return (this->state != nullptr);
}

gamestate::GameState *StateManager::getState(gamestate::State select) {
    if (select == gamestate::STARTING)
        return new gamestate::StartingState(this->gameManager);

    if (select == gamestate::PLAYING)
        return new gamestate::PlayingState(this->gameManager);

    if (select == gamestate::GOAL)
        return new gamestate::GoalState(this->gameManager);

    if (select == gamestate::ENDING)
        return new gamestate::EndingState(this->gameManager);

    if (select == gamestate::FOUL_FREEBALL)
        return new gamestate::foulstate::FreeBallState(this->gameManager);

    if (select == gamestate::FOUL_FREEKICK)
        return new gamestate::foulstate::FreeKickState(this->gameManager);

    if (select == gamestate::FOUL_GOALKICK)
        return new gamestate::foulstate::GoalKickState(this->gameManager);

    if (select == gamestate::FOUL_PENALTYKICK)
        return new gamestate::foulstate::PenaltyKickState(this->gameManager);

    return nullptr;
}

} // manager
} // server
} // simulator
} // tools
