//
// Created by Thiago on 25/04/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_MANAGER_STATEMANAGER_H
#define TOOLS_SIMULATOR_SERVER_MANAGER_STATEMANAGER_H

#include "gamestate/GameState.h"
#include "gamestate/States.h"
#include "gamestate/states/EndingState.h"
#include "gamestate/states/GoalState.h"
#include "gamestate/states/PlayingState.h"
#include "gamestate/states/StartingState.h"
#include "gamestate/states/foulstate/FreeBallState.h"
#include "gamestate/states/foulstate/FreeKickState.h"
#include "gamestate/states/foulstate/GoalKickState.h"
#include "gamestate/states/foulstate/PenaltyKickState.h"
#include "manager/GameManager.h"

namespace tools {
namespace simulator {
namespace server {
namespace manager {

class StateManager {
public:
    StateManager(GameManager &gameManager,
                 gamestate::State firstState = gamestate::STARTING);
    ~StateManager();

    bool isValidState();
    void loadState();
    void runState();
    void setState(gamestate::State select);
    gamestate::GameState *getState(gamestate::State select);

protected:
    std::shared_ptr<gamestate::GameState> state;
    GameManager &gameManager;
    gamestate::State currentState,
                     reloadState;
};

} // manager
} // server
} // simulator
} // tools

#endif
