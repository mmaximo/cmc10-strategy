//
// Created by igorribeiro on 9/17/16.
//

#include "utils/Timer.h"

namespace tools {
namespace simulator {
namespace server {
namespace utils {

bool Timer::seed_is_set = false;
double Timer::tic = 0.0;
double Timer::top = 0.0;

Timer::Timer() { }

Timer::~Timer() { }

void Timer::setSeed() {
    if(!Timer::seed_is_set) {
        Timer::seed_is_set = true;
        srand(time(NULL));
    }
}

double Timer::getCurrentTime() {
    struct timeval tv;
    struct timezone tz;

    gettimeofday(&tv, &tz);

    return (tv.tv_sec + tv.tv_usec / 1000000.0);
}

} // utils
} // server
} // simulator
} // tools
