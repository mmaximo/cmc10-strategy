//
// Created by Thiago in 22/05/18.
//

#ifndef TOOLS_SIMULATOR_SERVER_UTILS_SIMULATORUTILS_H
#define TOOLS_SIMULATOR_SERVER_UTILS_SIMULATORUTILS_H

#include <stdio.h>
#include "utils/Timer.h"

#define RANDOM_RANGE 1000000

namespace tools {
namespace simulator {
namespace server {
namespace utils {

enum SIDE {
    LEFT = 0,
    RIGHT
};

double getRandom();
void errorOut(const char *msg);

} // utils
} // server
} // simulator
} // tools

#endif
