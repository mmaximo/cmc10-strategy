//
// Created by igorribeiro on 9/17/16.
//

#ifndef TOOLS_SIMULATOR_SERVER_UTILS_TIMER_H
#define TOOLS_SIMULATOR_SERVER_UTILS_TIMER_H

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

namespace tools {
namespace simulator {
namespace server {
namespace utils {

class Timer {
public:
    Timer();
    virtual ~Timer();
    static double getCurrentTime();
    static void setSeed();

    static bool seed_is_set;
    static double tic,
                  top;
};

} // utils
} // server
} // simulator
} // tools

#endif
