//
// Created by Thiago on 24/05/18.
//

#include "utils/SimulatorUtils.h"

namespace tools {
namespace simulator {
namespace server {
namespace utils {

double getRandom() {
    Timer::setSeed();

    int random = rand() % RANDOM_RANGE;
    return (double) (((double) random)/RANDOM_RANGE);
}

void errorOut(const char *msg) {
    perror(msg);
    exit(1);
}

} // utils
} // server
} // simulator
} // tools
