//
// Created by Thiago on 22/02/18.
//

#include "colormapper/ColorMapper.hpp"

namespace tools {
namespace color_calibration {

ColorMapper *ColorMapper::instance = nullptr;

// Uses 'Singleton' Design Pattern
// https://sourcemaking.com/design_patterns/singleton
ColorMapper *ColorMapper::GetInstance() {
    if (!instance)
        instance = new ColorMapper();

    return instance;
}

ColorMapper::~ColorMapper() { }

// Maps (y, u, v) into a pre-defined color for corresponding index, if any
YUVpx ColorMapper::MapToColor(YUV y, YUV u, YUV v) {
    ColorIndex index = this->MapToIndex(y, u, v); // Finds corresponding index

    if (index != UNDEF && index <= NUMCOLORS) // if valid index, returns
        return indexToYUVpx(index);           // pre-defined color

    return YUVpx{y, u, v};
}

// Maps 'color' into a pre-defined color for corresponding index, if any
YUVpx ColorMapper::MapToColor(YUVpx color) {
    return this->MapToColor(color.y, color.u, color.v);
}

// Maps (y, u, v) into index, if any
ColorIndex ColorMapper::MapToIndex(YUV y, YUV u, YUV v) {
    return *(this->matrix + (1 << 14) * (y >> 1) + (1 << 7) * (u >> 1) + (v >> 1));
}

// Maps 'color' into index, if any
ColorIndex ColorMapper::MapToIndex(YUVpx color) {
    return this->MapToIndex(color.y, color.u, color.v);
}

// Sets LUT to default configuration
void ColorMapper::Clear() {
    this->ClearLUT();
}

// Sets 'index' subset of LUT to default configuration
void ColorMapper::ClearIndex() {
    ColorIndex index = StaticData::index;

    for (int i = 0; i < (1 << 7); i++)
        for (int j = 0; j < (1 << 7); j++)
            for (int k = 0; k < (1 << 7); k++) {
                if (*(this->matrix + (1 << 14) * i + (1 << 7) * j + k) == index)
                    *(this->matrix + (1 << 14) * i + (1 << 7) * j + k) = UNDEF;
            }

    //matrix[(2^14)*i + (2^7)*j + (2^0)*k] is equivalent to matrix[i][j][k]
}

// Sets LUT to default configuration
void ColorMapper::ClearLUT() {
    for (int i = 0; i < (1 << 7); i++)
        for (int j = 0; j < (1 << 7); j++)
            for (int k = 0; k < (1 << 7); k++)
                *(this->matrix + (1 << 14) * i + (1 << 7) * j + k) = UNDEF;

    //matrix[(2^14)*i + (2^7)*j + (2^0)*k] is equivalent to matrix[i][j][k]
}

// Loads LUT from file
void ColorMapper::LoadLUT() {
    FILE *file = fopen("../configs/yuvLUT.txt", "r");

    if (!file) {
        std::cout << "ERROR:\nCouldn't load LUT!!!\n" << std::endl;
        return;
    }

    for (int i = 0; i < (1 << 7); i++)
        for (int j = 0; j < (1 << 7); j++)
            for (int k = 0; k < (1 << 7); k++) {
                int read_items = fread(this->matrix + (i << 14) + (j << 7) + k, sizeof(ColorIndex), 1, file);

                if(read_items == 0) // if couldn't read data,
                    return;         // returns prematurely
            }

    fclose(file);
    emit LoadedLUT();
}

// Saves LUT to file
void ColorMapper::SaveLUT() {
    FILE *file = fopen("../configs/yuvLUT.txt", "w");
    core::modeling::ColorTable colorTable;

    if (!file) {
        std::cout << "ERROR:\nCouldn't save LUT!!!\n" << std::endl;
        return;
    }

    for (YUV y = 0; y < (1 << 7); y++)
        for (YUV u = 0; u < (1 << 7); u++)
            for (YUV v = 0; v < (1 << 7); v++) {
                colorTable.assignColor((y << 1), (u << 1), (v << 1),
                                       *(this->matrix + (y << 14) + (u << 7) + v));
            }

    fwrite(colorTable.getTable(), sizeof(ColorIndex), colorTable.getTableSize(), file);
    fclose(file);
}

// Maps (y, u, v) pixel into 'color' index
void ColorMapper::SetColor(YUV y, YUV u, YUV v, ColorIndex color) {
    *(this->matrix + (1 << 14) * (y >> 1) + (1 << 7) * (u >> 1) + (v >> 1)) = color;

    //matrix[(2^14)*i + (2^7)*j + (2^0)*k] is equivalent to matrix[i][j][k]
}

void ColorMapper::SetColor(YUVpx pixel, ColorIndex color) {
    this->SetColor(pixel.y, pixel.u, pixel.v, color);
}

ColorMapper::ColorMapper() {
    this->ClearLUT();
}

} // color_calibration
} // tools