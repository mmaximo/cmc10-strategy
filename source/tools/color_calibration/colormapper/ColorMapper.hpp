//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_COLORMAPPER_COLORMAPPER_HPP
#define ITANDROIDS_LIB_COLORMAPPER_COLORMAPPER_HPP

#include <QObject>

#include "core/modeling/vision/ColorTable.h" // from itandroids-lib
#include "core/modeling/vision/Image.h"      // from itandroids-lib

#include "support/Constants.h"
#include "support/Labels.h"
#include "support/StaticData.hpp"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

// Stores the Look-Up Table (LUT) and manages ColorIndex <-> YUVpx mappings
class ColorMapper : public QObject {
Q_OBJECT
public:
    static ColorMapper *GetInstance();
    ~ColorMapper();

    ColorIndex MapToIndex(YUV y, YUV u, YUV v);
    ColorIndex MapToIndex(YUVpx color);
    YUVpx MapToColor(YUV y, YUV u, YUV v);
    YUVpx MapToColor(YUVpx color);

    ColorIndex matrix[1 << 21];    // (2^7)*(2^7)*(2^7) = 2^21
    YUVrange range[NUMCOLORS + 1]; // TODO: remove this

public slots:
    void Clear();
    void ClearIndex();
    void ClearLUT();    // TODO: is this really needed?
    void LoadLUT();
    void SaveLUT();
    void SetColor(YUV y, YUV u, YUV v, ColorIndex color);
    void SetColor(YUVpx pixel, ColorIndex color);

private:
    ColorMapper();

    static ColorMapper *instance;

signals:
    void LoadedLUT();
};

} // color_calibration
} // tools

#endif
