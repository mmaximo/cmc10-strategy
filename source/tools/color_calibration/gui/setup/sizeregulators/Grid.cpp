//
// Created by Thiago on 22/02/18.
//

#include "gui/setup/sizeregulators/Grid.hpp"

namespace tools {
namespace color_calibration {

Grid::Grid(int min_size, int max_size, QWidget *parent) : Widget(parent) {
    this->load = new QLabel();
    this->self_hlayout = new QHBoxLayout();
    this->view = nullptr;

    this->min_size = min_size;
    this->max_size = max_size;
    this->pieces = 10;

    this->setToolTip("Use <MOUSE WHEEL> to regulate\n[SELECTION AREA SIZE] in VideoFrame");

    this->index = this->min_size / 2;
    this->image = cv::Mat(this->max_size, this->max_size, CV_8UC3);

    this->ManageLayout();
    this->Update();
}

Grid::~Grid() {
    delete this->load;
    delete this->view;
    delete this->self_hlayout;
}

void Grid::Clean() {
    unsigned char *frame = this->image.data;
    for (int x = 0; x < this->max_size; x++)
        for (int y = 0; y < this->max_size; y++) {
            int p = 3 * (y * this->max_size + x);

            frame[p + 0] = 0;
            frame[p + 1] = 0;
            frame[p + 2] = 0;
        }
}

void Grid::Load() {
    delete this->view;
    this->view = new QImage(this->image.data,
                            this->image.cols,
                            this->image.rows,
                            QImage::Format_RGB888);
}

void Grid::Show() {
    this->load->setPixmap(QPixmap::fromImage(*this->view));
}

void Grid::Update() {
    this->Gridfy(this->pieces);
    this->Load();
    this->Show();
}

void Grid::Gridfy(int step) {
    this->Clean();

    int side = this->max_size / step;

    int x_c = this->max_size / 2;
    int y_c = this->max_size / 2;

    int off_x = side / 2;
    int off_y = side / 2;

    bool alternate = false;

    RGBpx on = RGBpx{230, 230, 230};
    RGBpx off = RGBpx{170, 170, 170};
    RGBpx painted = RGBpx{0, 0, 0};

    for (int x = x_c - (step / 2) * side - off_x; x <= x_c - off_x + (1 + step / 2) * side; x += side) {
        for (int y = y_c - (step / 2) * side - off_y; y <= y_c - off_y + (1 + step / 2) * side; y += side) {
            if ((x - x_c) * (x - x_c) + (y - y_c) * (y - y_c) < this->index * this->index)
                this->Paint(y, x, side / 2, painted);
            else if (alternate)
                this->Paint(y, x, side / 2, on);
            else
                this->Paint(y, x, side / 2, off);

            alternate = !alternate;
        }
        alternate = !alternate;
    }
}

bool Grid::IsValid(int i, int j) {
    return i >= 0 && i < this->max_size && j >= 0 && j < this->max_size;
}

void Grid::ManageLayout() {
    this->self_hlayout->setMargin(0);
    this->self_hlayout->setSpacing(0);

    this->self_hlayout->addStretch(1);
    this->self_hlayout->addWidget(this->load);
    this->self_hlayout->addStretch(1);
    setLayout(this->self_hlayout);
}

void Grid::Paint(int i, int j, int delta, RGBpx color) {
    unsigned char *frame = this->image.data;

    // delta = delta - 2;

    for (int p = i - delta; p <= i + delta; p++)
        for (int q = j - delta; q <= j + delta; q++) {
            if (!this->IsValid(p, q))
                continue;

            int t = 3 * (p * this->max_size + q);

            frame[t + 0] = color.blue;
            frame[t + 1] = color.green;
            frame[t + 2] = color.red;
        }
}

void Grid::wheelEvent(QWheelEvent *ev) {
    int step = 1;

    if (ev->delta() > 0) {
        if (this->index + step < this->max_size / 2)
            this->index += step;
    } else if (this->index - step >= this->min_size / 2)
        this->index -= step;

    emit ChangedRadius(this->index, this->min_size / 2, this->max_size / 2);

    this->Update();
}

} // color_calibration
} // tools