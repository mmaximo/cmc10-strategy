//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SETUP_MODERN_GRID_HPP
#define ITANDROIDS_LIB_SETUP_MODERN_GRID_HPP

#include <QLabel>
#include <QWheelEvent>
#include <QWidget>

#include <opencv2/opencv.hpp>

#include "base/Widget.hpp"
#include "support/Labels.h"

namespace tools {
namespace color_calibration {

class Grid : public Widget {
Q_OBJECT
public:
    Grid(int min_size, int max_size, QWidget *parent = 0);
    ~Grid();
    void Clean();
    void Load();
    void Show();
    void Update();

    int index;

private:
    void Gridfy(int step);
    bool IsValid(int i, int j);
    void ManageLayout();
    void Paint(int i, int j, int delta, RGBpx color);
    void wheelEvent(QWheelEvent *ev);

    int min_size;
    int max_size;
    int pieces;
    cv::Mat image;
    QImage *view;
    QLabel *load;

signals:
    void ChangedRadius(int radius, int min_radius, int max_radius);
};

}
}

#endif
