//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SETUP_MODERN_BALL_HPP
#define ITANDROIDS_LIB_SETUP_MODERN_BALL_HPP

#include <QLabel>
#include <QWheelEvent>
#include <QWidget>

#include <opencv2/opencv.hpp>

#include "base/Widget.hpp"
#include "support/Constants.h"

namespace tools {
namespace color_calibration {

class Ball : public Widget {
Q_OBJECT
public:
    Ball(int min_size, int max_size, QWidget *parent = 0);
    ~Ball();
    void Clean();
    void Load();
    void Show();
    void Update();
    int index;

private:
    void Draw();
    void ManageLayout();
    void wheelEvent(QWheelEvent *ev);

    int max_size;
    int min_size;
    cv::Mat image;
    QImage *view;
    QLabel *load;

signals:
    void ChangedRadius(int radius, int min_radius, int max_radius);
};

} // color_calibration
} // tools

#endif
