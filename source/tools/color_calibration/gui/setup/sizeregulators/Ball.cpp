//
// Created by Thiago on 22/02/18.
//

#include "gui/setup/sizeregulators/Ball.hpp"

namespace tools {
namespace color_calibration {

Ball::Ball(int min_size, int max_size, QWidget *parent) : Widget(parent) {
    this->load = new QLabel();
    this->self_hlayout = new QHBoxLayout();
    this->view = nullptr;
    this->min_size = min_size;
    this->max_size = max_size;

    this->setToolTip("Use <MOUSE WHEEL> to regulate\n[PAINT AREA SIZE] in ColorBox");

    if (min_size < 0)
        this->min_size = 0;

    this->index = (this->min_size + this->max_size) / 4;
    this->image = cv::Mat(this->max_size, this->max_size, CV_8UC3);

    this->ManageLayout();
    this->Update();
}

Ball::~Ball() {
    delete this->load;
    delete this->view;
    delete this->self_hlayout;
}

void Ball::Clean() {
    unsigned char *frame = this->image.data;
    for (int x = 0; x < this->max_size; x++)
        for (int y = 0; y < this->max_size; y++) {
            int p = 3 * (y * this->max_size + x);

            frame[p + 0] = RGBDarkGrey.red;
            frame[p + 1] = RGBDarkGrey.green;
            frame[p + 2] = RGBDarkGrey.blue;
        }
}

void Ball::Load() {
    delete this->view;
    this->view = new QImage(this->image.data,
                            this->image.cols,
                            this->image.rows,
                            QImage::Format_RGB888);
}

void Ball::Show() {
    this->load->setPixmap(QPixmap::fromImage(*this->view));
}

void Ball::Update() {
    this->Draw();
    this->Load();
    this->Show();
}

void Ball::Draw() {
    this->Clean();
    unsigned char *frame = this->image.data;

    int x_c = this->max_size / 2;
    int y_c = this->max_size / 2;

    for (int x = x_c - this->index; x <= x_c + this->index; x++)
        for (int y = y_c - this->index; y <= y_c + this->index; y++)
            if ((x - x_c) * (x - x_c) + (y - y_c) * (y - y_c) < this->index * this->index) {
                int p = 3 * (y * this->max_size + x);

                frame[p + 0] = 255;
                frame[p + 1] = 255;
                frame[p + 2] = 255;
            }
}

void Ball::ManageLayout() {
    this->self_hlayout->setMargin(0);
    this->self_hlayout->setSpacing(0);

    this->self_hlayout->addStretch(1);
    this->self_hlayout->addWidget(this->load);
    this->self_hlayout->addStretch(1);
    setLayout(this->self_hlayout);
}

void Ball::wheelEvent(QWheelEvent *ev) {
    int step = 1;

    if (ev->delta() > 0) {
        if (this->index + step < this->max_size / 2)
            this->index += step;
    } else if (this->index - step >= this->min_size / 2)
        this->index -= step;

    emit ChangedRadius(this->index, this->min_size / 2, this->max_size / 2);

    this->Update();
}

} // color_calibration
} // tools