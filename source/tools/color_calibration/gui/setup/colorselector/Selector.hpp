//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_SETUP_SELECTOR_HPP
#define ITANDROIDS_LIB_GUI_SETUP_SELECTOR_HPP

#include <QFont>
#include <QRadioButton>
#include <QStyle>
#include <QtGui>
#include <QWidget>

#include "support/Constants.h"
#include "support/Labels.h"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

class Selector : public QRadioButton {
Q_OBJECT
public:
    Selector(qstrg label, int color, QWidget *parent = nullptr);
    ~Selector();
    void Check();

    int color;

public slots:
    void EmitColor();

private:
    void ManageConnection();
    void ManageInitialization();

signals:
    void clicked(int index);
};

}
}

#endif
