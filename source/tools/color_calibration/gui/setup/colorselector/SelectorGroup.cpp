//
// Created by Thiago on 22/02/18.
//

#include "gui/setup/colorselector/SelectorGroup.hpp"

namespace tools {
namespace color_calibration {
SelectorGroup::SelectorGroup(QWidget *parent) : Widget(parent) {
    this->pack_left = new QVBoxLayout();
    this->pack_mid = new QVBoxLayout();
    this->pack_right = new QVBoxLayout();
    this->self_hlayout = new QHBoxLayout();

    this->undefined = new Selector("No Color", UNDEF, this);

    this->green  = new Selector("Green",  NEWGREEN,  this);
    this->white  = new Selector("White",  NEWWHITE,  this);
    this->orange = new Selector("Orange", NEWORANGE, this);
    this->pink   = new Selector("Pink",   NEWPINK,   this);
    this->blue   = new Selector("Blue",   NEWBLUE,   this);
    this->yellow = new Selector("Yellow", NEWYELLOW, this);
    this->black  = new Selector("Black",  NEWBLACK,  this);
    this->red    = new Selector("Red",    NEWRED,    this);

    this->ManageLayout();
    this->ManageConnection();
    this->ManageInitialization();
}

SelectorGroup::~SelectorGroup() {
    delete this->pack_left;
    delete this->pack_mid;
    delete this->pack_right;

    delete this->undefined;
    delete this->green;
    delete this->white;
    delete this->orange;
    delete this->pink;
    delete this->blue;
    delete this->yellow;
    delete this->black;
    delete this->red;

    delete this->self_hlayout;
}

void SelectorGroup::SetIndex(int index) {
    StaticData::index = static_cast<ColorIndex>(index);
    emit this->UpdateIndex();
}

void SelectorGroup::ManageConnection() {
    connect(this->undefined, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->green, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->white, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->orange, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->pink, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->blue, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->yellow, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->black, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
    connect(this->red, SIGNAL(clicked(int)), this, SLOT(SetIndex(int)));
}

void SelectorGroup::ManageInitialization() {
    this->undefined->Check();
}

void SelectorGroup::ManageLayout() {
    this->self_hlayout->setMargin(0);
    this->self_hlayout->setSpacing(0);

    this->pack_left->addWidget(this->undefined);
    this->pack_left->addWidget(this->green);
    this->pack_left->addWidget(this->white);

    this->pack_mid->addWidget(this->orange);
    this->pack_mid->addWidget(this->pink);
    this->pack_mid->addWidget(this->blue);

    this->pack_right->addWidget(this->yellow);
    this->pack_right->addWidget(this->black);
    this->pack_right->addWidget(this->red);

    this->self_hlayout->addLayout(this->pack_left);
    this->self_hlayout->addLayout(this->pack_mid);
    this->self_hlayout->addLayout(this->pack_right);

    this->setLayout(this->self_hlayout);
}
}
}