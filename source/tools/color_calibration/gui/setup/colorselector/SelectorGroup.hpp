//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_SETUP_SELECTORGROUP_HPP
#define ITANDROIDS_LIB_GUI_SETUP_SELECTORGROUP_HPP

#include <QVBoxLayout>
#include <QWidget>

#include "base/Widget.hpp"
#include "gui/setup/colorselector/Selector.hpp"
#include "support/Constants.h"
#include "support/Labels.h"

namespace tools {
namespace color_calibration {

class SelectorGroup : public Widget {
Q_OBJECT
public:
    SelectorGroup(QWidget *parent = 0);
    ~SelectorGroup();

    Selector *undefined,
             *green,
             *white,
             *orange,
             *pink,
             *blue,
             *yellow,
             *black,
             *red;

public slots:
    void SetIndex(int index);

private:
    void ManageConnection();
    void ManageInitialization();
    void ManageLayout();

    QVBoxLayout *pack_left;
    QVBoxLayout *pack_mid;
    QVBoxLayout *pack_right;

signals:
    void UpdateIndex();
};

} // color_calibration
} // tools

#endif
