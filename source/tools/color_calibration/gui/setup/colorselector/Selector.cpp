//
// Created by Thiago on 22/02/18.
//

#include "gui/setup/colorselector/Selector.hpp"

namespace tools {
namespace color_calibration {

Selector::Selector(qstrg label, int color, QWidget *parent) : QRadioButton(label, parent) {
    this->color = color;
    this->ManageConnection();
    this->ManageInitialization();
}

Selector::~Selector() { }

void Selector::Check() {
    this->setChecked(true);
}

void Selector::EmitColor() {
    emit clicked(this->color);
}

void Selector::ManageConnection() {
    connect(this, SIGNAL(clicked(bool)), this, SLOT(EmitColor()));
}

void Selector::ManageInitialization() {
    qstrg font_color,
            unselected = rgbToQString(RGBGrey),
            selected;

    switch (this->color) {
        case UNDEF:
            font_color = rgbToQString(RGBWhite);
            selected = rgbToQString(RGBWhite);
            break;

        case NEWGREEN:
            font_color = rgbToQString(RGBGreen);
            selected = rgbToQString(RGBGreen);
            break;

        case NEWWHITE:
            font_color = rgbToQString(RGBWhite);
            selected = rgbToQString(RGBWhite);
            break;

        case NEWORANGE:
            font_color = rgbToQString(RGBOrange);
            selected = rgbToQString(RGBOrange);
            break;

        case NEWPINK:
            font_color = rgbToQString(RGBPink);
            selected = rgbToQString(RGBPink);
            break;

        case NEWBLUE:
            font_color = rgbToQString(RGBBlue);
            selected = rgbToQString(RGBBlue);
            break;

        case NEWYELLOW:
            font_color = rgbToQString(RGBYellow);
            selected = rgbToQString(RGBYellow);
            break;

        case NEWBLACK:
            font_color = rgbToQString(RGBBlack);
            selected = rgbToQString(RGBBlack);
            break;

        case NEWRED:
            font_color = rgbToQString(RGBRed);
            selected = rgbToQString(RGBRed);
            break;

        default:
            break;
    }

    this->setStyleSheet(WrapStyle("QRadioButton",
                                  Prop("font", "bold 20px")
                                  + Prop("color", "rgb(" + font_color + ")")
                                  + Prop("spacing", "5")
                                  + Prop("margin", "3") // outide tick
                                  + Prop("padding", "0") // inner tick
                                  + Prop("outline", "none")
                                  + Prop("width", "90px")
                                  + Prop("height", "20px")
                                  + Prop("background-color", "rgb(" + rgbToQString(RGBDarkGrey) + ")")
                        ) +

                        WrapStyle("QRadioButton::indicator",
                                  Prop("margin", "3")  // outide tick
                                  + Prop("padding", "0") // inner tick
                                  + Prop("outline", "none")
                                  + Prop("width", "23px")
                                  + Prop("height", "23px")
                                  + Prop("background-color", "rgba(" + unselected + ", 200)")
                        ) +

                        WrapStyle("QRadioButton::indicator:unchecked:hover",
                                  Prop("background-color", "rgb(" + rgbToQString(RGBLightGrey) + ")")) +

                        WrapStyle("QRadioButton::indicator:checked",
                                  Prop("background-color", "rgba(" + selected + ", 200)"))
    );
}

}
}