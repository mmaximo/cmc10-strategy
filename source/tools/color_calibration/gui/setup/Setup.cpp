//
// Created by Thiago on 22/02/18.
//

#include "gui/setup/Setup.hpp"

namespace tools {
namespace color_calibration {

SetUp::SetUp(QWidget *parent) : Widget(parent) {
    this->check_black_layout = new QVBoxLayout();
    this->tools_layout = new QHBoxLayout();
    this->group_button_layout = new QVBoxLayout();
    this->ball_grid_layout = new QVBoxLayout();
    this->self_vlayout = new QVBoxLayout();

    this->group = new SelectorGroup();
    this->save  = new QPushButton("Save");
    this->load  = new QPushButton("Load");
    this->clear = new QPushButton("Clear");
    this->clear_all = new QPushButton("Clear All");
    this->check = new QCheckBox("Hold");
    this->black = new QCheckBox("Black");
    this->ball  = new Ball(10, 72);
    this->grid  = new Grid(12, 72);

    this->ManageLayout();
    this->ManageConnection();
    this->ManageInitialization();
}

SetUp::~SetUp() {
    delete this->ball;
    delete this->check;
    delete this->clear;
    delete this->clear_all;
    delete this->group;
    delete this->load;
    delete this->save;

    delete this->grid;
    delete this->black;

    delete this->check_black_layout;
    delete this->group_button_layout;
    delete this->ball_grid_layout;

    delete this->tools_layout;
    delete this->self_vlayout;
}

void SetUp::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape)
        emit ClosedWindow();

    else if (QApplication::keyboardModifiers() && Qt::ControlModifier) {
        if (event->key() == Qt::Key_Z)
            emit this->UndoCommand();

        else if (event->key() == Qt::Key_X)
            emit this->RedoCommand();
    }

    QWidget::keyPressEvent(event);
}

void SetUp::ManageConnection() { }

void SetUp::ManageInitialization() {
    this->setWindowTitle(":: SetUp ::");
    this->Move(0.60, 0.45);

//    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_AlwaysShowToolTips);

    this->setStyleSheet(WrapStyle("QWidget",
                             Prop("background-color", "rgb(" + rgbToQString(RGBDarkGrey) + ")")
                                 ));

    qstrg button_style = WrapStyle("QPushButton",
                              Prop("color", "rgb(" + rgbToQString(RGBDarkGrey) + ")")
                            + Prop("font", "bold 18px")
                            + Prop("margin", "4")  // outide tick
                            + Prop("padding", "0") // inner tick
                            + Prop("outline", "none")
                            + Prop("border-style", "outset")
                            + Prop("border-width", "4px")
                            + Prop("border-radius", "10px")
                            + Prop("border-color", "rgb(" + rgbToQString(RGBMidDarkGrey) + ")")
                            + Prop("width", "120px")
                            + Prop("height", "40px")
                            + Prop("background-color", "rgb(" + rgbToQString(RGBGrey) + ")")
                                  ) +

                         WrapStyle("QPushButton:hover",
                              Prop("color", "rgb(" + rgbToQString(RGBLightGrey) + ")")
                            + Prop("font", "bold 21px")
                            + Prop("border-color", "rgb(" + rgbToQString(RGBLightGrey) + ")")
                            + Prop("background-color", "rgb(" + rgbToQString(RGBWhite) + ")")
                                  ) +

                         WrapStyle("QPushButton:pressed",
                              Prop("color", "rgb(" + rgbToQString(RGBDarkGrey) + ")")
                            + Prop("font", "bold 19px")
                            + Prop("border-style", "inset")
                            + Prop("border-color", "rgb(" + rgbToQString(RGBLightGrey) + ")")
                            + Prop("background-color", "rgb(" + rgbToQString(RGBWhite) + ")")
                                  );

    this->save->setStyleSheet(button_style);
    this->save->setToolTip("Push to [SAVE] LUT");

    this->load->setStyleSheet(button_style);
    this->load->setToolTip("Push to [LOAD] LUT");

    this->clear->setStyleSheet(button_style);
    this->clear->setToolTip("Push to [CLEAR] actual color in ColorBox");

    this->clear_all->setStyleSheet(button_style);
    this->clear_all->setToolTip("Push to [CLEAR] all colors in ColorBox");

    qstrg check_style =  WrapStyle("QCheckBox",
                              Prop("font", "bold 18px")
                            + Prop("color", "rgb(" + rgbToQString(RGBGrey) + ")")
                            + Prop("spacing", "5")
                            + Prop("margin-left", "50%")  // outide tick
                            + Prop("margin-right", "50%") // outide tick
                            + Prop("padding", "0") // inner tick
                            + Prop("outline", "none")
                            + Prop("width", "60px")
                            + Prop("height", "40px")
                            + Prop("background-color", "rgb(" + rgbToQString(RGBDarkGrey) + ")")
                                  ) +

                         WrapStyle("QCheckBox:hover",
                              Prop("color", "rgb(" + rgbToQString(RGBWhite) + ")")
                            + Prop("font", "bold 35px")
                                  ) +

                         WrapStyle("QCheckBox::indicator",
                              Prop("margin", "2")  // outide tick
                            + Prop("padding", "0") // inner tick
                            + Prop("outline", "none")
                            + Prop("width", "23px")
                            + Prop("height", "23px")
                            + Prop("background-color", "rgb(" + rgbToQString(RGBMidDarkGrey) + ")")
                                  ) +

                         WrapStyle("QCheckBox::indicator:unchecked",
                              Prop("background-color",
                                   "rgb(" + rgbToQString(RGBMidDarkGrey) + ")")) +

                         WrapStyle("QCheckBox::indicator:unchecked:hover",
                              Prop("background-color", "rgb(" + rgbToQString(RGBGrey) + ")")) +

                         WrapStyle("QCheckBox::indicator:checked",
                              Prop("background-color", "rgb(" + rgbToQString(RGBWhite) + ")"))
                                  ;

    this->check->setStyleSheet(check_style);
    this->check->setToolTip("Select to [FREEZE] frame in VideoFrame");

    this->black->setStyleSheet(check_style);
    this->black->setToolTip("Select to [HIDE] unpainted pixels in VideoFrame");

    this->setFixedSize(450, 400);
}

void SetUp::ManageLayout() {


    this->check_black_layout->addWidget(this->check);
    this->check_black_layout->addWidget(this->black);

    this->ball_grid_layout->addStretch(1);
    this->ball_grid_layout->addWidget(this->ball);
    this->ball_grid_layout->addSpacing(10);
    this->ball_grid_layout->addWidget(this->grid);
    this->ball_grid_layout->addSpacing(10);
    this->ball_grid_layout->addLayout(this->check_black_layout);
    this->ball_grid_layout->addStretch(1);

    this->group_button_layout->addStretch(1);
    this->group_button_layout->addWidget(this->save, 0, Qt::AlignHCenter);
    this->group_button_layout->addStretch(1);
    this->group_button_layout->addWidget(this->load, 0, Qt::AlignHCenter);
    this->group_button_layout->addStretch(1);
    this->group_button_layout->addWidget(this->clear, 0, Qt::AlignHCenter);
    this->group_button_layout->addStretch(1);
    this->group_button_layout->addWidget(this->clear_all, 0, Qt::AlignHCenter);
    this->group_button_layout->addStretch(1);

    this->tools_layout->addStretch(1);
    this->tools_layout->addLayout(this->ball_grid_layout);
    this->tools_layout->addStretch(1);
    this->tools_layout->addLayout(this->group_button_layout);
    this->tools_layout->addStretch(2);

    this->self_vlayout->addStretch(1);
    this->self_vlayout->addWidget(this->group);
    this->self_vlayout->addSpacing(15);
    this->self_vlayout->addLayout(this->tools_layout);
    this->self_vlayout->addStretch(1);

    this->self_vlayout->setSpacing(0);
    this->self_vlayout->setMargin(15);

    this->setLayout(this->self_vlayout);
}

} // color_calibration
} // tools