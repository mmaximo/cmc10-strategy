//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_SETUP_SETUP_HPP
#define ITANDROIDS_LIB_GUI_SETUP_SETUP_HPP

#include <QCheckBox>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QPushButton>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

#include "base/Widget.hpp"
#include "gui/setup/colorselector/SelectorGroup.hpp"
#include "gui/setup/sizeregulators/Ball.hpp"
#include "gui/setup/sizeregulators/Grid.hpp"
#include "support/Labels.h"
#include "support/StaticData.hpp"

namespace tools {
namespace color_calibration {

class SetUp : public Widget {
Q_OBJECT
public:
    SetUp(QWidget *parent = nullptr);
    ~SetUp();

    Ball *ball;
    Grid *grid;
    QCheckBox *check;
    QCheckBox *black;
    QPushButton *clear;
    QPushButton *clear_all;
    QPushButton *load;
    QPushButton *save;
    SelectorGroup *group;

private:
    void keyPressEvent(QKeyEvent *event);
    void ManageConnection();
    void ManageInitialization();
    void ManageLayout();

    QHBoxLayout *tools_layout;
    QVBoxLayout *check_black_layout;
    QVBoxLayout *ball_grid_layout;
    QVBoxLayout *group_button_layout;

signals:
    void ClosedWindow();
    void RedoCommand();
    void UndoCommand();
};

} // color_calibration
} // tools
#endif
