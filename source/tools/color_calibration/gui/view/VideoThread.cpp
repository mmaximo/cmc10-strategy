//
// Created by Thiago on 22/02/18.
//

#include "gui/view/VideoThread.hpp"

namespace tools {
namespace color_calibration {

VideoThread::VideoThread(CamViewer *viewer, AbstractCamera *camera) {
    this->camera = camera;
    this->viewer = viewer;

    this->ManageInitialization();
    this->ManageConnection();
}

VideoThread::~VideoThread() {
    delete[] this->thresholded;
    delete[] this->threshold_marker;
}

bool VideoThread::IsValidDot(int i, int j) {
    return i >= 0 && i < this->height && j >= 0 && j < this->width;
}

void VideoThread::run() {
    cv::Mat buffer;

    this->camera->Open();
    this->viewer->zooming = true;

    while (this->is_capturing) {
        if (this->is_locking) {
            this->captured = this->camera->GetFrame();
            buffer = this->captured.clone();
        } else
            this->captured = buffer.clone();

        this->Threshold();
//        this->CalculateBlobs();
//        this->DrawBlobs();
        this->viewer->Load(&this->captured, this->x_scale, this->y_scale);
        this->viewer->Show();
    }

    this->camera->Close();
    this->viewer->Finish();
}

void VideoThread::Threshold() {
    ColorIndex thresholded_index;
    YUVpx thresholded_color,
          original_color;

    for (int i = 0; i < this->height; i++)
        for (int j = 0; j < this->width; j++) {
            original_color = this->GetColor(i, j);
            thresholded_color = this->mapper->MapToColor(original_color);

            int p = i * width + j;
            this->threshold_marker[p] = false;

            thresholded_index = this->mapper->MapToIndex(original_color);
            this->thresholded[p] = thresholded_index;

            if(this->is_black && thresholded_index == UNDEF)
                thresholded_color = YUVBlack;

            this->SetColor(i, j, thresholded_color);
        }
}

void VideoThread::ChangeLock(int state) {
    if (!state)
        this->is_locking = true;
    else
        this->is_locking = false;
}

void VideoThread::ChangeBlack(int state) {
    if(!state)
        this->is_black = false;
    else
        this->is_black = true;
}

void VideoThread::MapSelfColor(double x, double y) {
    int delta = this->viewer->radius; // radius around clicked region in frame
    int thresh = 70; // threshold for color to be mapped in yuv

    int av_y = 0;
    int av_u = 0;
    int av_v = 0;

    int i = dtoi(y * this->viewer->full_height);
    int j = dtoi(x * this->viewer->full_width);

    /* calculates average color of the region of radius 'delta' in frame */
    std::set<YUVpx> dots;
    for (int p = i - delta; p <= i + delta; p++)
        for (int q = j - delta; q <= j + delta; q++) {
            if ((p - i) * (p - i) + (q - j) * (q - j) >= delta * delta) { continue; }
            if (!this->IsValidDot(p, q)) { continue; }

            YUVpx yuv_dot = this->GetColor(p, q);

            std::pair<std::set<YUVpx>::iterator, bool> result = dots.insert(yuv_dot);

            if (result.second) {             // checks if element was inserted
                av_y += yuvToInt(yuv_dot.y); // http://www.cplusplus.com/reference/set/set/insert/
                av_u += yuvToInt(yuv_dot.u);
                av_v += yuvToInt(yuv_dot.v);
            }
        }

    if (dots.size() > 0) {
        double num = itod(dots.size());
        av_y = dtoi(av_y / num);
        av_u = dtoi(av_u / num);
        av_v = dtoi(av_v / num);
    } else { return; }

    YUVpx average = YUVpx{intToYUV(av_y),
                          intToYUV(av_u),
                          intToYUV(av_v)};

    // TODO: this responsibility should be of ColorBox
//            int restrain = 170; // threshold for tagging color
//            YUVpx actual = indexToYUVpx(StaticData::index);

    std::set<YUVpx>::iterator it;
    for (it = dots.begin(); it != dots.end(); it++) {
        YUVpx yuv_dot = *it;

        if (yuvDist(yuv_dot, average) > thresh * thresh) { continue; }
//                if (yuvDist(yuv_dot, actual) > restrain * restrain) { continue; }

        ColorIndex idx = this->mapper->MapToIndex(yuv_dot.y, yuv_dot.u, yuv_dot.v);
        if (StaticData::index != UNDEF || idx != UNDEF)
                emit this->Painted(yuv_dot);
    }

    emit this->DonePainting();
}

void VideoThread::CalculateBlobs() {
    Blob new_blob;
    this->blobs.clear();

    for(int i = 0; i < this->height; i++) {
        for(int j = 0; j < this->width; j++) {
            int p = i * width + j;
            if(this->threshold_marker[p] || this->thresholded[p] == UNDEF)
                continue;

            new_blob = this->GetBlob(i, j);
            if(new_blob.size < 100)
                continue;

            this->blobs.push_back(new_blob);
        }
    }
}

Blob VideoThread::GetBlob(int i, int j) {
    int p = i * this->width + j,
        size = 0,
        center_x = 0,
        center_y = 0;

    ColorIndex color = this->thresholded[p];
    std::queue < std::pair<int, int> > next;

    this->threshold_marker[p] = true;

    next.push(std::make_pair(i, j));
    while(!next.empty()) {
        std::pair<int, int> actual = next.front();
        next.pop();

        int k = actual.first,
            l = actual.second,
            m = k * this->width + l;

        size++;
        center_x += l;
        center_y += k;

        if(k - 1 >= 0 && this->thresholded[m - this->width] == color && !this->threshold_marker[m - this->width]) {
            this->threshold_marker[m - this->width] = true;
            next.push(std::make_pair(k-1, l));
        }

        if(k + 1 < this->height && this->thresholded[m + this->width] == color && !this->threshold_marker[m + this->width]) {
            this->threshold_marker[m + this->width] = true;
            next.push(std::make_pair(k+1, l));
        }

        if(l - 1 >= 0 && this->thresholded[m - 1] == color && !this->threshold_marker[m - 1]) {
            this->threshold_marker[m - 1] = true;
            next.push(std::make_pair(k, l-1));
        }

        if(l + 1 < this->width && this->thresholded[m + 1] == color && !this->threshold_marker[m + 1]) {
            this->threshold_marker[m + 1] = true;
            next.push(std::make_pair(k, l+1));
        }
    }

    return Blob { color, size, center_x/size, center_y/size };
}

void VideoThread::DrawBlobs() {
    std::vector <Blob> :: iterator it = this->blobs.begin();

    while(it != this->blobs.end()) {
        Blob blob = *it;
        DrawCircle(&this->captured, iDot{ blob.center_x, blob.center_y }, 5);
        it++;
    }
}

YUVpx VideoThread::GetColor(int i, int j) {
    YUV *frame = this->captured.data;
    int p = 3*(width * i + j);

    return YUVpx{frame[p + 0],
                 frame[p + 1],
                 frame[p + 2]};
}

void VideoThread::ManageConnection() {
    connect(this->viewer, SIGNAL(clicked(double, double)), this, SLOT(MapSelfColor(double, double)));
    connect(this->viewer, SIGNAL(Interrupt()), this, SLOT(Finish()));
}

void VideoThread::ManageInitialization() {
    this->mapper = ColorMapper::GetInstance();

    this->is_capturing = true;
    this->is_locking = true;
    this->is_black = false;
    this->width = this->camera->width;
    this->height = this->camera->height;
    this->thresholded = new ColorIndex[this->width * this->height];
    this->threshold_marker = new bool[this->width * this->height];

    this->x_scale = 0.90;
    this->y_scale = 0.90;

    this->x_pos = 0.05;
    this->y_pos = 0.05;

    this->viewer->setWindowTitle(":: VideoFrame ::");
    this->viewer->Move(this->x_pos, this->y_pos);
    this->viewer->SetTracking(true, true);
}

void VideoThread::SetColor(int i, int j, YUVpx color) {
    YUV *frame = this->captured.data;
    PaintPixel(frame, this->width, i, j, color.y, color.u, color.v);
}

void VideoThread::Finish() {
    this->is_capturing = false;
}

}
}
