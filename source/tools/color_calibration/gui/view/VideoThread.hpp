//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_VIEW_VIDEOTHREAD_HPP
#define ITANDROIDS_LIB_GUI_VIEW_VIDEOTHREAD_HPP

#include <set>
#include <utility>
#include <vector>
#include <queue>

#include <QThread>

#include <opencv2/opencv.hpp>

#include "core/modeling/vision/Image.h"

#include "colormapper/ColorMapper.hpp"
#include "gui/colorbox/ColorBox.hpp"
#include "gui/view/camera/base/AbstractCamera.hpp"
#include "gui/view/viewer/CamViewer.hpp"
#include "support/Constants.h"
#include "support/Labels.h"
#include "support/StaticData.hpp"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {
class VideoThread : public QThread {
Q_OBJECT
public:
    VideoThread(CamViewer *viewer, AbstractCamera *camera);
    ~VideoThread();
    bool IsValidDot(int i, int j);
    void run();
    void Threshold();

    bool is_capturing;
    bool is_locking;
    bool is_black;
    AbstractCamera *camera;
    CamViewer *viewer;
    ColorMapper *mapper;
    cv::Mat captured;
    ColorIndex *thresholded;
    bool *threshold_marker;
    std::vector <Blob> blobs;

public slots:
    void ChangeLock(int state);
    void ChangeBlack(int state);
    void MapSelfColor(double x, double y);

private:
    void CalculateBlobs();
    Blob GetBlob(int i, int j);
    void DrawBlobs();
    YUVpx GetColor(int i, int j);
    void ManageConnection();
    void ManageInitialization();
    void SetColor(int i, int j, YUVpx color);

    int height, width;
    double x_scale, y_scale, x_pos, y_pos;

private slots:
    void Finish();

signals:
    void Painted(YUVpx color);
    void DonePainting();
};
}
}

#endif
