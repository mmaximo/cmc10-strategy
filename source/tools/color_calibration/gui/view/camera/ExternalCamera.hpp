//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_VIEW_CAMERA_EXTERNALCAMERA_HPP
#define ITANDROIDS_LIB_GUI_VIEW_CAMERA_EXTERNALCAMERA_HPP

#include "core/communication/camera/PointGreyFrameGrabber.h"

#include "gui/view/camera/base/AbstractCamera.hpp"
#include "support/Constants.h"
#include "support/Labels.h"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

/* TODO: support built-in notebook camera *AND* vss external camera */
class ExternalCamera : public AbstractCamera {
public:
    ExternalCamera(bool automatic = false);
    ~ExternalCamera();

    bool Close();
    bool Exists();
    cv::Mat GetFrame();
    bool IsOn();
    bool Open();

private:
    core::communication::PointGreyFrameGrabber *grabber;
};

}
}

#endif
