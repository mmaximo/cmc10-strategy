//
// Created by Thiago on 29/04/18.
//

#include "gui/view/camera/base/AbstractCamera.hpp"

namespace tools {
namespace color_calibration {

AbstractCamera::AbstractCamera() {
    this->width  = 0;
    this->height = 0;
}

AbstractCamera::~AbstractCamera() { }

} // color_calibration
} // tools