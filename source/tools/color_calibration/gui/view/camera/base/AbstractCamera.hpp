//
// Created by Thiago on 29/04/18.
//

#ifndef ITANDROIDS_LIB_GUI_VIEW_CAMERA_ABSTRACTCAMERA_HPP
#define ITANDROIDS_LIB_GUI_VIEW_CAMERA_ABSTRACTCAMERA_HPP

#include <opencv2/opencv.hpp>

namespace tools {
namespace color_calibration {

class AbstractCamera {
public:
    AbstractCamera();
    virtual ~AbstractCamera();

    virtual bool Close() = 0;
    virtual bool Exists() = 0;
    virtual cv::Mat GetFrame() = 0;
    virtual bool IsOn() = 0;
    virtual bool Open() = 0;

    int height, width;

protected:
    cv::Mat default_image;
};

} // color_calibration
} // tools

#endif
