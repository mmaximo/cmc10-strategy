//
// Created by Thiago on 30/04/18.
//

#include "gui/view/camera/BuiltInCamera.hpp"

namespace tools {
namespace color_calibration {

BuiltInCamera::BuiltInCamera(bool automatic) {
    this->camera = new cv::VideoCapture(DEF_CAM_INDEX);

    this->default_image = cv::imread("../resources/images/field_4.png", CV_LOAD_IMAGE_COLOR);
    core::modeling::Image::bgrToYuv444(this->default_image.data, this->default_image.data,
                                               this->default_image.cols, this->default_image.rows);

    if (this->Exists()) {
        this->width  = this->camera->get(CV_CAP_PROP_FRAME_WIDTH);
        this->height = this->camera->get(CV_CAP_PROP_FRAME_HEIGHT);

        if (automatic)
            this->Open();

    } else {
        this->width = this->default_image.cols;
        this->height = this->default_image.rows;
    }
}

BuiltInCamera::~BuiltInCamera() {
    if(this->Exists())
        delete this->camera;
}

bool BuiltInCamera::Close() {
    if (this->Exists()) {
        this->camera->release();
        return true;
    }

    return false;
}

bool BuiltInCamera::Exists() {
    return this->camera != nullptr;
}

cv::Mat BuiltInCamera::GetFrame() {
    cv::Mat frame;

    if (this->Exists()) {
        this->camera->read(frame);
        core::modeling::Image::bgrToYuv444(frame.data, frame.data,
                                                   frame.cols, frame.rows);
    } else
        frame = default_image.clone();

    return frame;
}

bool BuiltInCamera::IsOn() {
    if (!this->Exists())
        return false;

    return this->camera->isOpened();
}

bool BuiltInCamera::Open() {
    if (this->Exists())
        return this->camera->open(DEF_CAM_INDEX);

    return false;
}

} // color_calibration
} // tools
