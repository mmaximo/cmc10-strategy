//
// Created by Thiago on 30/04/18.
//

#ifndef ITANDROIDS_LIB_GUI_VIEW_CAMERA_BUILTINCAMERA_HPP
#define ITANDROIDS_LIB_GUI_VIEW_CAMERA_BUILTINCAMERA_HPP

#include "core/communication/camera/PointGreyFrameGrabber.h"
#include "core/modeling/vision/Image.h"

#include "gui/view/camera/base/AbstractCamera.hpp"
#include "support/Tools.h"

#define DEF_CAM_INDEX 0

namespace tools {
namespace color_calibration {

class BuiltInCamera : public AbstractCamera {
public:
    BuiltInCamera(bool automatic = false);
    ~BuiltInCamera();

    bool Close();
    bool Exists();
    cv::Mat GetFrame();
    bool IsOn();
    bool Open();

private:
    cv::VideoCapture *camera;
};

}
}

#endif
