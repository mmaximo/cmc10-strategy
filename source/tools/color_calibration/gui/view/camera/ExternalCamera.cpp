//
// Created by Thiago on 22/02/18.
//

#include "gui/view/camera/ExternalCamera.hpp"

    #include <iostream>
    using namespace std;

namespace tools {
namespace color_calibration {

ExternalCamera::ExternalCamera(bool automatic) {
    this->default_image = cv::imread("../resources/images/field_4.png", CV_LOAD_IMAGE_COLOR);

    core::modeling::Image::bgrToYuv444(this->default_image.data, this->default_image.data,
                                               this->default_image.cols, this->default_image.rows);

    this->grabber = new core::communication::PointGreyFrameGrabber;

    if (this->Exists()) {
        this->grabber->setPixelFormat(core::modeling::YUV444);

        if (automatic)
            this->Open();

        this->width  = this->grabber->getImageWidth();
        this->height = this->grabber->getImageHeight();

    } else {
        this->width  = this->default_image.cols;
        this->height = this->default_image.rows;
    }
}

ExternalCamera::~ExternalCamera() {
    delete this->grabber;
}

bool ExternalCamera::Close() {
    if (this->Exists())
        return true;

    return false;
}

bool ExternalCamera::Exists() {
    return this->grabber->check();
}

cv::Mat ExternalCamera::GetFrame() {
    cv::Mat frame;

    if (this->Exists()) {
        YUV *yuv444frame = this->grabber->grabFrame();
        frame = cv::Mat(this->height, this->width, CV_8UC3, yuv444frame);
        UYVToYUV(frame.data, this->width, this->height);
    } else
        frame = default_image.clone();

    return frame;
}

bool ExternalCamera::IsOn() {
    if (!this->Exists())
        return false;

    return true;
}

bool ExternalCamera::Open() {
    if (this->Exists()) {
        this->grabber->startCapturing();
        return true;
    }

    return false;
}

} // color_calibration
} // tools
