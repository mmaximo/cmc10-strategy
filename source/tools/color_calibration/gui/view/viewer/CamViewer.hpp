//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_CAMVIEWER_HPP
#define ITANDROIDS_LIB_GUI_CAMVIEWER_HPP

#include <QImage>
#include <QLabel>
#include <QMouseEvent>
#include <QWidget>

#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>

#include "core/modeling/vision/Image.h"

#include "support/Constants.h"
#include "support/Labels.h"
#include "support/StaticData.hpp"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

class CamViewer : public QLabel {
Q_OBJECT
public:
    CamViewer(QWidget *parent = nullptr);
    ~CamViewer();
    void FinishRun();
    void Load(cv::Mat *frame);
    void Load(cv::Mat *frame, int width, int height);
    void Load(cv::Mat *frame, double fraction_w, double fraction_h);
    void Load(strg path_name);
    void Move(double factor_x, double factor_y);
    void Resize(int width, int height);
    void SetTracking(bool status, bool show = false);
    void Show();

    bool zooming;
    bool zoom_lock;
    dDot last_pos;
    double zoom_frac;
    int radius;
    int height, width;
    int full_width, full_height;

public slots:
    void UpdateRadius(int radius, int min_radius, int max_radius);
    void InterruptRun();
    void Finish();

private:
    void CalculatePosition();
    int dh(double factor = 1.0); // desktop height
    int dw(double factor = 1.0); // desktop width
    void ManageInitialization();
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void Zoom(cv::Mat *frame);
    void keyPressEvent(QKeyEvent *event);

    dDot top_left;
    int zoom_width,
        zoom_height;

    bool is_tracking;
    bool show_tracking;

    std::shared_ptr <QImage> view;

signals:
    void ClosedWindow();
    void clicked(double x, double y);
    void Interrupt();
    void moved(double x, double y);
    void UndoCommand();
    void RedoCommand();
};

} // color_calibration
} // tools

#endif
