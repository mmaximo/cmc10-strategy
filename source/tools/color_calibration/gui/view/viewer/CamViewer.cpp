//
// Created by Thiago on 22/02/18.
//

#include "gui/view/viewer/CamViewer.hpp"

    #include <iostream>
    using namespace std;

namespace tools {
namespace color_calibration {

CamViewer::CamViewer(QWidget *parent) : QLabel(parent) {
    this->ManageInitialization();
}

CamViewer::~CamViewer() { }

void CamViewer::Load(cv::Mat *frame) {
    core::modeling::Image::yuv444ToRgb(frame->data, frame->data,
                                               frame->cols, frame->rows);

    this->full_width = frame->cols;
    this->full_height = frame->rows;

    if (this->zooming)
        this->Zoom(frame);

    if (this->show_tracking) {
        iDot pos = {dtoi(this->last_pos.x * frame->cols),
                    dtoi(this->last_pos.y * frame->rows)};
        DrawDonut(frame, pos, dtoi(this->radius / this->zoom_frac));
    }

    this->view = (std::shared_ptr<QImage>) (new QImage(frame->data, frame->cols,
                                                       frame->rows, QImage::Format_RGB888));
}

// void CamViewer::Load(const QString &image_path, int width, int height) {
//     this->Load(image_path);
//     this->Resize(width, height);
// }

void CamViewer::Load(cv::Mat *frame, int width, int height) {
    this->Load(frame);
    this->Resize(width, height);
}

void CamViewer::Load(cv::Mat *frame, double fraction_w, double fraction_h) {
    this->Load(frame);
    this->Resize(this->dw(fraction_w), this->dh(fraction_h));
}

void CamViewer::Load(strg path_name) {
    this->view = (std::shared_ptr<QImage>) (new QImage(stringToQString(path_name)));

    this->full_width = this->view->width();
    this->full_height = this->view->height();
}

void CamViewer::Move(double factor_x, double factor_y) {
    this->move(this->dw(factor_x),
               this->dh(factor_y));
}

void CamViewer::Resize(int width, int height) {
    this->view = (std::shared_ptr<QImage>) (new QImage(this->view->scaled(width, height, Qt::KeepAspectRatio)));
}

void CamViewer::SetTracking(bool status, bool show) {
    this->is_tracking = status;
    this->setMouseTracking(status);

    if (!status)
        this->show_tracking = false;
    else
        this->show_tracking = show;
}

void CamViewer::Show() {
    this->height = this->view->height();
    this->width = this->view->width();
    this->setFixedSize(this->width, this->height);

    this->setPixmap(QPixmap::fromImage(*this->view));
}

void CamViewer::InterruptRun() {
    emit Interrupt();
}

void CamViewer::UpdateRadius(int radius, int min_radius, int max_radius) {
    this->radius = MIN_VIEW_RADIUS + dtoi(itod((radius - min_radius) * (MAX_VIEW_RADIUS - MIN_VIEW_RADIUS)) /
                                          (max_radius - min_radius));
}

void CamViewer::Finish() {
    emit ClosedWindow();
    this->close();
}

void CamViewer::CalculatePosition() {
    int w = this->full_width,
        h = this->full_height,
        zw, zh, cx, cy, tx, ty;

    this->zoom_width = zw = dtoi(this->zoom_frac * w);
    this->zoom_height = zh = dtoi(this->zoom_frac * h);

    cx = dtoi(zw * this->last_pos.x);
    cy = dtoi(zh * this->last_pos.y);

    this->top_left.x = tx = dtoi(w * this->last_pos.x) - cx;
    this->top_left.y = ty = dtoi(h * this->last_pos.y) - cy;

    if (tx < 0)
        this->top_left.x = 0;
    else if (tx + zw >= w) {
        int delta = (tx + zw) - w;
        this->top_left.x -= delta;
    }

    if (ty < 0)
        this->top_left.y = 0;
    else if (ty + zh >= h) {
        int delta = (ty + zh) - h;
        this->top_left.y -= delta;
    }
}

int CamViewer::dh(double factor) {
    return static_cast<int>(factor * StaticData::desktop_height);
}

int CamViewer::dw(double factor) {
    return static_cast<int>(factor * StaticData::desktop_width);
}

void CamViewer::ManageInitialization() {
    this->full_width = 0;
    this->full_height = 0;
    this->width = 0;
    this->height = 0;
    this->radius = MIN_VIEW_RADIUS;
    this->zooming = false;
    this->zoom_lock = false;
    this->zoom_frac = 1.0;

    this->last_pos = dDot{-1.0, -1.0};
    this->SetTracking(false);
}

void CamViewer::mouseMoveEvent(QMouseEvent *ev) {
    dDot pos = dDot{itod(ev->x()) / this->width,
                    itod(ev->y()) / this->height};
    emit this->moved(pos.x, pos.y);

    if (!this->is_tracking)
        return;

    this->last_pos = pos;
}

void CamViewer::mousePressEvent(QMouseEvent *ev) {
    dDot pos = dDot{itod(this->top_left.x + itod(this->zoom_width * ev->x()) / this->width) / this->full_width,
                    itod(this->top_left.y + itod(this->zoom_height * ev->y()) / this->height) / this->full_height};

    emit this->clicked(pos.x, pos.y);
}

void CamViewer::wheelEvent(QWheelEvent *ev) {
    double step = 0.2,
            max_zoom = 0.05;

    if (ev->delta() > 0) {
        if (this->zoom_frac - step > max_zoom) {
            this->zoom_frac -= step;
            this->zoom_lock = false;
        }
    } else if (this->zoom_frac + step <= 1.0) {
        this->zoom_frac += step;
        this->zoom_lock = false;
    }
}

void CamViewer::Zoom(cv::Mat *frame) {
    if (!this->zoom_lock) {
        this->zoom_lock = true;
        this->CalculatePosition();
    }

    cv::Rect roi(this->top_left.x, this->top_left.y,
                 this->zoom_width, this->zoom_height);

    *frame = (*frame)(roi);

    cv::resize(*frame, *frame, cv::Size(this->full_width, this->full_height));
}

void CamViewer::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape)
        this->InterruptRun();

    else if (QApplication::keyboardModifiers() && Qt::ControlModifier) {
        if (event->key() == Qt::Key_Z)
            emit this->UndoCommand();

        else if (event->key() == Qt::Key_X)
            emit this->RedoCommand();
    }

    QWidget::keyPressEvent(event);
}
}
}
