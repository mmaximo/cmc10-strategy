//
// Created by Thiago on 22/02/18.
//

#include "gui/colorbox/ColorBox.hpp"

namespace tools {
namespace color_calibration {
ColorBox::ColorBox(QWidget *parent) : Widget(parent) {
    this->viewer = new BoxViewer(parent);
    this->memory = new Memory(100);
    this->self_hlayout = new QHBoxLayout(parent);

    for (int y = 0; y < (1 << 7); y++)
        this->box[y] = new cv::Mat((1 << 7), (1 << 7), CV_8UC3);

    this->memory_enabled = false;
    this->painting = false;

    this->ManageLayout();
    this->ManageConnection();
    this->ManageInitialization();
}

ColorBox::~ColorBox() {
    delete this->viewer;
    delete this->memory;

    for (int y = 0; y < (1 << 7); y++) // 256/2
        delete this->box[y];

    delete this->self_hlayout;
}

cv::Mat *ColorBox::GetSlice() {
    return this->box[this->y_index];
}

void ColorBox::Clear() {
    for (int y = 0; y < (1 << 7); y++) {
        YUV *slice = this->box[y]->data;

        for (int u = 0; u < (1 << 7); u++)
            for (int v = 0; v < (1 << 7); v++)
                PaintPixel(slice, (1 << 7), u, v, intToYUV(y << 1),
                                                  intToYUV(u << 1),
                                                  intToYUV(v << 1));
    }

    this->memory->ResetAll();
}

void ColorBox::Load() {
    cv::Mat copy = this->GetSlice()->clone();

    this->viewer->Load(&copy, this->dw(this->x_scale), this->dw(this->y_scale));
    this->viewer->Show();
}

void ColorBox::ClearAndLoad() {
    this->Clear();
    this->Load();
}

void ColorBox::LoadFromLUT() {
    YUVpx color;
    ColorIndex idx;

    for (int y = 0; y < (1 << 7); y++) { // 256/2
        YUV *slice = this->box[y]->data;

        for (int u = 0; u < (1 << 7); u++)
            for (int v = 0; v < (1 << 7); v++) {
                idx = this->mapper->MapToIndex((y << 1), (u << 1), (v << 1));

                if (idx != UNDEF)
                    color = indexToYUVpx(idx + NUMCOLORS);
                else
                    color = YUVpx{intToYUV(y << 1),
                                  intToYUV(u << 1),
                                  intToYUV(v << 1)};

                PaintPixel(slice, (1 << 7), u, v, color.y, color.u, color.v);
            }
    }

    this->Load();
}

void ColorBox::PaintSphere(YUV y, YUV u, YUV v) {
    this->painting = true;

    int iy = yuvToInt(y),
        iu = yuvToInt(u),
        iv = yuvToInt(v);

    int r = this->radius;

    for (int p = iy - r; p <= iy + r; p++) {
        if (!isValidColor(p)) continue;

        for (int q = iu - r; q <= iu + r; q++) {
            if (!isValidColor(q)) continue;

            for (int t = iv - r; t <= iv + r; t++) {
                if (!isValidColor(t)) continue;

                if ((p - iy) * (p - iy) + (q - iu) * (q - iu) + (t - iv) * (t - iv) < r * r)
                    this->SetColor(intToYUV(p), intToYUV(q), intToYUV(t), StaticData::index);
            }
        }
    }

    this->painting = false;
}

void ColorBox::PaintSphere(YUVpx pixel) {
    this->PaintSphere(pixel.y, pixel.u, pixel.v);
}

void ColorBox::RedoCommand() {
    std::vector<Change> changes = this->memory->PopRedo();

    if (changes.size() == 0)
        return;

    std::vector<Change>::iterator it = changes.begin();

    this->memory_enabled = false;
    while (it != changes.end()) {
        Change aux = *it;
        this->SetColor(aux.color.y, aux.color.u, aux.color.v, aux.new_ci);
        it++;
    }
    this->memory_enabled = true;

    this->Update();
    this->Load();
}

void ColorBox::SetColor(YUV y, YUV u, YUV v, ColorIndex color) {
    YUVpx paint;

    if (color != UNDEF)
        paint = indexToYUVpx(color);
    else
        paint = YUVpx{y, u, v};

    YUV *slice = this->box[y >> 1]->data;

    if (this->memory_enabled) {
        ColorIndex present = this->mapper->MapToIndex(y, u, v);
        if (present != color)
            this->memory->InsertChange(Change{YUVpx{y, u, v}, present, color});
    }

    PaintPixel(slice, (1 << 7), yuvToInt(u >> 1), yuvToInt(v >> 1), paint.y, paint.u, paint.v);
    this->mapper->SetColor(y, u, v, color);
}

void ColorBox::SetColor(YUVpx pixel, ColorIndex color) {
    this->SetColor(pixel.y, pixel.u, pixel.v, color);
}

void ColorBox::Update() {
    YUV *slice = this->GetSlice()->data;

    for (int i = NEWGREEN; i <= NUMCOLORS; i++) {
        YUVrange range = this->mapper->range[i];
        YUVpx color = indexToYUVpx(i);

        YUV index = (this->y_index << 1);

        if (range.minY <= index && index <= range.maxY) {
            for (int u = yuvToInt(range.minU >> 1); u <= yuvToInt(range.maxU >> 1); u++)
                for (int v = yuvToInt(range.minV >> 1); v <= yuvToInt(range.maxV >> 1); v++)
                    PaintPixel(slice, (1 << 7), u, v, color.y, color.u, color.v);
        }
    }
}

// TODO: improve this communication
void ColorBox::UpdateRadius(int radius, int min_radius, int max_radius) {
    this->radius = MIN_BOX_RADIUS + dtoi(itod((radius - min_radius) * (MAX_BOX_RADIUS - MIN_BOX_RADIUS)) /
                                              (max_radius - min_radius));
}

void ColorBox::ManageConnection() {
    connect(this->viewer, SIGNAL(clicked(double, double)), this, SLOT(SetSelfColor(double, double)));
    connect(this->viewer, SIGNAL(clicked(double, double)), this, SLOT(MouseClick()));
    connect(this->viewer, SIGNAL(moved(double, double)),   this, SLOT(MouseMove(double, double)));
}

void ColorBox::ManageInitialization() {
    this->mapper = ColorMapper::GetInstance();

    this->x_scale = 0.35;
    this->y_scale = 0.35;

    this->x_pos = 0.65;
    this->y_pos = 0.05;

    this->setWindowTitle(":: ColorBox ::");
    this->Move(this->x_pos, this->y_pos);
    this->setFixedSize(this->dw(this->x_scale), this->dw(this->y_scale));

    this->radius = (MIN_BOX_RADIUS + MAX_BOX_RADIUS) / 2;
    this->viewer->SetTracking(true, true);

    this->is_clicked = false;
    this->change = false;
    this->y_index = 0;

    this->ClearAndLoad();
    this->memory_enabled = true;
}

void ColorBox::ManageLayout() {
    this->self_hlayout->setMargin(0);
    this->self_hlayout->setSpacing(0);

    this->self_hlayout->addWidget(this->viewer);
    this->setLayout(this->self_hlayout);
}

void ColorBox::mouseReleaseEvent(QMouseEvent *ev) {
    this->is_clicked = false;
    this->SavePresent();
}

void ColorBox::wheelEvent(QWheelEvent *ev) {
    int step = 1; // step between 'y' slices

    if (this->is_clicked)
        return;

    if (ev->delta() > 0) {
        if (this->y_index + step <= (1 << 7) - 1) {
            this->y_index += step;
            this->change = true;
        }
    } else if (this->y_index - step >= 0) {
        this->y_index -= step;
        this->change = true;
    }

    if (this->change) {
        this->Load();
        this->change = false;
    }
}

void ColorBox::UndoCommand() {
    std::vector<Change> changes = this->memory->PopPresent();

    if (changes.size() == 0)
        return;

    std::vector<Change>::iterator it = changes.begin();

    this->memory_enabled = false;
    while (it != changes.end()) {
        Change aux = *it;
        this->SetColor(aux.color.y, aux.color.u, aux.color.v, aux.old_ci);
        it++;
    }
    this->memory_enabled = true;
    this->memory->PopUndo();

    this->Update();
    this->Load();
}

void ColorBox::SavePresent() {
    while (this->painting) { }

    this->memory->SavePresent();
    this->memory->ResetRedo();
}

void ColorBox::MouseClick() {
    this->is_clicked = true;
}

void ColorBox::MouseMove(double x, double y) {
    if (this->is_clicked)
        this->SetSelfColor(x, y);
}

void ColorBox::SetSelfColor(double x, double y) {
    int i = dtoi(y * 256.0);
    int j = dtoi(x * 256.0);

    // TODO: unify position validation
    if (i < 0 || i > 255 || j < 0 || j > 255)
        return;

    this->PaintSphere(intToYUV(this->y_index << 1),
                      intToYUV(i),
                      intToYUV(j));

    this->Load();
}

void ColorBox::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape)
        emit ClosedWindow();

    else if (QApplication::keyboardModifiers() && Qt::ControlModifier) {
        if (event->key() == Qt::Key_Z)
            this->UndoCommand();
        else if (event->key() == Qt::Key_X)
            this->RedoCommand();
    }

    QWidget::keyPressEvent(event);
}

} // color_calibration
} // tools