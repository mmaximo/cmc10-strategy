//
// Created by Thiago on 22/03/18.
//

#include "gui/colorbox/memory/Memory.hpp"

namespace tools {
namespace color_calibration {

Memory::Memory(int max_depth) : QWidget(nullptr) {
    this->max_depth = max_depth;
}

Memory::~Memory() { }

void Memory::InsertChange(Change new_change) {
    this->collector.push_back(new_change);
}

void Memory::SavePresent() {
    if (this->collector.size() == 0)
        return;

    if (this->present.size() > 0) {
        this->undo.push_front(this->present);
        this->present.clear();

        while (this->undo.size() > this->max_depth)
            this->undo.pop_back();
    }

    this->present = this->collector;
    this->collector.clear();
}

/* TODO: Change to void */
std::vector<Change> Memory::PopUndo() {
    std::vector<Change> Pop = this->present;

    this->collector.clear();

    if (this->present.size() > 0) {
        this->redo.push_front(this->present);
        this->present.clear();
    }

    if (this->AvailableUndo()) {
        this->present = this->undo.front();
        this->undo.pop_front();
    }

    return Pop;
}

/* TODO: Change to void */
std::vector<Change> Memory::PopRedo() {
    std::vector<Change> Pop;

    if (!this->AvailableRedo())
        return Pop;

    this->collector.clear();
    Pop = this->redo.front();

    if (this->present.size() > 0) {
        this->undo.push_front(this->present);
        this->present.clear();
    }

    this->present = Pop;
    this->redo.pop_front();

    return Pop;
}

std::vector<Change> Memory::PopPresent() {
    return this->present;
}

bool Memory::AvailableUndo() {
    return this->undo.size() > 0;
}

bool Memory::AvailableRedo() {
    return this->redo.size() > 0;
}

void Memory::ResetUndo() {
    this->present.clear();

    while (this->AvailableUndo()) {
        this->undo.back().clear();
        this->undo.pop_back();
    }
}

void Memory::ResetRedo() {
    while (this->AvailableRedo()) {
        this->redo.back().clear();
        this->redo.pop_back();
    }
}

void Memory::ResetAll() {
    this->ResetUndo();
    this->ResetRedo();
}

} // color_calibration
} // tools

