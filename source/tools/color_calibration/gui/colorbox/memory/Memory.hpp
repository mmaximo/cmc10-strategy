//
// Created by Thiago on 22/03/18.
//

#ifndef ITANDROIDS_LIB_GUI_COLORBOX_MEMORY_MEMORY_HPP
#define ITANDROIDS_LIB_GUI_COLORBOX_MEMORY_MEMORY_HPP

#include <QMouseEvent>
#include <QWheelEvent>
#include <QWidget>

#include <deque>
#include <vector>

#include <opencv2/opencv.hpp>

#include "support/Labels.h"

namespace tools {
namespace color_calibration {

class Memory : public QWidget {
Q_OBJECT
public:
    Memory(int max_depth);
    ~Memory();
    void SavePresent();
    void InsertChange(Change new_change);
    bool AvailableUndo();
    bool AvailableRedo();
    std::vector<Change> PopUndo();
    std::vector<Change> PopRedo();
    std::vector<Change> PopPresent();
    void ResetUndo();
    void ResetRedo();
    void ResetAll();

private:
    std::vector<Change> collector;
    std::vector<Change> present;
    std::deque<std::vector<Change> > undo;
    std::deque<std::vector<Change> > redo;

    unsigned int max_depth;
};

} // color_calibration
} // tools

#endif
