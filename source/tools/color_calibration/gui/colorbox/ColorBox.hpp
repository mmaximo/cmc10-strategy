//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_COLORBOX_COLORBOX_HPP
#define ITANDROIDS_LIB_GUI_COLORBOX_COLORBOX_HPP

#include <QMouseEvent>
#include <QWheelEvent>
#include <QWidget>

#include <opencv2/opencv.hpp>

#include "base/Widget.hpp"
#include "colormapper/ColorMapper.hpp"
#include "gui/colorbox/viewer/BoxViewer.hpp"
#include "gui/colorbox/memory/Memory.hpp"
#include "support/Constants.h"
#include "support/Labels.h"
#include "support/StaticData.hpp"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

class ColorBox : public Widget {
Q_OBJECT
public:
    ColorBox(QWidget *parent = nullptr);
    ~ColorBox();
    void Clear();
    void Load();
    cv::Mat *GetSlice();
    void keyPressEvent(QKeyEvent *event);

public slots:
    void ClearAndLoad();
    void LoadFromLUT();
    void PaintSphere(YUV y, YUV u, YUV v);
    void PaintSphere(YUVpx pixel);
    void RedoCommand();
    void SetColor(YUV y, YUV u, YUV v, ColorIndex color);
    void SetColor(YUVpx pixel, ColorIndex color);
    void UndoCommand();
    void Update();
    void UpdateRadius(int radius, int min_radius, int max_radius);
    void SavePresent();

private:
    void ManageConnection();
    void ManageInitialization();
    void ManageLayout();
    void mouseReleaseEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);

    bool change;
    bool painting;
    bool is_clicked;
    bool memory_enabled;
    ColorIndex index;
    ColorMapper *mapper;
    double x_scale, y_scale;
    double x_pos, y_pos;
    int y_index;
    int radius;
    cv::Mat *box[1 << 7];
    BoxViewer *viewer;
    Memory *memory;

private slots:
    void MouseClick();
    void MouseMove(double x, double y);
    void SetSelfColor(double x, double y);

signals:
    void ClosedWindow();
};

} // color_calibration
} // tools

#endif
