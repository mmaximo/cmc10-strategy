//
// Created by Thiago on 22/02/18.
//

#include "gui/colorbox/viewer/BoxViewer.hpp"

namespace tools {
namespace color_calibration {

BoxViewer::BoxViewer(QWidget *parent) : QLabel(parent) {
    this->ManageInitialization();
}

BoxViewer::~BoxViewer() { }

void BoxViewer::Load(cv::Mat *frame) {
    core::modeling::Image::yuv444ToRgb(frame->data, frame->data,
                                               frame->cols, frame->rows);

//            if (this->show_tracking) {
//                iDot pos = {dtoi(this->last_pos.x * frame->cols),
//                            dtoi(this->last_pos.y * frame->rows)};
//                DrawCircle(frame, pos, this->radius);
//            }

    this->view = (std::shared_ptr<QImage>) (new QImage(frame->data, frame->cols,
                                                  frame->rows, QImage::Format_RGB888));

    this->UpdateDimensions();
}

void BoxViewer::Load(cv::Mat *frame, int width, int height) {
    this->Load(frame);
    this->Resize(width, height);
}

void BoxViewer::Load(cv::Mat *frame, double fraction_w, double fraction_h) {
    this->Load(frame);
    this->Resize(this->dw(fraction_w), this->dh(fraction_h));
}

void BoxViewer::Move(double factor_x, double factor_y) {
    this->move(this->dw(factor_x),
               this->dh(factor_y));
}

void BoxViewer::Resize(int width, int height) {
    this->view = (std::shared_ptr<QImage>) (new QImage(this->view->scaled(width, height, Qt::KeepAspectRatio)));
    this->UpdateDimensions();
}

void BoxViewer::SetTracking(bool status, bool show) {
    this->is_tracking = status;
    this->setMouseTracking(status);

    if (!status)
        this->show_tracking = false;
    else
        this->show_tracking = show;
}

void BoxViewer::Show() {
    this->setPixmap(QPixmap::fromImage(*this->view));
}

void BoxViewer::UpdateRadius(int radius, int min_radius, int max_radius) {
    this->radius = MIN_VIEW_RADIUS + dtoi(itod((radius - min_radius) * (MAX_VIEW_RADIUS - MIN_VIEW_RADIUS)) /
                                               (max_radius - min_radius));
}

int BoxViewer::dh(double factor) {
    return static_cast<int>(factor * StaticData::desktop_height);
}

int BoxViewer::dw(double factor) {
    return static_cast<int>(factor * StaticData::desktop_width);
}

void BoxViewer::ManageInitialization() {
    this->width = 0;
    this->height = 0;
    this->radius = MIN_VIEW_RADIUS;

    this->last_pos = dDot{-1.0, -1.0};
    this->SetTracking(false);
}

void BoxViewer::mouseMoveEvent(QMouseEvent *ev) {
    dDot pos = dDot{itod(ev->x()) / this->width,
                    itod(ev->y()) / this->height};
    emit this->moved(pos.x, pos.y);

    if (!this->is_tracking)
        return;

    this->last_pos = pos;
}

void BoxViewer::mousePressEvent(QMouseEvent *ev) {
    dDot pos = dDot{itod(ev->x()) / this->width,
                    itod(ev->y()) / this->height};

    emit this->clicked(pos.x, pos.y);
}

void BoxViewer::UpdateDimensions() {
    this->height = this->view->height();
    this->width = this->view->width();

    this->setFixedSize(this->width, this->height);
}

} // color_calibration
} // tools
