//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_GUI_COLORBOX_BOXVIEWER_HPP
#define ITANDROIDS_LIB_GUI_COLORBOX_BOXVIEWER_HPP

#include <QImage>
#include <QLabel>
#include <QMouseEvent>
#include <QWidget>

#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>

#include "core/modeling/vision/Image.h"

#include "support/Constants.h"
#include "support/Labels.h"
#include "support/StaticData.hpp"
#include "support/Tools.h"

namespace tools {
namespace color_calibration {

class BoxViewer : public QLabel {
Q_OBJECT
public:
    BoxViewer(QWidget *parent = nullptr);
    ~BoxViewer();
    void Load(cv::Mat *frame);
    void Load(cv::Mat *frame, int width, int height);
    void Load(cv::Mat *frame, double fraction_w, double fraction_h);
    void Move(double factor_x, double factor_y);
    void Resize(int width, int height);
    void SetTracking(bool status, bool show = false);
    void Show();

    dDot last_pos;
    int radius;
    int height, width;

public slots:
    void UpdateRadius(int radius, int min_radius, int max_radius);

private:
    int dh(double factor = 1.0); // desktop height
    int dw(double factor = 1.0); // desktop width
    void ManageInitialization();
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void UpdateDimensions();

    bool is_tracking;
    bool show_tracking;
    std::shared_ptr <QImage> view;

signals:
    void clicked(double x, double y);
    void moved(double x, double y);
};

} // color_calibration
} // tools

#endif
