//
// Created by Thiago on 22/02/18.
//

#include <QApplication>
#include <boost/shared_ptr.hpp>
#include "Master.hpp"

using namespace tools::color_calibration;

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

//    Master *master = new Master();
    std::shared_ptr <Master> master = (std::shared_ptr <Master>) (new Master());
    master->Show();

    return app.exec();
}
