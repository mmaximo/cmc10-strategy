//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_MASTER_HPP
#define ITANDROIDS_LIB_MASTER_HPP

#include <QWidget>

#include "colormapper/ColorMapper.hpp"
#include "gui/colorbox/ColorBox.hpp"
#include "gui/setup/Setup.hpp"

#include "gui/view/camera/base/AbstractCamera.hpp"
#include "gui/view/camera/ExternalCamera.hpp"
#include "gui/view/camera/BuiltInCamera.hpp"

#include "gui/view/viewer/CamViewer.hpp"
#include "gui/view/VideoThread.hpp"
#include "support/StaticData.hpp"

namespace tools {
namespace color_calibration {

class Master : public QWidget {
Q_OBJECT
public:
    Master();
    ~Master();
    void ManageConnection();
    void ManageInitialization();
    void Show();

    AbstractCamera *camera;
    ColorBox *box;
    ColorMapper *mapper;
    SetUp *setup;
    VideoThread *video;
    CamViewer *viewer;
};

} // color_calibration
} // tools

#endif