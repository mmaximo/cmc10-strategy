//
// Created by Thiago on 22/02/18.
//

#include "base/Widget.hpp"

#include <iostream>

namespace tools {
namespace color_calibration {

// self_hlayout or self_vlayout may be chosen as the derived class layout
Widget::Widget(QWidget *parent) : QWidget(parent) {
    this->self_hlayout = nullptr;
    this->self_vlayout = nullptr;
}

Widget::~Widget() { }

// Moves the widgets to the (relative) coordinates (factor_x, factor_y)
// with respect to desktop resolution
void Widget::Move(double factor_x, double factor_y) {
    this->move(this->dw(factor_x), this->dh(factor_y));
}

// Given relative vertical coordinate "factor", returns the real vertical
// coordinate with respect to desktop (0.00 is the top of screen, 1.00 is the bottom)
int Widget::dh(double factor) {
    return static_cast<int>(factor * StaticData::desktop_height);
}

// Given relative horizontal coordinate "factor", returns the real horizontal
// coordinate with respect to desktop (0.00 is the left of screen, 1.00 is the right)
int Widget::dw(double factor) {
    return static_cast<int>(factor * StaticData::desktop_width);
}

} // color_calibration
} // tools
