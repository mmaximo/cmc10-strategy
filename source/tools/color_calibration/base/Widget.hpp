//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_BASE_WIDGET_HPP
#define ITANDROIDS_LIB_BASE_WIDGET_HPP

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>

#include "support/StaticData.hpp"

namespace tools {
namespace color_calibration {

// Base class for GUI widgets, enables moving widgets in screen using
// relative coordinates. Relative coordinates are used to make methods
// "resolution independent"
class Widget : public QWidget {
Q_OBJECT
public:
    Widget(QWidget *parent = nullptr);
    virtual ~Widget();

    void Move(double factor_x, double factor_y);

protected:
    int dh(double factor = 1.0); // factor = y/height
    int dw(double factor = 1.0); // factor = x/width

    QHBoxLayout *self_hlayout;
    QVBoxLayout *self_vlayout;
    QWidget *parent_class;
};

} // color_calibration
} // tools

#endif
