//
// Created by Thiago on 22/02/18.
//

#include "Master.hpp"

namespace tools {
namespace color_calibration {

// TODO: check if there are race conditions

Master::Master() {
    StaticData::Initialize();

    this->mapper = ColorMapper::GetInstance();
    this->setup = new SetUp();
    this->box = new ColorBox();

    this->camera = new ExternalCamera();
                // new BuiltInCamera();
    this->viewer = new CamViewer();
    this->video = new VideoThread(this->viewer, this->camera);

    this->ManageConnection();
    this->ManageInitialization();
}

Master::~Master() {
    delete this->video;
    delete this->viewer;
    delete this->camera;
    delete this->box;
    delete this->setup;
    delete this->mapper;
}

void Master::ManageConnection() {
    connect(this->setup->check, SIGNAL(stateChanged(int)), this->video, SLOT(ChangeLock(int)));
    connect(this->setup->black, SIGNAL(stateChanged(int)), this->video, SLOT(ChangeBlack(int)));

    connect(this->setup->save,      SIGNAL(clicked(bool)), this->mapper, SLOT(SaveLUT()));
    connect(this->setup->load,      SIGNAL(clicked(bool)), this->mapper, SLOT(LoadLUT()));
    connect(this->setup->clear,     SIGNAL(clicked(bool)), this->mapper, SLOT(ClearIndex()));
    connect(this->setup->clear_all, SIGNAL(clicked(bool)), this->mapper, SLOT(Clear()));

    connect(this->setup->clear,     SIGNAL(clicked(bool)), this->box, SLOT(LoadFromLUT()));
    connect(this->setup->clear_all, SIGNAL(clicked(bool)), this->box, SLOT(ClearAndLoad()));

    connect(this->video, SIGNAL(Painted(YUVpx)), this->box, SLOT(PaintSphere(YUVpx)));
    connect(this->video, SIGNAL(DonePainting()), this->box, SLOT(SavePresent()));

    connect(this->mapper, SIGNAL(LoadedLUT()), this->box, SLOT(LoadFromLUT()));

    connect(this->setup->ball, SIGNAL(ChangedRadius(int, int, int)), this->box, SLOT(UpdateRadius(int, int, int)));
    connect(this->setup->grid, SIGNAL(ChangedRadius(int, int, int)), this->viewer, SLOT(UpdateRadius(int, int, int)));

    connect(this->video->viewer, SIGNAL(UndoCommand()), this->box, SLOT(UndoCommand()));
    connect(this->video->viewer, SIGNAL(RedoCommand()), this->box, SLOT(RedoCommand()));
    connect(this->setup, SIGNAL(UndoCommand()), this->box, SLOT(UndoCommand()));
    connect(this->setup, SIGNAL(RedoCommand()), this->box, SLOT(RedoCommand()));

    connect(this->viewer, SIGNAL(ClosedWindow()), this->box,    SLOT(close()));
    connect(this->viewer, SIGNAL(ClosedWindow()), this->setup,  SLOT(close()));
    connect(this->box,    SIGNAL(ClosedWindow()), this->viewer, SLOT(InterruptRun()));
    connect(this->setup,  SIGNAL(ClosedWindow()), this->viewer, SLOT(InterruptRun()));
}

void Master::ManageInitialization() {
    this->video->start();
}

void Master::Show() {
    this->viewer->show();
    this->setup->show();
    this->box->show();
}

} // color_calibration
} // tools
