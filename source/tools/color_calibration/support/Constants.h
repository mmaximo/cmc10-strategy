//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SUPPORT_CONSTANTS_H
#define ITANDROIDS_LIB_SUPPORT_CONSTANTS_H

#include "support/Labels.h"

namespace tools {
namespace color_calibration {
/* TODO: use itandroids_lib VSS_Colors */
const int UNDEF     = 0;
const int NEWGREEN  = 1;
const int NEWWHITE  = 2;
const int NEWORANGE = 3;
const int NEWPINK   = 4;
const int NEWBLUE   = 5;
const int NEWYELLOW = 6;
const int NEWBLACK  = 7;
const int NEWRED    = 8;
const int NUMCOLORS = 8;

const int OLDGREEN   = 9;
const int OLDWHITE  = 10;
const int OLDORANGE = 11;
const int OLDPINK   = 12;
const int OLDBLUE   = 13;
const int OLDYELLOW = 14;
const int OLDBLACK  = 15;
const int OLDRED    = 16;

#define DEFAULT_INDEX UNDEF

const int MIN_BOX_RADIUS  = 2;
const int MAX_BOX_RADIUS = 16;

const int MIN_VIEW_RADIUS = 2;
const int MAX_VIEW_RADIUS = 8;

#define RGBGreen  RGBpx {   0, 255,   0}
#define RGBWhite  RGBpx { 255, 255, 255}
#define RGBOrange RGBpx { 255, 128,   0}
#define RGBPink   RGBpx { 255,  51, 255}
#define RGBBlue   RGBpx {   0,   0, 255}
#define RGBYellow RGBpx { 255, 255,  51}
#define RGBBlack  RGBpx {   0,   0,   0}
#define RGBRed    RGBpx { 255,   0,   0}

#define RGBDarkGrey     RGBpx {  64,  64,  64}
#define RGBMidDarkGrey  RGBpx {  96,  96,  96}
#define RGBGrey         RGBpx { 128, 128, 128}
#define RGBMidLightGrey RGBpx { 160, 160, 160}
#define RGBLightGrey    RGBpx { 192, 192, 192}

#define RGBOldGreen  RGBpx {   0, 153,   0}
#define RGBOldWhite  RGBpx { 192, 192, 192}
#define RGBOldOrange RGBpx { 203, 101,   0}
#define RGBOldPink   RGBpx { 202,   0, 100}
#define RGBOldBlue   RGBpx {   0,   0, 152}
#define RGBOldYellow RGBpx { 151, 153,   0}
#define RGBOldBlack  RGBpx {  63,  63,  63}
#define RGBOldRed    RGBpx { 203,   0,   0}

#define YUVGreen  YUVpx { 149,  54,  34}
#define YUVWhite  YUVpx { 235, 128, 128}
#define YUVOrange YUVpx { 143,  53, 192}
#define YUVPink   YUVpx { 132, 186, 202}
#define YUVBlue   YUVpx {  40, 239, 110}
#define YUVYellow YUVpx { 215,  38, 142}
#define YUVBlack  YUVpx {  16, 128, 128}
#define YUVRed    YUVpx {  81,  90, 239}

#define YUVOldGreen  YUVpx {  89,  76,  63 }
#define YUVOldWhite  YUVpx { 192, 128, 128 }
#define YUVOldOrange YUVpx { 120,  59, 187 }
#define YUVOldPink   YUVpx {  72, 144, 221 }
#define YUVOldBlue   YUVpx {  17, 204, 115 }
#define YUVOldYellow YUVpx { 135,  51, 140 }
#define YUVOldBlack  YUVpx {  63, 128, 128 }
#define YUVOldRed    YUVpx {  60,  93, 230 }
}
}

#endif