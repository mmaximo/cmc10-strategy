//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SUPPORT_STATICDATA_HPP
#define ITANDROIDS_LIB_SUPPORT_STATICDATA_HPP

#include <QApplication>
#include <QDesktopWidget>

#include "support/Constants.h"
#include "support/Labels.h"

// TODO: change this class into a Singleton

namespace tools {
namespace color_calibration {
class StaticData {
public:
    static void Initialize();

    static int desktop_width;
    static int desktop_height;
    static ColorIndex index;

private:
    StaticData() {}
};
}
}

#endif
