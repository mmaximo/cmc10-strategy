//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SUPPORT_LABELS_H
#define ITANDROIDS_LIB_SUPPORT_LABELS_H

#include <QString>

namespace tools {
namespace color_calibration {
    typedef std::__cxx11::string strg;
    typedef QString qstrg;
    // ----- ----- ----- ----- -----
    typedef unsigned char ColorIndex;
    // ----- ----- ----- ----- -----
    typedef unsigned char RGB;

    typedef struct {
        RGB red, green, blue;
    } RGBpx; // RGB pixel
    // ----- ----- ----- ----- -----
    typedef unsigned char YUV;

    typedef struct {
        YUV y, u, v;
    } YUVpx; // YUV pixel

    typedef struct {
        YUV minY, maxY,
                minU, maxU,
                minV, maxV;
    } YUVrange;
    // ----- ----- ----- ----- -----
    typedef struct {
        double x, y;
    } dDot; // double Dot

    typedef struct {
        int x, y;
    } iDot; // integer Dot
    // ----- ----- ----- ----- -----
    typedef struct {
        YUVpx color;
        ColorIndex old_ci,
                   new_ci;
    } Change;
    // ----- ----- ----- ----- -----
    typedef struct {
        ColorIndex color;
        int size,
            center_x,
            center_y;
    } Blob;
}
}

#endif  // ITANDROIDS_LIB_SUPPORT_LABELS_H
