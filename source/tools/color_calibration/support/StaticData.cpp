//
// Created by Thiago on 22/02/18.
//

#include "support/StaticData.hpp"

namespace tools {
namespace color_calibration {

int StaticData::desktop_width = 0;
int StaticData::desktop_height = 0;
ColorIndex StaticData::index = DEFAULT_INDEX;

void StaticData::Initialize() {
    QDesktopWidget *desktop = QApplication::desktop();
    StaticData::desktop_width = desktop->width();
    StaticData::desktop_height = desktop->height();
}

} // color_calibration
} // tools