//
// Created by Thiago on 22/02/18.
//

#ifndef ITANDROIDS_LIB_SUPPORT_TOOLS_H
#define ITANDROIDS_LIB_SUPPORT_TOOLS_H

#include <opencv2/opencv.hpp>

#include "support/Constants.h"
#include "support/Labels.h"

// TODO: change this class name to "Utils"

namespace tools {
namespace color_calibration {
    strg  intToString(int N);
    qstrg intToQString(int N);
    int   stringToInt(const strg &S);
    qstrg stringToQString(const strg &S);
    int   qstringToInt(const qstrg &S);
    strg  qstringToString(const qstrg &S);
    char  intToChar(int N);
    int   charToInt(char c);
    strg  rgbToString(RGBpx color);

    // ----- ----- ----- ----- -----

    int dtoi(double x);
    double itod(int x);
    bool isValidColor(int c);
    RGB intToRGB(int N);
    YUV intToYUV(int N);
    RGBpx indexToRGBpx(ColorIndex index);
    YUVpx indexToYUVpx(ColorIndex index);
    RGB unityToRGB(double x);
    YUV unityToYUV(double x);
    RGBpx YUVToRGB(YUVpx color);
    int yuvToInt(YUV N);
    double yuvToUnity(YUV n);
    YUVpx RGBToYUV(RGBpx color);
    int rgbToInt(RGB N);
    double rgbToUnity(RGB n);
    void UYVToYUV(YUV *frame, int width, int height);

    // ----- ----- ----- ----- -----

    // double GetAspectRatio(cv::Mat &matrix);
    int absl(int x);
    bool operator<(const YUVpx &A, const YUVpx &B);
    int yuvDist(YUVpx A, YUVpx B);
    void Show(YUVpx color);

    // TODO: make a file for drawing tools
    bool IsValid(cv::Mat *matrix, iDot position);
    bool IsInside(iDot position, iDot center, int radius);
    bool IsOutside(iDot position, iDot center, int radius);
    void DrawDonut(cv::Mat *matrix, iDot position, int radius, int thick = 2);
    void DrawCircle(cv::Mat *matrix, iDot position, int radius);
    void PaintPixel(YUV *frame, int width, int i, int j, YUV y, YUV u, YUV v);

    qstrg rgbToQString(RGBpx color);
    qstrg operator+(const qstrg &A, const strg &B);
    qstrg operator+(const strg &A, const qstrg &B);
    qstrg WrapStyle(qstrg name, qstrg style);
    qstrg Prop(qstrg field, qstrg value);
}
}
#endif
