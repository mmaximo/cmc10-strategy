//
// Created by Thiago on 22/02/18.
//

#include "support/Tools.h"

namespace tools {
namespace color_calibration {

strg intToString(int N) {
    return std::to_string(N);
}

qstrg intToQString(int N) {
    return QString::number(N);
}

int stringToInt(const strg &S) {
    return std::stoi(S);
}

qstrg stringToQString(const strg &S) {
    return QString::fromStdString(S);
}

int qstringToInt(const qstrg &S) {
    return S.toInt();
}

strg qstringToString(const qstrg &S) {
    return S.toStdString();
}

char intToChar(int N) {
    return (N % 10) + '0';
}

int charToInt(char c) {
    return (c - '0') % 10;
}

strg rgbToString(RGBpx color) {
    return intToString(rgbToInt(color.red)) + ", " +
           intToString(rgbToInt(color.green)) + ", " +
           intToString(rgbToInt(color.blue));
}

// ----- ----- ----- ----- -----
int dtoi(double x) {
    return static_cast<int>(x);
}

double itod(int x) {
    return static_cast<double>(x);
}

bool isValidColor(int c) {
    return c >= 0 && c <= 255;
}

RGB intToRGB(int N) {
    return static_cast<RGB>(N);
}

YUV intToYUV(int N) {
    return static_cast<YUV>(N);
}

RGBpx indexToRGBpx(ColorIndex index) {
    switch (index) {
        case NEWGREEN:
            return RGBGreen;
        case NEWWHITE:
            return RGBWhite;
        case NEWORANGE:
            return RGBOrange;
        case NEWPINK:
            return RGBPink;
        case NEWBLUE:
            return RGBBlue;
        case NEWYELLOW:
            return RGBYellow;
        case NEWBLACK:
            return RGBBlack;
        case NEWRED:
            return RGBRed;
        default:
            break;
    }

    return RGBBlack;
}

YUVpx indexToYUVpx(ColorIndex index) {
    switch (index) {
        case NEWGREEN:
            return YUVGreen;
        case NEWWHITE:
            return YUVWhite;
        case NEWORANGE:
            return YUVOrange;
        case NEWPINK:
            return YUVPink;
        case NEWBLUE:
            return YUVBlue;
        case NEWYELLOW:
            return YUVYellow;
        case NEWBLACK:
            return YUVBlack;
        case NEWRED:
            return YUVRed;

        case OLDGREEN:
            return YUVOldGreen;
        case OLDWHITE:
            return YUVOldWhite;
        case OLDORANGE:
            return YUVOldOrange;
        case OLDPINK:
            return YUVOldPink;
        case OLDBLUE:
            return YUVOldBlue;
        case OLDYELLOW:
            return YUVOldYellow;
        case OLDBLACK:
            return YUVOldBlack;
        case OLDRED:
            return YUVOldRed;

        default:
            break;
    }

    return YUVBlack;
}

YUV unityToYUV(double x) {
    if (x < 0.0)
        x = 0.0;
    else if (x > 1.0)
        x = 1.0;

    return static_cast<YUV>(dtoi(255 * x));
}

RGB unityToRGB(double x) {
    if (x < 0.0)
        x = 0.0;
    else if (x > 1.0)
        x = 1.0;

    return static_cast<RGB>(dtoi(255 * x));
}

RGBpx YUVToRGB(YUVpx color) {
    double y = yuvToUnity(color.y);
    double u = yuvToUnity(color.u) - 0.5;
    double v = yuvToUnity(color.v) - 0.5;

    double r = y + 1.403 * v;
    double g = y - 0.344 * u - 0.714 * v;
    double b = y + 1.770 * u;

    return RGBpx{unityToRGB(r), unityToRGB(g), unityToRGB(b)};
}

int yuvToInt(YUV N) {
    return static_cast<int>(N);
}

double yuvToUnity(YUV n) {
    return n / 255.0;
}

// TODO: a functional YUV <--> RGB converter
YUVpx RGBToYUV(RGBpx color) {
    double r = rgbToUnity(color.red);
    double g = rgbToUnity(color.green);
    double b = rgbToUnity(color.blue);

    double y = 0.299 * r + 0.587 * g + 0.114 * b;
    double u = (b - y) * 0.565 + 0.5;
    double v = (r - y) * 0.713 + 0.5;

    return YUVpx{unityToYUV(y), unityToYUV(u), unityToYUV(v)};
}

int rgbToInt(RGB N) {
    return static_cast<int>(N);
}

double rgbToUnity(RGB n) {
    return n / 255.0;
}

void UYVToYUV(YUV *frame, int width, int height) {
    int p;
    YUV aux;

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            p = 3 * (i * width + j);

            aux = frame[p + 0];
            frame[p + 0] = frame[p + 1];
            frame[p + 1] = aux;
        }
    }
}

// ----- ----- ----- ----- -----

// double GetAspectRatio(cv::Mat &matrix) {
// 	return itod(matrix.cols)/matrix.rows;
// }

int absl(int x) {
    if (x > 0)
        return x;
    return (-1) * x;
}

bool operator<(const YUVpx &A, const YUVpx &B) {
    return A.y < B.y || A.u < B.u || A.v < B.v;
}

int yuvDist(YUVpx A, YUVpx B) {
    int dY = absl(yuvToInt(A.y) - yuvToInt(B.y));
    int dU = absl(yuvToInt(A.u) - yuvToInt(B.u));
    int dV = absl(yuvToInt(A.v) - yuvToInt(B.v));

    return dY * dY + dU * dU + dV * dV;
}

void Show(YUVpx color) {
    std::cout << "Y: " << yuvToInt(color.y) <<
               "; U: " << yuvToInt(color.u) <<
               "; V: " << yuvToInt(color.v) << std::endl;
}

bool IsValid(cv::Mat *matrix, iDot position) {
    return position.y >= 0 && position.y < matrix->rows &&
           position.x >= 0 && position.x < matrix->cols;
}

bool IsInside(iDot position, iDot center, int radius) {
    return (position.y - center.y) * (position.y - center.y) +
           (position.x - center.x) * (position.x - center.x) < radius * radius;
}

bool IsOutside(iDot position, iDot center, int radius) {
    return (position.y - center.y) * (position.y - center.y) +
           (position.x - center.x) * (position.x - center.x) > radius * radius;
}

void DrawDonut(cv::Mat *matrix, iDot position, int radius, int thick) {
    int width = matrix->cols;
    RGB *frame = matrix->data;

    for (int p = position.y - radius - thick; p <= position.y + radius + thick; p++)
        for (int q = position.x - radius - thick; q <= position.x + radius + thick; q++) {
            if (!IsValid(matrix, iDot{q, p})) continue;

            int k = 3 * (p * width + q);
            if (IsInside(iDot{q, p}, position, radius + thick) &&
                IsOutside(iDot{q, p}, position, radius - thick)) {
                frame[k + 0] = 255 - frame[k + 0];
                frame[k + 1] = 255 - frame[k + 1];
                frame[k + 2] = 255 - frame[k + 2];
            }
        }
}

void DrawCircle(cv::Mat *matrix, iDot position, int radius) {
    int width = matrix->cols;
    RGB *frame = matrix->data;

    for (int p = position.y - radius; p <= position.y + radius; p++)
        for (int q = position.x - radius; q <= position.x + radius; q++) {
            if (!IsValid(matrix, iDot{q, p})) continue;

            int k = 3 * (p * width + q);
            if (IsInside(iDot{q, p}, position, radius)) {
                frame[k + 0] = 255 - frame[k + 0];
                frame[k + 1] = 255 - frame[k + 1];
                frame[k + 2] = 255 - frame[k + 2];
            }
        }
}

void PaintPixel(YUV *frame, int width, int i, int j, YUV y, YUV u, YUV v) {
    int p = 3 * (width * i + j);

    frame[p + 0] = y;
    frame[p + 1] = u;
    frame[p + 2] = v;
}

qstrg rgbToQString(RGBpx color) {
    return intToQString(rgbToInt(color.red)) + ", " +
           intToQString(rgbToInt(color.green)) + ", " +
           intToQString(rgbToInt(color.blue));
}

qstrg operator+(const qstrg &A, const strg &B) {
    return A + stringToQString(B);
}

qstrg operator+(const strg &A, const qstrg &B) {
    return stringToQString(A) + B;
}

qstrg WrapStyle(qstrg name, qstrg style) {
    return name + " {" + style + " }";
}

qstrg Prop(qstrg field, qstrg value) {
    return field + ": " + value + ";";
}
}
}