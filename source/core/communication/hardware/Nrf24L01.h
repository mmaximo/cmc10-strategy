//
// Created by junior on 13/11/17.
//

#ifndef ITANDROIDS_VSS_NRF24L01_H
#define ITANDROIDS_VSS_NRF24L01_H

#include "communication/writers/SerialPortWriter.h"
//#include <QtSerialPort/QtSerialPort>

namespace core {
namespace communication {

constexpr uint8_t RF24_PA_MIN = 0;
constexpr uint8_t RF24_PA_LOW = 1;
constexpr uint8_t RF24_PA_HIGH = 2;
constexpr uint8_t RF24_PA_MAX = 3;
constexpr uint8_t RF24_250KBPS = 0;
constexpr uint8_t RF24_1MBPS = 1;
constexpr uint8_t RF24_2MBPS = 2;
constexpr uint8_t RF24_CRC_DISABLED = 8;
constexpr uint8_t RF24_CRC_8 = 8;
constexpr uint8_t RF24_CRC_16 = 16;

class Nrf24L01 {
private:
    SerialPortWriter uart;
    uint8_t payloadSize;
    bool dynamicPayload;


public:
    Nrf24L01();

    ~Nrf24L01();

    /**
     * Set RF communication channel
     *
     * @param channel Which RF channel to communicate on, 0-125
     */
    void setChannel(uint8_t channel);

    /**
     * Get RF communication channel
     *
     * @return The currently configured RF Channel
     */
    uint8_t getChannel();

    /**
     * Set Static Payload Size
     *
     * This implementation uses a pre-established fixed payload size for all
     * transmissions.  If this method is never called, the driver will always
     * transmit the maximum payload size (32 bytes), no matter how much
     * was sent to write().
     *
     * @param size The number of bytes in the payload
     */
    void setPayloadSize(uint8_t size);

    /**
     * Get Static Payload Size
     *
     * @see setPayloadSize()
     *
     * @return The number of bytes in the payload
     */
    uint8_t getPayloadSize();

    /**
     * Get Dynamic Payload Size
     *
     * For dynamic payloads, this pulls the size of the payload off
     * the chip
     *
     * @return Payload length of last-received dynamic payload
     */
    uint8_t getDynamicPayloadSize();

    /**
     * Enable custom payloads on the acknowledge packets
     *
     * Ack payloads are a handy way to return data back to senders without manually changing the radio modes on both
     * units.
     *
     * @note Ack payloads are dynamic payloads. This only works on pipes 0&1 by default. Call enableDynamicPayloads()
     * to enable on all pipes.
     */
    void enableAckPayload();

    /**
     * Enable dynamically-sized payloads
     *
     * This way you don't always have to send large packets just to send them once in a while.
     * This enables dynamic payloads on ALL pipes.
     */
    void enableDynamicPayloads();

    /**
     * Enable dynamic ACKs (single write multicast or unicast) for chosen messages
     *
     * @note To enable full multicast or per-pipe multicast, use setAutoAck()
     *
     * @warning This MUST be called prior to attempting single write NOACK calls
     * @code
     * radio.enableDynamicAck();
     * radio.write(&data,32,1);  // Sends a payload with no acknowledgement requested
     * radio.write(&data,32,0);  // Sends a payload using auto-retry/autoACK
     * @endcode
     */
    void enableDynamicAck();

    /**
     * Determine whether the hardware is an nRF24L01+ or not.
     *
     * @return true if the hardware is nRF24L01+ (or compatible) and false
     * if its not.
     */
    bool isPVariant();

    /**
     * Enable or disable auto-acknowledge packets
     *
     * This is enabled by default, so it's only needed if you want to turn
     * it off for some reason.
     *
     * @param enable Whether to enable (true) or disable (false) auto-acks
     */
    void setAutoAck(bool enable);

    /**
     * Enable or disable auto-acknowlede packets on a per pipeline basis.
     *
     * This feature is enabled by default, so it's only needed if you want to turn
     * it off/on for some reason on a per pipeline basis.
     *
     * @param pipe Which pipeline to modify
     * @param enable Whether to enable (true) or disable (false) auto-acks
     */
    void setAutoAck(uint8_t pipe, bool enable);

    /**
     * Set Power Amplifier (PA) level to one of four levels:
     * RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH and RF24_PA_MAX
     *
     * The power levels correspond to the following output levels respectively:
     * NRF24L01: -18dBm, -12dBm,-6dBM, and 0dBm
     *
     * SI24R1: -6dBm, 0dBm, 3dBM, and 7dBm.
     *
     * @param level Desired PA level.
     */
    void setPALevel(uint8_t level);

    /**
     * Fetches the current PA level.
     *
     * NRF24L01: -18dBm, -12dBm, -6dBm and 0dBm
     * SI24R1:   -6dBm, 0dBm, 3dBm, 7dBm
     *
     * @return Returns values 0 to 3 representing the PA Level.
     */
    uint8_t getPALevel();

    /**
     * Set the transmission data rate
     *
     * @warning setting RF24_250KBPS will fail for non-plus units
     *
     * @param speed RF24_250KBPS for 250kbs, RF24_1MBPS for 1Mbps, or RF24_2MBPS for 2Mbps
     * @return true if the change was successful
     */
    void setDataRate(uint8_t speed);

    /**
     * Fetches the transmission data rate
     *
     * @return Returns the hardware's currently configured data rate. The value
     * is one of 250kbs (RF24_250KBPS), 1Mbps (RF24_1MBPS, default), or 2Mbps (RF24_2MBPS).
     */
    uint8_t getDataRate();

    /**
     * Set the CRC length
     * <br>CRC checking cannot be disabled if auto-ack is enabled
     * @param length RF24_CRC_8 for 8-bit or RF24_CRC_16 for 16-bit
     */
    void setCRCLength(uint8_t length);

    /**
     * Get the CRC length
     * <br>CRC checking cannot be disabled if auto-ack is enabled
     * @return RF24_CRC_DISABLED if disabled or RF24_CRC_8 for 8-bit or RF24_CRC_16 for 16-bit
     */
    uint8_t getCRCLength();

    /**
     * Disable CRC validation
     *
     * @warning CRC cannot be disabled if auto-ack/ESB is enabled.
     */
    void disableCRC();

    /**
     * The radio will generate interrupt signals when a transmission is complete,
     * a transmission fails, or a payload is received. This allows users to mask
     * those interrupts to prevent them from generating a signal on the interrupt
     * pin. Interrupts are enabled on the radio chip by default.
     *
     * @code
     * 	Mask all interrupts except the receive interrupt:
     *
     *		radio.maskIRQ(1,1,0);
     * @endcode
     *
     * @param tx_ok  Mask transmission complete interrupts
     * @param tx_fail  Mask transmit failure interrupts
     * @param rx_ready Mask payload received interrupts
     */
    void maskIRQ(bool tx_ok, bool tx_fail, bool rx_ready);

    /**
     * Start listening on the pipes opened for reading.
     *
     * 1. Be sure to call openReadingPipe() first (@see openReadingPipe()).
     * 2. Do not call write() while in this mode, without first calling stopListening().
     * 3. Call available() to check for incoming traffic, and read() to get it.
     *
     * @code
     * Open reading pipe 1 using address "CELSO"
     *
     * uint8_t address[] = "CELSO";
     * radio.openReadingPipe(1, address);
     * radio.startListening();
     * @endcode
     */
    void startListening();

    /**
     * Stop listening for incoming messages, and switch to transmit mode.
     *
     * Do this before calling write().
     * @code
     * radio.stopListening();
     * radio.write(&data, sizeof(data));
     * @endcode
     */
    void stopListening();

    /**
     * Check whether there are bytes available to be read
     *
     * @code
     * if(radio.available())
     * {
     *   radio.read(&data, sizeof(data));
     * }
     * @endcode
     *
     * @return True if there is a payload available, false if none is
     */
    bool available();

    /**
     * Test whether there are bytes available to be read in the
     * FIFO buffers.
     *
     * @param[out] pipe_num Which pipe has the payload available
     *
     * @code
     * uint8_t pipeNum;
     * if(radio.available(&pipeNum)){
     *   radio.read(&data,sizeof(data));
     *   Serial.print("Got data on pipe");
     *   Serial.println(pipeNum);
     * }
     * @endcode
     * @return True if there is a payload available, false if none is
     */
    bool available(uint8_t *pipe_num);

    /**
     * Read the available payload
     *
     * The size of data read is the fixed payload size, see getPayloadSize()
     *
     * @note I specifically chose 'void*' as a data type to make it easier
     * for beginners to use.  No casting needed.
     *
     * @note No longer boolean. Use available to determine if packets are
     * available. Interrupt flags are now cleared during reads instead of
     * when calling available().
     *
     * @param *buffer Pointer to a buffer where the data should be written
     * @param len Maximum number of bytes to read into the buffer
     *
     * @code
     * if(radio.available()){
     *   radio.read(&data,sizeof(data));
     * }
     * @endcode
     * @return No return value. Use available().
     */
    void read(unsigned char *rx_data, uint8_t len);

    /**
     * Be sure to call openWritingPipe() first to set the destination
     * of where to write to.
     *
     * This blocks until the message is successfully acknowledged by
     * the receiver or the timeout/retransmit maxima are reached.  In
     * the current configuration, the max delay here is 60-70ms.
     *
     * The maximum size of data written is the fixed payload size, see
     * getPayloadSize().  However, you can write less, and the remainder
     * will just be filled with zeroes.
     *
     * TX/RX/RT interrupt flags will be cleared every time write is called
     *
     * @param buf Pointer to the data to be sent
     * @param len Number of bytes to be sent
     *
     * @code
     * radio.stopListening();
     * radio.write(&data,sizeof(data));
     * @endcode
     * @return True if the payload was delivered successfully false if not
     *
     * @warning Remember to put the radio in TX mode before send any data
     * @see stopListening()
     */
    bool write(unsigned char *tx_data, uint8_t len);

    /**
     * New: Open a pipe for writing via array.
     *
     * Only one writing pipe can be open at once, but you can change the address
     * you'll write to. Call stopListening() first.
     *
     * Addresses are assigned via a byte array, default is 5 byte address length
     *
     * @code
     *   uint8_t addresses[][6] = {"1Node","2Node"};
     *   radio.openWritingPipe(addresses[0]);
     * @endcode
     * @code
     *  uint8_t address[] = { 0xCC,0xCE,0xCC,0xCE,0xCC };
     *  radio.openWritingPipe(address);
     *  address[0] = 0x33;
     *  radio.openReadingPipe(1,address);
     * @endcode
     * @see setAddressWidth
     *
     * @param address The address of the pipe to open. Coordinate these pipe
     * addresses amongst nodes on the network.
     */
    void openWritingPipe(const uint8_t *address);

    /**
     * Open a pipe for reading
     *
     * Up to 6 pipes can be open for reading at once.  Open all the required
     * reading pipes, and then call startListening().
     *
     * @see openWritingPipe
     * @see setAddressWidth
     *
     * @note Pipes 0 and 1 will store a full 5-byte address. Pipes 2-5 will technically
     * only store a single byte, borrowing up to 4 additional bytes from pipe #1 per the
     * assigned address width.
     * @warning Pipes 1-5 should share the same address, except the first byte.
     * Only the first byte in the array should be unique, e.g.
     * @code
     *   uint8_t addresses[][6] = {"1Node","2Node"};
     *   openReadingPipe(1,addresses[0]);
     *   openReadingPipe(2,addresses[1]);
     * @endcode
     *
     * @warning Pipe 0 is also used by the writing pipe.  So if you open
     * pipe 0 for reading, and then startListening(), it will overwrite the
     * writing pipe.  Ergo, do an openWritingPipe() again before write().
     *
     * @param number Which pipe# to open, 0-5.
     * @param address The 24, 32 or 40 bit address of the pipe to open.
     */
    void openReadingPipe(uint8_t pipeNumber, uint8_t *address);

    /**
     * Set timeout of UART of the microcontroller in the USB dongle
     *
     * The microcontroller has timeout miliseconds to read from UART. If this time is reached then the transmission
     * is dropped and nothing is done.
     *
     * @param timeout The value in miliseconds of the UART timeout of the microcontroller USB dongle
     */
    void setDongleTimeout(uint8_t timeout);


};

}
}

#endif //ITANDROIDS_VSS_NRF24L01_H
