//
// Created by junior on 13/11/17.
//

#include "Nrf24L01.h"

namespace core {
namespace communication {

Nrf24L01::~Nrf24L01() {
}

Nrf24L01::Nrf24L01() : uart(B1000000) {
}

void Nrf24L01::setChannel(uint8_t channel) {
    unsigned char data[2];
    data[0] = 1;

    if ((channel >= 0) && (channel <= 125))
        data[1] = (unsigned char) channel;

    uart.send(data, 2);
}

uint8_t Nrf24L01::getChannel() {
    unsigned char command = 2, channel;
    uart.send(&command, 1);
    uart.receive(&channel, 1);
    return (uint8_t) channel;
}

void Nrf24L01::setPayloadSize(uint8_t size) {
    unsigned char command[2] = {3, (unsigned char) size};
    uart.send(command, 2);
}

uint8_t Nrf24L01::getPayloadSize() {
    unsigned char command = 4, size;
    uart.send(&command, 1);
    uart.receive(&size, 1);
    return (uint8_t) size;
}

uint8_t Nrf24L01::getDynamicPayloadSize() {
    unsigned char command = 5, size;
    uart.send(&command, 1);
    uart.receive(&size, 1);
    return (uint8_t) size;
}

void Nrf24L01::enableAckPayload() {
    unsigned char command = 6;
    uart.send(&command, 1);
}

void Nrf24L01::enableDynamicPayloads() {
    unsigned char command = 7;
    uart.send(&command, 1);
}

void Nrf24L01::enableDynamicAck() {
    unsigned char command = 8;
    uart.send(&command, 1);
}

bool Nrf24L01::isPVariant() {
    unsigned char command = 9, isIt;
    uart.send(&command, 1);
    uart.receive(&isIt, 1);
    return (bool) isIt;
}

void Nrf24L01::setAutoAck(bool enable) {
    unsigned char command[3] = {10, 1, (unsigned char) enable};
    uart.send(command, 3);
}

void Nrf24L01::setAutoAck(uint8_t pipe, bool enable) {
    unsigned char command[4] = {10, 2, (unsigned char) enable, (unsigned char) pipe};
    uart.send(command, 4);
}

void Nrf24L01::setPALevel(uint8_t level) {
    unsigned char command[2] = {11, (unsigned char) level};
    uart.send(command, 2);
}

uint8_t Nrf24L01::getPALevel() {
    unsigned char command = 12, paLevel;
    uart.send(&command, 1);
    uart.receive(&paLevel, 1);
    return (uint8_t) paLevel;
}

void Nrf24L01::setDataRate(uint8_t speed) {
    unsigned char command[2] = {13, (unsigned char) speed};
    uart.send(command, 2);
}

uint8_t Nrf24L01::getDataRate() {
    unsigned char command = 14, speed;
    uart.send(&command, 1);
    uart.receive(&speed, 1);
    return (uint8_t) speed;
}

void Nrf24L01::setCRCLength(uint8_t length) {
    unsigned char command[2] = {15, (unsigned char) length};
    uart.send(command, 2);
}

uint8_t Nrf24L01::getCRCLength() {
    unsigned char command = 16, length;
    uart.send(&command, 1);
    uart.receive(&length, 1);
    return (uint8_t) length;
}

void Nrf24L01::disableCRC() {
    unsigned char command = 17;
    uart.send(&command, 1);
}

void Nrf24L01::maskIRQ(bool tx_ok, bool tx_fail, bool rx_ready) {
    unsigned char mask = 0;
    if (tx_ok == true)
        mask = 4;
    if (tx_fail == true)
        mask = 2;
    if (rx_ready == true)
        mask = 1;
    unsigned char command[2] = {18, mask};
    uart.send(command, 2);
}

void Nrf24L01::startListening() {
    unsigned char command = 19;
    uart.send(&command, 1);
}

void Nrf24L01::stopListening() {
    unsigned char command = 20;
    uart.send(&command, 1);
}

bool Nrf24L01::available() {
    unsigned char command = 21, isAvailable;
    uart.send(&command, 1);
    uart.receive(&isAvailable, 1);
    return (bool) isAvailable;
}

bool Nrf24L01::available(uint8_t *pipe_num) {
    unsigned char command = 22, data[2];
    uart.send(&command, 1);
    uart.receive(data, 2);
    *pipe_num = data[1];
    return (bool) data[0];
}

void Nrf24L01::read(unsigned char *rx_data, uint8_t len) {
    unsigned char command[2] = {23, (unsigned char) len};
    uart.send(command, 2);
    uart.receive(rx_data, len);
}

bool Nrf24L01::write(unsigned char *tx_data, uint8_t len) {
    unsigned char command[2] = {24, (unsigned char) len};
    uart.send(command, 2);
    uart.blockingSend(tx_data, std::min((int) len, 32));

    unsigned char success;
    uart.receive(&success, 1);
    if (success == 1)
        return true;
    else
        return false;
}

void Nrf24L01::openWritingPipe(const uint8_t *address) {
    unsigned char command = 25;
    uart.send(&command, 1);
    uart.blockingSend((unsigned char *) address, 5);
}

void Nrf24L01::openReadingPipe(uint8_t pipeNumber, uint8_t *address) {
    unsigned char command[2] = {26, (unsigned char) pipeNumber};
    uart.send(command, 2);
    uart.blockingSend((unsigned char *) address, 5);
}

void Nrf24L01::setDongleTimeout(uint8_t timeout) {
    unsigned char command[2] = {27, (unsigned char) timeout};
    uart.send(command, 2);
}


}
}
