#ifndef CORE_COMMUNICATION_SIMULATORCOMMUNICATION_H_
#define CORE_COMMUNICATION_SIMULATORCOMMUNICATION_H_

#include "communication/parsers/SerialMessageParser.h"
#include "communication/writers/SerialPortWriter.h"
#include "communication/AbstractCommunication.h"
#include "communication/writers/SocketWriter.h"
#include "communication/parsers/SocketMessageParser.h"

namespace core {
namespace communication {

class SocketCommunication : public AbstractCommunication {
public:
    SocketCommunication(SocketMessageParser *parser, SocketWriter *writer);

    ~SocketCommunication();

    void send(control::ControlInterface &controlInterface);

private:
    SocketMessageParser *parser;
    SocketWriter *writer;
};

} /* namespace communication */

} /* namespace core */

#endif /* CORE_COMMUNICATION_SIMULATORCOMMUNICATION_H_ */
