//
// Created by igor on 10/11/16.
//

#include "V4L2FrameGrabber.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

namespace core {
namespace communication {

#define CLEAR(x) memset (&(x), 0, sizeof(x))

V4L2Settings::V4L2Settings() :
        brightness(DEFAULT_SETTING), contrast(DEFAULT_SETTING), saturation(
        DEFAULT_SETTING), gain(DEFAULT_SETTING), exposure(
        DEFAULT_SETTING) {
}

V4L2Settings::V4L2Settings(int brightness, int contrast, int saturation,
                           int gain, int exposure) :
        brightness(brightness), contrast(contrast), saturation(saturation), gain(
        gain), exposure(exposure) {
}

V4L2FrameGrabber::V4L2FrameGrabber() : V4L2FrameGrabber(SEARCH_DEVICE_INDEX, 640, 480, 30, modeling::YUYV, V4L2Settings()) {
}

V4L2FrameGrabber::V4L2FrameGrabber(int deviceIndex, int imageWidth, int imageHeight, int frameRate,
                                   modeling::PixelFormat pixelFormat, V4L2Settings settings) :
        cameraFileHandler(-1), numBuffers(-1), buffers(NULL), image(imageWidth, imageHeight, pixelFormat) {
    initialize(deviceIndex, imageWidth, imageHeight, frameRate, pixelFormat, settings);
}

V4L2FrameGrabber::~V4L2FrameGrabber() {
}

V4L2Settings V4L2FrameGrabber::getSettings() {
    V4L2Settings settings;

    settings.brightness = getControl(V4L2_CID_BRIGHTNESS);
    settings.contrast = getControl(V4L2_CID_CONTRAST);
    settings.saturation = getControl(V4L2_CID_SATURATION);
    settings.gain = getControl(V4L2_CID_GAIN);
    settings.exposure = getControl(V4L2_CID_EXPOSURE_ABSOLUTE);

    return settings;
}

void V4L2FrameGrabber::setSettings(V4L2Settings settings) {
    if (settings.brightness == V4L2Settings::DEFAULT_SETTING)
        resetControl(V4L2_CID_BRIGHTNESS);
    else if (settings.brightness != getControl(V4L2_CID_BRIGHTNESS))
        setControl(V4L2_CID_BRIGHTNESS, settings.brightness);

    if (settings.contrast == V4L2Settings::DEFAULT_SETTING)
        resetControl(V4L2_CID_CONTRAST);
    else if (settings.contrast != getControl(V4L2_CID_CONTRAST))
        setControl(V4L2_CID_CONTRAST, settings.contrast);

    if (settings.saturation == V4L2Settings::DEFAULT_SETTING)
        resetControl(V4L2_CID_SATURATION);
    else if (settings.saturation != getControl(V4L2_CID_SATURATION))
        setControl(V4L2_CID_SATURATION, settings.saturation);

    if (settings.gain == V4L2Settings::DEFAULT_SETTING)
        resetControl(V4L2_CID_GAIN);
    else if (settings.gain != getControl(V4L2_CID_GAIN))
        setControl(V4L2_CID_GAIN, settings.gain);

    if (settings.exposure == V4L2Settings::DEFAULT_SETTING)
        resetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    else if (settings.exposure != getControl(V4L2_CID_EXPOSURE_ABSOLUTE))
        setControl(V4L2_CID_EXPOSURE_ABSOLUTE, settings.exposure);
}

unsigned char *V4L2FrameGrabber::grabFrame() {
    capture();
    return image.getData();
}

bool V4L2FrameGrabber::startCapturing() {
    // TODO: implement this method
    return false;
}

int V4L2FrameGrabber::getImageWidth() {
    return image.getWidth();
}

int V4L2FrameGrabber::getImageHeight() {
    return image.getHeight();
}

void V4L2FrameGrabber::capture() {
    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(cameraFileHandler, &fds);

        /* Timeout. */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        r = select(cameraFileHandler + 1, &fds, NULL, NULL, &tv);

        if (-1 == r) {
            if (EINTR == errno)
                continue;

            exit(EXIT_FAILURE);
        }

        if (0 == r) {
            fprintf(stderr, "select timeout\n");
            exit(EXIT_FAILURE);
        }

        if (readFrame())
            break;

        /* EAGAIN - continue select loop. */
    }
}

int V4L2FrameGrabber::initialize(int deviceIndex, int imageWidth, int imageHeight,
                                 int frameRate, modeling::PixelFormat pixelFormat, V4L2Settings settings) {
    deviceIndex = initializeDevice(deviceIndex);

    initializeFormat(imageWidth, imageHeight, pixelFormat);

    initializeFrameRate(frameRate);

    initializeMmap(deviceIndex);

    queueBuffers();

    initializeStreaming();

    setSettings(settings);

    setCameraAutoOff();

    return 1;
}

int V4L2FrameGrabber::initializeDevice(int deviceIndex) {
    struct stat st;

    char deviceName[15];
    // TODO: make this search get the camera
    // we desire instead of the camera with the
    // smallest index (we have problems with
    // notebooks that have embedded camera)
    if (deviceIndex == SEARCH_DEVICE_INDEX) {
        for (int i = 0; i <= 15; ++i) {
            sprintf(deviceName, "/dev/video%d", i);
            if (stat(deviceName, &st) != -1) {
                printf("%s: device found\n", deviceName);
                deviceIndex = i;
                break;
            }
        }
    }
    sprintf(deviceName, "/dev/video%d", deviceIndex);

    if (-1 == stat(deviceName, &st)) {
        fprintf(stderr, "Cannot identify '%s': %d, %s\n", deviceName, errno,
                strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
        fprintf(stderr, "%s is no device\n", deviceName);
        exit(EXIT_FAILURE);
    }

    cameraFileHandler = open(deviceName, O_RDWR | O_NONBLOCK, 0);
    if (-1 == cameraFileHandler) {
        fprintf(stderr, "Cannot open '%s': %d, %s\n", deviceName, errno,
                strerror(errno));
        exit(EXIT_FAILURE);
    }

    return deviceIndex;
}

void V4L2FrameGrabber::initializeFormat(int imageWidth, int imageHeight, modeling::PixelFormat pixelFormat) {
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;

    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (ioctl(cameraFileHandler, VIDIOC_CROPCAP, &cropcap) == 0) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        ioctl(cameraFileHandler, VIDIOC_S_CROP, &crop);
    } else {
        /* Errors ignored. */
    }

    struct v4l2_format format;
    CLEAR(format);

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = imageWidth;
    format.fmt.pix.height = imageHeight;
    format.fmt.pix.pixelformat = toV4l2PixelFormat(pixelFormat);
    format.fmt.pix.field = V4L2_FIELD_INTERLACED;

    if (-1 == ioctl(cameraFileHandler, VIDIOC_S_FMT, &format))
        errorExit("VIDIOC_S_FMT");

    unsigned int min = format.fmt.pix.width * 2;
    if (format.fmt.pix.bytesperline < min)
        format.fmt.pix.bytesperline = min;
    min = format.fmt.pix.bytesperline * format.fmt.pix.height;
    if (format.fmt.pix.sizeimage < min)
        format.fmt.pix.sizeimage = min;
}

void V4L2FrameGrabber::initializeFrameRate(int frameRate) {
    struct v4l2_streamparm fps;
    CLEAR(fps);

    fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(cameraFileHandler, VIDIOC_G_PARM, &fps) == -1)
        errorExit("VIDIOC_G_PARM");

    fps.parm.capture.timeperframe.numerator = 1;
    fps.parm.capture.timeperframe.denominator = frameRate;
    if (ioctl(cameraFileHandler, VIDIOC_S_PARM, &fps) == -1)
        errorExit("VIDIOC_S_PARM");
}

void V4L2FrameGrabber::initializeMmap(int deviceIndex) {
    char deviceName[15];
    sprintf(deviceName, "/dev/video%d", deviceIndex);

    struct v4l2_requestbuffers request;
    CLEAR(request);

    request.count = 4;
    request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    request.memory = V4L2_MEMORY_MMAP;

    if (-1 == ioctl(cameraFileHandler, VIDIOC_REQBUFS, &request)) {
        if (EINVAL == errno) {
            fprintf(stderr, "%s does not support "
                    "memory mapping\n", deviceName);
            exit(EXIT_FAILURE);
        } else {
            errorExit("VIDIOC_REQBUFS");
        }
    }

    if (request.count < 2) {
        fprintf(stderr, "Insufficient buffer memory on %s\n", deviceName);
        exit(EXIT_FAILURE);
    }

    buffers = (FrameBuffer *) calloc(request.count, sizeof(*buffers));
    if (!buffers) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }

    for (numBuffers = 0; numBuffers < request.count; ++numBuffers) {
        struct v4l2_buffer v4l2Buffer;
        CLEAR(v4l2Buffer);

        v4l2Buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2Buffer.memory = V4L2_MEMORY_MMAP;
        v4l2Buffer.index = numBuffers;

        if (-1 == ioctl(cameraFileHandler, VIDIOC_QUERYBUF, &v4l2Buffer))
            errorExit("VIDIOC_QUERYBUF");

        buffers[numBuffers].length = v4l2Buffer.length;
        buffers[numBuffers].start = mmap(NULL, v4l2Buffer.length,
                                         PROT_READ | PROT_WRITE,
                                         MAP_SHARED, cameraFileHandler, v4l2Buffer.m.offset);

        if (MAP_FAILED == buffers[numBuffers].start)
            errorExit("mmap");
    }
}

void V4L2FrameGrabber::queueBuffers() {
    for (unsigned int i = 0; i < numBuffers; ++i) {
        struct v4l2_buffer v4l2Buffer;
        CLEAR(v4l2Buffer);

        v4l2Buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2Buffer.memory = V4L2_MEMORY_MMAP;
        v4l2Buffer.index = i;

        if (-1 == ioctl(cameraFileHandler, VIDIOC_QBUF, &v4l2Buffer))
            errorExit("VIDIOC_QBUF");
    }
}

void V4L2FrameGrabber::initializeStreaming() {
    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == ioctl(cameraFileHandler, VIDIOC_STREAMON, &type))
        errorExit("VIDIOC_STREAMON");
}

void V4L2FrameGrabber::setCameraAutoOff() {
    setControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);
    setControl(V4L2_CID_AUTO_WHITE_BALANCE, 0);
    setControl(V4L2_CID_AUTOGAIN, 0);
    setControl(V4L2_CID_HUE_AUTO, 0);
}

int V4L2FrameGrabber::readFrame() {
    struct v4l2_buffer v4l2Buffer;

    CLEAR(v4l2Buffer);

    v4l2Buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    v4l2Buffer.memory = V4L2_MEMORY_MMAP;

    if (-1 == ioctl(cameraFileHandler, VIDIOC_DQBUF, &v4l2Buffer)) {
        switch (errno) {
            case EAGAIN:
                return 0;

            case EIO:
                /* Could ignore EIO, see spec. */

                /* fall through */

            default:
                exit(EXIT_FAILURE);
        }
    }

    assert(v4l2Buffer.index < numBuffers);

    unsigned char *imageData = image.getData();
    memcpy(imageData, buffers[v4l2Buffer.index].start, image.getSizeInBytes());
//    for (int i = 0; i < image.getSizeInBytes(); i++)
//        imageData[i] = ((unsigned char *) buffers[v4l2Buffer.index].start)[i];

    if (-1 == ioctl(cameraFileHandler, VIDIOC_QBUF, &v4l2Buffer))
        errorExit("VIDIOC_QBUF");

    return 1;
}

int V4L2FrameGrabber::getControl(int control) {
    struct v4l2_queryctrl queryCtrl;
    struct v4l2_control controlS;
    int error;

    queryCtrl.id = control;
    if (ioctl(cameraFileHandler, VIDIOC_QUERYCTRL, &queryCtrl) < 0)
        return -1;
    controlS.id = control;
    if ((error = ioctl(cameraFileHandler, VIDIOC_G_CTRL, &controlS)) < 0)
        return -1;

    return controlS.value;
}

int V4L2FrameGrabber::setControl(int control, int value) {
    struct v4l2_control controlS;
    struct v4l2_queryctrl queryCtrl;
    int min, max, step, defaultValue;
    int error;

    queryCtrl.id = control;
    if (ioctl(cameraFileHandler, VIDIOC_QUERYCTRL, &queryCtrl) < 0)
        return -1;
    if (queryCtrl.flags & V4L2_CTRL_FLAG_DISABLED)
        return -1;

    min = queryCtrl.minimum;
    max = queryCtrl.maximum;
    step = queryCtrl.step;
    defaultValue = queryCtrl.default_value;
    if ((value >= min) && (value <= max)) {
        controlS.id = control;
        controlS.value = value;
        if ((error = ioctl(cameraFileHandler, VIDIOC_S_CTRL, &controlS)) < 0)
            return -1;
    }
    return 0;
}

int V4L2FrameGrabber::resetControl(int control) {
    struct v4l2_control controlS;
    struct v4l2_queryctrl queryCtrl;
    int defaultValue;
    int error;

    queryCtrl.id = control;
    if (ioctl(cameraFileHandler, VIDIOC_QUERYCTRL, &queryCtrl) < 0)
        return -1;
    defaultValue = queryCtrl.default_value;
    controlS.id = control;
    controlS.value = defaultValue;
    if ((error = ioctl(cameraFileHandler, VIDIOC_S_CTRL, &controlS)) < 0)
        return -1;

    return 0;
}

__u32 V4L2FrameGrabber::toV4l2PixelFormat(modeling::PixelFormat format) {
    switch (format) {
        case modeling::YUYV:
            return V4L2_PIX_FMT_YUYV;
        case modeling::RGB:
            return V4L2_PIX_FMT_RGB24;
    }
}

void V4L2FrameGrabber::errorExit(const char *string) {
    fprintf(stderr, "%s error %d, %s\n", string, errno, strerror(errno));
    if (errno == 16)
        fprintf(stderr,
                "\nYou must free up camera resources used by running programs. \n"
                        "Kill a program that uses the camera.\n\n");
    exit(EXIT_FAILURE);
}

}
}