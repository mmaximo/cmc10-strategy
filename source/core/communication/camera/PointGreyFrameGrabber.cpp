//
// // Created by igor on 10/11/16.
//

#include <iostream>
#include "PointGreyFrameGrabber.h"

#define MAX_WIDTH 656
#define MAX_HEIGHT 516

namespace core {
namespace communication {

PointGreyFrameGrabber::PointGreyFrameGrabber() :
        PointGreyFrameGrabber(MAX_WIDTH, MAX_HEIGHT) {
}

PointGreyFrameGrabber::PointGreyFrameGrabber(int cols, int rows) {
    this->ConnectWithCamera(cols, rows);
}

PointGreyFrameGrabber::~PointGreyFrameGrabber() {
    error = camera.StopCapture();

    if (error != PGRERROR_OK) {
        // This may fail when camera is removed,
        // so don't show an error message
    }

    camera.Disconnect();
}

bool PointGreyFrameGrabber::check() {
    return successCommunicationWithCamera;
}

bool PointGreyFrameGrabber::ConnectWithCamera(int cols, int rows) {
    BusManager busMgr;
    CameraInfo camInfo;
    PGRGuid guid;
    unsigned int numCameras;

    checkError(busMgr.GetNumOfCameras(&numCameras));

    this->rows = rows;
    this->cols = cols;

    if (numCameras < 1) {
        successCommunicationWithCamera = false;
        std::cout << "PointGreyFrameGrabber:" << std::endl;
        std::cout << "\tNo Point Grey camera detected" << std::endl;
    } else {
        successCommunicationWithCamera = true;

        checkError(busMgr.GetCameraFromIndex(0, &guid));
        checkError(camera.Connect(&guid));
        checkError(camera.GetCameraInfo(&camInfo));
        printCameraInfo(&camInfo);

        setImageSize(cols, rows);
        setPixelFormat(core::modeling::PixelFormat::YUV422);
        setFrameRate(60);

        FC2Config fc2Config;
        camera.GetConfiguration(&fc2Config);
        fc2Config.grabMode = DROP_FRAMES;
        fc2Config.grabTimeout = TIMEOUT_INFINITE;
        fc2Config.highPerformanceRetrieveBuffer = true;
        camera.SetConfiguration(&fc2Config);
    }

    return successCommunicationWithCamera;
}

bool PointGreyFrameGrabber::startCapturing() {
    checkError(camera.StartCapture());
}

int PointGreyFrameGrabber::getImageHeight() {
    return rows;
}

int PointGreyFrameGrabber::getImageWidth() {
    return cols;
}

unsigned char *PointGreyFrameGrabber::grabFrame() {
    error = camera.RetrieveBuffer(&image);
    if (error != PGRERROR_OK)
        error.PrintErrorTrace();
    //std::cout << image.GetTimeStamp().seconds << " s " << image.GetTimeStamp().microSeconds << " us " << std::endl;
    return image.GetData();
}

unsigned int PointGreyFrameGrabber::CheckNumberOfCameras() {
    BusManager busMgr;
    unsigned int numCameras;

    checkError(busMgr.GetNumOfCameras(&numCameras));

    return numCameras;
}

void PointGreyFrameGrabber::BlockConnection() {
    this->successCommunicationWithCamera = false;
}

void PointGreyFrameGrabber::setFrameRate(float desiredFrameRate) {
    Property property;
    property.type = PropertyType::FRAME_RATE;
    property.absControl = true;
    property.onOff = true;
    property.absValue = desiredFrameRate;
    camera.SetProperty(&property);
}

void PointGreyFrameGrabber::setImageSize(int width, int height) {
    bool supported;
    bool valid;
    BusManager busMgr;
    float percentage;
    Format7ImageSettings format7ImageSettings;
    Format7PacketInfo format7PacketInfo;
    Format7Info format7Info;
    unsigned int packetSize;

    camera.GetFormat7Configuration(&format7ImageSettings, &packetSize, &percentage);
    camera.GetFormat7Info(&format7Info, &supported);
    format7ImageSettings.mode = MODE_4;
    format7ImageSettings.height = rows;
    format7ImageSettings.width = cols;
    format7ImageSettings.offsetX = 0;//(format7Info.maxWidth - format7ImageSettings.width) / 2;
    format7ImageSettings.offsetY = 0;//(format7Info.maxHeight - format7ImageSettings.height) / 2;

    checkError(camera.ValidateFormat7Settings(&format7ImageSettings, &valid, &format7PacketInfo));

    if (!valid) {
        std::cout << "Format7 settings are not valid" << std::endl;
        exit(EXIT_FAILURE);
    }

    checkError(camera.SetFormat7Configuration(&format7ImageSettings, format7PacketInfo.recommendedBytesPerPacket));
}

void PointGreyFrameGrabber::setPixelFormat(core::modeling::PixelFormat pixelFormat) {
    bool valid;
    float percentage;
    Format7ImageSettings format7ImageSettings;
    Format7PacketInfo format7PacketInfo;
    unsigned int packetSize;

    camera.GetFormat7Configuration(&format7ImageSettings, &packetSize, &percentage);

    switch (pixelFormat) {
        case core::modeling::PixelFormat::YUV444:
            format7ImageSettings.pixelFormat = PIXEL_FORMAT_444YUV8;
            break;
        case core::modeling::PixelFormat::YUV422:
            format7ImageSettings.pixelFormat = PIXEL_FORMAT_422YUV8;
            break;
        case core::modeling::PixelFormat::RGB:
            format7ImageSettings.pixelFormat = PIXEL_FORMAT_RGB;
            break;
    }

    checkError(camera.ValidateFormat7Settings(&format7ImageSettings, &valid, &format7PacketInfo));

    if (!valid) {
        std::cout << "Format7 settings are not valid" << std::endl;
        exit(EXIT_FAILURE);
    }
    checkError(camera.SetFormat7Configuration(&format7ImageSettings, format7PacketInfo.recommendedBytesPerPacket));
}

void PointGreyFrameGrabber::useBuffer(int numBuffers) {
    FC2Config fc2Config;
    camera.GetConfiguration(&fc2Config);
    fc2Config.numBuffers = numBuffers;
    fc2Config.grabMode = BUFFER_FRAMES;
    fc2Config.highPerformanceRetrieveBuffer = true;
    camera.SetConfiguration(&fc2Config);
}

void PointGreyFrameGrabber::checkError(Error error) {
    if (error != PGRERROR_OK) {
        error.PrintErrorTrace();
//        exit(EXIT_FAILURE);
    }
}


void PointGreyFrameGrabber::printCameraInfo(CameraInfo *pCamInfo) {
    std::cout << std::endl;
    std::cout << "*** CAMERA INFORMATION ***" << std::endl;
    std::cout << "Serial number - " << pCamInfo->serialNumber << std::endl;
    std::cout << "Camera model - " << pCamInfo->modelName << std::endl;
    std::cout << "Camera vendor - " << pCamInfo->vendorName << std::endl;
    std::cout << "Sensor - " << pCamInfo->sensorInfo << std::endl;
    std::cout << "Resolution - " << pCamInfo->sensorResolution << std::endl;
    std::cout << "Firmware version - " << pCamInfo->firmwareVersion << std::endl;
    std::cout << "Firmware build time - " << pCamInfo->firmwareBuildTime << std::endl << std::endl;
}

}
}