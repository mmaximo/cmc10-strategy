//
// Created by igor on 10/11/16.
//

#ifndef ITANDROIDS_LIB_V4L2FRAMEGRABBER_H
#define ITANDROIDS_LIB_V4L2FRAMEGRABBER_H

#include "communication/camera/AbstractFrameGrabber.h"

#include "modeling/vision/Image.h"

#include <stdlib.h>
#include <linux/videodev2.h>
#include <sys/time.h>

namespace core {
namespace communication {

struct V4L2Settings {
    static const int DEFAULT_SETTING = -1;

    int brightness; // 0-255
    int contrast; // 0-255
    int saturation; // 0-255
    int gain; // 0-255
    int exposure; // 0-10000

    V4L2Settings();

    V4L2Settings(int brightness, int contrast, int saturation, int gain,
                 int exposure);
};

class V4L2FrameGrabber : AbstractFrameGrabber {
public:
    static const int SEARCH_DEVICE_INDEX = -1;

    bool startCapturing();

    V4L2FrameGrabber();

    V4L2FrameGrabber(int deviceIndex, int imageWidth, int imageHeight, int frameRate, modeling::PixelFormat pixelFormat,
                     V4L2Settings settings);

    virtual ~V4L2FrameGrabber();

    int initialize(int deviceIndex, int imageWidth, int imageHeight, int frameRate,
                   modeling::PixelFormat pixelFormat, V4L2Settings settings);

    V4L2Settings getSettings();

    void setSettings(V4L2Settings settings);

    unsigned char *grabFrame();

    int getImageWidth();

    int getImageHeight();

private:
    int cameraFileHandler;
    modeling::Image image;

    struct FrameBuffer {
        void *start;
        size_t length;
    };
    FrameBuffer *buffers;
    int numBuffers;

    int initializeDevice(int deviceIndex);

    void initializeFormat(int imageWidth, int imageHeight,
                          modeling::PixelFormat pixelFormat);

    void initializeFrameRate(int frameRate);

    void initializeMmap(int deviceIndex);

    void queueBuffers();

    void initializeStreaming();

    void setCameraAutoOff();

    void capture();

    int readFrame();

    int getControl(int control);

    int setControl(int control, int value);

    int resetControl(int control);

    __u32 toV4l2PixelFormat(modeling::PixelFormat format);

    void errorExit(const char *string);
};

}
}


#endif //ITANDROIDS_LIB_V4L2FRAMEGRABBER_H
