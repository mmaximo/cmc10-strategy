//
// Created by igor on 10/11/16.
//

#ifndef ITANDROIDS_LIB_ABSTRACTFRAMEGRABBER_H
#define ITANDROIDS_LIB_ABSTRACTFRAMEGRABBER_H

namespace core {
namespace communication {

class AbstractFrameGrabber {
public:
    virtual ~AbstractFrameGrabber();
    virtual bool startCapturing() = 0;
    virtual unsigned char *grabFrame() = 0;
};

}
}

#endif //ITANDROIDS_LIB_ABSTRACTFRAMEGRABBER_H
