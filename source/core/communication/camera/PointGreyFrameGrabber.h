//
// Created by igor on 10/11/16.
//

#ifndef ITANDROIDS_LIB_POINTGREYFRAMEGRABBER_H
#define ITANDROIDS_LIB_POINTGREYFRAMEGRABBER_H

#include "AbstractFrameGrabber.h"
#include "flycapture/FlyCapture2.h"
#include "core/modeling/vision/Image.h"

using namespace FlyCapture2;

namespace core {
namespace communication {

class PointGreyFrameGrabber : public AbstractFrameGrabber {
public:
    PointGreyFrameGrabber();
    PointGreyFrameGrabber(int cols, int rows);
    ~PointGreyFrameGrabber();

    bool check();
    bool ConnectWithCamera(int cols, int rows);
    bool startCapturing();
    int getImageHeight();
    int getImageWidth();
    unsigned char *grabFrame(); // TODO: return Image class
    unsigned int CheckNumberOfCameras();
    void BlockConnection();
    void setFrameRate(float desiredFrameRate);
    void setImageSize(int width, int height);
    void setPixelFormat(core::modeling::PixelFormat pixelFormat);
    void useBuffer(int numBuffers);

private:
    bool successCommunicationWithCamera;
    Camera camera;
    Error error;
    Image image;
    unsigned int cols;
    unsigned int rows;

    void checkError(Error error);
    void printCameraInfo(CameraInfo *pCamInfo);
};

} /* namespace itandroids_lib */
} /* namespace hardware_interfaces */

#endif //ITANDROIDS_LIB_POINTGREYFRAMEGRABBER_H