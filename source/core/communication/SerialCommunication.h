//
// Created by gabriel on 01/02/17.
//

#ifndef CORE_COMMUNICATION_SERIALCOMMUNICATION_H
#define CORE_COMMUNICATION_SERIALCOMMUNICATION_H

#include "communication/AbstractCommunication.h"
#include "communication/parsers/SerialMessageParser.h"
#include "communication/writers/SerialPortWriter.h"

namespace core {
namespace communication {

class SerialCommunication : public AbstractCommunication {
public:
    SerialCommunication();

    void send(control::ControlInterface &controlInterface);

    virtual ~SerialCommunication();

private:
    SerialMessageParser parser;
    SerialPortWriter writer;
};


} /* namespace communication */
} /* namespace core */


#endif //CORE_COMMUNICATION_SERIALCOMMUNICATION_H
