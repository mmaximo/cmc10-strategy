//
// Created by gabriel on 01/02/17.
//

#include "communication/NrfSerialCommunication.h"

namespace core {
namespace communication {

NrfSerialCommunication::~NrfSerialCommunication() {}

NrfSerialCommunication::NrfSerialCommunication(bool debugMode) : debug(debugMode)//, writer(B1000000)
{
    Nrf24L01 radio;

    // Initialize the PID gains with zeros
    Kp = 0;
    Ki = 0;
    Kd = 0;

    // Initialize messages with zeros
    initializeMsgWithZeros(txData);
    initializeMsgWithZeros(rxData);

    // Set radio configuration
    radio.setPayloadSize(msgSize);
    radio.setChannel(100);
    radio.setDataRate(RF24_2MBPS);
    //radio.maskIRQ(true, true, false);         // Allows only RX events
    radio.setCRCLength(RF24_CRC_8);
    radio.setPALevel(RF24_PA_MAX);
    //radio.disableCRC();
    radio.setAutoAck(false);                   // Master acknowledges the players
    radio.openWritingPipe((unsigned char *) "B_ITA");
}

void NrfSerialCommunication::floatToHex(float number, char *data) {
    memcpy(data, &number, 4);
}

float NrfSerialCommunication::hexToFloat(char *packet) {
    float decodedValue;
    memcpy(&decodedValue, packet, 4);

    return decodedValue;
}

void NrfSerialCommunication::int16ToHex(int16_t number, char *data) {
    //data[0] = (char) (&number)[0];
    memcpy(data, &number, 2);
}

int16_t NrfSerialCommunication::hexToInt16(char *packet) {
    int16_t decodedValue;
    memcpy(&decodedValue, packet, 2);

    return decodedValue;
}

void NrfSerialCommunication::calculateChecksum(char *packet) {
    checksum = 0;

    int i;
    for (i = 0; i < msgSize; i++)
        checksum = checksum + packet[i];
}

void NrfSerialCommunication::initializeMsgWithZeros(char *packet) {
    int i;
    for (i = 0; i < msgSize; i++)
        packet[i] = 0;
}

void NrfSerialCommunication::setWheelSpeed(int id, double leftWheel, double rightWheel) {
    // Add WMS to service chunk
    txData[0] = txData[0] | (1 << WMS);

    int where = 4 * (id - 1) + 1;
    double leftWheelSpeed = math::MathUtils::radiansPerSecondToRpm(leftWheel) * representations::Player::MOTOR_REDUCTION;
    double rightWheelSpeed = math::MathUtils::radiansPerSecondToRpm(rightWheel) * representations::Player::MOTOR_REDUCTION;
    //double leftWheelSpeed = leftWheel;
    //double rightWheelSpeed = rightWheel;

    // Load left wheel speed
    int16_t rpmLeftSpeed = (int16_t) leftWheelSpeed;
    int16ToHex(rpmLeftSpeed, &txData[where]);
    where = where + 2;

    // Load right wheel speed
    int16_t rpmRightSpeed = (int16_t) rightWheelSpeed;
    int16ToHex(rpmRightSpeed, &txData[where]);
}

void NrfSerialCommunication::setPidGains(int id, double gainKp, double gainKi, double gainKd) {
    // Add WPID to service chunk
    txData[0] = txData[0] | (1 << WPID);

    // Load Kp, Ki and Kd
    Kp = gainKp;
    Ki = gainKi;
    Kd = gainKd;
}

void NrfSerialCommunication::send(control::ControlInterface &controlInterface) {
    int id;

//    for(int i = 0; i < 3; i ++) {
//        std::cout << i << " l = " << controlInterface.playersWheelSpeed[i].left
//                       << " r = " << controlInterface.playersWheelSpeed[i].right
//                       << std::endl;
//    }
//    std::cout << std::endl;

    txData[0] = (1 << WMS);

    for (id = 0; id < representations::Player::PLAYERS_PER_SIDE; id++)
        setWheelSpeed(id + 1, controlInterface.playersWheelSpeed[id].left,
                      controlInterface.playersWheelSpeed[id].right);

    calculateChecksum(txData);
    txData[msgSize - 1] = checksum;

    // Lock and load!
    if (debug == true) {
        int i;
        for (i = 0; i < msgSize; i++)
            printf("%02X ", txData[i] & 0xFF);
    } else {
            radio.write((unsigned char *) txData, msgSize);
    }

    initializeMsgWithZeros(txData);
}


} /* namespace communication */
} /* namespace core */

