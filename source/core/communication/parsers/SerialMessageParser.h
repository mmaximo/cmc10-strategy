//
// Created by gabriel on 17/03/17.
//

#ifndef CORE_COMMUNICATION_SERIALMESSAGEPARSER_H
#define CORE_COMMUNICATION_SERIALMESSAGEPARSER_H

#include "math/MathUtils.h"

namespace core {
namespace communication {

class SerialMessageParser {
public:
    SerialMessageParser();

    virtual ~SerialMessageParser();

    unsigned char *parse(int id, double leftSpeed, double rightSpeed);

    int getMessageSize();

private:
    void configPayload(int id, double leftSpeed, double rightSpeed);

    int messageSize = 14;
    unsigned char message[14];
    char payload[13];
    int rpmLeftSpeed;
    int rpmRightSpeed;
};

} /* namespace communication */

} /* namespace core */

#endif /* CORE_COMMUNICATION_SERIALMESSAGEPARSER_H */