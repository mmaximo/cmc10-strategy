#ifndef CORE_COMMUNICATION_SOCKETMESSAGEPARSER_H
#define CORE_COMMUNICATION_SOCKETMESSAGEPARSER_H

#include "control/ControlInterface.h"
#include "modeling/vision/VisionInterface.h"
#include "messages.pb.h"

namespace core {
namespace communication {

using tools::simulator::server::communication::CommandInfoMessage;
using tools::simulator::server::communication::FieldInfoMessage;

class SocketMessageParser {
public:
    SocketMessageParser();
    virtual ~SocketMessageParser();

    CommandInfoMessage &parseOut(control::ControlInterface &controlInterface);
    modeling::VisionInterface &parseIn(FieldInfoMessage fMessage);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
    CommandInfoMessage parsedMessage;
    modeling::VisionInterface visionInterface;
    double q0, q1, q2, q3; // Quaternion values
    double angle;

    double getAngleFromQuaternion(double a, double b, double c, double d);
};

} // communication
} // core

#endif
