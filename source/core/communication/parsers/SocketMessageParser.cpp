#include <iostream>
#include <modeling/vision/VisionInterface.h>
#include "communication/parsers/SocketMessageParser.h"

namespace core {
namespace communication {

using tools::simulator::server::communication::Representations_WheelSpeed;

SocketMessageParser::SocketMessageParser() {
    // TODO Auto-generated constructor stub

}

SocketMessageParser::~SocketMessageParser() {
    // TODO Auto-generated destructor stub
}

CommandInfoMessage &SocketMessageParser::parseOut(control::ControlInterface &controlInterface) {

    //creating packet
    parsedMessage.Clear();
    parsedMessage.set_id(1);

    for (int j = 0; j < core::representations::Player::PLAYERS_PER_SIDE; j++) {
        Representations_WheelSpeed *wheelSpeeds_info = parsedMessage.add_wheelspeeds();
        wheelSpeeds_info->set_left(controlInterface.playersWheelSpeed[j].left);
        wheelSpeeds_info->set_right(controlInterface.playersWheelSpeed[j].right);
    }

    return parsedMessage;
}

modeling::VisionInterface &SocketMessageParser::parseIn(FieldInfoMessage fMessage) {
    for (int i = 0; i < fMessage.leftplayer_size(); i++) {
        // Getting quaternion
        q0 = fMessage.leftplayer(i).body().rotation().a();
        q1 = fMessage.leftplayer(i).body().rotation().b();
        q2 = fMessage.leftplayer(i).body().rotation().c();
        q3 = fMessage.leftplayer(i).body().rotation().d();
        angle = fMessage.leftplayer(i).body().angle();
        visionInterface.teammates[i].setPose(Pose2D(
                angle,
                fMessage.leftplayer(i).body().position().x(),
                fMessage.leftplayer(i).body().position().y()
        ));
        visionInterface.teammates[i].setVelocity(Vector2<double>(
                fMessage.leftplayer(i).body().velocity().x(),
                fMessage.leftplayer(i).body().velocity().y()
        ));
        visionInterface.teammates[i].setSeen(true);
    }

    for (int i = 0; i < fMessage.rightplayer_size(); i++) {
        // Getting quaternion
        q0 = fMessage.rightplayer(i).body().rotation().a();
        q1 = fMessage.rightplayer(i).body().rotation().b();
        q2 = fMessage.rightplayer(i).body().rotation().c();
        q3 = fMessage.rightplayer(i).body().rotation().d();
        angle = fMessage.leftplayer(i).body().angle();
        visionInterface.opponents[i].setPose(Pose2D(
                angle,
                fMessage.rightplayer(i).body().position().x(),
                fMessage.rightplayer(i).body().position().y()
        ));
        visionInterface.opponents[i].setVelocity(Vector2<double>(
                fMessage.rightplayer(i).body().velocity().x(),
                fMessage.rightplayer(i).body().velocity().y()
        ));

        visionInterface.opponents[i].setSeen(true);
    }

    //Ball
    visionInterface.ball.setPosition(
            Vector2<double>(fMessage.ball().position().x(),
                            fMessage.ball().position().y()));
    visionInterface.ball.setVelocity(
            Vector2<double>(fMessage.ball().velocity().x(),
                            fMessage.ball().velocity().y()));
    visionInterface.ball.setSeen(true);
    return visionInterface;
}

double SocketMessageParser::getAngleFromQuaternion(double a, double b, double c, double d) {
    double angle;
    double test = a * b + c * d;
    if (test > 0.499) { // singularity at north pole
        angle = 2 * atan2(a, d);
        return angle;
    }
    if (test < -0.499) { // singularity at south pole
        angle = -2 * atan2(a, d);
        return angle;
    }
    double sqy = b * b;
    double sqz = c * c;
    return atan2(2 * b * d - 2 * a * c, 1 - 2 * sqy - 2 * sqz);
}

} // communication
} // core
