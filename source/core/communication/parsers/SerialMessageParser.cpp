//
// Created by gabriel on 17/03/17.
//

#include "SerialMessageParser.h"

namespace core {
namespace communication {

SerialMessageParser::SerialMessageParser() {
    strcpy(payload, "s3s000f000fz");
}

SerialMessageParser::~SerialMessageParser() {
}

void SerialMessageParser::configPayload(int id, double leftSpeed, double rightSpeed) {
    rpmLeftSpeed = (int) core::math::MathUtils::radiansPerSecondToRpm(leftSpeed);
    rpmRightSpeed = (int) core::math::MathUtils::radiansPerSecondToRpm(rightSpeed);

    //TODO - Remove (threshold)
    double maxVelocity = 300;
    if (rpmLeftSpeed < -maxVelocity)
        rpmLeftSpeed = -maxVelocity;
    if (rpmLeftSpeed > maxVelocity)
        rpmLeftSpeed = maxVelocity;
    if (rpmRightSpeed < -maxVelocity)
        rpmRightSpeed = -maxVelocity;
    if (rpmRightSpeed > maxVelocity)
        rpmRightSpeed = maxVelocity;

    // Check if motor should turn backward or forward
    char dirD = 0, dirE = 0;
    char ninthBitVE = (char) (rpmLeftSpeed / 255);
    char ninthBitVD = (char) (rpmRightSpeed / 255);

    if (rpmRightSpeed > 0)
        dirD = 0;  // 0 equals forward
    else {
        dirD = 1;  // 1 equals backward
    }
    if (rpmLeftSpeed > 0)
        dirE = 0;
    else {
        dirE = 1;
    }
    payload[0] = (char) ((0x00) | (id << 4) | (dirE << 3) | (dirD << 2)
                         | (ninthBitVE << 1) | (ninthBitVD));
    payload[1] = (char) ((abs(rpmLeftSpeed)) % 256);
    payload[2] = (char) ((abs(rpmRightSpeed)) % 256);
    payload[3] = payload[0] + payload[1] + payload[2];
    payload[4] = 'z';
}

unsigned char *SerialMessageParser::parse(int id, double leftSpeed, double rightSpeed) {

    // Header
    message[0] = 0x7E;
    message[1] = 0x00;
    message[2] = 0x0A;
    message[3] = 0x01;
    message[4] = 0x01;
    message[5] = 0x50; // XbeeMY[0]
    message[6] = id;   // XbeeMY[1]
    message[7] = 0x00;

    configPayload(id, leftSpeed, rightSpeed);

    // Add payload
    for (int i = 0; i < 5; i++) {
        message[8 + i] = (unsigned char) payload[i];
    }

    // Checksum
    unsigned char sum = 0x00;
    for (int i = 3; i < 13; i++) {
        sum += message[i];
    }
    message[13] = (unsigned char) (0xFF - sum);

    return message;
}

int SerialMessageParser::getMessageSize() {
    return messageSize;
}

} /* namespace communication */

} /* namespace core */
