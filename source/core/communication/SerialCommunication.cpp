#include "communication/SerialCommunication.h"

namespace core {
namespace communication {

SerialCommunication::SerialCommunication() : parser(), writer() {
}

SerialCommunication::~SerialCommunication() {}

void SerialCommunication::send(control::ControlInterface &controlInterface) {
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
        writer.send(parser.parse(i + 1, controlInterface.playersWheelSpeed[i].left,
                                 controlInterface.playersWheelSpeed[i].right), parser.getMessageSize());
    }

}

}
}