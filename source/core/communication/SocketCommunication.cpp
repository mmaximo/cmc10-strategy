#include "communication/SocketCommunication.h"

namespace core {
namespace communication {

SocketCommunication::SocketCommunication(SocketMessageParser *parser, SocketWriter *writer) {
    this->parser = parser;
    this->writer = writer;
    writer->connectToServer();
}

SocketCommunication::~SocketCommunication() {}

void SocketCommunication::send(control::ControlInterface &controlInterface) {
    writer->sendMessage(parser->parseOut(controlInterface));
}

} /* namespace communication */

} /* namespace core */
