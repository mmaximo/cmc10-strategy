//
// Created by gabriel on 01/02/17.
//

#ifndef CORE_COMMUNICATION_NRFSERIALCOMMUNICATION_H
#define CORE_COMMUNICATION_NRFSERIALCOMMUNICATION_H

#include "communication/AbstractCommunication.h"
#include "math/MathUtils.h"
#include "communication/hardware/Nrf24L01.h"

namespace core {
namespace communication {

constexpr uint8_t msgSize = 14; /*!< Message size */
constexpr uint8_t WPID = 3;     /*!< Write PID gains */
constexpr uint8_t RBV = 2;      /*!< Read Battery Voltage */
constexpr uint8_t RMS = 1;      /*!< Read Motor Speed */
constexpr uint8_t WMS = 0;      /*!< Write Motor Speed */
constexpr uint8_t BV = 1;       /*!< Battery Voltage */
constexpr uint8_t MS = 0;       /*!< Motor Speed */

/**
 * NrfSerialCommunication
 *
 * This class handles the communication with the nRF24L01
 *
 * See documentation on
 * https://docs.google.com/document/d/1L3LqoGH7eoP1DZyCqrG9xFrwT__L3e1usR-vqD7ht1Y/edit?usp=sharing
 */
class NrfSerialCommunication : public AbstractCommunication {
public:
    /**
     * Class constructor
     */
    NrfSerialCommunication(bool debugMode = false);

    /**
     * setPidGains (not implemented yet)
     *
     * This function has the purpose of setting PID gains to be transmitted
     * @param id (actually it does nothing because it is not implemented)
     * @param Kp
     * @param Ki
     * @param Kd
     */
    void setPidGains(int id, double Kp, double Ki, double Kd);

    /**
     * send
     *
     * This function has the purpose of sending data to the robots.
     *
     * @param controlInterface is the data structure holding all robots information
     */
    void send(control::ControlInterface &controlInterface);

    /**
     * I do not know what this is... Followed the monkey
     */
    virtual ~NrfSerialCommunication();

private:
    /**
     * Debug mode?
     */
    bool debug;

    /**
     * nRF24L01 object
     */
    Nrf24L01 radio;

    /**
     * Protocol definitions
     */

    /**
     * Variables to store the PID gains
     *
     * TODO add support to multiple robots, i.e., one PID gain per robot
     */
    double Kp, Ki, Kd;

    /**
     * Variables to store the TX and RX data
     */
    char txData[msgSize];
    char rxData[msgSize];

    /**
     * Checksum
     */
    uint8_t checksum;

    /**
     * This class inherits the writer class
     */
    //SerialPortWriter writer;

    /**
     * floatToHex
     *
     * This function has the purpose of converting a float number (32 bits or 4 bytes) to hexadecimal format
     * @param number is the float number to be converted
     * @param data is where the data will be stored
     */
    void floatToHex(float number, char *data);

    /**
     * hexToFloat
     *
     * This function will return a number in float representation (i.e., 4 bytes) given the number in hexadecimal format
     * @param packet is where the data is
     * @return a float number
     */
    float hexToFloat(char *packet);

    /**
     * int16ToHex
     *
     * This function has the purpose of of converting a 16 bit integer with sign into hexadecimal format
     * @param number
     */
    void int16ToHex(int16_t number, char *data);

    /**
     * hexToInt16
     *
     * This function return a 16 bit integer with sign given the number in hexadecimal format
     * @param packet
     * @return a 16 bits integer with sign
     */
    int16_t hexToInt16(char *packet);

    /**
     * calculateChecksum
     *
     * This function has the purpose of calculating the checksum of a message
     * @param packet is the address of a data packet (it may be rxData or txData, basically)
     */
    void calculateChecksum(char *packet);

    /**
     * initializeMsgWithZeros
     *
     * This function has the purpose of writing zeros in all the message
     * @param packet is the address of a data packet (it may be rxData or txData, basically)
     */
    void initializeMsgWithZeros(char *packet);

    /**
     * This function must be used to set the player's wheel speed
     *
     * @param id is an integer to identify the robot (example: 1 is the first robot)
     * @param leftWheel is the left wheel speed (in rad/s)
     * @param rightWheel is the right wheel speed (in rad/s)
     */
    void setWheelSpeed(int id, double leftWheel, double rightWheel);
};

} /* namespace communication */
} /* namespace core */


#endif //CORE_COMMUNICATION_SERIALCOMMUNICATION_H
