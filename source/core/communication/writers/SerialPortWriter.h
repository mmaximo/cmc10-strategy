//
// Created by gabriel on 17/03/17.
//

#ifndef CORE_COMMUNICATION_SERIALPORTWRITER_H
#define CORE_COMMUNICATION_SERIALPORTWRITER_H

#include <iostream>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <sys/select.h>
#include <linux/serial.h>
#include <linux/ioctl.h>
#include <asm/ioctls.h>

namespace core {
namespace communication {

class SerialPortWriter {
public:
    SerialPortWriter(int baud = B57600);

    virtual ~SerialPortWriter();

    /**
     * Send data through UART
     *
     * @param message is a pointer to where store data
     * @param size is how many bytes will be read
     */
    void send(unsigned char *message, int size);

    /**
     * Send data through UART (this function will block until all bytes were sent)
     *
     * @param message
     * @param size
     */
    void blockingSend(unsigned char *message, int size);

    /**
     * Receive data from UART
     *
     * @param message is a pointer to where store data
     * @param size is how many bytes will be read
     * @param timeout is the max wait time (in microseconds) to wait for data
     * @return true if reading was successful or false if shit happened
     */
    bool receive(unsigned char *message, int size, int timeout = 5000);

    /**
     * Fix FTDI FT232 latency timer
     */
    void fixFtdiLatencyTimer();

private:
    struct termios termiosP;
    char serialPortName[20];
    int fdHandler;
    fd_set reading;

    struct timeval timeoutConf;
    struct serial_struct ser_info;

};

} /* namespace communication */

} /* namespace core */

#endif /* CORE_COMMUNICATION_SERIALPORTWRITER_H */
