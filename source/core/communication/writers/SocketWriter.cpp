#include "communication/writers/SocketWriter.h"
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <iostream>

namespace core {
namespace communication {

using namespace tools::simulator::server::communication;

void errorIn(const char *message) {
    perror(message);
    exit(1);
}

SocketWriter::SocketWriter(int port) {
    std::cout << "port number " << port << std::endl;
    portno = port;
    int success;
    struct addrinfo hints;
    
    memset(&hints,0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    success = getaddrinfo(NULL,std::to_string(port).c_str(),&hints,&res);
    if(success != 0){
	errorIn(gai_strerror(success));
    }
    
    sockfd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);
    
    //did not get first message yet
    comMutex.lock();
    gotFirstMessage = false;
    comMutex.unlock();

}

SocketWriter::~SocketWriter() {
    // TODO Auto-generated destructor stub
}

void SocketWriter::receiveMessage() {
    google::protobuf::uint32 message_length; // recv_length; unused variable. Remove the ; and replace with , to put it back

    int header_length = sizeof(message_length);
    google::protobuf::uint8 header[header_length];

    recvData(sockfd,header,header_length);
    
    google::protobuf::io::CodedInputStream::ReadLittleEndian32FromArray(
            header, &message_length);

    google::protobuf::uint8 buffer[message_length];
    
    recvData(sockfd,buffer,message_length);

    google::protobuf::io::ArrayInputStream array_input(buffer, message_length);
    google::protobuf::io::CodedInputStream coded_input(&array_input);

    comMutex.lock();
    if (!simMessage.ParseFromCodedStream(&coded_input)) {
        comMutex.unlock();
        std::cout << "Could not parse message" << std::endl;
        return;
    }
    comMutex.unlock();

    comMutex.lock();
    gotFirstMessage = true;
    comMutex.unlock();
}

void SocketWriter::sendMessage(CommandInfoMessage &message) {

    google::protobuf::uint32 message_length = message.ByteSize();
    int header_length = sizeof(message_length);
    int buffer_length = header_length + message_length;

    google::protobuf::uint8 buffer[buffer_length];

    google::protobuf::io::ArrayOutputStream aos(buffer, buffer_length);
    google::protobuf::io::CodedOutputStream coded_output(&aos);

    coded_output.WriteLittleEndian32(message_length);
    message.SerializeToCodedStream(&coded_output);

    sendData(sockfd,buffer,buffer_length);

    //std::cout << "Succesfully sent packet" <<std::endl;
}

void SocketWriter::connectToServer() {
    if (connect(sockfd, (sockaddr *) res->ai_addr, res->ai_addrlen) < 0)
        errorIn("ERROR connecting ---------");
}

FieldInfoMessage SocketWriter::getLastMessage() {
    boost::lock_guard<boost::mutex> guard(comMutex);
    return simMessage;
}

bool SocketWriter::receivedMessage() {
    boost::lock_guard<boost::mutex> guard(comMutex);
    return gotFirstMessage;
}

/** 
 * @param sockID: socketId from socket from which data will be recovered
 * @param buffer: pointer to pre-allocated array with size equal to the 
 * number of bytes expected to be recovered 
 * @param bufferLength: number of bytes expected to be recovered
 */
void SocketWriter::recvData(int sockID, void* buffer, int bufferLength) {
    int recvBytes = 0;
    recvBytes = recv(sockID,buffer,bufferLength,0);
    while (recvBytes < bufferLength){std::cout << "Communication::recvData\n";
	if(recvBytes <= 0)
	    errorIn("COULD NOT READ DATA");
	bufferLength -= recvBytes;
	buffer = buffer + recvBytes;
	recvBytes = recv(sockID,buffer,bufferLength,0);
    }
}

/** 
 * @param sockID: socketId from socket from which data will be sent
 * @param buffer: pointer to pre-allocated array with size equal to the 
 * number of bytes expected to be sent 
 * @param bufferLength: number of bytes expected to be sent
 */
void SocketWriter::sendData(int sockID, void* buffer, int bufferLength) {
    int sentBytes = 0;
    sentBytes = send(sockID,buffer,bufferLength,0);
    while(sentBytes < bufferLength) {std::cout << "Communication::sendData\n";
	if(sentBytes <= 0)
	    errorIn("COULD NOT SEND DATA");
	bufferLength -= sentBytes;
	buffer = buffer + sentBytes;
	sentBytes = send(sockID,buffer,bufferLength,0);
    }
}


} /* namespace communication */

} /* namespace core */
