//
// Created by gabriel on 17/03/17.
//

#include <sys/ioctl.h>
#include "SerialPortWriter.h"

namespace core {
namespace communication {

SerialPortWriter::SerialPortWriter(int baud) {
    // Try to open ttyUSB0
    strcpy(serialPortName, "/dev/ttyUSB0");
    std::cout << "(SerialPortWriter) Opening serial port" << serialPortName << "..." << std::endl;
    fdHandler = open(serialPortName, O_NONBLOCK | O_RDWR | O_NOCTTY);

    // The uart oppened then fix FTDI low latency problem
    if (fdHandler != -1) {
        fixFtdiLatencyTimer();
    } else {
        //shit happened
        puts("(SerialPortWriter) Error Opening");

        // If shit happened, then try to open the ttyACM0
        strcpy(serialPortName, "/dev/ttyACM1");
        std::cout << "(SerialPortWriter) Opening serial port" << serialPortName << "..." << std::endl;
        fdHandler = open(serialPortName, O_NONBLOCK | O_RDWR | O_NOCTTY);
        if (fdHandler == -1) {
            //shit happened
            puts("(SerialPortWriter) Error Opening");
        }
    }

    puts("(SerialPortWriter) Port Open\n\rApply Settings");
    termiosP.c_iflag = 0/*IGNPAR | ICRNL*/;
    termiosP.c_oflag = 0;
    termiosP.c_cflag = baud/*|CRTSCTS*/| CREAD | CS8 | CLOCAL;
    termiosP.c_lflag = 0;
    termiosP.c_cc[VINTR] = 0; /* Ctrl-c */
    termiosP.c_cc[VQUIT] = 0; /* Ctrl-\ */
    termiosP.c_cc[VERASE] = 0; /* del */
    termiosP.c_cc[VKILL] = 0; /* @ */
    termiosP.c_cc[VEOF] = 0; /* Ctrl-d */
    termiosP.c_cc[VTIME] = 0; /* inter-character timer unused */
    termiosP.c_cc[VMIN] = 0; /* blocking read until 1 character arrives */
    termiosP.c_cc[VSWTC] = 0; /* ” */
    termiosP.c_cc[VSTART] = 0; /* Ctrl-q */
    termiosP.c_cc[VSTOP] = 0; /* Ctrl-s */
    termiosP.c_cc[VSUSP] = 0; /* Ctrl-z */
    termiosP.c_cc[VEOL] = 0; /* ” */
    termiosP.c_cc[VREPRINT] = 0; /* Ctrl-r */
    termiosP.c_cc[VDISCARD] = 0; /* Ctrl-u */
    termiosP.c_cc[VWERASE] = 0; /* Ctrl-w */
    termiosP.c_cc[VLNEXT] = 0; /* Ctrl-v */
    termiosP.c_cc[VEOL2] = 0; /* ” */
    tcflush(fdHandler, TCIFLUSH);
    if (tcsetattr(fdHandler, TCSANOW, &termiosP) == -1) {
        puts("(SerialPortWriter) Error setting configurations");
    }
    FD_ZERO(&reading);
}

SerialPortWriter::~SerialPortWriter() {
    close(fdHandler);
}

void SerialPortWriter::send(unsigned char *message, int size) {
    // Send message through serial
    write(fdHandler, message, size);
    tcflush(fdHandler, TCIOFLUSH);
}

void SerialPortWriter::blockingSend(unsigned char *message, int size) {
    send(message, size);
    tcdrain(fdHandler);
}

bool SerialPortWriter::receive(unsigned char *message, int size, int timeout) {
    bool success = true;

    // timeout configuration
    timeoutConf.tv_sec = 0;
    timeoutConf.tv_usec = timeout;
    FD_SET(fdHandler, &reading);

    if (select(fdHandler + 1, &reading, NULL, NULL, &timeoutConf) == 1)
        read(fdHandler, message, size);
    else
        success = false;

    FD_ZERO(&reading);

    return success;
}

void SerialPortWriter::fixFtdiLatencyTimer() {
    ioctl(fdHandler, TIOCGSERIAL, &ser_info);
    ser_info.flags |= ASYNC_LOW_LATENCY;
    ioctl(fdHandler, TIOCSSERIAL, &ser_info);

    // Wait 1 second to apply this setting
    sleep(1);
}


} /* namespace communication */

} /* namespace core */
