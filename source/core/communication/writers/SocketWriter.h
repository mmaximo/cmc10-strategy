#ifndef CORE_COMMUNICATION_SOCKETWRITER_H
#define CORE_COMMUNICATION_SOCKETWRITER_H

#include "messages.pb.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <boost/thread.hpp>


namespace core {
namespace communication {

class SocketWriter {

private:
    tools::simulator::server::communication::FieldInfoMessage simMessage;

    //networking stuff
    int sockfd, portno;
    struct addrinfo* res;

    //threading
    boost::thread comThread;
    boost::mutex comMutex;

    bool gotFirstMessage;
    
    void recvData(int sockID, void* buffer, int bufferLength);
    void sendData(int sockID, void* buffer, int bufferLength);

public:
    SocketWriter(int port);

    virtual ~SocketWriter();

    void connectToServer();

    void receiveMessage();

    tools::simulator::server::communication::FieldInfoMessage getLastMessage();

    void sendMessage(tools::simulator::server::communication::CommandInfoMessage &message);

    bool receivedMessage();

};

} /* namespace communication */

} /* namespace core */

#endif /* CORE_COMMUNICATION_SOCKETWRITER_H */
