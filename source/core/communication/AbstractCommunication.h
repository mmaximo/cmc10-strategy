#ifndef CORE_COMMUNICATION_ABSTRACTCOMMUNICATION_H
#define CORE_COMMUNICATION_ABSTRACTCOMMUNICATION_H

#include <control/ControlInterface.h>

namespace core {
namespace communication {

class AbstractCommunication {
public:
    virtual void send(control::ControlInterface &controlInterface) = 0;

    //virtual void getImage() = 0;

    virtual ~AbstractCommunication() {}
};

} /* namespace communication */

} /* namespace core */

#endif // CORE_COMMUNICATION_ABSTRACTCOMMUNICATION_H
