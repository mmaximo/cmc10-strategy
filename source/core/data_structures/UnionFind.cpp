//
// Created by mmaximo on 11/11/16.
//

#include "UnionFind.h"

namespace itandroids_lib {
namespace data_structures {

UnionFind::UnionFind(int numElements) {
    parent = new int[numElements];
    for (int i = 0; i < numElements; ++i)
        parent[i] = i;
}

UnionFind::~UnionFind() {
    delete[] parent;
}

int UnionFind::find(int index) {
    if (index == parent[index])
        return index;
    return (parent[index] = find(parent[index]));
}

} // namespace data_structures
} // namespace itandroids_lib