//
// Created by mmaximo on 11/11/16.
//

#ifndef ITANDROIDS_LIB_UNIONFIND_H
#define ITANDROIDS_LIB_UNIONFIND_H

namespace itandroids_lib {
namespace data_structures {

struct UnionFind {
    int *parent;

    UnionFind(int numElements);

    ~UnionFind();

    int find(int index);
};

} // namespace data_structures
} // namespace itandroids_lib

#endif //ITANDROIDS_LIB_UNIONFIND_H
