//
// Created by GRU on 01/06/17.
//

#ifndef CORE_GUI_GAMEWIDGET_H_
#define CORE_GUI_GAMEWIDGET_H_

#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QProgressBar>
#include <QSlider>
#include <QComboBox>
#include <QToolBar>
#include <QApplication>
#include <QMouseEvent>
#include <QTransform>
#include <QLabel>
#include <QGridLayout>
#include <team/SoccerTeam.h>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>
#include <QSpinBox>
#include <QtGui>

#include "gui/widgets/base/AbstractWindow.h"
#include "gui/Renderer.h"

namespace core {
namespace gui {

class GameWidget : public AbstractWindow {
Q_OBJECT
public:
    GameWidget(core::team::SoccerTeam *soccerTeam);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void setSelectedRobot(int robot) override;
    void load(std::ifstream &arc) override;
    void save() override;
    std::string getName() override;

private slots:
    void clickImageCheckBox(int);
    void clickCalibrationCheckBox(int);
    void clickFieldCheckBox(int);
    void clickPlayerCheckBox(int);
    void clickSpeedCheckBox(int);
    void clickStateCheckBox(int);
    void clickRoleCheckBox(int);
    void clickTrajectoryCheckBox(int);
    void clickStrategy1CheckBox(int);
    void clickStrategy2CheckBox(int);
    void clickControlCheckBox(int);
    void imageReceived(QMutex *, QImage *);

private:
    void saveSetUp();
    void loadSetUp();

    QCheckBox *imageCheckBox;
    QCheckBox *calibrationCheckBox;
    QCheckBox *fieldCheckBox;
    QCheckBox *playerCheckBox;
    QCheckBox *speedCheckBox;
    QCheckBox *stateCheckBox;
    QCheckBox *roleCheckBox;
    QCheckBox *trajectoryCheckBox;
    QCheckBox *strategy1CheckBox;
    QCheckBox *strategy2CheckBox;
    QCheckBox *controlCheckBox;
    QLabel *imageLabel;
    core::team::SoccerTeam *soccerTeam;
    int selectedRobot;
    bool loading;
};

} // gui
} // core

#endif
