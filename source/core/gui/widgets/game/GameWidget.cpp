//
// Created by GRU on 01/06/17.
//

#include "GameWidget.h"

namespace core {
namespace gui {

GameWidget::GameWidget(core::team::SoccerTeam *soccerTeam) {
    setMouseTracking(false);
    loading = true;
    this->soccerTeam = soccerTeam;

    this->imageLabel = new QLabel();
    imageCheckBox = new QCheckBox("Img");
    calibrationCheckBox = new QCheckBox("Calib");
    fieldCheckBox = new QCheckBox("Field");
    playerCheckBox = new QCheckBox("Player");
    speedCheckBox = new QCheckBox("Speed");
    stateCheckBox = new QCheckBox("Stat");
    roleCheckBox = new QCheckBox("Role");
    trajectoryCheckBox = new QCheckBox("Traje");
    strategy1CheckBox = new QCheckBox("Str1");
    strategy2CheckBox = new QCheckBox("Str2");
    controlCheckBox = new QCheckBox("Cont");

    connect(imageCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickImageCheckBox(int)));
    connect(calibrationCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickCalibrationCheckBox(int)));
    connect(fieldCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickFieldCheckBox(int)));
    connect(playerCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickPlayerCheckBox(int)));
    connect(speedCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickSpeedCheckBox(int)));
    connect(stateCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickStateCheckBox(int)));
    connect(roleCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickRoleCheckBox(int)));
    connect(trajectoryCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickTrajectoryCheckBox(int)));
    connect(strategy1CheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickStrategy1CheckBox(int)));
    connect(strategy2CheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickStrategy2CheckBox(int)));
    connect(controlCheckBox, SIGNAL(stateChanged(int)), this, SLOT(clickControlCheckBox(int)));
    connect(&Renderer::i(), SIGNAL(updateImage(QMutex * , QImage * )),
            this, SLOT(imageReceived(QMutex * , QImage * )));

    loadSetUp();

    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->setRowStretch(1, 1);
    mainLayout->addWidget(imageCheckBox, 0, 0);
    mainLayout->addWidget(calibrationCheckBox, 0, 1);
    mainLayout->addWidget(fieldCheckBox, 0, 2);
    mainLayout->addWidget(playerCheckBox, 0, 3);
    mainLayout->addWidget(speedCheckBox, 0, 4);
    mainLayout->addWidget(stateCheckBox, 0, 5);
    mainLayout->addWidget(roleCheckBox, 0, 6);
    mainLayout->addWidget(trajectoryCheckBox, 0, 7);
    mainLayout->addWidget(strategy1CheckBox, 0, 8);
    mainLayout->addWidget(strategy2CheckBox, 0, 9);
    mainLayout->addWidget(controlCheckBox, 0, 10);
    mainLayout->addWidget(imageLabel, 1, 0, -1, -1, Qt::AlignCenter);

    this->setLayout(mainLayout);
    Renderer::i().setImageViewer(true);
}

void GameWidget::imageReceived(QMutex *qMutex, QImage *qImage) {
    qMutex->lock();
    imageLabel->setPixmap(QPixmap::fromImage(*qImage));
    qMutex->unlock();
}

void GameWidget::clickImageCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::IMAGE, state != Qt::Unchecked);
    imageCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickCalibrationCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::CALIBRATION, state != Qt::Unchecked);
    calibrationCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickFieldCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::FIELD, state != Qt::Unchecked);
    fieldCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickPlayerCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::PLAYER, state != Qt::Unchecked);
    playerCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickSpeedCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::SPEED, state != Qt::Unchecked);
    speedCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickRoleCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::ROLE, state != Qt::Unchecked);
    roleCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickTrajectoryCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::TRAJECTORY, state != Qt::Unchecked);
    trajectoryCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickStrategy1CheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::STRATEGY1, state != Qt::Unchecked);
    strategy1CheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickStrategy2CheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::STRATEGY2, state != Qt::Unchecked);
    strategy2CheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickStateCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::STATE, state != Qt::Unchecked);
    stateCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::clickControlCheckBox(int state) {
    Renderer::i().setPermission(DRAW_ID::CONTROL, state != Qt::Unchecked);
    controlCheckBox->setChecked(state != Qt::Unchecked);
    saveSetUp();
}

void GameWidget::mouseMoveEvent(QMouseEvent *ev) { }

void GameWidget::mousePressEvent(QMouseEvent *ev) {
    if (!soccerTeam->getIsPlaying()) {
        soccerTeam->moveRobot(selectedRobot,
                              soccerTeam->getVision()->getCameraProjection()->project2DTo3D
                                      (Vector2<double>(ev->pos().x() - imageLabel->x(),
                                                       ev->pos().y() - imageLabel->y()),
                                       core::representations::Player::TOTAL_HEIGHT));
    }
}

void GameWidget::mouseReleaseEvent(QMouseEvent *ev) {
//    std::cout<<"mouseReleaseEvent\n";
}

void GameWidget::setSelectedRobot(int robot) {
    this->selectedRobot = robot;
}

void GameWidget::loadSetUp() {
    loading = true;
    std::ifstream arq;
    std::string name;
    int val;
    arq.open("../configs/guiSetUp.txt");
    if (arq.is_open()) {
        printf("\x1b[34m[SUCCESS]\x1b[0m Successfully loaded GUI config.\n");
        for (int i = 0; i < 10; i++) {
            arq >> name >> val;
            std::cout<<"name = "<<name<<"\tval = "<<val<<std::endl;
            if (name == "Image:") clickImageCheckBox(val);
            else if (name == "Calibration:") clickCalibrationCheckBox(val);
            else if (name == "Field:") clickFieldCheckBox(val);
            else if (name == "Player:") clickPlayerCheckBox(val);
            else if (name == "Speed:") clickSpeedCheckBox(val);
            else if (name == "Role:") clickRoleCheckBox(val);
            else if (name == "Trajectory:") clickTrajectoryCheckBox(val);
            else if (name == "Strategy1:") clickStrategy1CheckBox(val);
            else if (name == "Strategy2:") clickStrategy2CheckBox(val);
            else if (name == "Control:") clickControlCheckBox(val);
            else if (name == "State:") clickStateCheckBox(val);
        }
    } else {//create new file
        printf("\x1b[33m[WARNING]\x1b[0m Couldn't find gui config file, creating a new one.\n");
        clickImageCheckBox(Qt::Checked);
        clickCalibrationCheckBox(Qt::Unchecked);
        clickFieldCheckBox(Qt::Checked);
        clickPlayerCheckBox(Qt::Checked);
        clickSpeedCheckBox(Qt::Unchecked);
        clickRoleCheckBox(Qt::Unchecked);
        clickTrajectoryCheckBox(Qt::Checked);
        clickStrategy1CheckBox(Qt::Unchecked);
        clickStrategy2CheckBox(Qt::Unchecked);
        clickControlCheckBox(Qt::Unchecked);
        clickStateCheckBox(Qt::Unchecked);
        loading = false;
        saveSetUp();
    }
    loading = false;
}

void GameWidget::load(std::ifstream &arc) { }

void GameWidget::save() { }

std::string GameWidget::getName() {
    return "GameWidget";
}

void GameWidget::saveSetUp() {
    if (loading)
        return;

    std::ofstream arq("../configs/guiSetUp.txt");

    if (arq.is_open()) {
        arq << "Image: " << imageCheckBox->isChecked() << "\n";
        arq << "Field: " << fieldCheckBox->isChecked() << "\n";
        arq << "Player: " << playerCheckBox->isChecked() << "\n";
        arq << "Speed: " << speedCheckBox->isChecked() << "\n";
        arq << "State: " << stateCheckBox->isChecked() << "\n";
        arq << "Role: " << roleCheckBox->isChecked() << "\n";
        arq << "Trajectory: " << trajectoryCheckBox->isChecked() << "\n";
        arq << "Strategy1: " << strategy1CheckBox->isChecked() << "\n";
        arq << "Strategy2: " << strategy2CheckBox->isChecked() << "\n";
        arq << "Control: " << controlCheckBox->isChecked() << "\n";
        arq.close();
        printf("\x1b[34m[SUCCESS]\x1b[0m Successfully saved GUI config.\n");
    } else {
        printf("\x1b[33m[WARNING]\x1b[0m Couldn't save GUI config.\n");
    }
}

} // gui
} // core
