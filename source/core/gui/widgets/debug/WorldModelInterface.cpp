//
// Created by GRU on 02/03/18.
//

#include "gui/widgets/debug/WorldModelInterface.h"
#include <modeling/vision/VisionInterface.h>

namespace core {
namespace gui {

// Uses 'Singleton' Design Pattern (static implementation)
// https://sourcemaking.com/design_patterns/singleton
WorldModelInterface &WorldModelInterface::getInstance() {
    static WorldModelInterface instance;
    return instance;
};

WorldModelInterface::WorldModelInterface() {
    localizationInterface = new modeling::VisionInterface();
}

WorldModelInterface::~WorldModelInterface() {
    delete localizationInterface;
}

void WorldModelInterface::emitSignal(modeling::VisionInterface *original) {
    qMutex.lock();
    (*localizationInterface) = *original;
    qMutex.unlock();

    emit worldModelUpdateSignal(localizationInterface, &qMutex);
}

} // gui
} // core
