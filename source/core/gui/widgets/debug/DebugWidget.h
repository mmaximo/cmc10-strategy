//
// Created by GRU on 02/03/18.
//

#ifndef CORE_GUI_DEBUGWIDGET_H_
#define CORE_GUI_DEBUGWIDGET_H_

#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QProgressBar>
#include <QSlider>
#include <QComboBox>
#include <QToolBar>
#include <QApplication>
#include <QMouseEvent>
#include <QTransform>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <team/SoccerTeam.h>
#include <QComboBox>
#include <QCheckBox>
#include <QSpinBox>
#include <QtGui>

#include "gui/widgets/base/AbstractWindow.h"
#include "gui/widgets/debug/DebugWidgetInfos.h"
#include "gui/widgets/debug/WorldModelInterface.h"
#include "gui/Renderer.h"

namespace core {
namespace gui {

class DebugWidget : public AbstractWindow {
Q_OBJECT

public:
    DebugWidget(core::team::SoccerTeam *soccerTeam);
    void load(std::ifstream &arc) override;
    void save() override;
    std::string getName() override;
    void setSelectedRobot(int robot) override;

public slots:
    void updateParams(modeling::VisionInterface *, QMutex *);

private:
    void putDouble(QLabel *lbl, double value);

    QGridLayout *mainLayout;
    DebugWidgetInfos *infos[4];
    core::team::SoccerTeam *soccerTeam;
    bool updating;
    QLabel *xLbl;
    QLabel *yLbl;
    QLabel *thetaLbl;
    QLabel *vxLbl;
    QLabel *vyLbl;
    QLabel *vModLbl;

    QLabel *ballLbl1;
    QLabel *ballLbl2;
    QLabel *ballPosXLbl;
    QLabel *ballPosYLbl;
    QLabel *ballPosThetaLbl;
    QLabel *ballSpeedXLbl;
    QLabel *ballSpeedYLbl;
    QLabel *ballSpeedModLbl;

    QLabel *robot0Lbl1;
    QLabel *robot0Lbl2;
    QLabel *robot0PosXLbl;
    QLabel *robot0PosYLbl;
    QLabel *robot0PosThetaLbl;
    QLabel *robot0SpeedXLbl;
    QLabel *robot0SpeedYLbl;
    QLabel *robot0SpeedModLbl;

    QLabel *robot1Lbl1;
    QLabel *robot1Lbl2;
    QLabel *robot1PosXLbl;
    QLabel *robot1PosYLbl;
    QLabel *robot1PosThetaLbl;
    QLabel *robot1SpeedXLbl;
    QLabel *robot1SpeedYLbl;
    QLabel *robot1SpeedModLbl;

    QLabel *robot2Lbl1;
    QLabel *robot2Lbl2;
    QLabel *robot2PosXLbl;
    QLabel *robot2PosYLbl;
    QLabel *robot2PosThetaLbl;
    QLabel *robot2SpeedXLbl;
    QLabel *robot2SpeedYLbl;
    QLabel *robot2SpeedModLbl;

    bool ballAlreadyLost;
    bool robot0AlreadyLost;
    bool robot1AlreadyLost;
    bool robot2AlreadyLost;
};
}/* namespace gui */
}/* namespace core */

#endif //CORE_GUI_DEBUGWIDGET_H_