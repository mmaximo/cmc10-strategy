//
// Created by GRU on 02/03/18.
//

#include "gui/widgets/debug/DebugWidget.h"

namespace core {
namespace gui {

const int IdRole = Qt::UserRole;

DebugWidget::DebugWidget(core::team::SoccerTeam *soccerTeam) {
    this->soccerTeam = soccerTeam;

    connect(&core::gui::WorldModelInterface::getInstance(),
            SIGNAL(worldModelUpdateSignal(modeling::LocalizationInterface * ,
                                          QMutex * )),
            this, SLOT(updateParams(modeling::LocalizationInterface * ,
                                    QMutex * )));

    mainLayout = new QGridLayout;

    xLbl = new QLabel("x");
    yLbl = new QLabel("y");
    thetaLbl = new QLabel("theta");
    vxLbl = new QLabel("vx");
    vyLbl = new QLabel("vy");
    vModLbl = new QLabel("mod");

    ballLbl1 = new QLabel("Ball");
    ballLbl2 = new QLabel("Ball");
    ballPosXLbl = new QLabel("NAN");
    ballPosYLbl = new QLabel("NAN");
    ballPosThetaLbl = new QLabel("NAN");
    ballSpeedXLbl = new QLabel("NAN");
    ballSpeedYLbl = new QLabel("NAN");
    ballSpeedModLbl = new QLabel("NAN");

    robot0Lbl1 = new QLabel("robot0");
    robot0Lbl2 = new QLabel("robot0");
    robot0PosXLbl = new QLabel("NAN");
    robot0PosYLbl = new QLabel("NAN");
    robot0PosThetaLbl = new QLabel("NAN");
    robot0SpeedXLbl = new QLabel("NAN");
    robot0SpeedYLbl = new QLabel("NAN");
    robot0SpeedModLbl = new QLabel("NAN");

    robot1Lbl1 = new QLabel("robot1");
    robot1Lbl2 = new QLabel("robot1");
    robot1PosXLbl = new QLabel("NAN");
    robot1PosYLbl = new QLabel("NAN");
    robot1PosThetaLbl = new QLabel("NAN");
    robot1SpeedXLbl = new QLabel("NAN");
    robot1SpeedYLbl = new QLabel("NAN");
    robot1SpeedModLbl = new QLabel("NAN");

    robot2Lbl1 = new QLabel("robot2");
    robot2Lbl2 = new QLabel("robot2");
    robot2PosXLbl = new QLabel("NAN");
    robot2PosYLbl = new QLabel("NAN");
    robot2PosThetaLbl = new QLabel("NAN");
    robot2SpeedXLbl = new QLabel("NAN");
    robot2SpeedYLbl = new QLabel("NAN");
    robot2SpeedModLbl = new QLabel("NAN");

    mainLayout->addWidget(xLbl, 0, 1);
    mainLayout->addWidget(yLbl, 0, 2);
    mainLayout->addWidget(thetaLbl, 0, 3);

    mainLayout->addWidget(ballLbl1, 1, 0);
    mainLayout->addWidget(ballPosXLbl, 1, 1);
    mainLayout->addWidget(ballPosYLbl, 1, 2);
    mainLayout->addWidget(ballPosThetaLbl, 1, 3);

    mainLayout->addWidget(robot0Lbl1, 2, 0);
    mainLayout->addWidget(robot0PosXLbl, 2, 1);
    mainLayout->addWidget(robot0PosYLbl, 2, 2);
    mainLayout->addWidget(robot0PosThetaLbl, 2, 3);

    mainLayout->addWidget(robot1Lbl1, 3, 0);
    mainLayout->addWidget(robot1PosXLbl, 3, 1);
    mainLayout->addWidget(robot1PosYLbl, 3, 2);
    mainLayout->addWidget(robot1PosThetaLbl, 3, 3);

    mainLayout->addWidget(robot2Lbl1, 4, 0);
    mainLayout->addWidget(robot2PosXLbl, 4, 1);
    mainLayout->addWidget(robot2PosYLbl, 4, 2);
    mainLayout->addWidget(robot2PosThetaLbl, 4, 3);

    mainLayout->addWidget(vxLbl, 5, 1);
    mainLayout->addWidget(vyLbl, 5, 2);
    mainLayout->addWidget(vModLbl, 5, 3);

    mainLayout->addWidget(ballLbl2, 6, 0);
    mainLayout->addWidget(ballSpeedXLbl, 6, 1);
    mainLayout->addWidget(ballSpeedYLbl, 6, 2);
    mainLayout->addWidget(ballSpeedModLbl, 6, 3);

    mainLayout->addWidget(robot0Lbl2, 7, 0);
    mainLayout->addWidget(robot0SpeedXLbl, 7, 1);
    mainLayout->addWidget(robot0SpeedYLbl, 7, 2);
    mainLayout->addWidget(robot0SpeedModLbl, 7, 3);

    mainLayout->addWidget(robot1Lbl2, 8, 0);
    mainLayout->addWidget(robot1SpeedXLbl, 8, 1);
    mainLayout->addWidget(robot1SpeedYLbl, 8, 2);
    mainLayout->addWidget(robot1SpeedModLbl, 8, 3);

    mainLayout->addWidget(robot2Lbl2, 9, 0);
    mainLayout->addWidget(robot2SpeedXLbl, 9, 1);
    mainLayout->addWidget(robot2SpeedYLbl, 9, 2);
    mainLayout->addWidget(robot2SpeedModLbl, 9, 3);

    ballAlreadyLost = true;
    robot0AlreadyLost = true;
    robot1AlreadyLost = true;
    robot2AlreadyLost = true;
    this->setLayout(mainLayout);
    updating = false;
}

void DebugWidget::updateParams(modeling::VisionInterface *localizationInterface,
                               QMutex *qMutex) {
    qMutex->lock();
    Vector2<double> pos, speed;
//    double posAngle, speedAngle;

    if (localizationInterface->ball.isSeen()) {
        pos = localizationInterface->ball.getPosition();
        speed = localizationInterface->ball.getVelocity();
        putDouble(ballPosXLbl, pos.x);
        putDouble(ballPosYLbl, pos.y);
        putDouble(ballPosThetaLbl, pos.angle());
        putDouble(ballSpeedXLbl, speed.x);
        putDouble(ballSpeedYLbl, speed.y);
        putDouble(ballSpeedModLbl, speed.abs());
        ballAlreadyLost = false;
    } else if (!ballAlreadyLost) {
        ballPosXLbl->setText("NAN");
        ballPosYLbl->setText("NAN");
        ballPosThetaLbl->setText("NAN");
        ballSpeedXLbl->setText("NAN");
        ballSpeedYLbl->setText("NAN");
        ballSpeedModLbl->setText("NAN");
        ballAlreadyLost = true;
    }

    if (localizationInterface->teammates[0].isSeen()) {
        pos = localizationInterface->teammates[0].getPosition();
        speed = localizationInterface->teammates[0].getVelocity();
        putDouble(robot0PosXLbl, pos.x);
        putDouble(robot0PosYLbl, pos.y);
        putDouble(robot0PosThetaLbl, pos.angle());
        putDouble(robot0SpeedXLbl, speed.x);
        putDouble(robot0SpeedYLbl, speed.y);
        putDouble(robot0SpeedModLbl, speed.abs());
        robot0AlreadyLost = false;
    } else if (!robot0AlreadyLost) {
        robot0PosXLbl->setText("NAN");
        robot0PosYLbl->setText("NAN");
        robot0PosThetaLbl->setText("NAN");
        robot0SpeedXLbl->setText("NAN");
        robot0SpeedYLbl->setText("NAN");
        robot0SpeedModLbl->setText("NAN");
        robot0AlreadyLost = true;
    }

    if (localizationInterface->teammates[1].isSeen()) {
        pos = localizationInterface->teammates[1].getPosition();
        speed = localizationInterface->teammates[1].getVelocity();
        putDouble(robot1PosXLbl, pos.x);
        putDouble(robot1PosYLbl, pos.y);
        putDouble(robot1PosThetaLbl, pos.angle());
        putDouble(robot1SpeedXLbl, speed.x);
        putDouble(robot1SpeedYLbl, speed.y);
        putDouble(robot1SpeedModLbl, speed.abs());
        robot1AlreadyLost = false;
    } else if (!robot1AlreadyLost) {
        robot1PosXLbl->setText("NAN");
        robot1PosYLbl->setText("NAN");
        robot1PosThetaLbl->setText("NAN");
        robot1SpeedXLbl->setText("NAN");
        robot1SpeedYLbl->setText("NAN");
        robot1SpeedModLbl->setText("NAN");
        robot1AlreadyLost = true;
    }

    if (localizationInterface->teammates[2].isSeen()) {
        pos = localizationInterface->teammates[2].getPosition();
        speed = localizationInterface->teammates[2].getVelocity();
        putDouble(robot2PosXLbl, pos.x);
        putDouble(robot2PosYLbl, pos.y);
        putDouble(robot2PosThetaLbl, pos.angle());
        putDouble(robot2SpeedXLbl, speed.x);
        putDouble(robot2SpeedYLbl, speed.y);
        putDouble(robot2SpeedModLbl, speed.abs());
        robot2AlreadyLost = false;
    } else if (!robot2AlreadyLost) {
        robot2PosXLbl->setText("NAN");
        robot2PosYLbl->setText("NAN");
        robot2PosThetaLbl->setText("NAN");
        robot2SpeedXLbl->setText("NAN");
        robot2SpeedYLbl->setText("NAN");
        robot2SpeedModLbl->setText("NAN");
        robot2AlreadyLost = true;
    }
    qMutex->unlock();
}

void DebugWidget::putDouble(QLabel *lbl, double value) {
    lbl->setText(QString::number(value, 'f', 2));
}

void DebugWidget::load(std::ifstream &arc) {
}

void DebugWidget::save() {
}

std::string DebugWidget::getName() {
    return "DebugWidget";
}

void DebugWidget::setSelectedRobot(int robot) {

}

}/* namespace gui */
}/* namespace core */