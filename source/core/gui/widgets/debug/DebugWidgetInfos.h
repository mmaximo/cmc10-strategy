//
// Created by GRU on 02/03/18.
//

#ifndef CORE_GUI_DEBUGWIDGETINFOS_H_
#define CORE_GUI_DEBUGWIDGETINFOS_H_

#include <QLabel>
#include <QtCore/QObject>
#include <QtWidgets/QGridLayout>

#include "representations/Ball.h"
#include "representations/Player.h"
#include "representations/OpponentPlayer.h"

namespace core {
namespace gui {

class DebugWidgetInfos : public QWidget {
Q_OBJECT
public:
    DebugWidgetInfos();
    void update(double x, double y, double theta);

private:

    QLabel *xLbl;
    QLabel *yLbl;
    QLabel *thetaLbl;
};

};
};

#endif //CORE_GUI_DEBUGWIDGETINFOS_H_
