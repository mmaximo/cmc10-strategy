//
// Created by GRU on 02/03/18.
//

#include "gui/widgets/debug/DebugWidgetInfos.h"

namespace core {
namespace gui {

DebugWidgetInfos::DebugWidgetInfos() {
    this->update(0, 0, 0);

    xLbl = new QLabel("X: 0.000");
    yLbl = new QLabel("Y: 0.000");
    thetaLbl = new QLabel("Theta: 0.000");

    QGridLayout *mainLayout = new QGridLayout();
    mainLayout->setRowStretch(1, 1);
    mainLayout->addWidget(xLbl, 0, 0, Qt::AlignLeft);
    mainLayout->setColumnStretch(0, 8);
    mainLayout->addWidget(yLbl, 0, 1, Qt::AlignLeft);
    mainLayout->setColumnStretch(1, 8);
    mainLayout->addWidget(thetaLbl, 0, 2, Qt::AlignLeft);
    mainLayout->setColumnStretch(0, 8);
    this->setLayout(mainLayout);
}

void DebugWidgetInfos::update(double x, double y, double theta) {
    QString aux;

    aux = "X: " + QString::number(x);
//        xLbl->setText(QString::number(x));
//        xLbl->setText(QString::number(x));
//        aux = "Y: "+QString::number(y);
//        yLbl->setText(aux);
//        aux = "theta: "+QString::number(theta);
//        thetaLbl->setText(aux);

}

}
}
