//
// Created by GRU on 02/03/18.
//

#ifndef CORE_GUI_WORLDMODELINTERFACE_H
#define CORE_GUI_WORLDMODELINTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <modeling/vision/VisionInterface.h>

#include "modeling/vision/VisionInterface.h"

namespace core {
namespace gui {

class WorldModelInterface : public QObject {
Q_OBJECT
public:
    static WorldModelInterface &getInstance();
    void emitSignal(modeling::VisionInterface*);

signals:
    void worldModelUpdateSignal(modeling::VisionInterface*, QMutex *);

private:
    WorldModelInterface(); // Prevent construction
    WorldModelInterface(const WorldModelInterface &); // Prevent construction by copying
    WorldModelInterface & operator = (const WorldModelInterface &); // Prevent assignment
    ~WorldModelInterface(); // Prevent unwanted destruction

    QMutex qMutex;
    modeling::VisionInterface *localizationInterface;
};

}; // gui
}; // core

#endif
