//
// Created by GRU on 29/11/17.
//

#ifndef CORE_GUI_ABSTRACTWINDOW_H
#define CORE_GUI_ABSTRACTWINDOW_H

#include <QWidget>

namespace core {
namespace gui {

class AbstractWindow : public QWidget {
public:
    virtual void save() = 0;
    virtual void load(std::ifstream &arc) = 0;
    virtual std::string getName() = 0;
    virtual void setSelectedRobot(int robot) = 0;
};

}
}

#endif //CORE_GUI_ABSTRACTWINDOW_H
