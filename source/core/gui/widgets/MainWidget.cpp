//
// Created by GRU on 05/06/17.
//

#include "MainWidget.h"

namespace core {
namespace gui {

MainWidget::MainWidget(core::team::SoccerTeam *soccerTeam){
    this->loading = true;
    this->setMouseTracking(false);

    for (int i = 0; i < 4; i++)
        qWidget[i] = nullptr;

    this->soccerTeam = soccerTeam;

    playButton = new QPushButton(tr("&Play"));

//    if (ReplayLog::getInstance().info.trackingEnabled)
//        this->trackButton = new QPushButton(tr("T&rack"));
//    else
        this->trackButton = nullptr;

    stopButton = new QPushButton(tr("&Stop"));

    invertButton = new QPushButton("Invert");
    invertButton->setStyleSheet("QPushButton {background-color:red; color: blue;}");

    changeUniformButton = new QPushButton(tr("Se&t yellow"));
    changeUniformButton->setStyleSheet("QPushButton {background-color:blue; color: yellow;}");
    selectRobotCombo = new QComboBox(this);
    selectRobotCombo->addItem("robot 0");
    selectRobotCombo->addItem("robot 1");
    selectRobotCombo->addItem("robot 2");
    selectRobotCombo->addItem("team");

    customActionCombo = new QComboBox(this);
    customActionCombo->addItem(":)");
    customActionCombo->addItem("Alive");
    customActionCombo->addItem("Warm up");
    customActionCombo->addItem("Comemoration");
    customActionCombo->addItem("Parade");

    selectStrategyCombo = new QComboBox(this);
    selectStrategyCombo->addItem("Offensive");
    selectStrategyCombo->addItem("Defensive");
    selectStrategyCombo->addItem("Random");

    connect(playButton, SIGNAL(clicked()), this, SLOT(playButtonClicked()));
    connect(stopButton, SIGNAL(clicked()), this, SLOT(stopButtonClicked()));
    connect(invertButton, SIGNAL(clicked()), this, SLOT(invertButtonClicked()));
    connect(changeUniformButton, SIGNAL(clicked()), this, SLOT(chUniformButtonClicked()));
    connect(selectRobotCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(handleRobotSelect(int)));
    connect(customActionCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(playCustomAction(int)));
    connect(selectStrategyCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(handleStrategySelect(int)));

//    if(ReplayLog::getInstance().info.trackingEnabled)
//        connect(this->trackButton, SIGNAL(clicked()), this, SLOT(trackButtonClicked()));

    quantQWidget = 0;
    handleRobotSelect(0);
    resetLayout();
    load();
    setWindowTitle(tr("ITAndroids VSS"));
}

void MainWidget::resetLayout() {
    mainLayout = new QGridLayout;
    mainLayout->addWidget(playButton, 3, 0, Qt::AlignRight);
    mainLayout->addWidget(stopButton, 3, 1, Qt::AlignLeft);
    mainLayout->addWidget(invertButton, 3, 2, Qt::AlignRight);
    mainLayout->addWidget(changeUniformButton, 3, 3, Qt::AlignLeft);
    mainLayout->addWidget(selectRobotCombo, 3, 4, Qt::AlignRight);
    mainLayout->addWidget(customActionCombo, 3, 5, Qt::AlignLeft);
    mainLayout->addWidget(selectStrategyCombo, 3, 6, Qt::AlignRight);

//    if(ReplayLog::getInstance().info.trackingEnabled)
//        mainLayout->addWidget(trackButton, 3, 6, Qt::AlignRight);

    setLayout(mainLayout);
}

void MainWidget::resetQWidget() {
    for (int i = 0; i < quantQWidget; i++)
        qWidget[i]->deleteLater();
    quantQWidget = 0;
}

void MainWidget::changeToBehaviorWidget() {
    resetQWidget();
    qWidget[0] = new GameWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 2, -1);
//    qWidget[0] = new BehaviorWidget(soccerTeam);
//    mainLayout->addWidget(qWidget[0], 0, 0, 2, 6);
//    quantQWidget = 1;
    quantQWidget = 1;
    this->save();
}

void MainWidget::changeToGameWidget() {
    resetQWidget();
    qWidget[0] = new GameWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 2, -1);
    quantQWidget = 1;
    this->save();
}

void MainWidget::changeToSplitWidget() {
    resetQWidget();
    qWidget[0] = new GameWidget(soccerTeam);
//    qWidget[1] = new BehaviorWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 2, 3);
//    mainLayout->addWidget(qWidget[1], 0, 3, 2, -1);
//    quantQWidget = 2;
    quantQWidget = 1;
    this->save();
}

void MainWidget::changeToFourWidget() {
    resetQWidget();
    qWidget[0] = new GameWidget(soccerTeam);
//    qWidget[1] = new BehaviorWidget(soccerTeam);
//    qWidget[2] = new BehaviorWidget(soccerTeam);
//    qWidget[3] = new BehaviorWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 1, 3);
//    mainLayout->addWidget(qWidget[1], 0, 3, 1, -1);
//    mainLayout->addWidget(qWidget[2], 1, 0, 1, 3);
//    mainLayout->addWidget(qWidget[3], 1, 3, 1, -1);
//    quantQWidget = 4;
    quantQWidget = 1;
    this->save();
}

void MainWidget::changeToMonitorWidget(){
    resetQWidget();
    qWidget[0] = new GameWidget(soccerTeam);
    qWidget[1] = new MonitorWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 1, 3);
    mainLayout->addWidget(qWidget[1], 0, 3, 1, -1);
    quantQWidget = 2;
    this->save();
}

void MainWidget::changeToDebugWidget() {
    resetQWidget();
    qWidget[0] = new DebugWidget(soccerTeam);
    qWidget[1] = new GameWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 2, 3);
    mainLayout->addWidget(qWidget[1], 0, 3, 2, -1);
    quantQWidget = 2;
    this->save();
}

void MainWidget::changeToFullMode() {
    resetQWidget();
    qWidget[0] = new DebugWidget(soccerTeam);
    qWidget[1] = new GameWidget(soccerTeam);
    qWidget[2] = new MonitorWidget(soccerTeam);
    mainLayout->addWidget(qWidget[0], 0, 0, 2, 2);
    mainLayout->addWidget(qWidget[1], 0, 2, 2, 3);
    mainLayout->addWidget(qWidget[2], 0, 5, 2, -1);
    quantQWidget = 3;
    this->save();
}

void MainWidget::playButtonClicked() {
    soccerTeam->start();
}

void MainWidget::playCustomAction(int action){
    if(action==0) return;
    soccerTeam->playCustomAction((core::team::CUSTOM_ACTION) (action-1));
    customActionCombo->setCurrentIndex(0);

    printf("\x1b[32m ACTION \x1b[0m The fun is on.\n");

}

void MainWidget::stopButtonClicked() {
    if(playButton->text()[1] == 'l'){//is in play
        soccerTeam->start();
        playButton->setText(tr("&Pause"));
    }else{//is in pause
        soccerTeam->pause();
        playButton->setText(tr("&Play"));
    }
}

void MainWidget::invertButtonClicked() {
    soccerTeam->invertCamera();
    invertButton->setText("Invert");

    if (soccerTeam->isInverted())
        invertButton->setStyleSheet("QPushButton {background-color:darkred; color: cyan;}");
    else
        invertButton->setStyleSheet("QPushButton {background-color:red; color: blue;}");

    this->save();
}

void MainWidget::chUniformButtonClicked() {
    if (soccerTeam->getUniform() == "blue") {
        changeUniformButton->setText(tr("Se&t blue"));
        changeUniformButton->setStyleSheet("QPushButton {background-color:yellow; color: blue;}");
    } else {
        changeUniformButton->setText(tr("Se&t yellow"));
        changeUniformButton->setStyleSheet("QPushButton {background-color:blue; color: yellow;}");
    }
    soccerTeam->changeUniform();
    this->save();
}

void MainWidget::trackButtonClicked() {
//    if(ReplayLog::getInstance().info.isTracking){
//        this->trackButton->setText(tr("T&rack"));
//        ReplayLog::getInstance().info.isTracking = false;
//    }else{
//        this->trackButton->setText(tr("Unt&rack"));
//        ReplayLog::getInstance().info.isTracking = true;
//    }
}

void MainWidget::handleStrategySelect(int strategy) {
    if(strategy==0) return;
    if(this->selectStrategyCombo->itemText(strategy) == "Random") {
        strategy = (int) std::time(0)%(this->selectStrategyCombo->count() - 1);
        this->selectStrategyCombo->setCurrentIndex(strategy);
    }

//    if(this->selectStrategyCombo->itemText(strategy) == "Defense"){
//        soccerTeam->setStrategy(core::decision_making::Strategies::DEFENSIVE);
//    }else if(this->selectStrategyCombo->itemText(strategy) == "Offensive"){
//        soccerTeam->setStrategy(core::decision_making::Strategies::OFFENSIVE);
//    }


    printf("\x1b[32m ACTION \x1b[0m The fun is on.\n");
}

void MainWidget::handleRobotSelect(int selectedRobot) {
    for (int i = 0; i < this->quantQWidget; i++) {
        if (this->qWidget[i] != nullptr)
            this->qWidget[i]->setSelectedRobot(selectedRobot);
    }
    this->selectedRobot = selectedRobot;
    this->save();
}

void MainWidget::save() {
    if (!loading) {
        std::ofstream arc;
        arc.open("../configs/guiSettings.txt");
        if (!arc.is_open()) {
            printf("\x1b[31m[ERROR]\x1b[0m Couldn't open file to save current gui settings.\n");
            return;
        }

        arc << "Invert: " << this->soccerTeam->isInverted() << '\n';
        arc << "UniformColor: " << this->soccerTeam->getUniform() << '\n';
        arc << "Robot: " << this->selectRobotCombo->currentIndex() << '\n';
        arc << "Strategy: " << this->selectStrategyCombo->currentIndex() << '\n';

        if (quantQWidget > 0) {
            arc << "QuantWidgets: " << this->quantQWidget << '\n';
            for (int i = 0; i < quantQWidget; i++) {
                arc << "\nWidget: " << qWidget[i]->getName() << '\n';
                qWidget[i]->save();
            }
        } else {
//            create a default version
            arc << "QuantWidgets: 1\n";
            arc << "Widget: GameWidget\n";
            arc << "Robot: 0\n";
        }

        arc.close();
        printf("\x1b[34m[SUCCESS]\x1b[0m Successfully saved the current gui settings.\n");
    }
}


void MainWidget::load() {
    std::ifstream arc;
    arc.open("../configs/guiSettings.txt");
    if (!arc.is_open()) {
        printf("\x1b[31m[ERROR]\x1b[0m Couldn't open file to load current GUI settings.\n");
        loading = false;
        this->save();
        return;
    }
    load(arc);
    arc.close();
    printf("\x1b[34m[SUCCESS]\x1b[0m Successfully loaded the current GUI settings.\n");
}


void MainWidget::load(std::ifstream &arc) {
    loading = true;
    handleRobotSelect(0);

    std::string command, value;
    int currCol = 0;
    while (arc >> command >> value) {
        if (command == "Invert:") {
            if ((value == "1") ^ soccerTeam->isInverted())
                this->invertButtonClicked();
        } else if (command == "UniformColor:") {
            if (value != soccerTeam->getUniform())
                this->chUniformButtonClicked();
        } else if (command == "Robot:") {
            this->selectRobotCombo->setCurrentIndex(value[0] - 48);
            handleRobotSelect(value[0] - 48);
        } else if (command == "Strategy:") {
            this->selectStrategyCombo->setCurrentIndex(value[0] - 48);
            handleStrategySelect(value[0] - 48);
        } else if (command == "QuantWidgets:") {
            this->quantQWidget = std::min(std::stoi(value), 3);
            for (int i = 0; i < this->quantQWidget; i++) {
                arc >> command >> value;
                int width = 6;
                if (value == "GameWidget") {
                    qWidget[i] = new GameWidget(soccerTeam);
                    width = 7;
                } else if (value == "BehaviorWidget") {
//                    qWidget[i] = new BehaviorWidget(soccerTeam);
                } else if (value == "MonitorWidget") {
                    qWidget[i] = new MonitorWidget(soccerTeam);
                    width = 1;
                } else if (value == "DebugWidget") {
                    qWidget[i] = new DebugWidget(soccerTeam);
                    width = 1;
                }
                mainLayout->addWidget(qWidget[i], 0, currCol, 1, width);
                currCol += width;
            }
        } else {
            std::cout << command << std::endl;
            printf("\x1b[33m[WARNING]\x1b[0m Couldn't understand command on GUI settings.\n");
        }
    }

    handleRobotSelect(this->selectedRobot);
    loading = false;
}

std::string MainWidget::getName() {
    return "MainWidget";
}

void MainWidget::setSelectedRobot(int robot) { }

} // gui
} // core
