//
// Created by GRU on 15/05/17.
//

#ifndef CORE_GUI_DATAMANAGER_H_
#define CORE_GUI_DATAMANAGER_H_

#include <cstdio>
#include <fstream>
#include <functional>
#include <map>
#include <vector>

#include <QMutex>

namespace core {
namespace gui {

class DataManager {
public:
    static DataManager &getInstance();
    void save();
    void save(std::string key, std::map<std::string, double> values);
    void load();
    double *registerParam(std::string group, std::string key);
    std::vector<std::string> getParamGroupNames();
    std::map<std::string, double> &getParamMap(std::string group);
    void setParamValue(std::string group, std::string key, double value);

    QMutex &getMutex() {
        return mutex;
    }

private:
    DataManager(); // Prevent construction
    DataManager(const DataManager &); // Prevent construction by copying
    DataManager & operator = (const DataManager &); // Prevent assignment
    ~DataManager(); // Prevent unwanted destruction

    std::map<std::string, std::map<std::string, double> > params;
    std::map<std::string, std::map<std::string, double *> > paramsPointers;
    QMutex mutex;
};

}/* namespace gui */
}/* namespace core */

#endif //CORE_GUI_DATAMANAGER_H_
