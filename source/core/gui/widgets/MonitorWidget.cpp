//
// Created by GRU on 31/05/17.
//

#include "gui/widgets/MonitorWidget.h"

namespace core {
namespace gui {

MonitorWidget::MonitorWidget(core::team::SoccerTeam *soccerTeam){
    this->soccerTeam = soccerTeam;


    this->fpsCaptionLbl = new QLabel("FPS: ");
    this->fpsValueLbl = new QLabel("-1");


    for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
        this->robotCaptionLbl[i] = new QLabel(tr("Robot %1: ").arg(i));
        this->robotValueLbl[i] = new QLabel("-1%");
    }

    mainLayout = new QGridLayout;

    this->setLayout(mainLayout);
    mainLayout->setRowStretch(1, 1);
    mainLayout->addWidget(fpsCaptionLbl, 0, 0);
    mainLayout->addWidget(fpsValueLbl, 0, 1);

    connect(&Renderer::i(), SIGNAL(updateFps(double)),
            this, SLOT(setFPS(double)));

    for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
        mainLayout->addWidget(robotCaptionLbl[i], i+2, 0);
        mainLayout->addWidget(robotValueLbl[i], i+2, 1);
    }


    setWindowTitle(tr("Basic Drawing"));
}

void MonitorWidget::load(std::ifstream &arc) {
}

void MonitorWidget::save() {
}

std::string MonitorWidget::getName() {
    return "MonitorWidget";
}

void MonitorWidget::setSelectedRobot(int robot) {
}

void MonitorWidget::setFPS(double fps){
    this->fpsValueLbl->setText(QString::number(fps, 'f', 2));
}

}/* namespace gui */
}/* namespace core */