//
// Created by GRU on 31/05/17.
//

#ifndef CORE_GUI_MONITORWIDGET_H
#define CORE_GUI_MONITORWIDGET_H

#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QProgressBar>
#include <QSlider>
#include <QComboBox>
#include <QToolBar>
#include <QApplication>
#include <QMouseEvent>
#include <QTransform>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <team/SoccerTeam.h>
#include <QComboBox>
#include <QCheckBox>
#include <QSpinBox>
#include <QtGui>

#include "gui/widgets/base/AbstractWindow.h"
#include "gui/Renderer.h"

namespace core {
namespace gui {

class MonitorWidget : public AbstractWindow {
Q_OBJECT

public:
    explicit MonitorWidget(core::team::SoccerTeam *soccerTeam);
    void load(std::ifstream &arc) override;
    void save() override;
    std::string getName() override;
    void setSelectedRobot(int robot) override;

public slots:
    void setFPS(double fps);




private:
    QGridLayout *mainLayout;

    core::team::SoccerTeam *soccerTeam;
    QLabel* fpsCaptionLbl;
    QLabel* fpsValueLbl;
    QLabel* robotCaptionLbl[core::representations::Player::PLAYERS_PER_SIDE];
    QLabel* robotValueLbl[core::representations::Player::PLAYERS_PER_SIDE];

};
}/* namespace gui */
}/* namespace core */

#endif //CORE_GUI_MONITORWIDGET_H_