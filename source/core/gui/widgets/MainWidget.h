//
// Created by GRU on 05/06/17.
//

#ifndef CORE_GUI_MAINWIDGET_H_
#define CORE_GUI_MAINWIDGET_H_

#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QProgressBar>
#include <QSlider>
#include <QComboBox>
#include <QToolBar>
#include <QApplication>
#include <QMouseEvent>
#include <QTransform>
#include <QLabel>
#include <QGridLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QSpinBox>
#include <QtGui>

#include "gui/widgets/base/AbstractWindow.h"
#include "gui/widgets/debug/DebugWidget.h"
#include "gui/widgets/game/GameWidget.h"
#include "gui/widgets/MonitorWidget.h"
#include "team/SoccerTeam.h"

namespace core {
namespace gui {

class MainWidget : public AbstractWindow {
Q_OBJECT

public:
    MainWidget(core::team::SoccerTeam *soccerTeam);
    void save() override;
    void load();
    void load(std::ifstream &arc) override;
    std::string getName();
    int selectedRobot;
    void setSelectedRobot(int robot) override;

public slots:
    void changeToGameWidget();
    void changeToBehaviorWidget();
    void changeToSplitWidget();
    void changeToFourWidget();
    void changeToMonitorWidget();
    void changeToDebugWidget();
    void changeToFullMode();

private slots:
    void stopButtonClicked();
    void playButtonClicked();
    void chUniformButtonClicked();
    void invertButtonClicked();
    void handleRobotSelect(int);
    void handleStrategySelect(int);
    void playCustomAction(int);
    void trackButtonClicked();

private:
    void resetLayout();
    void resetQWidget();

    core::team::SoccerTeam *soccerTeam;

    QPushButton *playButton;
    QPushButton *stopButton;
    QPushButton *changeUniformButton;
    QPushButton *invertButton;
    QPushButton *trackButton;
    QComboBox *selectRobotCombo;
    QComboBox *selectStrategyCombo;
    QComboBox *customActionCombo;

    AbstractWindow *qWidget[4];
    QGridLayout *mainLayout;
    int quantQWidget;
    bool loading;
};
} // gui
} // core

#endif
