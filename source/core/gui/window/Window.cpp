//
// Created by GRU on 31/05/17.
//

#include "Window.h"

namespace core {
namespace gui {

Window::Window(core::team::SoccerTeam *soccerTeam) {
    this->soccerTeam = soccerTeam;

    this->qWidget = new MainWidget(soccerTeam);
    this->setCentralWidget(this->qWidget);

    this->createActions();
    this->createMenus();
}

Window::~Window() {
    delete this->qWidget;
    delete this->modeMenu;
    delete this->changeBehaviorAct;
    delete this->changeGameAct;
    delete this->changeSplitAct;
    delete this->changeFourAct;
    delete this->changeMonitorAct;
    delete this->changeDebugAct;
    delete this->changeFullAct;
}

void Window::createActions() {
    changeBehaviorAct = new QAction(tr("&Behavior Mode"), this);
    changeBehaviorAct->setStatusTip(tr("Change to behavior mode screen."));
    connect(changeBehaviorAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToBehaviorWidget()));

    changeGameAct = new QAction(tr("&Game Mode"), this);
    changeGameAct->setStatusTip(tr("Change to game mode screen."));
    connect(changeGameAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToGameWidget()));

    changeSplitAct = new QAction(tr("&Split Mode"), this);
    changeSplitAct->setStatusTip(tr("Split the screen."));
    connect(changeSplitAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToSplitWidget()));

    changeFourAct = new QAction(tr("&Four Mode"), this);
    changeFourAct->setStatusTip(tr("Split the screen."));
    connect(changeFourAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToFourWidget()));

    changeMonitorAct = new QAction(tr("&Monitor Mode"), this);
    changeMonitorAct->setStatusTip(tr("Monitor time."));
    connect(changeMonitorAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToMonitorWidget()));

    changeDebugAct = new QAction(tr("&Debug Mode"), this);
    changeDebugAct->setStatusTip(tr("See all variables."));
    connect(changeDebugAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToDebugWidget()));

    changeFullAct = new QAction(tr("F&ull Mode"), this);
    changeFullAct->setStatusTip(tr("Ready for true developers."));
    connect(changeFullAct, SIGNAL(triggered()),
            this->qWidget, SLOT(changeToFullMode()));

    this->quitAct = new QAction(tr("&Exit"), this);
    this->quitAct->setStatusTip(tr("Close App D:"));
    this->quitAct->setShortcut(tr("ESC"));
    connect(this->quitAct, &QAction::triggered,
            qApp, QApplication::quit);
}

void Window::createMenus() {
    this->modeMenu = this->menuBar()->addMenu(tr("&Mode"));

    this->modeMenu->addAction(this->changeBehaviorAct);
    this->modeMenu->addAction(this->changeGameAct);
    this->modeMenu->addAction(this->changeSplitAct);
    this->modeMenu->addAction(this->changeFourAct);
    this->modeMenu->addAction(this->changeMonitorAct);
    this->modeMenu->addAction(this->changeDebugAct);
    this->modeMenu->addAction(this->changeFullAct);

    this->modeMenu->addSeparator();

    this->modeMenu->addAction(this->quitAct);
}

} // gui
} // core
