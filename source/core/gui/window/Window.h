//
// Created by GRU on 31/05/17.
//

#ifndef Window_H
#define Window_H

#include <QMainWindow>
#include <cstdio>
#include <QAction>
#include <QMenu>
#include <QWidget>
#include <QMenuBar>
#include <QPlainTextEdit>
#include <QtGui>

#include "gui/widgets/MainWidget.h"
#include "team/SoccerTeam.h"

namespace core {
namespace gui {

class Window : public QMainWindow {
Q_OBJECT

public:
    Window(core::team::SoccerTeam *);
    ~Window();

private:
    void createActions();
    void createMenus();

    core::team::SoccerTeam *soccerTeam;
    MainWidget *qWidget;
    QMenu *modeMenu;
    QAction *changeBehaviorAct;
    QAction *changeGameAct;
    QAction *changeSplitAct;
    QAction *changeFourAct;
    QAction *changeMonitorAct;
    QAction *changeDebugAct;
    QAction *changeFullAct;
    QAction *quitAct;
};

} // gui
} // core

#endif