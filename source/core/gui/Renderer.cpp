//
// Created by GRU on 08/03/17.
//

#include "gui/Renderer.h"

namespace core {
namespace gui {

// Doesn't use 'Singleton' Design Pattern (static implementation)
// Because its bad, really bad
Renderer &Renderer::i() {
    static Renderer instance;
    return instance;
}

Renderer::Renderer():
                  cameraProjection("../configs/intrinsicParameters.txt",
                                   "../configs/extrinsicParameters.txt") {

    webCamImage = cv::Mat(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);
    webCamImage.setTo(cv::Scalar(255, 255, 255));

    nullImage = cv::Mat(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);
    nullImage.setTo(cv::Scalar(0, 0, 0));

    image = cv::Mat(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);
    image.setTo(cv::Scalar(0, 0, 0));

    idPermited[(int) DRAW_ID::IMAGE] = true;
    imageViewerReady = false;

    this->setUpField();
    this->lastTimeStamp = std::chrono::system_clock::now();
}

Renderer::~Renderer() { }

void Renderer::drawImage() {
    if (imageViewerReady && rendererRefreshCounter % refreshPeriod == 0) {
        auto currTimeStamp = std::chrono::system_clock::now();
        std::chrono::duration<double> delta = currTimeStamp-this->lastTimeStamp;
        this->lastTimeStamp = currTimeStamp;

        rendererRefreshCounter = 1;

        qMutex.lock();
        QImage *qImage = new QImage(image.data, IMAGE_WIDTH, IMAGE_HEIGHT, QImage::Format_RGB888);
        qMutex.unlock();

        emit updateImage(&qMutex, qImage);
        if(delta.count()>0)
            emit updateFps(1.0/delta.count());
        else
            emit updateFps(2019);

        if (!this->checkPermission(DRAW_ID::IMAGE))
            image = nullImage.clone();
    }
    else
        rendererRefreshCounter++;
}

void Renderer::drawPlayers(WorldModel &wm, int invert) {
    if (!this->checkPermission(DRAW_ID::PLAYER))
        return;

    Vector2<double> pos;
    Ball &ball = wm.getBall();

    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        Player teamPlayer = wm.getTeammateById(i);
        OpponentPlayer opponentPlayer = wm.getOpponents()[i];
        if (teamPlayer.isSeen()) {
            pos = Vector2<double>(teamPlayer.getPosition().x * invert, invert * teamPlayer.getPosition().y);
            this->drawArrow(DRAW_ID::PLAYER,
                            pos,
                            Player::TOTAL_HEIGHT,
                            teamPlayer.getRotation() + (1 - invert) * M_PI/2,
                            Color::PERSIANBLUE,
                            3.5e-2);
        }
        if (opponentPlayer.isSeen()) {
            pos = Vector2<double>(opponentPlayer.getPosition().x * invert, invert * opponentPlayer.getPosition().y);
            this->drawCircle(DRAW_ID::PLAYER,
                             pos,
                             Player::TOTAL_HEIGHT,
                             4,
                             Color::LAVENDER,
                             4);
        }
    }
    if (ball.isSeen()) {
        pos = Vector2<double>(ball.getPosition().x * invert, invert * ball.getPosition().y);
        this->drawCircle(DRAW_ID::PLAYER,
                         pos,
                         Ball::DIAMETER,
                         4,
                         Color::DODGERBLUE,
                         4);
    }
}

void Renderer::drawSpeed(WorldModel &wm, int invert) {
    if (!this->checkPermission(DRAW_ID::SPEED))
        return;

    Vector2<double> pos;
    Ball &ball = wm.getBall();

    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
        Player &teamPlayer = wm.getTeammateById(i);
        OpponentPlayer &opponentPlayer = wm.getOpponents()[i];
        if (teamPlayer.isSeen()) {
            pos = Vector2<double>(teamPlayer.getPosition().x * invert, invert * teamPlayer.getPosition().y);
            this->drawArrow(DRAW_ID::SPEED,
                            pos,
                            Player::TOTAL_HEIGHT,
                            teamPlayer.getVelocity().angle() + (1 - invert) * M_PI/2,
                            Color::MAGENTA,
                            0.2 * teamPlayer.getVelocity().abs());
        }
        //there's no robot that can go faster than 10m/s
        if (opponentPlayer.isSeen() && opponentPlayer.getVelocity().abs() < 10) {
            pos = Vector2<double>(opponentPlayer.getPosition().x * invert, invert * opponentPlayer.getPosition().y);
            this->drawArrow(DRAW_ID::SPEED,
                            pos,
                            Player::TOTAL_HEIGHT,
                            opponentPlayer.getVelocity().angle() + (1 - invert) * M_PI/2,
                            Color::NAVY,
                            0.2 * opponentPlayer.getVelocity().abs());
        }
    }
    if (ball.isSeen()) {
        pos = Vector2<double>(ball.getPosition().x * invert, invert * ball.getPosition().y);
        this->drawArrow(DRAW_ID::SPEED,
                        pos,
                        Ball::DIAMETER,
                        ball.getVelocity().angle() + (1 - invert) * M_PI/2,
                        Color::FUCHSIA,
                        0.2 * ball.getVelocity().abs());
    }
}

void Renderer::drawField(int invert) {
    if (!this->checkPermission(DRAW_ID::FIELD))
        return;

    // borders and midlines
    this->drawPolyLines(DRAW_ID::FIELD, this->midLines, invert);
    this->drawPolyLines(DRAW_ID::FIELD, this->allyBorder, invert);
    this->drawPolyLines(DRAW_ID::FIELD, this->opponentBorder, invert);

    // arrows
    this->drawPolyArrows(DRAW_ID::FIELD, this->allyArrow, invert);
    this->drawPolyArrows(DRAW_ID::FIELD, this->opponentArrow, invert);
}

//void Renderer::drawStates(core::decision_making::States state) {
//    if (!this->checkPermission(DRAW_ID::STATE))
//        return;
//
//    Color color = Color::CORNFLOWERBLUE;
//    switch(state){
//        case core::decision_making::States::ROCK_DEFENSE:
//            color = Color::TAN;
//            break;
//        case core::decision_making::States::ROCK_DEFENSE_BACK:
//            color = Color::DARKPINK;
//            break;
//        case core::decision_making::States::TWO_ATTACKERS:
//            color = Color::DARKRED;
//            break;
//        case core::decision_making::States::ONE_ATTACKER_ONE_DEFENDER:
//            color = Color::DARKGREEN;
//            break;
//        case core::decision_making::States::TWO_DEFENDERS:
//            color = Color::DODGERBLUE;
//            break;
//        default:
//            color = Color::FUCHSIA;
//    }
//
//
//    // borders and midlines
//    cv::rectangle(image, cv::Point(5, 5), cv::Point(image.cols-5, image.rows-5), this->convertColorToScalar(color), 5);
//}

void Renderer::drawRoles(WorldModel &worldModel, int invert) {
    if(!this->checkPermission(DRAW_ID::ROLE))
        return;

//    for (int i = 0; i < Player::PLAYERS_PER_SIDE; i++) {
//        Player &teamMate = worldModel.getPlayerById(i);
//
//        Color color = Color::GRAY;
//        switch (teamMate.getRole()) {
//            case RoleList::GOALIER:
//                color = Color::RED;
//                break;
//            case RoleList::PRINCIPAL:
//                color = Color::GREEN;
//                break;
//            case RoleList::AUXILIARY:
//                color = Color::CYAN;
//                break;
//            case RoleList::DEFENDER:
//                color = Color::FUCHSIA;
//                break;
//        }
//
//        this->drawCircle(DRAW_ID::ROLE, teamMate.getPosition() * invert, Player::HEIGHT, 10, color, 5);
//    }
}

void Renderer::drawCalib(modeling::Image *segmentedImage) {
    if (segmentedImage == nullptr)
        return;

    if (this->checkPermission(DRAW_ID::CALIBRATION)) {
        unsigned char *segmentedData = segmentedImage->getData();
        for (int i = 0; i < IMAGE_HEIGHT; i++) {
            for (int j = 0; j < IMAGE_WIDTH; j++) {
                int curr = i * IMAGE_WIDTH + j;
                int first = 3 * curr;
                switch (segmentedData[curr]) {
                    case 1:
                        image.data[first] = 0;
                        image.data[first + 1] = 255;
                        image.data[first + 2] = 0;
                        break;
                    case 2:
                        image.data[first] = 255;
                        image.data[first + 1] = 255;
                        image.data[first + 2] = 255;
                        break;
                    case 3:
                        image.data[first] = 250;
                        image.data[first + 1] = 200;
                        image.data[first + 2] = 100;
                        break;
                    case 4:
                        image.data[first] = 250;
                        image.data[first + 1] = 150;
                        image.data[first + 2] = 200;
                        break;
                    case 5:
                        image.data[first] = 0;
                        image.data[first + 1] = 0;
                        image.data[first + 2] = 255;
                        break;
                    case 6:
                        image.data[first] = 250;
                        image.data[first + 1] = 250;
                        image.data[first + 2] = 100;
                        break;
                    case 7:
                        image.data[first] = 0;
                        image.data[first + 1] = 0;
                        image.data[first + 2] = 0;
                        break;
                    case 8:
                        image.data[first] = 255;
                        image.data[first + 1] = 0;
                        image.data[first + 2] = 0;
                        break;
                }
            }
        }
    }
}

void Renderer::setPermission(DRAW_ID drawId, bool newPermission) {
    idPermited[(int) drawId] = newPermission;
    if (drawId == DRAW_ID::IMAGE) {
        if (newPermission)
            image.data = webCamImage.data;
        else
            image = nullImage.clone();
    }
}

bool Renderer::checkPermission(DRAW_ID drawId) {
    return this->idPermited[(int) drawId];
}

void Renderer::setImageViewer(bool newValue) {
    this->imageViewerReady = newValue;
}

void Renderer::refreshImage(uchar *webcamImage) {
    this->webCamImage.data = webcamImage;
}

void Renderer::setUpField() {
    Color color = Color::GREEN;
    int thickness = 1;
    double angle, size;

    //ally goal area
    this->midLines.push_back(LineProperties {Vector2<double>(Field::LEFT_WALL_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             Vector2<double>(Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             thickness, color}); // top horizontal

    this->midLines.push_back(LineProperties {Vector2<double>(Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             Vector2<double>(Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             thickness, color}); // vertical

    this->midLines.push_back(LineProperties {Vector2<double>(Field::LEFT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             Vector2<double>(Field::LEFT_WALL_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             thickness, color}); // bottom horizontal

    //opponent goal area
    this->midLines.push_back(LineProperties {Vector2<double>(Field::RIGHT_WALL_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             Vector2<double>(Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             thickness, color}); // top horizontal

    this->midLines.push_back(LineProperties {Vector2<double>(Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_TOP_Y),
                                             Vector2<double>(Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             thickness, color}); // vertical

    this->midLines.push_back(LineProperties {Vector2<double>(Field::RIGHT_GOALIE_AREA_FRONT_LINE_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             Vector2<double>(Field::RIGHT_WALL_X,
                                                             Field::GOALIE_AREA_BOTTOM_Y),
                                             thickness, color}); // bottom horizontal

    //mid field
    this->midLines.push_back(LineProperties {Vector2<double>(0,
                                                             Field::TOP_WALL_Y),
                                             Vector2<double>(0,
                                                             Field::BOTTOM_WALL_Y),
                                             thickness, color}); // vertical

    color = Color::RED;
    thickness = 2;

    this->opponentBorder.push_back(LineProperties {Vector2<double>(0,
                                                                   Field::TOP_WALL_Y),
                                                   Vector2<double>((Field::RIGHT_WALL_X - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                                   Field::TOP_WALL_Y),
                                                   thickness, color}); // top horizontal

    this->opponentBorder.push_back(LineProperties {Vector2<double>((Field::RIGHT_WALL_X - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                                   Field::TOP_WALL_Y),
                                                   Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::TOP_WALL_Y - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                   thickness, color}); // top corner

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::TOP_WALL_Y - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                   Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::GOAL_TOP_Y),
                                                   thickness, color}); // top vertical

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::GOAL_TOP_Y),
                                                   Vector2<double>(Field::GOAL_BACK_RIGHT_X,
                                                                   Field::GOAL_TOP_Y),
                                                   thickness, color}); // top horizontal (goal)

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::GOAL_BACK_RIGHT_X,
                                                                   Field::GOAL_TOP_Y),
                                                   Vector2<double>(Field::GOAL_BACK_RIGHT_X,
                                                                   Field::GOAL_BOTTOM_Y),
                                                   thickness, color}); // vertical (goal)

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::GOAL_BACK_RIGHT_X,
                                                                   Field::GOAL_BOTTOM_Y),
                                                   Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::GOAL_BOTTOM_Y),
                                                   thickness, color}); // bottom horizontal (goal)

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::GOAL_BOTTOM_Y),
                                                   Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::BOTTOM_WALL_Y + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                   thickness, color}); // bottom vertical

    this->opponentBorder.push_back(LineProperties {Vector2<double>(Field::RIGHT_WALL_X,
                                                                   Field::BOTTOM_WALL_Y + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                   Vector2<double>((Field::RIGHT_WALL_X - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                                   Field::BOTTOM_WALL_Y),
                                                   thickness, color}); // bottom corner

    this->opponentBorder.push_back(LineProperties {Vector2<double>((Field::RIGHT_WALL_X - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                                   Field::BOTTOM_WALL_Y),
                                                   Vector2<double>(0,
                                                                   Field::BOTTOM_WALL_Y),
                                                   thickness, color}); // bottom horizontal

    color = Color::RED;
    thickness = 1;
    size = Field::FIELD_LENGTH_X / 5;
    angle = 0.0;

    //arrows in opponent field
    this->opponentArrow.push_back(ArrowProperties {Vector2<double>((Field::RIGHT_WALL_X * 0.8 - size),
                                                                   Field::TOP_WALL_Y * 0.75),
                                                   angle, size, thickness, color}); // upper

    this->opponentArrow.push_back(ArrowProperties {Vector2<double>((Field::RIGHT_WALL_X * 0.8 - size),
                                                                   Field::BOTTOM_WALL_Y * 0.75),
                                                   angle, size, thickness, color}); // lower

    color = Color::BLUE;
    thickness = 2;

    this->allyBorder.push_back(LineProperties {Vector2<double>(0,
                                                               Field::BOTTOM_WALL_Y),
                                               Vector2<double>((Field::LEFT_WALL_X + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                               Field::BOTTOM_WALL_Y),
                                               thickness, color}); // bottom horizontal

    this->allyBorder.push_back(LineProperties {Vector2<double>((Field::LEFT_WALL_X + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                               Field::BOTTOM_WALL_Y),
                                               Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::BOTTOM_WALL_Y + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                               thickness, color}); // bottom corner

    this->allyBorder.push_back(LineProperties {Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::BOTTOM_WALL_Y + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                               Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::GOAL_BOTTOM_Y),
                                               thickness, color}); // bottom vertical

    this->allyBorder.push_back(LineProperties {Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::GOAL_BOTTOM_Y),
                                               Vector2<double>(Field::GOAL_BACK_LEFT_X,
                                                               Field::GOAL_BOTTOM_Y),
                                               thickness, color}); // bottom horizontal (goal)

    this->allyBorder.push_back(LineProperties {Vector2<double>(Field::GOAL_BACK_LEFT_X,
                                                               Field::GOAL_BOTTOM_Y),
                                               Vector2<double>(Field::GOAL_BACK_LEFT_X,
                                                               Field::GOAL_TOP_Y),
                                               thickness, color}); // vertical (goal)

    this->allyBorder.push_back(LineProperties{Vector2<double>(Field::GOAL_BACK_LEFT_X,
                                                              Field::GOAL_TOP_Y),
                                              Vector2<double>(Field::LEFT_WALL_X,
                                                              Field::GOAL_TOP_Y),
                                              thickness, color}); // top horizontal (goal)

    this->allyBorder.push_back(LineProperties {Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::GOAL_TOP_Y),
                                               Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::TOP_WALL_Y - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                               thickness, color}); // top vertical

    this->allyBorder.push_back(LineProperties {Vector2<double>(Field::LEFT_WALL_X,
                                                               Field::TOP_WALL_Y - Field::CORNER_TRIANGLE_SIDE_SIZE),
                                               Vector2<double>((Field::LEFT_WALL_X + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                               Field::TOP_WALL_Y),
                                               thickness, color}); // top corner

    this->allyBorder.push_back(LineProperties {Vector2<double>((Field::LEFT_WALL_X + Field::CORNER_TRIANGLE_SIDE_SIZE),
                                                               Field::TOP_WALL_Y),
                                               Vector2<double>(0,
                                                               Field::TOP_WALL_Y),
                                               thickness, color}); // top horizontal

    color = Color::BLUE;
    thickness = 1;
    size = Field::FIELD_LENGTH_X / 5;
    angle = 0.0;

    //arrows in ally field
    this->allyArrow.push_back(ArrowProperties {Vector2<double>(Field::LEFT_WALL_X * 0.8,
                                                               Field::TOP_WALL_Y * 0.75),
                                               angle, size, thickness, color}); // upper

    this->allyArrow.push_back(ArrowProperties{Vector2<double>(Field::LEFT_WALL_X * 0.8,
                                                              Field::BOTTOM_WALL_Y * 0.75),
                                              angle, size, thickness, color}); // lower
}

void Renderer::drawPolyLines(DRAW_ID drawId, PolyLine &poly, int invert) {
    if(!checkPermission(drawId)) return;
    Vector2 <double> aux1, aux2;
    auto linesIt = poly.begin(); // PolyLine :: iterator

    while(linesIt != poly.end()) {
        aux1 = linesIt->P1;
        aux2 = linesIt->P2;

        if(invert < 0) {
            aux1.x = (-1.0) * aux1.x;
            aux2.x = (-1.0) * aux2.x;
        }

        this->drawLine(drawId, aux1, aux2, 0, linesIt->color, linesIt->thickness);

        linesIt++;
    }
}

void Renderer::drawPolyArrows(DRAW_ID drawId, PolyArrow &poly, int invert) {
    if(!checkPermission(drawId)) return;
    double auxAngle;
    Vector2 <double> aux;
    auto itArrow = poly.begin(); // PolyArrow :: iterator

    while(itArrow != poly.end()) {
        aux = itArrow->O;
        auxAngle = itArrow->angle;

        if(invert < 0) {
            aux.x = (-1.0) * aux.x;
            auxAngle += M_PI;
        }

        this->drawArrow( drawId, aux, 0.0, auxAngle, itArrow->color, itArrow->size, itArrow->thickness);

        itArrow++;
    }
}

void Renderer::drawCircle( DRAW_ID drawId, Vector2<double> point, double height, int radius, Color color, int thickness) {
    if(!checkPermission(drawId)) return;
    cv::circle(this->image, this->convertVector2DToPoint(cameraProjection.project3DTo2D(point, height)),
               radius, this->convertColorToScalar(color), thickness);
}

void Renderer::drawTarget( DRAW_ID drawId, Vector2<double> point, double height, int radius, Color color, int thickness) {
    if(!checkPermission(drawId)) return;
    this->drawCircle(drawId, point, height, radius, color, thickness);
    int innerRadius = radius/2;
    if(innerRadius == 0)
        innerRadius = 1;

    this->drawCircle(drawId, point, height, innerRadius, color, thickness);
}

void Renderer::drawArrow( DRAW_ID drawId, Vector2<double> orig, double height, double angle, Color color, double arrowSize, int thickness) {
    if(!checkPermission(drawId)) return;

    // Calculate arrow top postion
    Vector2<double> dest = orig + Vector2<double>(cos(angle), sin(angle)) * arrowSize;
    this->drawLine(drawId, orig, dest, height, color, thickness);

    // Calculate left tio postion
    Vector2<double> p1((dest.x - arrowSize * cos(angle + M_PI / 4) / 3),
                       (dest.y - arrowSize * sin(angle + M_PI / 4) / 3));
    this->drawLine(drawId, dest, p1, height, color, thickness);

    // Calculate left tio postion
    Vector2<double> p2((dest.x - arrowSize * cos(angle - M_PI / 4) / 3),
                       (dest.y - arrowSize * sin(angle - M_PI / 4) / 3));
    this->drawLine(drawId, dest, p2, height, color, thickness);
}

void Renderer::drawLine(DRAW_ID drawId, Vector2<double> orig, Vector2<double> dest, double height, Color color, int thickness) {
    if(!checkPermission(drawId)) return;

    Vector2<double> pixelOrig = this->cameraProjection.project3DTo2D(orig, height);
    Vector2<double> pixelDest = this->cameraProjection.project3DTo2D(dest, height);

    cv::line(image, this->convertVector2DToPoint(pixelOrig), this->convertVector2DToPoint(pixelDest),
             convertColorToScalar(color), thickness);
}

void Renderer::drawRect(DRAW_ID drawId, Vector2<double> orig, Vector2<double> size, double height, Color color, int thickness){
    if(!checkPermission(drawId)) return;
    Vector2<double> p2(orig.x+size.x, orig.y+size.y);
    cv::rectangle(image, this->convertVector2DToPoint(orig), this->convertVector2DToPoint(p2), this->convertColorToScalar(color), thickness);
}



cv::Point Renderer::convertVector2DToPoint(Vector2<double> p) {
    cv::Point ret;
    ret.x = (int) p.x;
    ret.y = (int) p.y;

    return ret;
}

cv::Scalar Renderer::convertColorToScalar(Color c) {
    cv::Scalar ret;
    ret.val[0] = c.r;
    ret.val[1] = c.g;
    ret.val[2] = c.b;
    ret.val[3] = c.alpha;

    return ret;
}

} // gui
} // core
