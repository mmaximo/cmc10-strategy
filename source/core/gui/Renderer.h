//
// Created by GRU on 10/27/16.
//

#ifndef CORE_UTILS_RENDERER_H
#define CORE_UTILS_RENDERER_H

#include <opencv2/opencv.hpp>
#include <QtWidgets/QLabel>
#include <QtCore/QMutex>
#include <chrono>
#include "core/utils/Colors.h"
#include "modeling/vision/CameraProjection.h"

#include "math/Vector2.h"
#include "modeling/WorldModel.h"
#include "modeling/vision/AbstractVision.h"
#include "representations/Field.h"
#include "modeling/vision/Image.h"

namespace core {
namespace gui {

enum DRAW_ID {
    IMAGE = 0,  CALIBRATION, FIELD,
    PLAYER,     SPEED,       ROLE,
    TRAJECTORY, STRATEGY1,   STRATEGY2,
    STATE, CONTROL
};

using math::Vector2;
using utils::Color;
using core::modeling::WorldModel;
using core::representations::Field;
using core::representations::Ball;
using core::representations::Player;
using core::representations::OpponentPlayer;

class LineProperties{
public:
    Vector2 <double> P1, P2;
    int thickness;
    Color color;
};

class ArrowProperties {
public:
    Vector2 <double> O;
    double angle, size;
    int thickness;
    Color color;
};

typedef std::vector <LineProperties>  PolyLine;
typedef std::vector <ArrowProperties> PolyArrow;

class Renderer : public QObject {
Q_OBJECT
public:
    static Renderer &i();
    void drawImage();
    void drawPlayers(WorldModel &wm, int invert);
    void drawSpeed(WorldModel &wm, int invert);
    void drawField(int invert);
    void drawRoles(WorldModel &wm, int invert);
    void drawCalib(modeling::Image *segmentedImage);
//    void drawStates(core::decision_making::States state);
    void setPermission(DRAW_ID drawId, bool newPermission);
    bool checkPermission(DRAW_ID drawId);
    void setImageViewer(bool newValue);
    void refreshImage(uchar *webcamImage); // Update the image to be drawn

    cv::Mat image; // Image related to the field renderings

    void drawCircle(DRAW_ID drawId, Vector2<double> point, double height = 0, int radius = 6, Color color = Color::WHITE, int thickness = 1);
    void drawTarget(DRAW_ID drawId, Vector2<double> point,double height = 0,  int radius = 6, Color color = Color::WHITE, int thickness = 1);
    void drawArrow(DRAW_ID drawId, Vector2<double> orig, double angle, double height = 0, Color color = Color::WHITE, double arrowSize = 25, int thickness = 4);
    void drawLine(DRAW_ID drawId, Vector2<double> orig, Vector2<double> dest, double height = 0, Color color = Color::WHITE, int thickness = 1);
    void drawRect(DRAW_ID drawId, Vector2<double> orig, Vector2<double> size, double height = 0, Color color = Color::WHITE, int thickness = 4);

    cv::Scalar convertColorToScalar(Color c);
signals:
    void updateImage(QMutex *qmutex, QImage *img);
    void updateFps(double fps);

private:
    void setUpField();
    void drawPolyLines(DRAW_ID drawId, PolyLine &poly, int invert);
    void drawPolyArrows(DRAW_ID drawId, PolyArrow &poly, int invert);

    Renderer(); // Prevent construction
    Renderer(const Renderer &); // Prevent construction by copying
    Renderer & operator = (const Renderer &); // Prevent assignment
    ~Renderer(); // Prevent unwanted destruction

    cv::Mat nullImage; //Black Image
    cv::Mat webCamImage;
    int rendererRefreshCounter;
    int refreshPeriod = 2;
    std::chrono::time_point<std::chrono::system_clock> lastTimeStamp;
    bool idPermited[10]; //true if the id can be drawn in the image
    bool imageViewerReady;
    QMutex qMutex;

    PolyLine  allyBorder,     //
              opponentBorder, //
              midLines;       // helps to draw the Field
    PolyArrow allyArrow,      //
              opponentArrow;  //

    cv::Point convertVector2DToPoint(Vector2<double> p);

    modeling::CameraProjection cameraProjection;
};

} // modeling
} // core

#endif
