//
// Created by mmaximo on 10/18/18.
//

#ifndef ITANDROIDS_VSS_RECTANGLE_H
#define ITANDROIDS_VSS_RECTANGLE_H

namespace core {
namespace math {

struct Rectangle {
    double xLeft;
    double xRight;
    double yTop;
    double yBottom;

    Rectangle(double xLeft, double xRight, double yTop, double yBottom);
};

} // namespace math
} // namespace core

#endif //ITANDROIDS_VSS_RECTANGLE_H
