//
// Created by mmaximo on 10/18/18.
//

#include "Rectangle.h"

namespace core {
namespace math {

Rectangle::Rectangle(double xLeft, double xRight, double yTop, double yBottom) : xLeft(xLeft), xRight(xRight),
                                                                                 yTop(yTop), yBottom(yBottom) {
}


} // namespace math
} // namespace core