//
// Created by igor on 27/04/17.
//

#include <math/MathUtils.h>
#include "WheelSpeed.h"

#include "representations/Player.h"
#include "representations/UnicycleSpeed.h"

namespace core {
namespace representations {

WheelSpeed::WheelSpeed() {
    left = 0.0;
    right = 0.0;
}

WheelSpeed::WheelSpeed(double left, double right) {
    this->left = left;
    this->right = right;
}

WheelSpeed::WheelSpeed(const UnicycleSpeed &unicycleSpeed) {
    using representations::Player;
    this->left = (unicycleSpeed.linear - unicycleSpeed.angular * Player::DISTANCE_WHEELS / 2.0) / Player::WHEEL_RADIUS;
    this->right = (unicycleSpeed.linear + unicycleSpeed.angular * Player::DISTANCE_WHEELS / 2.0) / Player::WHEEL_RADIUS;
}

void WheelSpeed::saturate(double maxAngularSpeed) {
    left = math::MathUtils::clamp(left, -maxAngularSpeed, maxAngularSpeed);
    right = math::MathUtils::clamp(right, -maxAngularSpeed, maxAngularSpeed);
}

} // representation
} // core
