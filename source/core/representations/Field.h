//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_REPRESENTATIONS_FIELD_H_
#define CORE_REPRESENTATIONS_FIELD_H_

#include <math/Vector2.h>

namespace core {
namespace representations {

using core::math::Vector2;

class Field {
public:
    ~Field() = default;

    // The used conventions are:
    //      -The origin is located in the middle of the field
    //      - X-axis is parallel to the largest walls (the length of the field)
    //      - X is positive when it goes to the opponent goal
    //      - X is negative when it goes to the ally goal
    //      - Y-axis is coincident with the central line (the width of the field)
    //      - when viewed from above:
    //              >left = negative X
    //              >right = positive x
    //              >top = positive Y
    //              >bottom = negative Y

    // Field constants
    constexpr static double WALL_HEIGHT = 0.1;
    constexpr static double WALL_THICKNESS = 0.025;
    constexpr static double GOAL_LENGTH_X = 0.10;
    constexpr static double GOAL_LENGTH_Y = 0.40;
    constexpr static double FIELD_LENGTH_Y = 1.30;
    constexpr static double FIELD_LENGTH_X = 1.50;
    constexpr static double CORNER_TRIANGLE_SIDE_SIZE = 0.07;
    constexpr static double CENTRAL_CIRCLE_RADIUS = 0.2;

    //Goalier area
    constexpr static double GOALIE_AREA_LENGTH_Y = 0.70;
    constexpr static double GOALIE_AREA_LENGTH_X = 0.15;
    constexpr static double GOALIE_AREA_RADIUS_CIRCLE = 0.125;
    constexpr static double GOALIE_AREA_ARC_LENGHT_X = 0.05;

    // Dependent constants of the Field
    constexpr static double TOP_WALL_Y = FIELD_LENGTH_Y / 2;
    constexpr static double BOTTOM_WALL_Y = -FIELD_LENGTH_Y / 2;
    constexpr static double LEFT_WALL_X = -(FIELD_LENGTH_X / 2);
    constexpr static double RIGHT_WALL_X = (FIELD_LENGTH_X / 2);
    constexpr static double PENALTY_LEFT_X = -(FIELD_LENGTH_X) / 4;
    constexpr static double PENALTY_RIGHT_X = (FIELD_LENGTH_X) / 4;

    //Free ball
    constexpr static double FREE_BALL_TOP_Y = 0.4;
    constexpr static double FREE_BALL_BOTTOM_Y = -FREE_BALL_TOP_Y;
    constexpr static double FREE_BALL_LEFT_X = -(FIELD_LENGTH_X) / 4;
    constexpr static double FREE_BALL_RIGHT_X = FIELD_LENGTH_X / 4;

    //Goalie area
    constexpr static double LEFT_GOALIE_AREA_FRONT_LINE_X = -(FIELD_LENGTH_X / 2 - GOALIE_AREA_LENGTH_X);
    constexpr static double RIGHT_GOALIE_AREA_FRONT_LINE_X = FIELD_LENGTH_X / 2 - GOALIE_AREA_LENGTH_X;
    constexpr static double GOALIE_AREA_MIDDLE_X = FIELD_LENGTH_X / 2 - GOALIE_AREA_LENGTH_X / 2;
    constexpr static double GOALIE_POSITION = -GOALIE_AREA_MIDDLE_X - 0.01;
    constexpr static double GOALIE_AREA_TOP_Y = GOALIE_AREA_LENGTH_Y / 2;
    constexpr static double GOALIE_AREA_BOTTOM_Y = -GOALIE_AREA_LENGTH_Y / 2;
    constexpr static double GOAL_TOP_Y = GOAL_LENGTH_Y / 2;
    constexpr static double GOAL_BOTTOM_Y = -GOAL_LENGTH_Y / 2;
    constexpr static double GOAL_LEFT_X = -FIELD_LENGTH_X / 2;
    constexpr static double GOAL_RIGHT_X = FIELD_LENGTH_X / 2;
    constexpr static double OPPONENT_GOAL_X = FIELD_LENGTH_X / 2;
    constexpr static double TEAM_GOAL_X = -FIELD_LENGTH_X / 2;
    constexpr static double GOAL_BACK_LEFT_X = LEFT_WALL_X - GOAL_LENGTH_X;
    constexpr static double GOAL_BACK_RIGHT_X = RIGHT_WALL_X + GOAL_LENGTH_X;
    constexpr static double GOALIE_AREA_CENTER_CIRCLE_LEFT_X =
            LEFT_GOALIE_AREA_FRONT_LINE_X - (GOALIE_AREA_RADIUS_CIRCLE - GOALIE_AREA_ARC_LENGHT_X);
    constexpr static double GOALIE_AREA_CENTER_CIRCLE_RIGHT_X =
            RIGHT_GOALIE_AREA_FRONT_LINE_X + (GOALIE_AREA_RADIUS_CIRCLE - GOALIE_AREA_ARC_LENGHT_X);

private:
    Field() = default;
};

} /* namespace representation */
} /* namespace core */

#endif //CORE_REPRESENTATIONS_FIELD_H_