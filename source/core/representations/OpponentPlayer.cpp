//
// Created by davi on 18/05/17.
//

#include "OpponentPlayer.h"

namespace core {
namespace representations {

OpponentPlayer::OpponentPlayer() : OpponentPlayer(Pose2D(0, 0, 0), Vector2<double>(0, 0), 0) {
    seen = false;
}

OpponentPlayer::OpponentPlayer(int id) : OpponentPlayer(Pose2D(0, 0, 0), Vector2<double>(0, 0), id) {
}

OpponentPlayer::OpponentPlayer(Pose2D initialPosition, Vector2<double> initialVelocity, int id) {
    setId(id);
    setPose(initialPosition);
    setVelocity(initialVelocity);
    seen = false;
}

OpponentPlayer::~OpponentPlayer() = default;


Pose2D &OpponentPlayer::getPose() {
    return pose;
}

Vector2<double> &OpponentPlayer::getPosition() {
    return pose.translation;
}

Vector2<double> &OpponentPlayer::getVelocity() {
    return velocity;
}

int OpponentPlayer::getId() {
    return playerId;
}

void OpponentPlayer::setId(int playerId) {
    this->playerId = playerId;
}

void OpponentPlayer::setPose(Pose2D pose) {
    this->pose = pose;
}

void OpponentPlayer::setVelocity(Vector2<double> velocity) {
    this->velocity = velocity;
}

void OpponentPlayer::setPosition(Vector2<double> position) {
    this->pose.translation = position;
}

bool OpponentPlayer::isSeen() {
    return seen;
}

void OpponentPlayer::setSeen(bool seen) {
    this->seen = seen;
}

} /* namespace representations */

} /* namespace core */

