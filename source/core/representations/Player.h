//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_REPRESENTATIONS_PLAYER_H_
#define CORE_REPRESENTATIONS_PLAYER_H_

//#define NEW_ROBOT

#include "math/Vector2.h"
#include "math/Pose2D.h"
#include "WheelSpeed.h"
#include "Eigen/Dense"

using core::math::Vector2;
using core::math::Pose2D;

namespace core {
namespace representations {

class Player {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Player();

    Player(Pose2D initialPosition, Vector2<double> initialVelocity);

    Player(Pose2D initialPosition, Vector2<double> initialVelocity, int id);

    virtual ~Player();

    /**
    * Gets the pose of the player in cartesian coordinates relative
    * to the field center
    * @return pose2D of the position
    */
    const Pose2D &getPose() const;

    /**
    * Sets the pose of the player in cartesian coordinates relative
    * to the field center
    * @param pose The new position of the player
    */
    void setPose(Pose2D pose);

    /**
    * Sets the pose of the player in cartesian coordinates relative
    * to the field center
    * @param position The new pose.translation of the player and
    * rotation is set to 0
    */
    void setPosition(Vector2<double> position);

    void setRotation(double rotation);

    /**
    * Gets the position of the player in cartesian coordinates relative
    * to the field center
    * @return position The new position of the player
    */
    const Vector2<double> &getPosition() const;

    /**
    * Gets the linear velocity of the player as a vector2
    * @return vector2 of the velocity
    */
    const Vector2<double> &getVelocity() const;

    /**
    * Sets the linear velocity of the player as a vector2
    * @param velocity The new velocity of the player
    */
    void setVelocity(Vector2<double> velocity);

    /**
    * Gets the identifier of the player as a integer
    * @return int of the playerId
    */
    const int getId();

    /**
    * Sets the identifier of the player as a int
    * @param playerId: The new identifier of the player
    */
    void setId(int playerId);

    /**
    * Gets the rotation of the player as a double
    * @return Double of the rotation
    */
    const double getRotation();

    Eigen::VectorXd getEigenPose();

    bool isSeen();

    void setSeen(bool seen);

    constexpr static int PLAYERS_PER_SIDE = 3; // Number of players on each team

    constexpr static double SIZE = 0.075; // The cubic total dimensions to the robot
    constexpr static double LENGTH = 0.079; // chassis length
    constexpr static double WIDTH = 0.079; // chassis width
    constexpr static double HEIGHT = 0.07; // chassis height
    constexpr static double TOTAL_HEIGHT = 0.081; //from the ground until the color indicators on the top
    constexpr static double DISTANCE_FROM_GROUND = 0.0001; //distance from the chassis to the ground
    constexpr static double BALL_DISTANCE_FROM_FRONT = 0.0002;
    constexpr static double BALL_EPSILON_FROM_GROUND = 0.000001; // diference between the wheel and the balls that stabilise the robot
    constexpr static double TOTAL_MASS = 0.3003; // robot mass

    constexpr static double MASS_WITHOUT_HOOD = 0.24605;
    constexpr static double LENGTH_WITHOUT_HOOD = 0.074;
    constexpr static double WIDTH_WITHOUT_HOOD = 0.074;
    constexpr static double HEIGHT_WITHOU_HOOD = 0.048;

    constexpr static double MOTOR_REDUCTION = 51.45;
    constexpr static double WHEEL_THICKNESS = 0.0073; // wheel thickness
    constexpr static double WHEEL_MIDDLE_RADIUS = 0.016; // the radius of the circle on the center of the wheel
    constexpr static double WHEEL_RADIUS = 0.03; //wheel radius
    constexpr static double WHEELS_TO_CENTER = 0.0331;
    constexpr static double DISTANCE_WHEELS = 2 * WHEELS_TO_CENTER;
    constexpr static double WHEEL_MASS = 0.0113398; // wheel mass

    constexpr static double CENTER_MASS_POS_X = 0.03871; //position of the center of mass on axis X
    constexpr static double CENTER_MASS_POS_Y = 0.03871; //position of the center of mass on axis Y
    constexpr static double CENTER_MASS_POS_Z = 0.03; //0.03645; //position of the center of mass on axis Z, measure from the point of contact of the wheels with the ground
    constexpr static double MOMENT_OF_INERTIA_Z = 0.0002; //moment of inertia measured with axis in the center in vertical
    constexpr static double MOMENT_OF_INERTIA_Y = 0.0008;//0.00018; //moment of inertia measured with axis in the side in vertical passing by the wheel
    constexpr static double MOMENT_OF_INERTIA_X = 0.0008;//0.00019; //moment of inertia measured with axis in the front
    constexpr static double EFFECTIVE_X = sqrt(
            6 * (MOMENT_OF_INERTIA_Y + MOMENT_OF_INERTIA_Z - MOMENT_OF_INERTIA_X) / TOTAL_MASS);//Equivalent length
    constexpr static double EFFECTIVE_Y = sqrt(
            6 * (MOMENT_OF_INERTIA_X + MOMENT_OF_INERTIA_Z - MOMENT_OF_INERTIA_Y) / TOTAL_MASS);//to provide the right
    constexpr static double EFFECTIVE_Z = sqrt(
            6 * (MOMENT_OF_INERTIA_X + MOMENT_OF_INERTIA_Y - MOMENT_OF_INERTIA_Z) / TOTAL_MASS);//moment of inertia
    constexpr static double CENTER_MASS_TO_CENTER_GEOMETRIC = HEIGHT / 2 - CENTER_MASS_POS_Z;
    constexpr static double CENTER_GEOMETRIC_TO_CENTER_MASS = CENTER_MASS_POS_Z - HEIGHT / 2;
    constexpr static double MAX_WHEEL_SPEED = 37; // rad/s
    constexpr static double SPIN_SPEED = 33; // rad/s
    constexpr static double NORMAL_SPEED = 25.00; // rad/s
    constexpr static double GOALIE_SPEED = 25.00; // rad/s
    constexpr static double LINEAR_SPEED = MAX_WHEEL_SPEED * WHEEL_RADIUS; // m/s
    constexpr static double MAX_WHEEL_ANGULAR_ACCEL = 6.0 / WHEEL_RADIUS;
    constexpr static double SCREW_RADIUS = 0.0025;
    constexpr static double HINGE_ACCELERATION = 1; //0 to 1 value. 0->no acceleration 1->body will reach desired speed at the next step, force applied might be too big
    constexpr static double HINGE_FORCE_MAX = 0.1059232774999995; //maximum force applied to the wheel

private:
    /**
    * The position of the player in cartesian coordinates relative
    * to the field center
    */
    Pose2D pose;

    /**
    * The linear velocity of the player as a vector2
    */
    Vector2<double> velocity;

    /**
     * The identifier of the player. Non-identified players value set to 0.
     */
    int playerId;

    bool seen;

};

} // representation
} // core

#endif