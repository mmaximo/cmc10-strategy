//
// Created by davi on 18/05/17.
//

#ifndef CORE_REPRESENTATIONS_OPPONENTPLAYER_H_
#define CORE_REPRESENTATIONS_OPPONENTPLAYER_H_

#include "math/Vector2.h"
#include "math/Pose2D.h"
#include "WheelSpeed.h"

using core::math::Vector2;
using core::math::Pose2D;

namespace core {
namespace representations {

class OpponentPlayer {
public:
    /**
     * Default constructor of the EnemyPlayer
     */
    OpponentPlayer();

    /**
    * Constructor of the Player class that initializes the id
    */
    OpponentPlayer(int id);

    /**
    * Constructor of the Player class that initializes the position, velocity and id
    */
    OpponentPlayer(Pose2D initialPosition, Vector2<double> initialVelocity, int id);

    /**
    * Default destructor of the EnemyPlayer
    */
    virtual ~OpponentPlayer();

    /**
    * Gets the pose of the player in cartesian coordinates relative
    * to the field center
    * @return pose2D of the position
    */
    Pose2D &getPose();

    /**
    * Gets the position of the player in cartesian coordinates relative
    * to the field center
    * @return position The new position of the player
    */
    Vector2<double> &getPosition();

    /**
    * Gets the linear velocity of the player as a vector2
    * @return vector2 of the velocity
    */
    Vector2<double> &getVelocity();

    /**
    * Gets the identifier of the player as a integer
    * @return int of the playerId
    */
    int getId();

    /**
    * Sets the identifier of the player as a int
    * @param playerId: The new identifier of the player
    */
    void setId(int playerId);

    /**
    * Sets the pose of the player in cartesian coordinates relative
    * to the field center
    * @param pose The new position of the player
    */
    void setPose(Pose2D pose);

    /**
    * Sets the pose of the player in cartesian coordinates relative
    * to the field center
    * @param position The new pose.translation of the player and
    * rotation is set to 0
    */
    void setPosition(Vector2<double> position);

    /**
    * Sets the linear velocity of the player as a vector2
    * @param velocity The new velocity of the player
    */
    void setVelocity(Vector2<double> velocity);

    bool isSeen();

    void setSeen(bool seen);

private:
    /**
    * The position of the player in cartesian coordinates relative
    * to the field center
    */
    Pose2D pose;

    /**
    * The linear velocity of the player as a vector2
    */
    Vector2<double> velocity;

    /**
     * The identifier of the player. Non-identified players value set to -1.
     */
    int playerId;

    bool seen;
};

} /* namespace representation */

} /* namespace core */

#endif //CORE_REPRESENTATIONS_OPPONENTPLAYER_H_
