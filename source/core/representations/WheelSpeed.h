//
// Created by igor on 27/04/17.
//

#ifndef CORE_REPRESENTATIONS_WHEELSPEED_H_
#define CORE_REPRESENTATIONS_WHEELSPEED_H_

namespace core {
namespace representations {

class UnicycleSpeed;

class WheelSpeed {
public:
    WheelSpeed();
    WheelSpeed(double left, double right);
    explicit WheelSpeed(const UnicycleSpeed &unicycleSpeed);
    void saturate(double maxAngularSpeed);
    bool operator==(const WheelSpeed &other) const {
        return (right == other.right && left == other.left);
    }

    double left, right;
};

} // representation
} // core

#endif
