//
// Created by mmaximo on 7/3/18.
//

#ifndef ITANDROIDS_VSS_UNICYCLESPEED_H
#define ITANDROIDS_VSS_UNICYCLESPEED_H

namespace core {
namespace representations {

class WheelSpeed;

class UnicycleSpeed {
public:
    explicit UnicycleSpeed(const WheelSpeed &wheelSpeed);
    UnicycleSpeed(double linear, double angular);
    UnicycleSpeed();

    double linear;
    double angular;
};

} // namespace representations
} // namespace core

#endif //ITANDROIDS_VSS_UNICYCLESPEED_H
