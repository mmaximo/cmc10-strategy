//
// Created by mmaximo on 7/3/18.
//

#include "UnicycleSpeed.h"

#include "representations/WheelSpeed.h"
#include "representations/Player.h"

namespace core {
namespace representations {

UnicycleSpeed::UnicycleSpeed(const WheelSpeed &wheelSpeed) {
    using representations::Player;
    this->linear = (wheelSpeed.right + wheelSpeed.left) * Player::WHEEL_RADIUS / 2.0;
    this->angular = (wheelSpeed.right - wheelSpeed.left) * Player::WHEEL_RADIUS / Player::DISTANCE_WHEELS;
}

UnicycleSpeed::UnicycleSpeed(double linear, double angular) : linear(linear), angular(angular) {

}

UnicycleSpeed::UnicycleSpeed() : linear(0.0), angular(0.0) {

}

} // namespace representations
} // namespace core