//
// Created by igorribeiro on 10/27/16.
//

#include "Ball.h"

namespace core {
namespace representations {

using core::math::Vector2;

Ball::Ball() : Ball(Vector2<double>(0.0, 0.0), Vector2<double>(0.0, 0.0)) { }

Ball::Ball(Vector2<double> position, Vector2<double> velocity) : position(position), velocity(velocity), seen(false) {
}

Ball::~Ball() = default;

const Vector2<double> &Ball::getPosition() const {
    return position;
}

void Ball::setPosition(Vector2<double> position) {
    this->position = position;
}

const Vector2<double> &Ball::getVelocity() const {
    return velocity;
}

void Ball::setVelocity(Vector2<double> velocity) {
    this->velocity = velocity;
}

bool Ball::isSeen() {
    return seen;
}

bool Ball::setSeen(bool seen) {
    this->seen = seen;
}


} // representation
} // core
