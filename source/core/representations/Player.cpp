//
// Created by igorribeiro on 10/27/16.
//

#include <math/MathUtils.h>
#include "Player.h"


namespace core {
namespace representations {

Player::Player() : Player(Pose2D(0, 0, 0), Vector2<double>(0, 0), 1) {
    seen = false;
}

Player::Player(Pose2D initialPosition, Vector2<double> initialVelocity) : Player(
        initialPosition, initialVelocity, 1) {
}

Player::Player(Pose2D initialPosition, Vector2<double> initialVelocity, int id) : pose(initialPosition), velocity(initialVelocity) {
}

Player::~Player() = default;

const Pose2D &Player::getPose() const {
    return pose;
}

Eigen::VectorXd Player::getEigenPose() {
    return Eigen::Vector3d(pose.translation.x, pose.translation.y, pose.rotation);
}

void Player::setPose(Pose2D pose) {
    this->pose = pose;
}

const Vector2<double> &Player::getVelocity() const {
    return velocity;
}

void Player::setVelocity(Vector2<double> velocity) {
    this->velocity = velocity;
}

void Player::setPosition(Vector2<double> position) {
    this->pose.translation = position;
}

void Player::setRotation(double rotation) {
    this->pose.rotation = rotation;
}

void Player::setId(int id) {
    this->playerId = id;
}

const double Player::getRotation() {
    return pose.rotation;
}

const int Player::getId() {
    return playerId;
}

const Vector2<double> &Player::getPosition() const {
    return pose.translation;
}

bool Player::isSeen() {
    return seen;
}

void Player::setSeen(bool seen) {
    this->seen = seen;
}

} /* namespace representation */

} /* namespace core */
