//
// Created by igor on 11/06/17.
//

#ifndef CORE_REPRESENTATIONS_TEAM_H_
#define CORE_REPRESENTATIONS_TEAM_H_

namespace core {
namespace representations {

enum class Team {
    ALLY,
    OPPONENT
};

} // representation
} // core

#endif