////
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_REPRESENTATIONS_BALL_H_
#define CORE_REPRESENTATIONS_BALL_H_

#include "math/Vector2.h"
#include "Eigen/Dense"

using namespace Eigen;
using core::math::Vector2;

namespace core {
namespace representations {

class Ball {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Ball();
    Ball(Vector2<double> position, Vector2<double> velocity);
    virtual ~Ball();

    /**
    * Gets the position of the ball in cartesian coordinates relative
    * to the field center
    * @return vector2 of the position
    */
    const Vector2<double> &getPosition() const;

    /**
    * Sets the position of the ball in cartesian coordinates relative
    * to the field center
    * @param position The new position of the ball
    */
    void setPosition(Vector2<double> position);

    /**
    * Gets the linear velocity of the ball as a vector2
    * @return vector2 of the velocity
    */
    const Vector2<double> &getVelocity() const;

    /**
    * Sets the linear velocity of the ball as a vector2
    * @param position The new velocity of the ball
    */
    void setVelocity(Vector2<double> velocity);

    bool isSeen();

    bool setSeen(bool seen);

    static constexpr double DIAMETER = 0.0427;    // ball diameter
    static constexpr double MASS = 0.046;        // ball mass

private:
    /**
   * The position of the ball in cartesian coordinates relative
   * to the field center
   */
    Vector2<double> position;

    /**
    * The linear velocity of the ball as a vector2
    */
    Vector2<double> velocity;

    bool seen;

};

} // representation
} // core

#endif
