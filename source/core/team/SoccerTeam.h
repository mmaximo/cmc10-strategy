#ifndef CORE_SOCCER_TEAM_H_
#define CORE_SOCCER_TEAM_H_

#include <thread>
#include <mutex>
#include <iostream>
#include <atomic>
#include "control/Control.h"
#include "control/request/UnivectorControlRequest.h"
#include "communication/NrfSerialCommunication.h"
#include "communication/AbstractCommunication.h"
#include "decision_making/DecisionMaking.h"
#include "decision_making/DecisionMakingInterface.h"
#include <decision_making/UnivectorField.h>
#include "math/Vector2.h"
#include "modeling/vision/AbstractVision.h"
#include "modeling/vision/CameraVision.h"
#include "modeling/vision/DummyVision.h"
#include "modeling/vision/StaticVision.h"
#include "modeling/vision/VisionInterface.h"
#include "modeling/WorldModel.h"
#include "control/ControlInterface.h"

namespace core {
namespace team {

enum class CUSTOM_ACTION {
    ALIVE,
    WARMUP,
    COMEMORATION,
    PARADE,
};

using math::Vector2;

class SoccerTeam {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    explicit SoccerTeam(bool startPlaying = false,
                        communication::AbstractCommunication *communication = nullptr,
                        modeling::AbstractVision *vision = nullptr);
    virtual ~SoccerTeam();

    /*
     * Main code. Runs a game step -
     * Updates each of it's modules.
    */
    void modelingUpdate();
//    void actionUpdate(bool playing);
    void actionComemoration();
    void actionAlive();
    void actionWarmUp();
    void changeUniform();
    void dynamicPositionUpdate(Vector2<double> pixelDest);
    void invertCamera();
    void pause();
    void start();
    void playCustomAction(CUSTOM_ACTION action);
    bool getIsPlaying();
    std::string getUniform();
    bool isInverted();

    /**
    * Move robot to a given screen position.
    * @return True if robot arrived at destination.
    */
    void moveRobot(int playerId, Vector2<double> pixelDest);
    timespec diff(timespec start, timespec end);
    modeling::AbstractVision *getVision();
    decision_making::DecisionMaking *getDecisionMaking();

protected:
    std::mutex mutex;
    modeling::WorldModel worldModel;
    control::Control *control;
    control::ControlInterface controlInterface;
    decision_making::DecisionMakingInterface decisionMakingInterface;
    communication::AbstractCommunication *communication;

private:
    /**
    * Team modules
    */

    modeling::AbstractVision *vision;
    decision_making::DecisionMaking *decisionMaking;
    modeling::VisionInterface* visionInterface;
    int invert;

    std::atomic<bool> isPlaying; /* Stops action update */
    std::atomic<bool> isRunning; /* Finishes action thread */
    std::atomic<bool> acquiredImage;
    std::atomic<bool> showImageInGUI;
    std::atomic<bool> isMoving;
    std::string currUniform;

    void initActionThread();
    void initModelingThread();
    virtual void actionUpdate();
    void movingUpdate(int playerId, Vector2<double> pixelDest);
    void updateRenderer();

    std::thread actionThread;
    std::thread modelingThread;
    std::thread *movingThread;
    std::mutex movingMutex;

};

} // team
} // core

#endif
