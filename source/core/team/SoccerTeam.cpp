#include "team/SoccerTeam.h"

#include <core/utils/Clock.h>
#include <core/gui/Renderer.h>
#include <core/gui/widgets/params/DataManager.h>

namespace core {
namespace team {

SoccerTeam::SoccerTeam(bool startPlaying, communication::AbstractCommunication *communication,
                       modeling::AbstractVision *vision) {

    worldModel = modeling::WorldModel();
    control = new control::Control();
    decisionMaking = new decision_making::DecisionMaking();

    isPlaying = startPlaying;
    isRunning = true;
    acquiredImage = false;
    isMoving = false;
    invert = 1;

    movingThread = nullptr;

    if (communication != nullptr) this->communication = communication;
    else this->communication = new communication::NrfSerialCommunication(false);

    if (vision != nullptr)
        this->vision = vision;
    else {
        this->vision = new modeling::CameraVision();
        if (!this->vision->check())
            this->vision = new modeling::StaticVision();
    }

    currUniform = "blue";
    this->vision->setTeamColor(core::modeling::VSSColor::BLUE);

    if (startPlaying) start();

    core::gui::Renderer::i().refreshImage(this->vision->getImagePtr());
    modelingThread = std::thread(&SoccerTeam::initModelingThread, this);
    actionThread = std::thread(&SoccerTeam::initActionThread, this);
//    core::gui::DataManager::getInstance().load();
}

SoccerTeam::~SoccerTeam() {
    isRunning = false;
    mutex.lock();
    modelingThread.join();
    actionThread.join();
    pause();
    if (movingThread != nullptr) {
        movingThread->join();
        delete (movingThread);
    }
    delete (communication);
    delete (vision);
    delete (decisionMaking);
    mutex.unlock();
}

void SoccerTeam::modelingUpdate() { //The modeling thread
    auto visionInterfaceAux = vision->update();
    mutex.lock();
    visionInterface = visionInterfaceAux;
    acquiredImage = true;
    mutex.unlock();
//    std::cout << "Ball position: " << visionInterface->ball.getPosition() << std::endl;
}

void SoccerTeam::actionUpdate() { //The action thread
//    std::cout << "actionUpdate()" << std::endl;
    decisionMaking->decide(worldModel);
    decisionMakingInterface = decisionMaking->getDecisionMakingInterface();
    control->control(decisionMakingInterface, worldModel);
    auto controlInterfaceAux = control->getControlInterface();

    controlInterface = controlInterfaceAux;

    communication->send(controlInterface);
}

void SoccerTeam::initModelingThread() {
    while (isRunning) {
        modelingUpdate();
    }
}

void SoccerTeam::initActionThread() {
    core::utils::Clock clock;
    double time, previousTime;
    time = previousTime = clock.getTime();
    while (isRunning) {
        bool expected = true;

        if (acquiredImage.compare_exchange_strong(expected, false)) {
            mutex.lock();
            auto visionInterfaceAux = *visionInterface;
            mutex.unlock();
            // TODO: change this part to use Modeling instead of accessing worldModel directly
            time = clock.getTime();
            worldModel.update(time - previousTime, visionInterfaceAux);
            previousTime = time;

//            std::cout << "isPlaying: " << isPlaying << std::endl;

            if (isPlaying)
                actionUpdate();

            updateRenderer();
        }
    }
}

void SoccerTeam::updateRenderer() {
    // TODO: implement render update
    core::gui::Renderer &renderer = core::gui::Renderer::i();
    renderer.drawCalib(this->vision->getSegmentedImage());
    renderer.drawRoles(worldModel, invert);
    renderer.drawPlayers(worldModel, invert);
    renderer.drawSpeed(worldModel, invert);
    renderer.drawField(invert);
//    renderer.drawStates(worldModel.getState());
    renderer.drawImage();
}

void SoccerTeam::movingUpdate(int playerId, Vector2<double> pixelDest) {
//    double rotation = 0.0;
//
//    isMoving = true;
//
//    const double angleTolerance = 0.001;
//
//    double distance = worldModel.getPlayerById(playerId).getPosition().distance(pixelDest);
//    double errorAngle = fabs(worldModel.getPlayerById(playerId).getRotation() - rotation);
//    Pose2D desiredPosition;
//    desiredPosition.translation = pixelDest;
//    desiredPosition.rotation = 0;
//    Vector2<double> lineRef(0, 1);
//
////    control::AbstractControlRequest *abstractControlRequest = new control::LineControlRequest(worldModel.getPlayerById(playerId),
////                                                                                              worldModel,
////                                                                                              desiredPosition, lineRef,
////                                                                                               representations::Player::NORMAL_SPEED);
//
//    bool gotToDest = false;
//
//    while (isMoving && isRunning && !isPlaying && !gotToDest) {
//        control::ControlInterface cInterface = control::ControlInterface();
//
//        distance = worldModel.getPlayerById(playerId).getPosition().distance(pixelDest);
//        errorAngle = fabs(worldModel.getPlayerById(playerId).getRotation() - rotation);
//        Pose2D desiredPosition;
//        desiredPosition.translation = pixelDest;
//        desiredPosition.rotation = 0;
//        Vector2<double> lineRef(0, 1);
////        control::AbstractControlRequest *abstractControlRequest = new
////                control::LineControlRequest(
////                worldModel.getPlayerById(playerId),
////                worldModel,
////                desiredPosition, lineRef, representations::Player::NORMAL_SPEED);
//
//        core::decision_making::UnivectorShoot univectorPlanner;
//
//        auto &player = worldModel.getPlayerById(playerId);
//        univectorPlanner.setTarget(cv::Point2d(desiredPosition.translation.x, desiredPosition.translation.y), 0);
//        //univectorPlanner.insertAllies(playerId, worldModel);
//        //univectorPlanner.insertOpponents(worldModel);
//        univectorPlanner.insertVSSFieldWalls();
//        univectorPlanner.drawField(core::gui::DRAW_ID::STRATEGY1);
//        univectorPlanner.drawTrajectory(cv::Point2d(player.getPosition().x, player.getPosition().y),
//                                        cv::Point2d(player.getVelocity().x, player.getVelocity().y), core::gui::DRAW_ID::TRAJECTORY);
//
//        // Request
//        auto trackingPose = univectorPlanner.nextPosition(player);
//        double angle = trackingPose.rotation;
//
//        control::AbstractControlRequest *abstractControlRequest =
//                new control::UnivectorControlRequest(worldModel.getPlayerById(playerId), worldModel, angle, univectorPlanner);
//
//
//
//
//
//
//
//        gotToDest = distance < 0.02 &&
//                    (fabs(errorAngle) < angleTolerance ||
//                     fabs(errorAngle - M_PI) < angleTolerance);
//
//
//        control->setControlInterface(abstractControlRequest, playerId);
//
//        cInterface.playersWheelSpeed[playerId] =
//                control->getControlInterface().playersWheelSpeed[playerId];
//
//        communication->send(cInterface);
////        updateRenderer();
//        usleep(16666);
//        delete (abstractControlRequest);
//    }
//
//    isMoving = false;
}

void SoccerTeam::playCustomAction(CUSTOM_ACTION action){
    if (movingThread != nullptr) {
        isMoving = false;
        movingThread->join();
        delete (movingThread);
    }

    switch(action){
        case CUSTOM_ACTION::ALIVE:
            printf("I am alive\n");
            movingThread = new std::thread(&SoccerTeam::actionAlive, this);
            break;
        case CUSTOM_ACTION::WARMUP:
            printf("Let\'s warm up\n");
            movingThread = new std::thread(&SoccerTeam::actionWarmUp, this);
            break;
        case CUSTOM_ACTION::COMEMORATION:
            printf("Celebrating forever\n");
            movingThread = new std::thread(&SoccerTeam::actionComemoration, this);
            break;
        case CUSTOM_ACTION::PARADE:
            printf("Party time\n");
            break;
    }
}

void SoccerTeam::changeUniform() {
    if (!isPlaying && !isMoving && isRunning) {
        if (currUniform == "blue") {
            currUniform = "yellow";
            vision->setTeamColor(core::modeling::VSSColor::YELLOW);
        } else {
            currUniform = "blue";
            vision->setTeamColor(core::modeling::VSSColor::BLUE);
        }

        std::cout << "CURRENT UNIFORM = " << currUniform << std::endl;
    }
}

void SoccerTeam::invertCamera() {
    if (!isPlaying && !isMoving && isRunning) {
        worldModel.invert();
        invert *= -1;
    }
}

void SoccerTeam::pause() {
    isPlaying = false;
    isMoving = false;

    if (movingThread != nullptr) {
        movingThread->join();
        delete movingThread;
        movingThread = nullptr;
    }

    control::ControlInterface controlInterface = control::ControlInterface();
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
        controlInterface.playersWheelSpeed[i].left = 0;
        controlInterface.playersWheelSpeed[i].right = 0;
    }

//    worldModel.pause();

    //Gambi to make the robot stop
    for (int i = 0; i < 100; i++)
        communication->send(controlInterface);
}

void SoccerTeam::start() {
    delete(decisionMaking);
    decisionMaking = new decision_making::DecisionMaking();
    isPlaying = true;
}

// FIX: move to correct position when 'invert' is active
void SoccerTeam::moveRobot(int playerId, Vector2<double> pixelDest) {
    if (movingThread != nullptr) {
        isMoving = false;
        movingThread->join();
        delete (movingThread);
    }

    if(playerId<representations::Player::PLAYERS_PER_SIDE)
        movingThread = new std::thread(&SoccerTeam::movingUpdate, this, playerId, pixelDest);
    else
        movingThread = new std::thread(&SoccerTeam::dynamicPositionUpdate, this, pixelDest);
}

void SoccerTeam::actionAlive() {
//    isMoving = true;
//
//    const int iterToEnd = 36;
//    bool completed = false;
//    double iter = 0;
//    double original[representations::Player::PLAYERS_PER_SIDE];
//    for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
//        original[i] = worldModel.getPlayerById(i).getRotation();
//    }
//
//    while (isMoving && isRunning && !isPlaying && !completed) {
//        iter++;
//        completed = iter>iterToEnd;
//        control::ControlInterface cInterface = control::ControlInterface();
//
//        for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
//            auto& player = worldModel.getPlayerById(i);
//
//
//            control::AbstractControlRequest *abstractControlRequest;
//
//            double desiredAngle = original[i];
//            if(iter<iterToEnd/3){
//                desiredAngle += M_PI / 4;
//            }else if(iter<2*iterToEnd/3){
//                desiredAngle -= M_PI / 4;
//            }
//
//            abstractControlRequest = new
//                    control::AngleControlRequest(player, worldModel, desiredAngle);
//
//            control->setControlInterface(abstractControlRequest, i);
//
//            cInterface.playersWheelSpeed[i] =
//                    control->getControlInterface().playersWheelSpeed[i];
//
//            communication->send(cInterface);
//            usleep(16666);
//        }
//
//        usleep(16666);
//    }
//    printf("finished\n");
//
//    isMoving = false;
}
void SoccerTeam::dynamicPositionUpdate(Vector2<double> pixelDest){
//    isMoving = true;
//    int iter = 0;
//    const int maxIter = 100;
//
//    while (isMoving && isRunning && !isPlaying && iter<maxIter){
//        iter++;
//        control::ControlInterface cInterface = control::ControlInterface();
//
//        for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
//            auto &player = worldModel.getPlayerById(i);
//            decision_making::DelaunayFormationType::DELAUNAY_FORMATION_TYPE delaunayType =
//                    decision_making::DelaunayFormationType::DELAUNAY_FORMATION_TYPE::DYNAMIC_POSITION;
//
//            auto &formations = worldModel.getFormation();
//            auto robotsPose = formations[delaunayType]->interpolate(pixelDest);
//
//            double desiredAngle = robotsPose[i].z;
//            cv::Point2d dest(robotsPose[i].x, robotsPose[i].y);
//
//            core::decision_making::UnivectorSymmetricPoint univectorPlanner;
//            univectorPlanner.setTarget(dest, desiredAngle);
//            univectorPlanner.insertAllies(i, worldModel);
//            univectorPlanner.insertOpponents(worldModel);
//            univectorPlanner.addObstacle(
//                    cv::Point2d(worldModel.getBall().getPosition().x,
//                                worldModel.getBall().getPosition().y),
//                    cv::Point2d(worldModel.getBall().getVelocity().x,
//                                worldModel.getBall().getVelocity().y));
//
//            auto trackingPose = univectorPlanner.nextPosition(player);
//
//            control::AbstractControlRequest *abstractControlRequest;
//
//            if(iter<maxIter && hypot(player.getPosition().y - dest.y, player.getPosition().x - dest.x) > 0.06){
//                abstractControlRequest = new
//                        control::LineControlRequest(
//                        player, worldModel, trackingPose, trackingPose.translation - player.getPosition(),
//                        representations::Player::NORMAL_SPEED / 3);
//            }else{
//                if(iter>=maxIter) desiredAngle = player.getRotation();
//                abstractControlRequest = new
//                        control::AngleControlRequest(player, worldModel, desiredAngle);
//            }
//
//            control->setControlInterface(abstractControlRequest, i);
//
//            cInterface.playersWheelSpeed[i] =
//                    control->getControlInterface().playersWheelSpeed[i];
//
//            communication->send(cInterface);
//            usleep(16666);
//        }
//
//        usleep(16666);
//    }
//
//    isMoving = false;
}

void SoccerTeam::actionWarmUp(){
//    isMoving = true;
//
////    const double angleTolerance = 0.001;
//    const int iterPerTurn = 200;
//    const double radius = 0.6;
//    int iter = 0;
////    bool completed = false;
//
//    while (isMoving && isRunning && !isPlaying) {
//        iter = (iter + 1)%iterPerTurn;
//        control::ControlInterface cInterface = control::ControlInterface();
//        double angle = iter*M_PI/100;
//
//        for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
//            int atual = (iter + i*iterPerTurn/representations::Player::PLAYERS_PER_SIDE)%iterPerTurn;
//            auto& player = worldModel.getPlayerById(i);
//
//            cv::Point2d dest;
//            double desiredAngle;
//            if(atual<3*iterPerTurn/7){
//                angle = desiredAngle = 0;
//                dest = cv::Point2d(-radius, -0.3);
//
//            }else if(atual<6*iterPerTurn/7){
//                angle = atual * M_PI / (6 * iterPerTurn / 7);
//                dest = cv::Point2d(-radius * cos(angle), -0.3 + radius * sin(angle));
//                desiredAngle = angle - M_PI/2;
//            }else{
//                angle = 0;
//                dest = cv::Point2d(-radius, -0.3);
//                desiredAngle = 0;
//
//
//            }
//            core::decision_making::UnivectorSymmetricPoint univectorPlanner;
//            univectorPlanner.setTarget(dest, desiredAngle);
//            univectorPlanner.insertAllies(i, worldModel);
//            univectorPlanner.insertOpponents(worldModel);
//
//            auto trackingPose = univectorPlanner.nextPosition(player);
//
//            control::AbstractControlRequest *abstractControlRequest;
//
//            if(hypot(player.getPosition().y-dest.y, player.getPosition().x-dest.x)<0.06) continue;
//            abstractControlRequest = new
//                    control::LineControlRequest(
//                    player, worldModel, trackingPose, trackingPose.translation - player.getPosition(),
//                    representations::Player::NORMAL_SPEED / 3);
//
//            control->setControlInterface(abstractControlRequest, i);
//
//            cInterface.playersWheelSpeed[i] =
//                    control->getControlInterface().playersWheelSpeed[i];
//
//            communication->send(cInterface);
//
//            usleep(16666);
//        }
//
//        usleep(16666);
//    }
//
//    isMoving = false;
}


void SoccerTeam::actionComemoration() {
//    isMoving = true;
//
////    const double angleTolerance = 0.001;
////    bool completed = false;
//    double iter = 0;
//    const double radius = 0.3;
//
//    while (isMoving && isRunning && !isPlaying) {
//        iter++;
//        control::ControlInterface cInterface = control::ControlInterface();
//        double angle = iter*M_PI/100;
//
//        for(int i = 0; i<representations::Player::PLAYERS_PER_SIDE; i++){
//            auto& player = worldModel.getPlayerById(i);
//            angle += 2*M_PI/3;
//
//            cv::Point2d dest(radius*cos(angle), radius*sin(angle));
//            double desiredAngle = angle + M_PI/2;
//            if(i==0)
//                std::cout<<angle<<" "<<desiredAngle<<" "<<dest<<std::endl;
//
//            core::decision_making::UnivectorSymmetricPoint univectorPlanner;
//            univectorPlanner.setTarget(dest, desiredAngle);
////            univectorPlanner.insertAllies(i, worldModel);
////            univectorPlanner.insertOpponents(worldModel);
//
//            auto trackingPose = univectorPlanner.nextPosition(player);
//            control::AbstractControlRequest *abstractControlRequest;
//
//            if(hypot(player.getPosition().y-dest.y, player.getPosition().x-dest.x)<0.06) continue;
//            abstractControlRequest = new
//                    control::LineControlRequest(
//                    player, worldModel, trackingPose, trackingPose.translation - player.getPosition(),
//                    representations::Player::NORMAL_SPEED / 3);
////                                     control::ReedsSheppControlRequest(player,
////                                                                       worldModel,
////                                                                       radius,
////                                                                       2.0 * M_PI,
////                                                                       (decision_making::Directions) decision_making::Turn::LEFT,
////                                                                       false);
//
//            control->setControlInterface(abstractControlRequest, i);
//
//            cInterface.playersWheelSpeed[i] =
//                    control->getControlInterface().playersWheelSpeed[i];
//
//            communication->send(cInterface);
//            usleep(16666);
//        }
//
//        usleep(16666);
//    }
//
//    isMoving = false;
}

timespec SoccerTeam::diff(timespec start, timespec end) {
    timespec temp;

    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

bool SoccerTeam::getIsPlaying() {
    return isPlaying;
}

std::string SoccerTeam::getUniform() {
    return currUniform;
}

bool SoccerTeam::isInverted() {
    return invert < 0;
}

modeling::AbstractVision *SoccerTeam::getVision() {
    return vision;
}

core::decision_making::DecisionMaking *SoccerTeam::getDecisionMaking() {
    return decisionMaking;
}

} // team
} // core
