//
// Created by GRU on 15/03/17.
//

#include <QtWidgets/QApplication>
#include "communication/SocketCommunication.h"
#include "communication/writers/SocketWriter.h"
#include "modeling/vision/DummyVision.h"
#include "team/SoccerTeam.h"
#include <gui/window/Window.h>

void showUsage();

int main(int argc, char *argv[]) {
    using namespace core;
    using namespace core::modeling;
    using namespace core::communication;

    std::string inSocketFlag = "-inSocket";
    std::string outSocketFlag = "-outSocket";
    std::string startPlayingFlag = "-startPlaying";
    std::string simulatorPlayer1Flag = "-1";
    std::string simulatorPlayer2Flag = "-2";
    std::string simulatorTrackFlag = "-t";
    std::string simulatorPlayer1StartPlayingFlag = "-1s";
    std::string simulatorPlayer2StartPlayingFlag = "-2s";
    const int port1 = 8234;
    const int port2 = 8235;

    bool outSocket = false;
    bool inSocket = false;
    bool startPlaying = false;

    int port = 0;
    for (int i = 1; i < argc; i++) {
        if (outSocketFlag.compare(argv[i]) == 0 && i <= argc - 2) {
            outSocket = true;
            try {
                port = std::stoi(argv[i++ + 1]);
            } catch (...) {
                std::cout << "Invalid port number after -outSocket flag" << i << std::endl;
            }
        } else if (inSocketFlag.compare(argv[i]) == 0) {
            inSocket = true;
        } else if (startPlayingFlag.compare(argv[i]) == 0) {
            startPlaying = true;
        } else if (simulatorPlayer1Flag.compare(argv[i]) == 0) {
            port = port1;
            outSocket = true;
            inSocket = true;
        } else if (simulatorPlayer2Flag.compare(argv[i]) == 0) {
            port = port2;
            outSocket = true;
            inSocket = true;
        } else if (simulatorPlayer1StartPlayingFlag.compare(argv[i]) == 0) {
            port = port1;
            outSocket = true;
            inSocket = true;
            startPlaying = true;
        } else if (simulatorPlayer2StartPlayingFlag.compare(argv[i]) == 0) {
            port = port2;
            outSocket = true;
            inSocket = true;
            startPlaying = true;
        } else {
            showUsage();
            return 1;
        }
    }

    team::SoccerTeam *soccerTeam;
    if (outSocket && inSocket) {
        std::cout << "Starting with inSocket and outSocket flags" << std::endl;

        SocketMessageParser *parser = new SocketMessageParser;
        SocketWriter *writer = new SocketWriter(port);
        SocketCommunication *communication = new SocketCommunication(parser, writer);
        DummyVision *vision = new DummyVision(parser, writer);
        soccerTeam = new team::SoccerTeam(startPlaying, communication, vision);
    } else if (outSocket) {
        std::cout << "Starting with outSocket flag" << std::endl;
        SocketMessageParser *parser = new SocketMessageParser;
        SocketWriter *writer = new SocketWriter(port);
        SocketCommunication *communication = new SocketCommunication(parser, writer);
        soccerTeam = new team::SoccerTeam(startPlaying, communication);
    } else if (!inSocket) {
        std::cout << "Default start" << std::endl;
        soccerTeam = new team::SoccerTeam();
    } else {
        showUsage();
        return 1;
    }

    QApplication app(argc, argv);
    core::gui::Window mainWindow(soccerTeam);
    mainWindow.show();
    return app.exec();
}

void showUsage() {
    std::cout << "usage: [-startPlaying] [-inSocket] [-outSocket p] [-t]" << std::endl;
    std::cout << "    -startPlaying: robots start playing " <<
              "instead of paused" << std::endl;
    std::cout << "    -inSocket: receives localization info " <<
              "directly from simulation" << std::endl;
    std::cout << "    -outSocket p: sends commands to simulation " <<
              "instead of USB XBee radio with p being one of the " <<
              "available ports: 8234 or 8235" << std::endl;
    std::cout << "      -t  : is to enable tracking\n";
    std::cout << "      -1  : is equivalent to -inSocket -outSocket 8234\n";
    std::cout << "      -2  : is equivalent to -inSocket -outSocket 8235\n";
    std::cout << "      -1s : is equivalent to -inSocket -outSocket 8234 -startPlaying\n";
    std::cout << "      -2s : is equivalent to -inSocket -outSocket 8234 -startPlaying\n";
    std::cout << "Try to run the following: " << std::endl;
    std::cout << "./agent -startPlaying -inSocket -outSocket 8234" << std::endl;
}