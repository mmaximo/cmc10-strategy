//
// Created by mmaximo on 11/1/18.
//

#include <math/MathUtils.h>
#include "AngleController.h"

namespace core {
namespace control {

AngleController::AngleController(double kp, double maxAngularSpeed) : kp(kp), maxAngularSpeed(maxAngularSpeed) {

}

double AngleController::control(double reference, double currentAngle) {
    // Implement
}

}
}