//
// Created by mmaximo on 11/1/18.
//

#include "SpinRequest.h"

#include <control/Control.h>

namespace core {
namespace control {

SpinRequest::SpinRequest(double angularSpeed, int playerId) : ActionRequest(ActionRequestType ::SPIN, playerId), angularSpeed(angularSpeed) {

}

double SpinRequest::getAngularSpeed() const {
    return angularSpeed;
}

void SpinRequest::accept(Control &control, modeling::WorldModel &worldModel) {
    control.handleSpin(*this, worldModel);
}

}
}
