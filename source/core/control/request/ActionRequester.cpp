//
// Created by mmaximo on 11/1/18.
//

#include "ActionRequester.h"
#include "AngleControlRequest.h"
#include "DirectCommandRequest.h"
#include "FollowLineRequest.h"
#include "SpinRequest.h"
#include "UnivectorControlRequest.h"

namespace core {
namespace control {

ActionRequester::ActionRequester() {
    clearRequests();
}

void ActionRequester::clearRequests() {
    for (auto &request : requests)
        request = nullptr;
}

const std::shared_ptr<ActionRequest> *ActionRequester::getRequests() const {
    return requests;
}

const std::shared_ptr<ActionRequest> ActionRequester::getRequestByPlayerId(int playerId) const {
    return requests[playerId];
}

void ActionRequester::requestAction(std::shared_ptr<ActionRequest> request) {
    requests[request->getPlayerId()] = request;
}

void ActionRequester::requestAngleControl(double desiredAngle, int playerId) {
    requestAction(std::make_shared<AngleControlRequest>(desiredAngle, playerId));
}

void ActionRequester::requestDirectCommand(const representations::WheelSpeed &wheelSpeed, int playerId) {
    requestAction(std::make_shared<DirectCommandRequest>(wheelSpeed, playerId));
}

void ActionRequester::requestFollowLine(const math::Pose2D &desiredPose, int playerId) {
    requestAction(std::make_shared<FollowLineRequest>(desiredPose, playerId));
}

void ActionRequester::requestSpin(double angularSpeed, int playerId) {
    requestAction(std::make_shared<SpinRequest>(angularSpeed, playerId));
}

void ActionRequester::requestUnivector(const double desiredAngle, int playerId) {
    requestAction(std::make_shared<UnivectorControlRequest>(desiredAngle, playerId));
}

}
}
