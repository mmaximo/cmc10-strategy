//
// Created by mmaximo on 11/1/18.
//

#include "ActionRequest.h"

namespace core {
namespace control {

ActionRequest::ActionRequest(ActionRequestType type, int playerId) : type(type), playerId(playerId) {

}

ActionRequest::~ActionRequest() {

}

ActionRequestType ActionRequest::getType() const {
    return type;
}

int ActionRequest::getPlayerId() const {
    return playerId;
}

}
}