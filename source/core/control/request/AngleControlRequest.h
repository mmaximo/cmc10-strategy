//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_ANGLECONTROLREQUEST_H
#define CMC10_STRATEGY_PRIVATE_ANGLECONTROLREQUEST_H

#include "control/request/ActionRequest.h"

namespace core {
namespace control {

class AngleControlRequest : public ActionRequest {
public:
    AngleControlRequest(double desiredAngle, int playerId);

    double getDesiredAngle() const;

    void accept(Control &control, modeling::WorldModel &worldModel) override;

private:
    double desiredAngle;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_ANGLECONTROLREQUEST_H
