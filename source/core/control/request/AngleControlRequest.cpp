//
// Created by mmaximo on 11/1/18.
//

#include "AngleControlRequest.h"

#include <control/Control.h>

namespace core {
namespace control {

AngleControlRequest::AngleControlRequest(double desiredAngle, int playerId) : ActionRequest(ActionRequestType::ANGLE, playerId),
                                                                desiredAngle(desiredAngle) {

}

double AngleControlRequest::getDesiredAngle() const {
    return desiredAngle;
}

void AngleControlRequest::accept(Control &control, modeling::WorldModel &worldModel) {
    control.handleAngleControl(*this, worldModel);
}

}

}