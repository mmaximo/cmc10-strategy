//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_CONTROLREQUEST_H
#define CMC10_STRATEGY_PRIVATE_CONTROLREQUEST_H

#include <modeling/WorldModel.h>

namespace core {
namespace control {

class Control;

enum class ActionRequestType {
    DIRECT_COMMAND,
    SPIN, // a.k.a. Beyblade
    ANGLE,
    FOLLOW_LINE,
    UNIVECTOR_CONTROL,
    UNDEFINED,
};

class ActionRequest {
public:
    ActionRequest(ActionRequestType type, int playerId);

    virtual ~ActionRequest();

    ActionRequestType getType() const;

    int getPlayerId() const;

    virtual void accept(Control &control, modeling::WorldModel &worldModel) = 0;

protected:
    ActionRequestType type;
    int playerId;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_CONTROLREQUEST_H
