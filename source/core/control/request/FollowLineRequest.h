//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_FOLLOWLINEREQUEST_H
#define CMC10_STRATEGY_PRIVATE_FOLLOWLINEREQUEST_H

#include "control/request/ActionRequest.h"
#include "math/Pose2D.h"

namespace core {
namespace control {

class FollowLineRequest : public ActionRequest {
public:
    FollowLineRequest(const math::Pose2D &desiredPose, int playerId);

    const math::Pose2D &getDesiredPose() const;

    void accept(Control &control, modeling::WorldModel &worldModel) override;

private:
    math::Pose2D desiredPose;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_FOLLOWLINEREQUEST_H
