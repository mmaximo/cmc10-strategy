//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_DIRECTCOMMANDREQUEST_H
#define CMC10_STRATEGY_PRIVATE_DIRECTCOMMANDREQUEST_H

#include <control/request/ActionRequest.h>
#include <representations/WheelSpeed.h>

namespace core {
namespace control {

class DirectCommandRequest : public ActionRequest {
public:
    DirectCommandRequest(const representations::WheelSpeed &wheelSpeed, int playerId);

    DirectCommandRequest(const representations::UnicycleSpeed &unicycleSpeed, int playerId);

    const representations::WheelSpeed &getWheelSpeed() const;

    void accept(Control &control, modeling::WorldModel &worldModel) override;

private:
    representations::WheelSpeed wheelSpeed;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_DIRECTCOMMANDREQUEST_H
