//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_ACTIONREQUESTER_H
#define CMC10_STRATEGY_PRIVATE_ACTIONREQUESTER_H

#include <control/request/ActionRequest.h>
#include <memory>
#include <representations/Player.h>

namespace core {
namespace control {

class ActionRequester {
public:
    ActionRequester();

    void clearRequests();

    const std::shared_ptr<ActionRequest> *getRequests() const;

    const std::shared_ptr<ActionRequest> getRequestByPlayerId(int playerId) const;

    void requestAction(std::shared_ptr<ActionRequest> request);

    void requestAngleControl(double desiredAngle, int playerId);

    void requestDirectCommand(const representations::WheelSpeed &wheelSpeed, int playerId);

    void requestFollowLine(const math::Pose2D &desiredPose, int playerId);

    void requestSpin(double angularSpeed, int playerId);

    void requestUnivector(double desiredAngle, int playerId);

private:
    std::shared_ptr<ActionRequest> requests[representations::Player::PLAYERS_PER_SIDE];
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_ACTIONREQUESTER_H
