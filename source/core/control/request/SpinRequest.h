//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_SPINREQUEST_H
#define CMC10_STRATEGY_PRIVATE_SPINREQUEST_H

#include <control/request/ActionRequest.h>

namespace core {
namespace control {

class SpinRequest : public ActionRequest {
public:
    SpinRequest(double angularSpeed, int playerId);

    double getAngularSpeed() const;

    void accept(Control &control, modeling::WorldModel &worldModel) override;

private:
    double angularSpeed;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_SPINREQUEST_H
