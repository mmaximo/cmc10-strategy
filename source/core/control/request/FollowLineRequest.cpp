//
// Created by mmaximo on 11/1/18.
//

#include "FollowLineRequest.h"

#include <control/Control.h>

namespace core {
namespace control {

FollowLineRequest::FollowLineRequest(const math::Pose2D &desiredPose, int playerId) : ActionRequest(
        ActionRequestType::FOLLOW_LINE, playerId), desiredPose(desiredPose) {

}

const math::Pose2D &FollowLineRequest::getDesiredPose() const {
    return desiredPose;
}

void FollowLineRequest::accept(Control &control, modeling::WorldModel &worldModel) {
    control.handleFollowLine(*this, worldModel);
}

}
}