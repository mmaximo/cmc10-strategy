//
// Created by mmaximo on 11/1/18.
//

#include "DirectCommandRequest.h"

#include <control/Control.h>

namespace core {
namespace control {

DirectCommandRequest::DirectCommandRequest(const representations::WheelSpeed &wheelSpeed, int playerId)
        : ActionRequest(ActionRequestType::DIRECT_COMMAND, playerId), wheelSpeed(wheelSpeed) {

}

DirectCommandRequest::DirectCommandRequest(const representations::UnicycleSpeed &unicycleSpeed, int playerId) :
        DirectCommandRequest(representations::WheelSpeed(unicycleSpeed), playerId) {

}

const representations::WheelSpeed &DirectCommandRequest::getWheelSpeed() const {
    return wheelSpeed;
}

void DirectCommandRequest::accept(Control &control, modeling::WorldModel &worldModel) {
    control.handleDirectCommand(*this, worldModel);
}

}
}