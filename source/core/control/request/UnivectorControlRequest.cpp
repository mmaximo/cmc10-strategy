//
// Created by mmaximo on 11/1/18.
//

#include "UnivectorControlRequest.h"

#include <control/Control.h>

namespace core {
namespace control {

UnivectorControlRequest::UnivectorControlRequest(double desiredAngle, int playerId) : ActionRequest(
        ActionRequestType::UNIVECTOR_CONTROL, playerId), desiredAngle(desiredAngle) {

}

double UnivectorControlRequest::getDesiredAngle() const {
    return desiredAngle;
}

void UnivectorControlRequest::accept(Control &control, modeling::WorldModel &worldModel) {
    control.handleUnivectorControl(*this, worldModel);
}

}
}