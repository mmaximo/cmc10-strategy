//
// Created by mmaximo on 11/1/18.
//

#include "Control.h"

#include <control/request/DirectCommandRequest.h>
#include <control/request/SpinRequest.h>
#include <control/request/AngleControlRequest.h>
#include <control/request/FollowLineRequest.h>
#include <control/request/UnivectorControlRequest.h>

namespace core {
namespace control {

Control::Control() : angleController(2.0 * M_PI * 2.0,
                                     2.0 * representations::Player::MAX_WHEEL_SPEED * representations::Player::WHEEL_RADIUS /
                                     representations::Player::DISTANCE_WHEELS),
                     followLineController(FollowLineParams::getDefaultParams()),
                     univectorController(
                             0.7 * representations::Player::MAX_WHEEL_SPEED * representations::Player::WHEEL_RADIUS,
                             2.0 * M_PI,
                             2.0 * representations::Player::MAX_WHEEL_SPEED * representations::Player::WHEEL_RADIUS /
                             representations::Player::DISTANCE_WHEELS) {

}

void Control::control(decision_making::DecisionMakingInterface &decisionMakingInterface, modeling::WorldModel &worldModel) {
    for (int playerId = 0; playerId < representations::Player::PLAYERS_PER_SIDE; ++playerId) {
        std::shared_ptr<ActionRequest> request = decisionMakingInterface.getRequestByPlayerId(playerId);
        if (request != nullptr)
            request->accept(*this, worldModel);
    }
}

void Control::handleDirectCommand(const DirectCommandRequest &request, modeling::WorldModel &worldModel) {
    controlInterface.playersWheelSpeed[request.getPlayerId()] = request.getWheelSpeed();
}

void Control::handleSpin(const SpinRequest &request, modeling::WorldModel &worldModel) {
    representations::UnicycleSpeed unicycleSpeed(0.0,
                                                 request.getAngularSpeed());
    controlInterface.playersWheelSpeed[request.getPlayerId()] = representations::WheelSpeed(unicycleSpeed);
}

void Control::handleAngleControl(const AngleControlRequest &request, modeling::WorldModel &worldModel) {
    int playerId = request.getPlayerId();
    representations::UnicycleSpeed unicycleSpeed;
    unicycleSpeed.angular = angleController.control(request.getDesiredAngle(),
                                                    worldModel.getTeammateById(playerId).getPose().rotation);
    controlInterface.playersWheelSpeed[playerId] = representations::WheelSpeed(unicycleSpeed);
}

void Control::handleFollowLine(const FollowLineRequest &request, modeling::WorldModel &worldModel) {
    int playerId = request.getPlayerId();
    controlInterface.playersWheelSpeed[playerId] = followLineController.control(request.getDesiredPose(),
                                                                                worldModel.getTeammateById(
                                                                                        playerId).getPose());
}

void Control::handleUnivectorControl(const UnivectorControlRequest &request, modeling::WorldModel &worldModel) {
    int playerId = request.getPlayerId();
    controlInterface.playersWheelSpeed[playerId] = univectorController.control(request.getDesiredAngle(),
                                                                               worldModel.getTeammateById(
                                                                                       playerId).getPose().rotation);
}

ControlInterface &Control::getControlInterface() {
    return controlInterface;
}

}
}