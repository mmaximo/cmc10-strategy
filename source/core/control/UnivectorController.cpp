//
// Created by mmaximo on 11/2/18.
//

#include "UnivectorController.h"

#include "DifferentialUtils.h"
#include <math/MathUtils.h>
#include <representations/UnicycleSpeed.h>

namespace core {
namespace control {

UnivectorController::UnivectorController(double maxLinearSpeed, double kpsi, double maxAngularSpeed) : maxLinearSpeed(
        maxLinearSpeed), angleController(kpsi, maxAngularSpeed) {

}

representations::WheelSpeed UnivectorController::control(double referenceAngle, double currentAngle) {
    // Implement
}

}
}