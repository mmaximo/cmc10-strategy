//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_ANGLECONTROLLER_H
#define CMC10_STRATEGY_PRIVATE_ANGLECONTROLLER_H

namespace core {
namespace control {

/**
 * Implements a proportional controller for tracking a reference angle.
 */
class AngleController {
public:
    /**
     * Constructs an angle controller.
     * @param kp proportional gain
     * @param maxAngularSpeed maximum angular speed (for saturation)
     */
    AngleController(double kp, double maxAngularSpeed);

    /**
     * Updates the controller and outputs the angular speed.
     * @param reference reference angle
     * @param currentAngle current angle
     * @return angular speed to be sent to the robot
     */
    double control(double reference, double currentAngle);

private:
    double kp;
    double maxAngularSpeed;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_ANGLECONTROLLER_H
