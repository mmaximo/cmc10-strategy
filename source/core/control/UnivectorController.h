//
// Created by mmaximo on 11/2/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_UNIVECTORCONTROLLER_H
#define CMC10_STRATEGY_PRIVATE_UNIVECTORCONTROLLER_H

#include <representations/WheelSpeed.h>
#include <math/Pose2D.h>
#include "AngleController.h"

namespace core {
namespace control {

/**
 * Implements a controller to be used with the univector trajectory planner.
 */
class UnivectorController {
public:
    /**
     * Constructs a controller to be used with the univector trajectory planner.
     * @param maxLinearSpeed maximum linear speed used when following the trajectory
     * @param kpsi angle controller's proportional gain
     * @param maxAngularSpeed maximum angular speed for the angle controller
     */
    UnivectorController(double maxLinearSpeed, double kpsi, double maxAngularSpeed);

    /**
     * Updates the controller and outputs speeds for the left and right wheels.
     * @param referenceAngle reference angle requested by the univector
     * @param currentAngle robot's current angle
     * @return speeds of the wheels
     */
    representations::WheelSpeed control(double referenceAngle, double currentAngle);

private:
    double maxLinearSpeed;
    AngleController angleController;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_UNIVECTORCONTROLLER_H
