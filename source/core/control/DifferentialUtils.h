//
// Created by mmaximo on 11/2/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_DIFFERENTIALUTILS_H
#define CMC10_STRATEGY_PRIVATE_DIFFERENTIALUTILS_H

#include <representations/WheelSpeed.h>

namespace core {
namespace control {

/**
 * Containts utilitary methods for differential drive robots.
 */
class DifferentialUtils {
public:
    /**
     * Saturates the speeds of the wheels while keeping the same turn radius.
     * @param wheelSpeed speeds of the wheels
     * @return saturated speeds of the wheels
     */
    static representations::WheelSpeed saturateKeepingRadius(representations::WheelSpeed wheelSpeed);
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_DIFFERENTIALUTILS_H
