//
// Created by mmaximo on 11/1/18.
//

#include <math/MathUtils.h>
#include <representations/WheelSpeed.h>
#include <representations/Player.h>
#include "FollowLineController.h"
#include "DifferentialUtils.h"

namespace core {
namespace control {

FollowLineParams FollowLineParams::getDefaultParams() {
    FollowLineParams params;
    params.kx = 2.0 * M_PI * 1.0;
    params.wn = 2.0 * M_PI * 1.0;
    params.xi = 0.7;
    params.maxAngleReference = math::MathUtils::degreesToRadians(80.0);
    double linearSpeedLimit = representations::Player::MAX_WHEEL_SPEED * representations::Player::WHEEL_RADIUS;
    params.minLinearSpeedForKh = 0.1 * linearSpeedLimit;
    params.maxLinearSpeed = 0.9 * linearSpeedLimit;
    params.maxWheelSpeed = representations::Player::MAX_WHEEL_SPEED;
    return params;
}

FollowLineController::FollowLineController(const FollowLineParams &params) : params(params) {
}

representations::WheelSpeed
FollowLineController::control(const math::Pose2D &reference, const math::Pose2D &currentPose) {
    // Implement
}

}
}