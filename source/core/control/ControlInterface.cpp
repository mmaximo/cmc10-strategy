//
// Created by igorribeiro on 22/09/17.
//

#include "ControlInterface.h"

namespace core {
namespace control {

ControlInterface::ControlInterface() {
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
        playersWheelSpeed[i].right = 0.0;
        playersWheelSpeed[i].left = 0.0;
    }
}

ControlInterface::~ControlInterface() = default;

} // control
} // core
