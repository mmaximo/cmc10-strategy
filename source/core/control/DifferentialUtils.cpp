//
// Created by mmaximo on 11/2/18.
//

#include "DifferentialUtils.h"

#include <ios>
#include <math.h>
#include <representations/UnicycleSpeed.h>
#include <math/MathUtils.h>
#include <representations/Player.h>

namespace core {
namespace control {

representations::WheelSpeed DifferentialUtils::saturateKeepingRadius(representations::WheelSpeed wheelSpeed) {
    using representations::Player;

    if (wheelSpeed.left < Player::MAX_WHEEL_SPEED && wheelSpeed.left > -Player::MAX_WHEEL_SPEED &&
        wheelSpeed.right < Player::MAX_WHEEL_SPEED &&
        wheelSpeed.right > -Player::MAX_WHEEL_SPEED)
        return wheelSpeed;

    representations::UnicycleSpeed unicycleSpeed(wheelSpeed);
    if (std::abs(unicycleSpeed.linear) < 1.0e-3 || std::abs(unicycleSpeed.angular) < 1.0e-3 ||
        std::abs(wheelSpeed.left) < 1.0e-3 || std::abs(wheelSpeed.right) < 1.0e-3) {
        wheelSpeed.saturate(Player::MAX_WHEEL_SPEED);
        return wheelSpeed;
    }

    double radius = unicycleSpeed.linear / unicycleSpeed.angular;
    double beta = (Player::DISTANCE_WHEELS + 2.0 * radius) / (2.0 * radius - Player::DISTANCE_WHEELS);
    if (unicycleSpeed.linear > 0.0 && unicycleSpeed.angular > 0.0) {
        wheelSpeed.right = Player::MAX_WHEEL_SPEED;
        wheelSpeed.left = wheelSpeed.right / beta;
    } else if (unicycleSpeed.linear < 0.0 && unicycleSpeed.angular < 0.0) {
        wheelSpeed.right = -Player::MAX_WHEEL_SPEED;
        wheelSpeed.left = wheelSpeed.right / beta;
    } else if (unicycleSpeed.linear > 0.0 && unicycleSpeed.angular < 0.0) {
        wheelSpeed.left = Player::MAX_WHEEL_SPEED;
        wheelSpeed.right = wheelSpeed.left * beta;
    } else if (unicycleSpeed.linear < 0.0 && unicycleSpeed.angular > 0.0) {
        wheelSpeed.left = -Player::MAX_WHEEL_SPEED;
        wheelSpeed.right = wheelSpeed.left * beta;
    }

    return wheelSpeed;
}

}
}