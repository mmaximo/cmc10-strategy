//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_CONTROL_H
#define CMC10_STRATEGY_PRIVATE_CONTROL_H

#include <modeling/WorldModel.h>
#include <decision_making/DecisionMakingInterface.h>
#include "ControlInterface.h"
#include "AngleController.h"
#include "FollowLineController.h"
#include "UnivectorController.h"

namespace core {
namespace control {

class DirectCommandRequest;

class SpinRequest;

class AngleControlRequest;

class FollowLineRequest;

class UnivectorControlRequest;

class Control {
public:
    Control();

    void control(decision_making::DecisionMakingInterface &decisionMakingInterface, modeling::WorldModel &worldModel);

    ControlInterface &getControlInterface();

    void handleDirectCommand(const DirectCommandRequest &request, modeling::WorldModel &worldModel);

    void handleSpin(const SpinRequest &request, modeling::WorldModel &worldModel);

    void handleAngleControl(const AngleControlRequest &request, modeling::WorldModel &worldModel);

    void handleFollowLine(const FollowLineRequest &request, modeling::WorldModel &worldModel);

    void handleUnivectorControl(const UnivectorControlRequest &request, modeling::WorldModel &worldModel);

private:
    ControlInterface controlInterface;
    AngleController angleController;
    FollowLineController followLineController;
    UnivectorController univectorController;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_CONTROL_H
