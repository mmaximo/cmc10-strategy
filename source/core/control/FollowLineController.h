//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_FOLLOWLINECONTROLLER_H
#define CMC10_STRATEGY_PRIVATE_FOLLOWLINECONTROLLER_H

#include <representations/UnicycleSpeed.h>
#include <math/Pose2D.h>

namespace core {
namespace control {

struct FollowLineParams {
    double kx; /// frontal system's proportional gain
    double wn; /// lateral system's natural frequency
    double xi; /// lateral system's damping factor
    double maxAngleReference; /// maximum angle reference used in the lateral system
    double minLinearSpeedForKh; /// minimum linear speed used in the Kh gain scheduling mechanism
    double maxLinearSpeed; /// maximum linear speed
    double maxWheelSpeed; /// maximum wheel speed of the robot

    /**
     * Obtains default parameters for the follow line controller.
     * @return default parameters
     */
    static FollowLineParams getDefaultParams();
};

/**
 * Implements a follow line controller. Using this controller, the robot stops on a desired point along a line.
 */
class FollowLineController {
public:
    /**
     * Constructs a follow line controller.
     * @param params controller's parameters
     */
    explicit FollowLineController(const FollowLineParams &params);

    /**
     * Updates the controller and outputs speeds for the left and right wheels.
     * The line is defined by the reference pose in the sense that it is a line which passes through
     * the point and its orientation is defined by the orientation of the pose.
     * The robot stops at the reference pose.
     * @param reference reference pose for the controller
     * @param currentPose robot's current pose
     * @return speeds for the left and right wheels
     */
    representations::WheelSpeed control(const math::Pose2D &reference, const math::Pose2D &currentPose);

private:
    FollowLineParams params;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_FOLLOWLINECONTROLLER_H
