//
// Created by igor on 05/04/17.
//

#ifndef CORE_CONTROL_CONTROLINTERFACE_H_
#define CORE_CONTROL_CONTROLINTERFACE_H_

#include "representations/WheelSpeed.h"
#include "representations/Player.h"

namespace core {
namespace control {

class ControlInterface {
public:
    ControlInterface();
    ~ControlInterface();

    representations::WheelSpeed playersWheelSpeed[representations::Player::PLAYERS_PER_SIDE];
};

} // control
} // core

#endif
