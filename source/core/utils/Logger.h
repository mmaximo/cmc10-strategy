//
// Created by muzio on 12/03/16.
//

#ifndef ITANDROIDS_LIB_UTILS_LOGGER_H
#define ITANDROIDS_LIB_UTILS_LOGGER_H

#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <boost/preprocessor.hpp>

namespace core {
namespace utils {

//http://stackoverflow.com/questions/5093460/how-to-convert-an-enum-type-variable-to-a-string
#define X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE(r, data, elem)    \
            case elem : return BOOST_PP_STRINGIZE(elem);

#define DEFINE_ENUM_WITH_STRING_CONVERSIONS(name, enumerators)                \
            enum name {                                                               \
                BOOST_PP_SEQ_ENUM(enumerators)                                        \
            };                                                                        \
                                                                                      \
            inline const char* toString(name v)                                       \
            {                                                                         \
                switch (v)                                                            \
                {                                                                     \
                    BOOST_PP_SEQ_FOR_EACH(                                            \
                        X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE,          \
                        name,                                                         \
                        enumerators                                                   \
                    )                                                                 \
                    default: return "[Unknown " BOOST_PP_STRINGIZE(name) "]";         \
                }                                                                     \
            }

DEFINE_ENUM_WITH_STRING_CONVERSIONS(Level,
                                    (ACTION)
                                            (AGENT)
                                            (COMMUNICATION)
                                            (DECISION_MAKING)
                                            (MODELING)
                                            (PERCEPTION)
                                            (REPRESENTATIONS)
                                            (UTILS))

class LogSystem;

class Logger {
public:
    virtual ~Logger();

    Logger& log(Level level);
    Logger& log(std::string level);

    template<typename T>
    Logger &operator<<(T const &value) {
        *stream << value;
        return *this;
    }

    void closeStream();

    Logger(Logger const &) = delete;

private:
    Logger(std::string filePath);

    void writeToStream(std::string message);

    bool initialized = false;
    Level debugLevel;
    std::string filePath;
    std::ofstream *stream;

    friend class LogSystem;
};

}
}
#endif //ITANDROIDS_LIB_UTILS_LOGGER_H
