//
// Created by Matheus Camargo on 4/9/16.
//

#ifndef ITANDROIDS_LIB_UTILS_COLORS_H
#define ITANDROIDS_LIB_UTILS_COLORS_H

namespace core {
namespace utils {

struct Color {
    Color(double r, double g, double b, double alpha) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->alpha = alpha;
    }

    Color(double r, double g, double b) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->alpha = 255;
    }

    double r;
    double g;
    double b;
    double alpha;

    static Color WHITE;
    static Color BLACK;
    static Color RED;
    static Color BLUE;
    static Color GREEN;
    static Color PINK;
    static Color ORANGE;
    static Color YELLOW;

    static Color AQUA;
    static Color BROWN;
    static Color CORNFLOWERBLUE;
    static Color CYAN;
    static Color DARKBLUE;
    static Color DARKCYAN;
    static Color DARKGREEN;
    static Color DARKMAGENTA;
    static Color DARKORANGE;
    static Color DARKRED;
    static Color DARKSALMON;
    static Color DARKPINK;
    static Color DODGERBLUE;
    static Color FUCHSIA;
    static Color GRAY;
    static Color GREENYELLOW;
    static Color LAVENDER;
    static Color LIGHTBLUE;
    static Color LIGHTCYAN;
    static Color LIGHTGREEN;
    static Color LIGHTGRAY;
    static Color LIGHTPINK;
    static Color LIGHTYELLOW;
    static Color LIME;
    static Color LIMEGREEN;
    static Color MAGENTA;
    static Color MAROON;
    static Color MEDIUMBLUE;
    static Color MEDIUMORCHID;
    static Color MEDIUMPURPLE;
    static Color NAVY;
    static Color PERSIANBLUE;
    static Color PURPLE;
    static Color SALMON;
    static Color TAN;
    static Color TEAL;
    static Color VIOLET;
    static Color YELLOWGREEN;
};

} /* namespace utils */
} /* namespace core */

#endif //ITANDROIDS_LIB_UTILS_COLORS_H
