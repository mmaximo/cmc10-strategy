//
// Created by Thiago on 22/07/18.
//

#include "FileManager.h"

namespace core {
namespace utils {

bool FileManager::openFile(std::ifstream &file, std::string name) {
    file.open(name, std::ios::binary);
    return file.is_open();
}

bool FileManager::openFile(std::ofstream &file, std::string name, bool append) {
    if(append)
        file.open(name, std::ios::binary | std::ios::app);
    else
        file.open(name, std::ios::binary);

    return file.is_open();
}

bool FileManager::exists(std::string name) {
    std::fstream file(name);
    return file.is_open();
}

}
}