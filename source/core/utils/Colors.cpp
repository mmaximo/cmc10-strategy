//
// Created by Matheus Camargo on 4/9/16.
//

#include "utils/Colors.h"

namespace core {
namespace utils {

//Defining default Colors RGB
Color Color::WHITE = Color(255, 255, 255);
Color Color::BLACK = Color(0, 0, 0);
Color Color::RED = Color(255, 0, 0);
Color Color::BLUE = Color(0, 0, 255);
Color Color::GREEN = Color(0, 255, 0);
Color Color::PINK = Color(255, 51, 255);
Color Color::ORANGE = Color(255, 128, 0);
Color Color::YELLOW = Color(255, 255, 0);

Color Color::AQUA = Color(0, 255, 255);
Color Color::BROWN = Color(165, 42, 42);
Color Color::CORNFLOWERBLUE = Color(100, 149, 237);
Color Color::CYAN = Color(0, 255, 255);
Color Color::DARKBLUE = Color(0, 0, 139);
Color Color::DARKCYAN = Color(0, 139, 139);
Color Color::DARKGREEN = Color(0, 100, 0);
Color Color::DARKMAGENTA = Color(139, 0, 139);
Color Color::DARKORANGE = Color(255, 140, 0);
Color Color::DARKRED = Color(139, 0, 0);
Color Color::DARKSALMON = Color(233, 150, 122);
Color Color::DARKPINK = Color(255, 20, 147);
Color Color::DODGERBLUE = Color(0, 128, 255);
Color Color::FUCHSIA = Color(255, 0, 255);
Color Color::GRAY = Color(128, 128, 128);
Color Color::GREENYELLOW = Color(173, 255, 47);
Color Color::LAVENDER = Color(192, 186, 255);
Color Color::LIGHTBLUE = Color(173, 216, 230);
Color Color::LIGHTCYAN = Color(224, 255, 255);
Color Color::LIGHTGREEN = Color(144, 238, 144);
Color Color::LIGHTGRAY = Color(211, 211, 211);
Color Color::LIGHTPINK = Color(255, 182, 193);
Color Color::LIGHTYELLOW = Color(255, 255, 224);
Color Color::LIME = Color(0, 255, 0);
Color Color::LIMEGREEN = Color(50, 205, 50);
Color Color::MAGENTA = Color(255, 0, 255);
Color Color::MAROON = Color(128, 0, 0);
Color Color::MEDIUMBLUE = Color(0, 0, 205);
Color Color::MEDIUMORCHID = Color(186, 85, 211);
Color Color::MEDIUMPURPLE = Color(147, 112, 219);
Color Color::NAVY = Color(0, 0, 128);
Color Color::PERSIANBLUE = Color(42, 42, 165);
Color Color::PURPLE = Color(128, 0, 128);
Color Color::SALMON = Color(250, 128, 114);
Color Color::TAN = Color(210, 180, 140);
Color Color::TEAL = Color(0, 128, 128);
Color Color::VIOLET = Color(238, 130, 238);
Color Color::YELLOWGREEN = Color(154, 205, 50);

} /* namespace utils */
} /* namespace core */
