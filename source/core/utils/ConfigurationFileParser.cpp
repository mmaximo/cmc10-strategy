//
// Created by mmaximo on 6/3/18.
//

#include "ConfigurationFileParser.h"

#include <string>
#include <fstream>

namespace core {
namespace utils {

ConfigurationFileParser::ConfigurationFileParser(const std::string &filepath) {
    std::ifstream file(filepath.c_str());
    if (!file.good()) {
        std::cerr << filepath << " not found!" << std::endl;
        return;
    }

    std::string key;
    double value = 0;
    while (readLine(file, key, value)) {
        paramMap.insert(std::pair<std::string, double>(key, value));
    }

    file.close();
}

bool ConfigurationFileParser::readLine(std::ifstream &file, std::string &key, double &value) {
    std::string line;
    if (std::getline(file, line)) {
        auto i = (int) line.rfind(':');
        key = line.substr(0, (unsigned long) i);
        std::string valueString = line.substr((unsigned long) i + 1, line.length() - i);
        value = std::stod(valueString);
        return true;
    }
    return false;
}

double ConfigurationFileParser::getParameter(const std::string &paramName) {
    return paramMap[paramName];
}

}
}