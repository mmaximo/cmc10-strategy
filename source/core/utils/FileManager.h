//
// Created by Thiago on 22/07/18.
//

#ifndef ITANDROIDS_VSS_FILEMANAGER_H
#define ITANDROIDS_VSS_FILEMANAGER_H

#include <fstream>

namespace core {
namespace utils {

class FileManager {
public:
    static bool openFile(std::ifstream &file, std::string name);
    static bool openFile(std::ofstream &file, std::string name, bool append = false);
    static bool exists(std::string name);
};

}
}

#endif //ITANDROIDS_VSS_FILEMANAGER_H
