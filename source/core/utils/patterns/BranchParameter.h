//
// Created by francisco on 07/12/16.
//

#ifndef ITANDROIDS_LIB_TREEPARAMETER_H
#define ITANDROIDS_LIB_TREEPARAMETER_H
#include "Parameter.h"
#include <map>
namespace core {
    namespace utils {
        namespace patterns {
            template <class Content = ptree, class Key = std::string>
            class BranchParameter : public Parameter<Content,Key>{
            public:
                /**
                 *
                 * @return true if there is any child
                 */
                virtual bool hasChild(){
                    return !children.empty();
                }
                /**
                 * Gets a Parameter node
                 * @param usedKey to find the element in the map
                 * @return a reference to the child node
                 */
                virtual Parameter<Content,Key> & getChild(const Key & usedKey){
                    return (*children.at(usedKey));
                }
                /**
                 * Can't get content of a BranchParameter node
                 * @throw std::__throw_bad_function_call
                 */
                virtual Content & getContent(){

                    throw std::__throw_bad_function_call;

                }
                /**
                 * Function specific to a BranchParameter
                 * @param key for adding in the map
                 * @param parameter to be added to the map
                 */
                virtual void addChild(Key key,std::shared_ptr<Parameter<Content,Key>> parameter){
                    std::pair<Key,std::shared_ptr<Parameter<Content,Key>>>auxPair(key,parameter);
                    children.insert(auxPair);
                }
                /**
                 *
                 * @return the map to the children of this node
                 */
                virtual std::map<Key,std::shared_ptr<Parameter<Content,Key>>> & getChildren() {
                    return children;
                }

            private:
                /**
                 * Only a TreeParamter node has a map of children
                 */
                std::map<Key,std::shared_ptr<Parameter<Content,Key>>> children;
            };
        }
    }
}




#endif //ITANDROIDS_LIB_TREEPARAMETER_H
