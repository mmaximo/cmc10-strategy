//
// Created by francisco on 07/12/16.
//

#ifndef ITANDROIDS_LIB_LEAFPARAMETER_H
#define ITANDROIDS_LIB_LEAFPARAMETER_H

#include "Parameter.h"
namespace core {
    namespace utils{
        namespace patterns{
            /**
             * Specialization for the Leaf nodes of a parameter tree, having no children
             * @tparam Content
             * @tparam Key
             */
            template <class Content = ptree, class Key = std::string>
            class LeafParameter :public Parameter<Content,Key>{
            public:
            /**
             * LeafParameters have no children
             * @return false
             */
            virtual bool hasChild(){
                return false;
            }
            /**
             * You can't get a child from a leaf parameter, it throws a bad function call
             * @throw std::__throw_bad_function_call
             * @param key
             */
            virtual Parameter<Content,Key> & getChild(const Key & key){
                   throw std::__throw_bad_function_call;
            }

            /**
             * Only a leaf tree has internal content for now
             * @return the internal content of the tree
             */
            virtual Content & getContent(){
                return content;
            }
            /**
             * Can't get children from a leaf node
             * @throw std::__throw_bad_function_call
             */
            virtual std::map<Key,std::shared_ptr<Parameter<Content,Key>>> & getChildren(){
                throw std::__throw_bad_function_call;
            }
            private:
                /**
                 * has a Content variable
                 */
               Content content;

            };
        }
    }
}






#endif //ITANDROIDS_LIB_LEAFPARAMETER_H
