//
// Created by francisco on 07/12/16.
//

#ifndef ITANDROIDS_LIB_PARAMETER_H
#define ITANDROIDS_LIB_PARAMETER_H

#include <boost/property_tree/ptree.hpp>
#include <map>
#include <memory>
using boost::property_tree::ptree;
namespace core {
    namespace utils{
        namespace patterns{
            template <class Content = ptree, class Key = std::string>
            class Parameter {
              public:
                /**
                 *
                 * @return true is Parameter has a child
                 */
                virtual bool hasChild()=0;
                /**
                 *
                 * @param usedKey
                 * @return a child that has that key
                 */
                virtual Parameter<Content,Key> & getChild(const Key & usedKey) = 0;
                /**
                 *
                 * @return the content of the parameter
                 */
                virtual Content & getContent() =0 ;
                /**
                 * Gets the children from the tree, using a specific key
                 * @todo templatize the container, so that the definition doesn't have to use a map
                 * @return a map of children from the tree
                 */
                virtual std::map<Key,std::shared_ptr<Parameter<Content,Key>>> & getChildren() = 0;

              private:

            };
        }


    }

}




#endif //ITANDROIDS_LIB_PARAMETER_H
