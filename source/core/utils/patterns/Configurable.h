//
// Created by francisco on 07/12/16.
//

#ifndef ITANDROIDS_LIB_CONFIGURABLE_H
#define ITANDROIDS_LIB_CONFIGURABLE_H

#include <iostream>
#include "Parameter.h"
namespace core {
    namespace utils {
        namespace patterns {
            /**
             * Class that allows a set parameter function, using CRTP for calling the correct function
             * @tparam T class that will accept a template function to be called
             */
        template< class T>
        class Configurable {

        public:
            /**
             * Function that allows a dynamic parameter setting in code
             * @tparam Content content of the parameter tree
             * @tparam Key of the parameter tree
             * @param parameter the node of the parameter tree that will be used in this section
             */
            template<class Content, class Key>
            void setParameter(Parameter<Content, Key> & parameter){
                (static_cast<T*>(this))->setParameter(parameter);
            }

        };
        }
    }
}



#endif //ITANDROIDS_LIB_CONFIGURABLE_H
