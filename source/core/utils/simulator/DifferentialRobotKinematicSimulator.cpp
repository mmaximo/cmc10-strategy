//
// Created by mmaximo on 11/2/18.
//

#include <math/MathUtils.h>
#include "DifferentialRobotKinematicSimulator.h"

namespace core {
namespace utils {

DifferentialRobotKinematicSimulator::DifferentialRobotKinematicSimulator(double wheelRadius,
                                                                         double distanceBetweenWheels,
                                                                         double maxWheelSpeed,
                                                                         const math::Pose2D &initialPose) : wheelRadius(
        wheelRadius), distanceBetweenWheels(distanceBetweenWheels), maxWheelSpeed(maxWheelSpeed), pose(initialPose) {

}

void DifferentialRobotKinematicSimulator::step(double elapsedTime, double linearSpeed, double angularSpeed) {
    double wl = (linearSpeed - angularSpeed * distanceBetweenWheels / 2.0) / wheelRadius;
    double wr = (linearSpeed + angularSpeed * distanceBetweenWheels / 2.0) / wheelRadius;
    wl = math::MathUtils::clamp(wl, -maxWheelSpeed, maxWheelSpeed);
    wr = math::MathUtils::clamp(wr, -maxWheelSpeed, maxWheelSpeed);
    linearSpeed = (wr + wl) * wheelRadius / 2.0;
    angularSpeed = (wr - wl) * wheelRadius / distanceBetweenWheels;

    math::Pose2D oldPose = pose;
    if (std::abs(angularSpeed) < 1.0e-3) {
        pose.translation.x =
                oldPose.translation.x + linearSpeed * cos(oldPose.rotation + angularSpeed * elapsedTime / 2.0) * elapsedTime;
        pose.translation.y =
                oldPose.translation.y + linearSpeed * sin(oldPose.rotation + angularSpeed * elapsedTime / 2.0) * elapsedTime;
        pose.rotation = oldPose.rotation + angularSpeed * elapsedTime;
    } else {
        pose.translation.x = oldPose.translation.x + 2.0 * (linearSpeed / angularSpeed) *
                                                     cos(oldPose.rotation + angularSpeed * elapsedTime / 2.0) *
                                                     sin(angularSpeed * elapsedTime / 2.0);
        pose.translation.y = oldPose.translation.y + 2.0 * (linearSpeed / angularSpeed) *
                                                     sin(oldPose.rotation + angularSpeed * elapsedTime / 2.0) *
                                                     sin(angularSpeed * elapsedTime / 2.0);
        pose.rotation = oldPose.rotation + angularSpeed * elapsedTime;
    }
    pose.rotation = math::MathUtils::normalizeAngle(pose.rotation);
}

const math::Pose2D &DifferentialRobotKinematicSimulator::getPose() {
    return pose;
}

}
}