//
// Created by mmaximo on 11/2/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_DIFFERENTIALROBOTKINEMATICSIMULATOR_H
#define CMC10_STRATEGY_PRIVATE_DIFFERENTIALROBOTKINEMATICSIMULATOR_H

#include <math/Pose2D.h>
#include <representations/WheelSpeed.h>

namespace core {
namespace utils {

class DifferentialRobotKinematicSimulator {
public:
    DifferentialRobotKinematicSimulator(double wheelRadius, double distanceBetweenWheels, double maxWheelSpeed, const math::Pose2D &initialPose);

    void step(double elapsedTime, double linearSpeed, double angularSpeed);

    const math::Pose2D &getPose();

private:
    double wheelRadius;
    double distanceBetweenWheels;
    double maxWheelSpeed;
    math::Pose2D pose;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_DIFFERENTIALROBOTKINEMATICSIMULATOR_H
