//
// Created by mmaximo on 6/3/18.
//

#ifndef ITANDROIDS_HUMANOID_CONFIGURATIONFILEPARSER_H
#define ITANDROIDS_HUMANOID_CONFIGURATIONFILEPARSER_H

#include <iostream>
#include <map>

namespace core {
namespace utils {

/**
* File parser for configuration files. The configuration file must follow a key:value pattern
* for all its parameters. Example:
* kp:1.0
* kd:2.0
* ki:3.0
* threshold:4.0
* maximumCommand:10.0
*/
class ConfigurationFileParser {
public:
    /**
     * Constructs a configuration file parser.
     *
     * @param filepath filepath of the file to be parsed.
     */
    explicit ConfigurationFileParser(const std::string &filepath);

    /**
     * Read a line from a configuration file.
     *
     * @param file file stream.
     * @param key key read.
     * @param value value read.
     * @return if the read was successful.
     */
    bool readLine(std::ifstream &file, std::string &key, double &value);

    /**
     * Gets a parameter value present in the configuration file.
     *
     * @param paramName param name.
     * @return param value.
     */
    double getParameter(const std::string &paramName);

private:
    std::map<std::string, double> paramMap;
};

}
}

#endif //ITANDROIDS_HUMANOID_CONFIGURATIONFILE_H
