//
// Created by mmaximo on 11/1/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_DECISIONMAKINGINTERFACE_H
#define CMC10_STRATEGY_PRIVATE_DECISIONMAKINGINTERFACE_H

#include <control/request/ActionRequest.h>
#include <memory>
#include <representations/Player.h>

namespace core {
namespace decision_making {

/**
 * Represents the decision making interface, which contains actions
 * requested by the decision making layer. These actions are then
 * handled by the control layer.
 */
class DecisionMakingInterface {
public:
    /**
     * Constructs the decision making interface.
     */
    DecisionMakingInterface();

    /**
     * Clear all action requests.
     */
    void clearRequests();

    /**
     * Obtains an array of requests (one for each player).
     * @return
     */
    const std::shared_ptr<control::ActionRequest> *getRequests() const;

    /**
     * Obtains the action request for a specific player.
     * @param playerId player id
     * @return action request for the player whose id is player id
     */
    const std::shared_ptr<control::ActionRequest> getRequestByPlayerId(int playerId) const;

    /**
     * Request an action.
     */
    void requestAction(std::shared_ptr<control::ActionRequest> request);

    /**
     * Requests an angle control action.
     * @param desiredAngle desired angle
     * @param playerId player id
     */
    void requestAngleControl(double desiredAngle, int playerId);

    /**
     * Requests a direct command action, which manipulates the robot's wheels speeds directly.
     * @param wheelSpeed desired wheels speeds
     * @param playerId player id
     */
    void requestDirectCommand(const representations::WheelSpeed &wheelSpeed, int playerId);

    /**
     * Requests a followline action. The line is represented by a Pose2D, where the translation
     * represents a point where the line passes and the rotation is the line's angle.
     * The robot is expected to stop when the desiredPose is reached.
     * @param desiredPose desired pose
     * @param playerId player id
     */
    void requestFollowLine(const math::Pose2D &desiredPose, int playerId);

    /**
     * Requests a robot to spin with a given angular speed.
     * @param angularSpeed desired angular speed
     * @param playerId player id
     */
    void requestSpin(double angularSpeed, int playerId);

    /**
     * Requests an univector control, where the robot moves forward while
     * attaining a desired angle.
     * @param desiredAngle desired angle
     * @param playerId player id
     */
    void requestUnivectorControl(double desiredAngle, int playerId);

private:
    std::shared_ptr<control::ActionRequest> requests[representations::Player::PLAYERS_PER_SIDE]; /// array of requests
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_DECISIONMAKINGINTERFACE_H
