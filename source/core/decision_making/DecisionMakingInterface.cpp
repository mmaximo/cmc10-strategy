//
// Created by mmaximo on 11/1/18.
//

#include "DecisionMakingInterface.h"
#include <core/control/request/AngleControlRequest.h>
#include <core/control/request/DirectCommandRequest.h>
#include <core/control/request/FollowLineRequest.h>
#include <core/control/request/SpinRequest.h>
#include <core/control/request/UnivectorControlRequest.h>

namespace core {
namespace decision_making {

DecisionMakingInterface::DecisionMakingInterface() {
    clearRequests();
}

void DecisionMakingInterface::clearRequests() {
    for (auto &request : requests)
        request = nullptr;
}

const std::shared_ptr<control::ActionRequest> *DecisionMakingInterface::getRequests() const {
    return requests;
}

const std::shared_ptr<control::ActionRequest> DecisionMakingInterface::getRequestByPlayerId(int playerId) const {
    return requests[playerId];
}

void DecisionMakingInterface::requestAction(std::shared_ptr<control::ActionRequest> request) {
    requests[request->getPlayerId()] = request;
}

void DecisionMakingInterface::requestAngleControl(double desiredAngle, int playerId) {
    requestAction(std::make_shared<control::AngleControlRequest>(desiredAngle, playerId));
}

void DecisionMakingInterface::requestDirectCommand(const representations::WheelSpeed &wheelSpeed, int playerId) {
    requestAction(std::make_shared<control::DirectCommandRequest>(wheelSpeed, playerId));
}

void DecisionMakingInterface::requestFollowLine(const math::Pose2D &desiredPose, int playerId) {
    requestAction(std::make_shared<control::FollowLineRequest>(desiredPose, playerId));
}

void DecisionMakingInterface::requestSpin(double angularSpeed, int playerId) {
    requestAction(std::make_shared<control::SpinRequest>(angularSpeed, playerId));
}

void DecisionMakingInterface::requestUnivectorControl(double desiredAngle, int playerId) {
    requestAction(std::make_shared<control::UnivectorControlRequest>(desiredAngle, playerId));
}

}
}
