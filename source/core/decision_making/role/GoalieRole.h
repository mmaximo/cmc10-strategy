//
// Created by mmaximo on 11/25/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_GOALIEROLE_H
#define CMC10_STRATEGY_PRIVATE_GOALIEROLE_H

#include <decision_making/role/Role.h>

namespace core {
namespace decision_making {

/**
 * Represents states of the goalie role.
 */
enum class GoalieState {
    LINE_DEFENSE,
    SPINNING
};

/**
 * Represents the goalie role.
 */
class GoalieRole : public Role {
public:
    /**
     * Constructs the goalie role.
     * @param playerId player id
     */
    explicit GoalieRole(int playerId);

    /**
     * Executes the goalie role.
     * @param worldModel world model
     * @param decisionMakingInterface decision making interface
     */
    void execute(modeling::WorldModel &worldModel, decision_making::DecisionMakingInterface &decisionMakingInterface) override;

private:
    GoalieState state;

    /**
     * Changes the state of the goalie role.
     * @param worldModel world model
     * @param decisionMakingInterface decision making interface
     */
    void changeState(modeling::WorldModel &worldModel,
                     decision_making::DecisionMakingInterface &decisionMakingInterface);

    /**
     * Executes the current state.
     * @param worldModel world model
     * @param decisionMakingInterface decision making interface
     */
    void executeState(modeling::WorldModel &worldModel,
                      decision_making::DecisionMakingInterface &decisionMakingInterface);

};

}
}

#endif //CMC10_STRATEGY_PRIVATE_GOALIEROLE_H
