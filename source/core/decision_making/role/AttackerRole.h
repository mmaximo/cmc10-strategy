//
// Created by mmaximo on 11/25/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_ATTACKERROLE_H
#define CMC10_STRATEGY_PRIVATE_ATTACKERROLE_H

#include <decision_making/role/Role.h>
#include <decision_making/UnivectorField.h>

namespace core {
namespace decision_making {

/**
 * Represents an attacker role.
 */
class AttackerRole : public Role {
public:
    /**
     * Constructs an attacker role.
     * @param playerId player id
     */
    explicit AttackerRole(int playerId);

    /**
     * Executes the attacker role.
     * @param worldModel world model
     * @param decisionMakingInterface decision making interface
     */
    void execute(modeling::WorldModel &worldModel, decision_making::DecisionMakingInterface &decisionMakingInterface) override;

private:
    UnivectorField univectorField; /// univector field path planner
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_ATTACKERROLE_H
