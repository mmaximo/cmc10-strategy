//
// Created by mmaximo on 11/25/18.
//

#include "GoalieRole.h"

namespace core {
namespace decision_making {

GoalieRole::GoalieRole(int playerId) : state(GoalieState::LINE_DEFENSE), Role(playerId) {

}

void GoalieRole::execute(modeling::WorldModel &worldModel,
                         decision_making::DecisionMakingInterface &decisionMakingInterface) {
    changeState(worldModel, decisionMakingInterface);
    executeState(worldModel, decisionMakingInterface);
}

void GoalieRole::changeState(modeling::WorldModel &worldModel,
                             decision_making::DecisionMakingInterface &decisionMakingInterface) {
    switch (state) {
        case GoalieState ::LINE_DEFENSE:
            // Add logic to change state
            break;
        case GoalieState::SPINNING:
            // Add logic to change state
            break;
    }
}

void GoalieRole::executeState(modeling::WorldModel &worldModel,
                              decision_making::DecisionMakingInterface &decisionMakingInterface) {
    switch (state) {
        case GoalieState::LINE_DEFENSE:
            // Add state execution logic
            break;
        case GoalieState::SPINNING:
            // Add state execution logic
            break;
    }
}

}
}