//
// Created by mmaximo on 11/25/18.
//

#include "AttackerRole.h"

namespace core {
namespace decision_making {

AttackerRole::AttackerRole(int playerId) : Role(playerId), univectorField(UnivectorFieldParams::getDefaultParams()) {

}

void AttackerRole::execute(modeling::WorldModel &worldModel,
                           decision_making::DecisionMakingInterface &decisionMakingInterface) {
    if (worldModel.getBall().isSeen() && worldModel.getTeammates()[playerId].isSeen()) {
        double angle = univectorField.computeMoveToGoalField(worldModel.getBall().getPosition(),
                                                             worldModel.getTeammates()[playerId].getPose().translation);
        decisionMakingInterface.requestUnivectorControl(angle, playerId);
    }
}

}
}