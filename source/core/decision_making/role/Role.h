//
// Created by mmaximo on 11/25/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_ROLE_H
#define CMC10_STRATEGY_PRIVATE_ROLE_H

#include <modeling/WorldModel.h>
#include <decision_making/DecisionMakingInterface.h>

namespace core {
namespace decision_making {

/**
 * Represents a player role.
 */
class Role {
public:
    /**
     * Constructs a player role.
     * @param playerId player id
     */
    explicit Role(int playerId);

    /**
     * Executes the player role.
     * @param worldModel world model
     * @param decisionMakingInterface decision making interface
     */
    virtual void execute(modeling::WorldModel &worldModel, decision_making::DecisionMakingInterface &decisionMakingInterface) = 0;

protected:
    int playerId;
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_ROLE_H
