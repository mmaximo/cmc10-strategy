//
// Created by mmaximo on 11/3/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_DECISIONMAKING_H
#define CMC10_STRATEGY_PRIVATE_DECISIONMAKING_H

#include <core/decision_making/DecisionMakingInterface.h>
#include <core/decision_making/UnivectorField.h>
#include <core/decision_making/role/Role.h>
#include <core/representations/Player.h>

namespace core {
namespace decision_making {

/**
 * Represents the decision making layer.
 */
class DecisionMaking {
public:
    /**
     * Constructs the decision making layer.
     */
    DecisionMaking();

    /**
     * Executes the decision making layer.
     * @param worldModel world model
     */
    void decide(modeling::WorldModel &worldModel);

    /**
     * Obtains the decision making interface, which contains the actions requested
     * by the decision making layer.
     * @return decision making interface
     */
    const DecisionMakingInterface &getDecisionMakingInterface();

private:
    DecisionMakingInterface decisionMakingInterface;
    std::shared_ptr<Role> roles[representations::Player::PLAYERS_PER_SIDE]; /// array of roles
};

}
}

#endif //CMC10_STRATEGY_PRIVATE_DECISIONMAKING_H
