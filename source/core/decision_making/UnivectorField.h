//
// Created by mmaximo on 11/2/18.
//

#ifndef CMC10_STRATEGY_PRIVATE_UNIVECTORFIELD_H
#define CMC10_STRATEGY_PRIVATE_UNIVECTORFIELD_H

#include <math/Pose2D.h>

namespace core {
namespace decision_making {

/**
 * Set of parameters for an univector field.
 */
struct UnivectorFieldParams {
    double kr; /// parameter to tune how fast the robot converges to the hyperbolic spiral
    double de; /// radius that define the size of the hyperbolic spiral
    double ko; /// how much the obstacle position is modified due to the robot's velocity
    double dmin; /// minimum safe distance to consider move-to-goal in a composed field
    double delta; /// "standard deviation" of the "gaussian" used for composing fields

    static UnivectorFieldParams getDefaultParams();
};

/**
 * Represents an univector field trajectory planner.
 */
class UnivectorField {
public:
    /**
     * Constructs an univector field.
     * @param params univector field's parameters
     */
    explicit UnivectorField(const UnivectorFieldParams &params);

    /**
     * Computes a hyperbolic spiral univector field.
     * @param currentPosition current position of the robot
     * @param clockwise if the spiral should rotate clockwise or counterclockwise
     * @return angle of the univector field
     */
    double computeHyperbolicSpiralField(const math::Vector2<double> &currentPosition, bool clockwise);

    /**
     * Computes a repulsive univector field.
     * @param currentPosition current position of the robot
     * @return angle of the univector field
     */
    double computeRepulsiveField(const math::Vector2<double> &currentPosition);

    /**
     * Computes a move-to-goal univector field.
     * @param goal goal target
     * @param currentPosition current position of the robot
     * @return angle of the univector field
     */
    double computeMoveToGoalField(const math::Pose2D &goal, const math::Vector2<double> &currentPosition);

    /**
     * Computes an avoid obstacle univector field.
     * @param currentPosition current position of the robot
     * @param currentVelocity current velocity of the robot
     * @param obstacle obstacle position
     * @return angle of the univector field
     */
    double computeAvoidObstacleField(const math::Vector2<double> &currentPosition, const math::Vector2<double> &currentVelocity, const math::Vector2<double> &obstacle);

    /**
     * Computes a composed univector field.
     * @param goal goal target
     * @param currentPosition current position of the robot
     * @param currentVelocity current velocity of the robot
     * @param obstacle obstacle position
     * @return angle of the univector field
     */
    double computeComposedField(const math::Pose2D &goal, const math::Vector2<double> &currentPosition, const math::Vector2<double> &currentVelocity, const math::Vector2<double> &obstacle);

private:
    UnivectorFieldParams params;

    math::Vector2<double> computeModifiedObstacle(const math::Vector2<double> &currentPosition, const math::Vector2<double> &currentVelocity, const math::Vector2<double> &obstacle);

};

}
}

#endif //CMC10_STRATEGY_PRIVATE_UNIVECTORFIELD_H
