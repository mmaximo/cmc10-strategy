//
// Created by mmaximo on 11/3/18.
//

#include "DecisionMaking.h"

#include <core/decision_making/role/GoalieRole.h>
#include <core/decision_making/role/AttackerRole.h>

namespace core {
namespace decision_making {

DecisionMaking::DecisionMaking() {
    roles[0] = std::make_shared<GoalieRole>(0);
    roles[1] = std::make_shared<AttackerRole>(1);
    roles[2] = std::make_shared<AttackerRole>(2);
}

void DecisionMaking::decide(modeling::WorldModel &worldModel) {
    decisionMakingInterface.clearRequests();
    for (int id = 0; id < representations::Player::PLAYERS_PER_SIDE; ++id) {
        roles[id]->execute(worldModel, decisionMakingInterface);
    }
}

const DecisionMakingInterface &DecisionMaking::getDecisionMakingInterface() {
    return decisionMakingInterface;
}

}
}