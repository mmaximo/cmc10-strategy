//
// Created by mmaximo on 11/2/18.
//

#include <math/MathUtils.h>
#include "UnivectorField.h"

namespace core {
namespace decision_making {

UnivectorFieldParams UnivectorFieldParams::getDefaultParams() {
    UnivectorFieldParams params;
    params.kr = 0.1;
    params.de = 0.3;
    params.ko = 0.1;
    params.dmin = 0.3;
    params.delta = 0.1;
    return params;
}

UnivectorField::UnivectorField(const UnivectorFieldParams &params) : params(params) {

}

double UnivectorField::computeHyperbolicSpiralField(const math::Vector2<double> &currentPosition, bool clockwise) {
    // Implement
}

double UnivectorField::computeRepulsiveField(const math::Vector2<double> &currentPosition) {
    // Implement
}

double
UnivectorField::computeMoveToGoalField(const math::Pose2D &goal, const math::Vector2<double> &currentPosition) {
    // Implement
}

double UnivectorField::computeAvoidObstacleField(const math::Vector2<double> &currentPosition,
                                                 const math::Vector2<double> &currentVelocity,
                                                 const math::Vector2<double> &obstacle) {
    // Implement
}

double UnivectorField::computeComposedField(const math::Pose2D &goal, const math::Vector2<double> &currentPosition,
                                            const math::Vector2<double> &currentVelocity,
                                            const math::Vector2<double> &obstacle) {
    // Implement
}

math::Vector2<double> UnivectorField::computeModifiedObstacle(const math::Vector2<double> &currentPosition,
                                                              const math::Vector2<double> &currentVelocity,
                                                              const math::Vector2<double> &obstacle) {
    // Implement
}


}
}
