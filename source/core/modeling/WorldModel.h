#ifndef CORE_MODELING_WORLDMODEL_H_
#define CORE_MODELING_WORLDMODEL_H_

#include <core/representations/Ball.h>
#include <core/representations/Player.h>
#include <core/representations/OpponentPlayer.h>
#include "core/modeling/vision/VisionInterface.h"

#include "representations/Field.h"

namespace core {
namespace modeling {

class WorldModel {

public:
    WorldModel();

    virtual ~WorldModel();

    void update(double elapsedTime, VisionInterface &visionInterface);

    representations::Player* getTeammates();

    representations::Player& getTeammateById(int id);

    representations::OpponentPlayer* getOpponents();

    representations::Ball& getBall();

    void invert();

private:
    const static int PLAYERS_PER_SIDE = representations::Player::PLAYERS_PER_SIDE;
    double side;

    representations::Ball ball;
    representations::Player teamMatePlayers[PLAYERS_PER_SIDE];
    representations::OpponentPlayer opponentPlayers[PLAYERS_PER_SIDE];
    VisionInterface previousVisionInterface;

};

} /* namespace modeling */

} /* namespace core */

#endif /* CORE_MODELING_WORLDMODEL_H_ */

