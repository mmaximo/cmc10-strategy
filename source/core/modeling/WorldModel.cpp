#include <modeling/vision/VisionInterface.h>
#include <math/MathUtils.h>
#include "modeling/WorldModel.h"
//#include "gui/widgets/debug/WorldModelInterface.h"

namespace core {
namespace modeling {

WorldModel::WorldModel() {
    // Create players
    for (int i = 0; i < PLAYERS_PER_SIDE; i++) {
        teamMatePlayers[i].setId(i);
        opponentPlayers[i].setId(-1);
    }
    side = 1.0;
}

WorldModel::~WorldModel() {
    for (auto &teamMatePlayer : teamMatePlayers) {
        teamMatePlayer.setVelocity(Vector2<double>(0.0, 0.0));
    }
}

void WorldModel::invert() {
    std::cout << "Inverting sides" << std::endl;
    side *= (-1.0);
}

void WorldModel::update(double elapsedTime, VisionInterface &visionInterface) {
    Pose2D aux;

    // Update ball position and velocity
    if (previousVisionInterface.ball.isSeen() && visionInterface.ball.isSeen()) {
        math::Vector2<double> deltaPosition = visionInterface.ball.getPosition() - previousVisionInterface.ball.getPosition();
        ball.setVelocity(deltaPosition * (1.0 / elapsedTime) * side);
    } else if (!previousVisionInterface.ball.isSeen() && !visionInterface.ball.isSeen()) {
        ball.setVelocity(math::Vector2<double>(0.0, 0.0));
    }
    if (visionInterface.ball.isSeen())
        ball.setPosition(visionInterface.ball.getPosition() * side);
    ball.setSeen(visionInterface.ball.isSeen());

    for (int i = 0; i < PLAYERS_PER_SIDE; ++i) {
        if (previousVisionInterface.teammates[i].isSeen() && visionInterface.teammates[i].isSeen()) {
            math::Vector2<double> deltaPosition = visionInterface.teammates[i].getPosition() - previousVisionInterface.teammates[i].getPosition();
            teamMatePlayers[i].setVelocity(deltaPosition * (1.0 / elapsedTime) * side);
        } else if (!previousVisionInterface.teammates[i].isSeen() && !visionInterface.teammates[i].isSeen()) {
            teamMatePlayers[i].setVelocity(math::Vector2<double>(0.0, 0.0));
        }
        if (visionInterface.teammates[i].isSeen()) {
            teamMatePlayers[i].setPosition(visionInterface.teammates[i].getPosition() * side);
            teamMatePlayers[i].setRotation(math::MathUtils::normalizeAngle(visionInterface.teammates[i].getRotation() - (side >= 0.5 ? 0.0 : M_PI)));
        }
        teamMatePlayers[i].setSeen(visionInterface.teammates[i].isSeen());
    }

    for (int i = 0; i < PLAYERS_PER_SIDE; ++i) {
        if (visionInterface.opponents[i].isSeen())
            opponentPlayers[i].setPosition(visionInterface.opponents[i].getPosition() * side);
        opponentPlayers[i].setSeen(visionInterface.opponents[i].isSeen());
    }

    previousVisionInterface = visionInterface;
////    core::gui::WorldModelInterface::getInstance().emitSignal(&localizationInterface);
}

representations::Ball& WorldModel::getBall() {
    return ball;
}

representations::Player* WorldModel::getTeammates() {
    return teamMatePlayers;
}

representations::Player &WorldModel::getTeammateById(int id) {
    int answer = 0;
    for (int i = 0; i < PLAYERS_PER_SIDE; i++) {
        if (teamMatePlayers[i].getId() == id) {
            answer = i;
        }
    }
    return teamMatePlayers[answer];
}


representations::OpponentPlayer* WorldModel::getOpponents() {
    return opponentPlayers;
}

} /* namespace modeling */
} /* namespace core */
