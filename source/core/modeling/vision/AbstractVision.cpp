//
// Created by root on 13/09/17.
//

#include "AbstractVision.h"

namespace core {
namespace modeling {

AbstractVision::AbstractVision() {
    cameraProjection = new core::modeling::CameraProjection(
            "../configs/intrinsicParameters.txt",
            "../configs/extrinsicParameters.txt");
}

core::modeling::CameraProjection *AbstractVision::getCameraProjection() {
    return cameraProjection;
}

}
}