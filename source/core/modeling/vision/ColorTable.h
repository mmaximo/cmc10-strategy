//
// Created by mmaximo on 12/6/16.
//

#ifndef ITANDROIDS_LIB_COLORTABLE_H
#define ITANDROIDS_LIB_COLORTABLE_H

namespace core {
namespace modeling {

class Image;

/**
 * Represents a color look-up table (LUT).
 */
class ColorTable {
public:
    /**
     * Default constructor.
     */
    ColorTable();

    /**
     * Resets the table.
     */
    void reset();

    /**
     * Segments an image using the color table.
     * @param cameraImage camera image (input)
     * @param segmentedImage segmented image (output)
     */
    void segmentImage(Image& cameraImage, Image& segmentedImage);

    /**
     * Gets the color class associated with (c1, c2, c3), where c1, c2, and c3 are
     * color channels.
     * @param c1 value of the first color channel
     * @param c2 value of the second color channel
     * @param c3 value of the third color channel
     * @return color class associated with (c1, c2, c3)
     */
    unsigned char getColor(unsigned char c1, unsigned char c2, unsigned char c3);

    /**
     * Assigns the color class associated with (c1, c2, c3), where c1, c2, and c3 are
     * color channels.
     * @param c1 value of the first color channel
     * @param c2 value of the second color channel
     * @param c3 value of the third color channel
     * @param c color class associated with (c1, c2, c3)
     */
    void assignColor(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c);

    static const unsigned char UNDEFINED_COLOR_ID; /// color id associated with undefined color

    /**
     * Gets the color look-up table.
     * @return color look-up table
     */
    unsigned char *getTable();

    /**
     * Gets the table size in bytes.
     * @return table size
     */
    const int getTableSize();

    /**
     * Loads look-up table from a file.
     * @param fileName file to load the LUT from
     */
    void loadLUT(const char *fileName);

private:
    static const int CHANNEL_NUM_BITS = 7; /// number of bits used to store each channel
    static const int CHANNEL_SIZE = 1 << CHANNEL_NUM_BITS;
    static const int TABLE_SIZE = CHANNEL_SIZE * CHANNEL_SIZE * CHANNEL_SIZE;

    unsigned char table[TABLE_SIZE];

};

}
}


#endif //ITANDROIDS_LIB_COLORTABLE_H
