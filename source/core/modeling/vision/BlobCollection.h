//
// Created by mmaximo on 11/11/16.
//

#ifndef ITANDROIDS_LIB_BLOBCOLLECTION_H
#define ITANDROIDS_LIB_BLOBCOLLECTION_H

namespace core {
namespace modeling {

class Blob;

/**
 * Represents a collection of blobs.
 */
class BlobCollection {
private:
    int numColors;
    int maxBlobs;

public:
    Blob **table; /// table[color][i]
    int *count; /// number of blobs for each color

    /**
     * Constructs a collection of blobs.
     * @param numColors number of colors considered
     * @param maxBlobs maximum number of blobs for each color
     */
    BlobCollection(int numColors, int maxBlobs);

    /**
     * Default destructor.
     */
    ~BlobCollection();

    /**
     * Clears the collection.
     */
    void clear();

    /**
     * Index operator for obtaining the blob array for a given color.
     * @param color color index
     * @return the blob array for the respective color
     */
    Blob* operator[](int color);

    /**
     * Adds a new blob to the collection.
     * @param color blob color
     * @param blob blob object
     */
    void addBlob(int color, const Blob& blob);

    /**
     * Gets the number of colors considered in this collection.
     * @return number of colors
     */
    int getNumColors();

    /**
     * Gets the number of blobs in a given color.
     * @return number of blobs in the respective color
     */
    int getBlobCountByColor(int color) const;

    /**
     * Gets the maximum number of blobs supported by this collection for each color.
     * @return maximum number of blobs supported
     */
    int getMaxBlobs();
};

}
}

#endif //ITANDROIDS_LIB_BLOBCOLLECTION_H
