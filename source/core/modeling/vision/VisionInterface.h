#ifndef CORE_MODELING_VISION_VISION_INTERFACE_H_
#define CORE_MODELING_VISION_VISION_INTERFACE_H_

#include "representations/Player.h"
#include "representations/OpponentPlayer.h"
#include "representations/Ball.h"
#include <vector>

namespace core {
namespace modeling {

struct VisionInterface {
    representations::Player teammates[representations::Player::PLAYERS_PER_SIDE];
    representations::OpponentPlayer opponents[representations::Player::PLAYERS_PER_SIDE];
    representations::Ball ball;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    VisionInterface();
};

}
}


#endif // CORE_MODELING_VISION_VISION_INTERFACE_H_