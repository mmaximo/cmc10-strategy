//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_MODELING_VISION_CAMERA_VISION_H_
#define CORE_MODELING_VISION_CAMERA_VISION_H_

#include "AbstractVision.h"
#include <modeling/vision/ColorTable.h>
#include <modeling/vision/Image.h>
#include <modeling/vision/BlobCollection.h>
#include <modeling/vision/BlobDetector.h>
#include <modeling/vision/Blob.h>
#include <modeling/vision/CameraProjection.h>
#include "modeling/vision/ObjectDetector.h"
#include <communication/camera/PointGreyFrameGrabber.h>
#include <memory>

namespace core {
namespace modeling {
class CameraVision : public AbstractVision {
public:
    CameraVision();

    ~CameraVision();

    VisionInterface *update();

    int getWidth() override;

    int getHeight() override;

    unsigned char *getImagePtr() override;

    bool check() override;

    void setTeamColor(core::modeling::VSSColor teamColor) override;

    core::modeling::Image *getSegmentedImage() override;

private:
    int width;
    int height;
    core::communication::PointGreyFrameGrabber frameGrabber;
    std::shared_ptr<ColorTable> colorTable;
    std::shared_ptr<Image> cameraImage;
    std::shared_ptr<Image> segmentedImage;
    std::shared_ptr<BlobDetector> blobDetector;
    BlobCollection blobs;
    std::shared_ptr<ObjectDetector> objectDetector;
    VisionInterface visionInterface;
    core::modeling::VSSColor teamColor;
    std::shared_ptr<Image> rgbFrame;

private:
    core::modeling::VSSColor opponentColor;
    unsigned char *imagePtr;
};

} /* namespace modeling */

} /* namespace core */

#endif // CORE_MODELING_VISION_CAMERA_VISION_H_