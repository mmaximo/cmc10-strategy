//
// Created by mmaximo on 11/11/16.
//

#include "Blob.h"

namespace core {
namespace modeling {

Blob::Blob() {
}

Blob::Blob(int xi, int xf, int yi, int yf) : xi(xi), xf(xf), yi(yi), yf(yf), valid(true) {
    position.x = (xi + xf) / 2.0;
    position.y = (yi + yf) / 2.0;
    area = (xf - xi + 1) * (yf - yi + 1);
}

void Blob::merge(Blob &other) {
    // Implement
}

bool Blob::overlaps(Blob &other) {
    return (xi <= other.xf && other.xi <= xf && yi <= other.yf && other.yi <= yf);
}

bool Blob::overlapsX(Blob &other) {
    return (xi <= other.xf && other.xi <= xf);
}

bool Blob::overlapsY(Blob &other) {
    return (yi <= other.yf && other.yi <= yf);
}

bool Blob::overlapsWithMargin(Blob &other, int marginX, int marginY) {
    return (xi - other.xf <= marginX && other.xi - xf <= marginX &&
            yi - other.yf <= marginY && other.yi - yf <= marginY);
}

bool Blob::overlapsWithMarginX(Blob &other, int marginX) {
    return (xi - other.xf <= marginX && other.xi - xf <= marginX);
}

bool Blob::overlapsWithMarginY(Blob &other, int marginY) {
    return (yi - other.yf <= marginY && other.yi - yf <= marginY);
}

}
}