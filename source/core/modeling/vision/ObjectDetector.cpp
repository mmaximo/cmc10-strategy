//
// Created by mmaximo on 10/19/18.
//

#include "ObjectDetector.h"

#include <iostream>
#include "modeling/vision/Blob.h"

namespace core {
namespace modeling {


VisibleTeammate::VisibleTeammate(int id, const math::Pose2D &pose) : id(id), pose(pose) {

}

int VisibleTeammate::getId() const {
    return id;
}

const math::Pose2D &VisibleTeammate::getPose() const {
    return pose;
}

ObjectDetector::ObjectDetector(VSSColor ourTeamColor, const std::vector<VSSColor> &idToColorMapping, VSSColor opponentColor,
                               VSSColor ballColor) : ourTeamColor(ourTeamColor), opponentColor(opponentColor), ballColor(ballColor) {

}

ObjectDetector::ObjectDetector(VSSColor ourTeamColor) : ourTeamColor(ourTeamColor) {
    if (ourTeamColor == VSSColor::BLUE)
        opponentColor = VSSColor::YELLOW;
    else
        opponentColor = VSSColor::BLUE;

    idToColorMapping = getDefaultIdToColorMapping();

    ballColor = VSSColor::ORANGE;
}

void ObjectDetector::detectTeammates(BlobCollection &blobs, std::vector<VisibleTeammate> &visibleTeammates) {
    visibleTeammates.clear();

    // Implement
}

void ObjectDetector::detectOpponents(BlobCollection &blobs, std::vector<math::Vector2<double>> &visibleOpponents) {
    visibleOpponents.clear();

    // Implement
}

bool ObjectDetector::detectBall(BlobCollection &blobs, math::Vector2<double> &visibleBall) {
    // Implement

    return false; // You may modify this return
}

std::vector<VSSColor> ObjectDetector::getDefaultIdToColorMapping() {
    std::vector<VSSColor> idToColorMapping;
    idToColorMapping.push_back(VSSColor::RED);
    idToColorMapping.push_back(VSSColor::PINK);
    idToColorMapping.push_back(VSSColor::GREEN);
    return idToColorMapping;
}

}
}