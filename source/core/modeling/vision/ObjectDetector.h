//
// Created by mmaximo on 10/19/18.
//

#ifndef ITANDROIDS_VSS_OBJECTDETECTOR_H
#define ITANDROIDS_VSS_OBJECTDETECTOR_H

#include <vector>

#include "math/Pose2D.h"
#include "modeling/vision/Image.h"
#include "modeling/vision/BlobCollection.h"

namespace core {
namespace modeling {

/**
 * Represents a teammate detected by the vision system.
 */
class VisibleTeammate {
public:
    /**
     * Constructs a visible teammate.
     * @param id player id
     * @param pose player pose
     */
    VisibleTeammate(int id, const math::Pose2D &pose);

    /**
     * Gets the teammate id.
     * @return teammate id
     */
    int getId() const;

    /**
     * Gets the teammate pose.
     * @return teammate pose
     */
    const math::Pose2D& getPose() const;

private:
    int id;
    math::Pose2D pose;
};

/**
 * Represents a module of the vision system which detect field objects based on blobs.
 */
class ObjectDetector {
public:
    /**
     * Constructs the object detector.
     * @param ourTeamColor our team's marker color
     * @param idToColorMapping mapping between robot ids and colors
     * @param opponentColor opponent's marker color
     * @param ballColor ball color
     */
    ObjectDetector(VSSColor ourTeamColor, const std::vector<VSSColor> &idToColorMapping, VSSColor opponentColor, VSSColor ballColor);

    /**
     * Constructs the object detector considering default colors used by ITAndroids VSS.
     * @param ourTeamColor our team marker color
     */
    explicit ObjectDetector(VSSColor ourTeamColor);

    /**
     * Detects teammates.
     * @param blobs blob collection
     * @param visibleTeammates teammates detected by the vision system
     */
    void detectTeammates(BlobCollection &blobs, std::vector<VisibleTeammate> &visibleTeammates);

    /**
     * Detects opponents. Since opponent's marker pattern is not known beforehand, the detector
     * uses the centroid of the opponent team marker as its position estimate.
     * @param blobs blob collection
     * @param visibleOpponents opponents detected by the vision system
     */
    void detectOpponents(BlobCollection &blobs, std::vector<math::Vector2<double>> &visibleOpponents);

    /**
     * Detects the ball.
     * @param blobs blob collection
     * @param visibleBall ball detected by the vision system
     * @return if a ball was detected
     */
    bool detectBall(BlobCollection &blobs, math::Vector2<double> &visibleBall);

    /**
     * Gets the default robot id to color mapping used by ITAndroids VSS.
     * @return default robot id to color mapping
     */
    static std::vector<VSSColor> getDefaultIdToColorMapping();

private:
    VSSColor ourTeamColor;
    std::vector<VSSColor> idToColorMapping;
    VSSColor opponentColor;
    VSSColor ballColor;

    static constexpr double MARKERS_MAX_DISTANCE_THRESHOLD = 0.05; /// maximum allowed distance between team and id markers during teammate detected
    static constexpr double MARKERS_DISTANCE_DIFF_THRESHOLD = 1.2; /// the id markers distances to the team marker should differ between themselves more than this factor
    static constexpr double BALL_ASPECT_RATIO_THRESHOLD = 1.2; /// maximum allowed aspect ratio during ball detection
};

}
}

#endif //ITANDROIDS_VSS_OBJECTDETECTOR_H
