//
// Created by igorribeiro on 10/27/16.
//

#include <utils/Clock.h>
#include "CameraVision.h"

using namespace core::modeling;

namespace core {
namespace modeling {

CameraVision::CameraVision() : blobs(VSSColor::NUM_COLORS, 500) {
    colorTable = std::make_shared<ColorTable>();
    colorTable->loadLUT("../configs/yuvLUT.txt");
    width = frameGrabber.getImageWidth();
    height = frameGrabber.getImageHeight();
    cameraImage = std::make_shared<Image>(width, height, core::modeling::PixelFormat::YUV422);
    segmentedImage = std::make_shared<Image>(width, height, core::modeling::PixelFormat::SEGMENTED);
    blobDetector = std::make_shared<BlobDetector>(frameGrabber.getImageWidth(), frameGrabber.getImageHeight(), 20, 3000, 7, 7);
    objectDetector = std::make_shared<ObjectDetector>(BLUE);
    setTeamColor(BLUE);
    if (this->check())
        frameGrabber.startCapturing();
    rgbFrame = std::make_shared<Image>(width, height, core::modeling::PixelFormat::RGB);
}

CameraVision::~CameraVision() {
}

VisionInterface *CameraVision::update() {
    if (frameGrabber.CheckNumberOfCameras() < 1) {
        frameGrabber.BlockConnection();
        frameGrabber.ConnectWithCamera(656, 516);

        if (this->check())
            frameGrabber.startCapturing();

        return &visionInterface;
    }

    imagePtr = frameGrabber.grabFrame();
    memcpy(cameraImage->getData(), imagePtr, cameraImage->getSizeInBytes());
//    cameraImage->setData(imagePtr);
    colorTable->segmentImage(*cameraImage, *segmentedImage);
    blobs.clear();
    blobDetector->computeBlobs(*segmentedImage, blobs);
    blobDetector->mergeBlobs(blobs);
    blobDetector->filterBlobs(blobs);
    for (int i = 1; i < NUM_COLORS; ++i) {
        for (int j = 0; j < blobs.count[i]; ++j) {
            if (blobs.table[i][j].valid) {
                if (i == ORANGE)
                    blobs.table[i][j].worldPosition = cameraProjection->project2DTo3D(blobs.table[i][j].position,
                                                                                       representations::Ball::DIAMETER);
                else {
                    blobs.table[i][j].worldPosition = cameraProjection->project2DTo3D(blobs.table[i][j].position,
                                                                                       representations::Player::TOTAL_HEIGHT);
                }
            }
        }
    }
    std::vector<VisibleTeammate> visibleTeammates;
    std::vector<math::Vector2<double>> visibleOppponents;
    math::Vector2<double> visibleBall;
    objectDetector->detectTeammates(blobs, visibleTeammates);
    objectDetector->detectOpponents(blobs, visibleOppponents);
    bool ballSeen = objectDetector->detectBall(blobs, visibleBall);

    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; ++i) {
        visionInterface.teammates[i].setSeen(false);
        visionInterface.opponents[i].setSeen(false);
    }

    for (auto &visibleTeammate : visibleTeammates) {
        visionInterface.teammates[visibleTeammate.getId()].setSeen(true);
        visionInterface.teammates[visibleTeammate.getId()].setPose(visibleTeammate.getPose());
    }
    for (int i = 0; i < visibleOppponents.size(); ++i) {
        visionInterface.opponents[i].setSeen(true);
        visionInterface.opponents[i].setPosition(visibleOppponents[i]);
    }
    visionInterface.ball.setSeen(ballSeen);
    visionInterface.ball.setPosition(visibleBall);

    Image::yuv422ToRgb(cameraImage->getData(), rgbFrame->getData(), width, height);

    return &visionInterface;
}

int CameraVision::getWidth() {
    return width;
}

int CameraVision::getHeight() {
    return height;
}

unsigned char *CameraVision::getImagePtr() {
    return rgbFrame->getData();
}

void CameraVision::setTeamColor(VSSColor teamColor) {
    this->teamColor = teamColor;
    if (teamColor == YELLOW)
        opponentColor = BLUE;
    else if (teamColor == BLUE)
        opponentColor = YELLOW;
    objectDetector = std::make_shared<ObjectDetector>(teamColor);
}

bool CameraVision::check() {
    return this->frameGrabber.check();
}

core::modeling::Image *CameraVision::getSegmentedImage() {
    return segmentedImage.get();

}

} /* namespace modeling */

} /* namespace core */