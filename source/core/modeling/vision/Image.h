//
// Created by mmaximo on 11/11/16.
//

#ifndef ITANDROIDS_LIB_IMAGE_H
#define ITANDROIDS_LIB_IMAGE_H

namespace core {
namespace modeling {

enum VSSColor {
    UNDEFINED = 0, GREEN, WHITE, ORANGE, PINK, BLUE, YELLOW, BLACK, RED, NUM_COLORS
};

enum PixelFormat {
    YUYV = 0, RGB, SEGMENTED, YUV444, YUV422
};

typedef unsigned char UC;

class Image {
public:
    Image(UC* data, int width, int height, PixelFormat pixelFormat);
    Image(int width, int height, PixelFormat pixelFormat);
    Image(Image &image, PixelFormat pixelFormat);
    ~Image();

    UC *getData();
    void copyData(UC *data);
    void setData(UC *data);
    int getWidth();
    int getHeight();
    int getSize();
    int getSizeInBytes();
    PixelFormat getPixelFormat();
    int getPixelSize(PixelFormat pixelFormat);

    static void yuyvToRgb  (UC *yuyv,   UC *rgb, int width, int height);
    static void yuv444ToRgb(UC *yuv444, UC *rgb, int width, int height, bool invert = false);
    static void yuv422ToRgb(UC *yuv422, UC *rgb, int width, int height);

    static void yuv444ToBgr(UC *yuv444, UC *bgr, int width, int height, bool invert = false);
    static void yuv422ToBgr(UC *yuv422, UC *bgr, int width, int height);
    static void bgrToYuv422(UC* bgr, UC* yuv422, int width, int height);

    static void rgbToYuyv  (UC *rgb, UC *yuyv,   int width, int height);
    static void rgbToYuv444(UC *rgb, UC *yuv444, int width, int height, bool invert = false);

    static void rgbToBgr   (UC *rgb, UC *bgr,    int width, int height);
    static void bgrToRgb   (UC *bgr, UC *rgb,    int width, int height);
    static void bgrToYuv444(UC *bgr, UC *yuv444, int width, int height, bool invert = false);

    static void rgbToYuv444(int r, int g, int b, int *y, int *u, int *v);
    static void yuv444ToRgb(int y, int u, int v, int *r, int *g, int *b);

private:
    void initialize();

    UC *data;
    int width;
    int height;
    PixelFormat pixelFormat;
    int size;
    int sizeInBytes;
};

}
}

#endif //ITANDROIDS_LIB_IMAGE_H