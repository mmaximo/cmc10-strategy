//
// Created by igor on 19/05/17.
//
#include "DummyVision.h"

namespace core {
namespace modeling {

DummyVision::DummyVision(communication::SocketMessageParser *parser,
                         communication::SocketWriter *writer) {
    rgbFrame = new unsigned char[3 * IMAGE_WIDTH * IMAGE_HEIGHT];

    this->parser = parser;
    this->writer = writer;

    for (int i = 0; i < delay; ++i)
        stateFIFO.push({});
    positionNoise = std::make_shared<std::normal_distribution<double>>(0, positionSTD);
    angleNoise = std::make_shared<std::normal_distribution<double>>(0, angleSTD);
}

VisionInterface *DummyVision::update() {
    memset(rgbFrame, 0, 3 * IMAGE_WIDTH * IMAGE_HEIGHT * sizeof(unsigned char));

    writer->receiveMessage();
    stateFIFO.emplace(parseSimulatorMessage()); // put new interface on FIFO
    // get delayed interface and add noise
    visionInterface = stateFIFO.front();
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; i++) {
        auto &teammatePlayer = visionInterface.teammates[i];
        Pose2D teammatePoseNoise((*angleNoise)(generator), (*positionNoise)(generator), (*positionNoise)(generator));
        teammatePlayer.setPose(teammatePlayer.getPose() + teammatePoseNoise);
        teammatePlayer.setSeen(true);

        auto &opponentPlayer = visionInterface.opponents[i];
        Pose2D opponentPoseNoise((*angleNoise)(generator), (*positionNoise)(generator), (*positionNoise)(generator));
        opponentPlayer.setPose(opponentPlayer.getPose() + opponentPoseNoise);
        opponentPlayer.setSeen(true);
    }
    auto &ball = visionInterface.ball;
    Vector2<double> noise((*positionNoise)(generator), (*positionNoise)(generator));
    ball.setPosition(ball.getPosition() + noise);
    ball.setSeen(true);

    stateFIFO.pop();

    return &visionInterface;
}

VisionInterface &DummyVision::parseSimulatorMessage() {
    return parser->parseIn(writer->getLastMessage());
}

unsigned char *DummyVision::getImagePtr() {
    return rgbFrame;
}

bool DummyVision::check() {
    return true;
}

}
}