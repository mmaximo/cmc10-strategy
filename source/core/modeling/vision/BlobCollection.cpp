//
// Created by mmaximo on 11/11/16.
//

#include "BlobCollection.h"

#include "modeling/vision/Blob.h"

namespace core {
namespace modeling {

BlobCollection::BlobCollection(int numColors, int maxBlobs) : numColors(numColors), maxBlobs(maxBlobs) {
    table = new Blob*[numColors];
    for (int i = 0; i < numColors; ++i)
        table[i] = new Blob[maxBlobs];

    count = new int[numColors];

    clear();
}

BlobCollection::~BlobCollection() {
    for (int i = 0; i < numColors; ++i)
        delete[] table[i];
    delete[] table;

    delete[] count;
}

void BlobCollection::clear() {
    for (int i = 0; i < numColors; ++i) {
        count[i] = 0;
    }
}

Blob *BlobCollection::operator[](int color) {
    return table[color];
}

int BlobCollection::getNumColors() {
    return numColors;
}

int BlobCollection::getMaxBlobs() {
    return maxBlobs;
}

void BlobCollection::addBlob(int color, const Blob &blob) {
    table[color][count[color]] = blob;
    ++count[color];
}

int BlobCollection::getBlobCountByColor(int color) const {
    return count[color];
}

}
}