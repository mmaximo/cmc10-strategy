//
// Created by igor on 10/11/16.
//

#ifndef ITANDROIDS_LIB_BLOBDETECTOR_H
#define ITANDROIDS_LIB_BLOBDETECTOR_H

#include <queue>

namespace core {
namespace modeling {

class Image;

class BlobCollection;

class Blob;

/**
 * Represents a blob detector. The implementation assumes that image resolution never changes
 * after the detector has been constructed.
 */
class BlobDetector {
public:
    /**
     * Constructs the blob detector.
     * @param imageWidth camera image width
     * @param imageHeight camera image height
     * @param minBlobArea minimum area a blob must have to be considered
     * @param maxBlobArea maximum area a blob may have before being disconsidered
     * @param mergeMarginX overlap margin in x axis when merging two blobs
     * @param mergeMarginY overlap margin in y axis when merging two blobs
     */
    BlobDetector(int imageWidth, int imageHeight, int minBlobArea, int maxBlobArea, int mergeMarginX, int mergeMarginY);

    /**
     * Default destructor.
     */
    ~BlobDetector();

    /**
     * Extracts blobs from a image.
     * @param image camera image (input)
     * @param blobs collection of extracted blobs (output)
     */
    void computeBlobs(Image &image, BlobCollection &blobs);

    /**
     * Merges blobs if they overlap.
     * @param blobs collection of blobs
     */
    void mergeBlobs(BlobCollection &blobs);

    /**
     * Filters bad blobs, e.g. blobs which are too small or too large.
     * @param blobs collection of blobs
     */
    void filterBlobs(BlobCollection &blobs);

private:

    int imageWidth; /// camera image width
    int imageHeight; /// camera image height
    int imageSize; /// camera image size in pixels
    int minBlobArea; /// minimum area a blob must have to be considered
    int maxBlobArea; /// maximum area a blob may have before being disconsidered
    int mergeMarginX; /// overlap margin in x axis when merging two blobs
    int mergeMarginY; /// overlap margin in y axis when merging two blobs
    bool *visited; /// mark visited nodes during graph search

    /**
     * Extracts a new blob using breath-first search.
     * @param image camera image
     * @param startX x-coordinate of the pixel where the search begins
     * @param startY y-coordinate of the pixel where the search begins
     * @param color blob color
     * @param blob extracted blob (output)
     */
    void computeBlobBFS(Image &image, int startX, int startY, int color, Blob& blob);
};

}
}


#endif //ITANDROIDS_LIB_BLOBDETECTOR_H
