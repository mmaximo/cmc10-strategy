//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_MODELING_VISION_STATICVISION_H_
#define CORE_MODELING_VISION_STATICVISION_H_

#include <opencv2/opencv.hpp>

#include "AbstractVision.h"
#include "communication/camera/PointGreyFrameGrabber.h"
#include "modeling/vision/ColorTable.h"
#include "modeling/vision/Image.h"
#include "modeling/vision/BlobCollection.h"
#include "modeling/vision/BlobDetector.h"
#include "modeling/vision/Blob.h"
#include "modeling/vision/CameraProjection.h"

namespace core {
namespace modeling {

class StaticVision : public AbstractVision {
public:
    StaticVision();

    ~StaticVision();

    VisionInterface *update();

    int getWidth() override;

    int getHeight() override;

    unsigned char *getImagePtr() override;

    bool check() override;

    void setTeamColor(core::modeling::VSSColor teamColor);

    core::modeling::Image *getSegmentedImage() { return nullptr; };

private:
    int width;
    int height;
    cv::Mat imageMat;
    VisionInterface *visionInterface;
    unsigned char *rgbFrame;

};

} /* namespace modeling */


} /* namespace core */

#endif // CORE_MODELING_VISION_STATICVISION_H_
