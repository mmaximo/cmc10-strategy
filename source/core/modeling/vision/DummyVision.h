//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_MODELING_VISION_DUMMY_VISION_H_
#define CORE_MODELING_VISION_DUMMY_VISION_H_

#include <communication/parsers/SocketMessageParser.h>
#include <communication/writers/SocketWriter.h>
#include "VisionInterface.h"
#include "AbstractVision.h"
#include <queue>
#include <random>

namespace core {
namespace modeling {

class DummyVision : public AbstractVision {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    DummyVision(communication::SocketMessageParser *parser, communication::SocketWriter *writer);

    VisionInterface *update();

    unsigned char *rgbFrame;

    unsigned char *getImagePtr();

    void setTeamColor(core::modeling::VSSColor teamColor) {};

    bool check() override;

    core::modeling::Image *getSegmentedImage() { return nullptr; };

private:
    VisionInterface &parseSimulatorMessage();

    communication::SocketMessageParser *parser;
    communication::SocketWriter *writer;
    VisionInterface visionInterface;
    const int delay = 2;
    Vector3d state;
    Vector3d noise;
    std::default_random_engine generator;
    std::shared_ptr<std::normal_distribution<double>> positionNoise;
    std::shared_ptr<std::normal_distribution<double>> angleNoise;
    std::queue<VisionInterface> stateFIFO;
    const double positionSTD = 0.2e-3;
    const double angleSTD = 1.5 * M_PI / 180;
};

} // namespace modeling
} // namespace core


#endif // CORE_MODELING_VISION_DUMMY_VISION_H_