//
// Created by mmaximo on 10/25/18.
//

#include "modeling/vision/VisionInterface.h"

namespace core {
namespace modeling {

VisionInterface::VisionInterface() {
    ball.setSeen(false);
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; ++i) {
        teammates[i].setId(i);
        teammates[i].setSeen(false);
    }
}

}
}