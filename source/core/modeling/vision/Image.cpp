//
// Created by mmaximo on 11/11/16.
//

#include "Image.h"
#include "math/MathUtils.h"

namespace core {
namespace modeling {

Image::Image(UC *data, int width, int height, PixelFormat pixelFormat) :
        width(width), height(height), pixelFormat(pixelFormat) {
    initialize();
    copyData(data);
}

Image::Image(int width, int height, PixelFormat pixelFormat) :
        width(width), height(height), pixelFormat(pixelFormat) {
    initialize();
}

Image::Image(Image &image, PixelFormat pixelFormat) :
        width(image.width), height(image.height), pixelFormat(pixelFormat) {
    initialize();
    if (image.pixelFormat == YUYV && pixelFormat == RGB)
        yuyvToRgb(image.data, data, width, height);
}

Image::~Image() {
    delete[] data;
}

void Image::initialize() {
    size = width * height;
    sizeInBytes = size * getPixelSize(pixelFormat);
    data = new UC[sizeInBytes];
}

UC *Image::getData() {
    return data;
}

void Image::copyData(UC *data) {
    memcpy(this->data, data, sizeInBytes);
}

void Image::setData(UC *data) {
    this->data = data;
}

int Image::getWidth() {
    return width;
}

int Image::getHeight() {
    return height;
}

int Image::getSize() {
    return size;
}

int Image::getSizeInBytes() {
    return sizeInBytes;
}

PixelFormat Image::getPixelFormat() {
    return pixelFormat;
}

int Image::getPixelSize(PixelFormat format) {
    switch (format) {
        case YUYV:
            return 2;//return 4;
        case RGB:
            return 3;
        case SEGMENTED:
            return 1;
        case YUV422:
            return 2;
        case YUV444:
            return 3;
    }
}

void Image::yuyvToRgb(UC *yuyv, UC *rgb, int width, int height) {
    int r, g, b,
            y, u, v;

    int lim = (width * height)/2;
    for (int p = 0; p < lim; p++) {
        y = yuyv[4*p + 0] << 8;
        u = yuyv[4*p + 1] - 128;
        v = yuyv[4*p + 3] - 128;

        r = (y +   0 * u + 359 * v) >> 8;
        g = (y -  88 * u - 183 * v) >> 8;
        b = (y + 454 * u +   0 * v) >> 8;

        rgb[6*p + 0] = math::MathUtils::clamp(r, 0, 255);
        rgb[6*p + 1] = math::MathUtils::clamp(g, 0, 255);
        rgb[6*p + 2] = math::MathUtils::clamp(b, 0, 255);

        y = yuyv[4*p + 2] << 8;

        r = (y +   0 * u + 359 * v) >> 8;
        g = (y -  88 * u - 183 * v) >> 8;
        b = (y + 454 * u +   0 * v) >> 8;

        rgb[6*p + 3] = math::MathUtils::clamp(r, 0, 255);
        rgb[6*p + 4] = math::MathUtils::clamp(g, 0, 255);
        rgb[6*p + 5] = math::MathUtils::clamp(b, 0, 255);
    }
}

void Image::yuv444ToRgb(UC *yuv444, UC *rgb, int width, int height, bool invert) {
    int r, g, b;

    int lim = 3 * width * height;
    for (int i = 0; i < lim; i += 3) {
        if(invert)
            yuv444ToRgb(yuv444[i + 1], yuv444[i + 0], yuv444[i + 2], &r, &g, &b);
        else
            yuv444ToRgb(yuv444[i + 0], yuv444[i + 1], yuv444[i + 2], &r, &g, &b);

        rgb[i + 0] = (UC) r; // red
        rgb[i + 1] = (UC) g; // green
        rgb[i + 2] = (UC) b; // blue
    }
}

void Image::yuv422ToRgb(UC *yuv422, UC *rgb, int width, int height) {
    int r, g, b,
            y, u, v;

    int lim = (width * height)/2;
    for (int p = 0; p < lim; p++) {
        y = yuv422[4*p + 1];
        u = yuv422[4*p + 0];
        v = yuv422[4*p + 2];
        yuv444ToRgb(y, u, v, &r, &g, &b);

        rgb[6*p + 0] = (UC) r;
        rgb[6*p + 1] = (UC) g;
        rgb[6*p + 2] = (UC) b;

        y = yuv422[4*p + 3];
        yuv444ToRgb(y, u, v, &r, &g, &b);

        rgb[6*p + 3] = (UC) r;
        rgb[6*p + 4] = (UC) g;
        rgb[6*p + 5] = (UC) b;
    }
}

void Image::yuv444ToBgr(UC *yuv444, UC *bgr, int width, int height, bool invert) {
    int r, g, b;

    int lim = 3 * width * height;
    for (int i = 0; i < lim; i += 3) {
        if(invert)
            yuv444ToRgb(yuv444[i + 1], yuv444[i + 0], yuv444[i + 2], &r, &g, &b);
        else
            yuv444ToRgb(yuv444[i + 0], yuv444[i + 1], yuv444[i + 2], &r, &g, &b);

        bgr[i + 0] = (UC) b;
        bgr[i + 1] = (UC) g;
        bgr[i + 2] = (UC) r;
    }
}

void Image::yuv422ToBgr(UC *yuv422, UC *bgr, int width, int height) {
    int r, g, b,
            y, u, v;

    int lim = (width * height)/2;
    for (int p = 0; p < lim; p++) {
        y = yuv422[4*p + 1];
        u = yuv422[4*p + 0];
        v = yuv422[4*p + 2];
        yuv444ToRgb(y, u, v, &r, &g, &b);

        bgr[6*p + 0] = (UC) b;
        bgr[6*p + 1] = (UC) g;
        bgr[6*p + 2] = (UC) r;

        y = yuv422[4*p + 3];
        yuv444ToRgb(y, u, v, &r, &g, &b);

        bgr[6*p + 3] = (UC) b;
        bgr[6*p + 4] = (UC) g;
        bgr[6*p + 5] = (UC) r;
    }
}

void Image::bgrToYuv422(UC* bgr, UC* yuv422, int width, int height) {
    static int r, g, b, y, u1, v1, u2, v2;

    int size = (width * height) / 2;
    for (int i = 0; i < size; ++i) {
        b = bgr[6 * i + 0];
        g = bgr[6 * i + 1];
        r = bgr[6 * i + 2];
        rgbToYuv444(r, g, b, &y, &u1, &v1);
        yuv422[4 * i + 1] = y;

        b = bgr[6 * i + 3];
        g = bgr[6 * i + 4];
        r = bgr[6 * i + 5];
        rgbToYuv444(r, g, b, &y, &u2, &v2);
        yuv422[4 * i + 3] = y;
        yuv422[4 * i + 0] = (u1 + u2) / 2;
        yuv422[4 * i + 2] = (v1 + v2) / 2;
    }
}

// http://www.equasys.de/colorconversion.html
void Image::rgbToYuyv(UC *rgb, UC *yuyv, int width, int height) {
    int numPixelsPairs = height * width / 2;

    int r1, g1, b1,
            r2, g2, b2;

    int y1, u1, v1,
            y2, u2, v2;

    for (int i = 0; i < numPixelsPairs; ++i) {
        r1 = rgb[0];
        g1 = rgb[1];
        b1 = rgb[2];

        r2 = rgb[3];
        g2 = rgb[4];
        b2 = rgb[5];

        y1 = (  77 * r1 + 150 * g1 +  29 * b1) >> 8;
        u1 = ((-43 * r1 -  85 * g1 + 128 * b1) >> 8) + 128;
        v1 = ((128 * r1 - 107 * g1 -  21 * b1) >> 8) + 128;

        y2 = (  77 * r2 + 150 * g2 +  29 * b2) >> 8;
        u2 = ((-43 * r2 -  85 * g2 + 128 * b2) >> 8) + 128;
        v2 = ((128 * r2 - 107 * g2 -  21 * b2) >> 8) + 128;

        yuyv[0] = math::MathUtils::clamp(y1, 0, 255);
        yuyv[1] = math::MathUtils::clamp((u1 + u2) / 2, 0, 255);

        yuyv[2] = math::MathUtils::clamp(y2, 0, 255);
        yuyv[3] = math::MathUtils::clamp((v1 + v2) / 2, 0, 255);

        rgb += 6;
        yuyv += 4;
    }
}

void Image::rgbToYuv444(UC *rgb, UC *yuv444, int width, int height, bool invert) {
    int y, u, v;

    for (int i = 0; i < 3 * width * height; i += 3) {
        if(invert)
            rgbToYuv444((int) rgb[i + 0], (int) rgb[i + 1], (int) rgb[i + 2], &u, &y, &v);
        else
            rgbToYuv444((int) rgb[i + 0], (int) rgb[i + 1], (int) rgb[i + 2], &y, &u, &v);

        yuv444[i + 0] = (UC) y;
        yuv444[i + 1] = (UC) u;
        yuv444[i + 2] = (UC) v;
    }
}

void Image::rgbToBgr(UC *rgb, UC *bgr, int width, int height) {
    UC r, g, b;

    int lim = 3 * width * height;
    for (int p = 0; p < lim; p += 3) {
        r = rgb[p + 2];
        g = rgb[p + 1];
        b = rgb[p + 0];

        bgr[p + 0] = b;
        bgr[p + 1] = g;
        bgr[p + 2] = r;
    }
}

void Image::bgrToRgb(UC *bgr, UC *rgb, int width, int height) {
    UC r, g, b;

    int lim = 3 * width * height;
    for (int p = 0; p < lim; p += 3) {
        b = bgr[p + 0];
        g = bgr[p + 1];
        r = bgr[p + 2];

        rgb[p + 0] = r;
        rgb[p + 1] = g;
        rgb[p + 2] = b;
    }
}

void Image::bgrToYuv444(UC *bgr, UC *yuv444, int width, int height, bool invert) {
    int y, u, v;

    for (int i = 0; i < 3 * width * height; i += 3) {
        if(invert)
            rgbToYuv444((int) bgr[i + 2], (int) bgr[i + 1], (int) bgr[i + 0], &u, &y, &v);
        else
            rgbToYuv444((int) bgr[i + 2], (int) bgr[i + 1], (int) bgr[i + 0], &y, &u, &v);

        yuv444[i + 0] = (UC) y;
        yuv444[i + 1] = (UC) u;
        yuv444[i + 2] = (UC) v;
    }
}

void Image::rgbToYuv444(int r, int g, int b, int *y, int *u, int *v) {
    *y = (( 66 * r + 129 * g +  25 * b + 128) >> 8) +  16;
    *u = ((-38 * r -  74 * g + 112 * b + 128) >> 8) + 128;
    *v = ((112 * r -  94 * g -  18 * b + 128) >> 8) + 128;

    *y = math::MathUtils::clamp(*y, 0, 255);
    *u = math::MathUtils::clamp(*u, 0, 255);
    *v = math::MathUtils::clamp(*v, 0, 255);
}

// https://www.ptgrey.com/support/downloads/10120
void Image::yuv444ToRgb(int y, int u, int v, int *r, int *g, int *b) {
    y = y -  16;
    u = u - 128;
    v = v - 128;

    *r = (298 * y +   0 * u + 408 * v + 128) >> 8;
    *g = (298 * y - 100 * u - 208 * v + 128) >> 8;
    *b = (298 * y + 516 * u +   0 * v + 128) >> 8;

    *r = math::MathUtils::clamp(*r, 0, 255);
    *g = math::MathUtils::clamp(*g, 0, 255);
    *b = math::MathUtils::clamp(*b, 0, 255);
}

} // namespace vision
} // namespace itandroids_lib