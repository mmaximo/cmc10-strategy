//
// Created by igorribeiro on 10/27/16.
//

#ifndef CORE_MODELING_VISION_ABSTRACT_VISION_H_
#define CORE_MODELING_VISION_ABSTRACT_VISION_H_

#include <modeling/vision/CameraProjection.h>
#include "VisionInterface.h"
#include <modeling/vision/Image.h>

#define IMAGE_WIDTH 656
#define IMAGE_HEIGHT 516

namespace core {
namespace modeling {

class AbstractVision {
public:
    virtual VisionInterface *update() = 0;

    AbstractVision();

    virtual ~AbstractVision() {}

    virtual int getWidth() { return 0; };

    virtual int getHeight() { return 0; };

//
    virtual unsigned char *getImagePtr() { return nullptr; };

    virtual void setTeamColor(core::modeling::VSSColor teamColor) {};

    virtual bool check() { return true; };

    virtual core::modeling::Image *getSegmentedImage() { return nullptr; };

//
    core::modeling::CameraProjection *getCameraProjection();

protected:
    core::modeling::CameraProjection *cameraProjection;
};

} /* namespace modeling */

} /* namespace core */

#endif // CORE_MODELING_VISION_ABSTRACT_VISION_H_