//
// Created by igor on 15/02/17.
//

#ifndef ITANDROIDS_LIB_REPROJECTION3D_H
#define ITANDROIDS_LIB_REPROJECTION3D_H

#include <Eigen/Dense>
#include "math/Vector2.h"

using namespace Eigen;

namespace core {
namespace modeling {

class CameraProjection {
private:
    Matrix3d rotationMatrix;
    Matrix3d cameraMatrix;
    Matrix3d auxMatrix;
    Vector3d translationVector;
    Vector3d b;
    double fx;
    double fy;
    double cx;
    double cy;
    double k1;
    double k2;
    double p1;
    double p2;
    double k3;
public:
    CameraProjection(const char *intrinsicParamsFile, const char *extrinsicParamsFile);

    math::Vector2<double> project2DTo3D(const math::Vector2<double> &inputPos, double objectHeight);
    math::Vector2<double> project3DTo2D(const math::Vector2<double> &inputPos, double objectHeight);

    void loadIntrinsicParams(const char *intrinsicParamsFile);

    void loadExtrinsicParams(const char *extrinsicParamsFile);

    static void
    saveExtrinsicParams(const char *fileName, double r11, double r12, double r13, double r21, double r22,
                        double r23, double r31, double r32, double r33, double t1, double t2, double t3);

    static void
    saveIntrinsicParams(const char *fileName, double fx, double fy, double cx, double cy, double k1, double k2,
                        double p1, double p2, double k3);
};

}
}
#endif //ITANDROIDS_LIB_REPROJECTION3D_H
