//
// Created by igor on 10/11/16.
//

#include "BlobDetector.h"

#include <iostream>
#include "modeling/vision/Image.h"
#include "modeling/vision/BlobCollection.h"
#include "modeling/vision/Blob.h"
#include "data_structures/UnionFind.h"

namespace core {
namespace modeling {

BlobDetector::BlobDetector(int imageWidth, int imageHeight, int minBlobArea, int maxBlobArea, int mergeMarginX,
                           int mergeMarginY) : imageWidth(imageWidth),
                                               imageHeight(
                                                       imageHeight),
                                               imageSize(imageWidth * imageHeight),
                                               minBlobArea(
                                                       minBlobArea),
                                               maxBlobArea
                                                       (maxBlobArea), mergeMarginX(mergeMarginX),
                                               mergeMarginY(mergeMarginY) {
    visited = new bool[imageWidth * imageHeight];
}

BlobDetector::~BlobDetector() {
    delete[] visited;
}

void BlobDetector::computeBlobBFS(Image &image, int startX, int startY, int color, Blob &blob) {
    // Implement
}

void BlobDetector::computeBlobs(Image &image, BlobCollection &blobs) {
    // Implement
}

void BlobDetector::mergeBlobs(BlobCollection &blobs) {
    // Implement
}

void BlobDetector::filterBlobs(BlobCollection &blobs) {
    // Implement
}

}
}