//
// Created by mmaximo on 12/6/16.
//

#include "ColorTable.h"

#include <fstream>
#include <iostream>
#include "modeling/vision/Image.h"

namespace core {
namespace modeling {

const unsigned char ColorTable::UNDEFINED_COLOR_ID = 0;

ColorTable::ColorTable() {
}

void ColorTable::reset() {
    for (int i = 0; i < TABLE_SIZE; ++i)
        table[i] = UNDEFINED_COLOR_ID;
}

void ColorTable::segmentImage(Image &cameraImage, Image &segmentedImage) {
    PixelFormat format = cameraImage.getPixelFormat();

    unsigned char *cameraData = cameraImage.getData();
    unsigned char *segmentedData = segmentedImage.getData();
    int size = segmentedImage.getSize();
    if (format == YUV422) {
        // Implement image segmentation
    }
}

unsigned char ColorTable::getColor(unsigned char c1, unsigned char c2, unsigned char c3) {
    return *(table + (c1 >> 1 << 14) + (c2 >> 1 << 7) + (c3 >> 1));
}

void ColorTable::assignColor(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c) {
    *(table + (c1 >> 1 << 14) + (c2 >> 1 << 7) + (c3 >> 1)) = c;
}

unsigned char *ColorTable::getTable() {
    return table;
}

const int ColorTable::getTableSize() {
    return TABLE_SIZE;
}

void ColorTable::loadLUT(const char *fileName) {
    reset();
    FILE *readFile = fopen(fileName, "r");
    if(readFile == NULL) {
        printf("Could not open %s. Closing...", fileName);
        exit(0);
    }
    size_t result = fread(getTable(), sizeof(unsigned char), getTableSize(), readFile);
    fclose(readFile);
}

}
}