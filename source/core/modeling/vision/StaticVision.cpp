//
// Created by igorribeiro on 10/27/16.
//

#include <utils/Clock.h>
#include <highgui.h>
#include "StaticVision.h"

using namespace core::modeling;

namespace core {
namespace modeling {

StaticVision::StaticVision() {
    visionInterface = new VisionInterface();
    width = IMAGE_WIDTH;
    height = IMAGE_HEIGHT;

    imageMat = cv::imread("../resources/images/logo.png", CV_LOAD_IMAGE_COLOR);
    rgbFrame = imageMat.data;
}

StaticVision::~StaticVision() {
}

VisionInterface *StaticVision::update() {
    for (int i = 0; i < representations::Player::PLAYERS_PER_SIDE; ++i) {
        visionInterface->teammates[i].setSeen(false);
        visionInterface->opponents[i].setSeen(false);
    }
    visionInterface->ball.setSeen(false);

    return visionInterface;
}

int StaticVision::getWidth() {
    return width;
}

int StaticVision::getHeight() {
    return height;
}

unsigned char *StaticVision::getImagePtr() {
    return rgbFrame;
}

void StaticVision::setTeamColor(VSSColor teamColor) {
}

bool StaticVision::check() {
    return true;
}


} /* namespace modeling */

} /* namespace core */
