//
// Created by igor on 15/02/17.
//

#include "CameraProjection.h"
#include <fstream>
#include <locale>

namespace core {
namespace modeling {

CameraProjection::CameraProjection(const char *intrinsicParamsFile,
                                                           const char *extrinsicParamsFile) {
    loadIntrinsicParams(intrinsicParamsFile);
    loadExtrinsicParams(extrinsicParamsFile);
}

void CameraProjection::loadIntrinsicParams(const char *intrinsicParamsFile) {
    std::ifstream file(intrinsicParamsFile);
    if (file.is_open()) {
        std::string line;
        char dummy;

        std::getline(file, line);
        file >> fx >> dummy >> fy >> dummy >> cx >> dummy >> cy >> dummy;
        std::getline(file, line);
        file >> k1 >> dummy >> k2 >> dummy >> p1 >> dummy >> p2 >> dummy >> k3;

        file.close();

        cameraMatrix << fx, 0, cx,
                0, fy, cy,
                0, 0, 1;
    } else {
        printf("Could not open %s. Closing...", intrinsicParamsFile);
        exit(0);
    }
}

void CameraProjection::loadExtrinsicParams(const char *extrinsicParamsFile) {
    std::ifstream file(extrinsicParamsFile);
    if (file.is_open()) {
        std::string line;
        char dummy;

        std::getline(file, line);
        file >> rotationMatrix(0, 0) >> dummy >> rotationMatrix(0, 1) >> dummy >> rotationMatrix(0, 2) >> dummy >>
             rotationMatrix(1, 0) >> dummy >> rotationMatrix(1, 1) >> dummy >> rotationMatrix(1, 2) >> dummy >>
             rotationMatrix(2, 0) >> dummy >> rotationMatrix(2, 1) >> dummy >> rotationMatrix(2, 2) >> dummy;
        std::getline(file, line);
        file >> translationVector(0) >> dummy >> translationVector(1) >> dummy >> translationVector(2);

        file.close();

        auxMatrix = (cameraMatrix * rotationMatrix).inverse();// rotationMatrix.inverse() * cameraMatrix.inverse();
        b = rotationMatrix.inverse() * translationVector;
    } else {
        printf("Could not open %s. Closing...", extrinsicParamsFile);
        exit(0);
    }
}

math::Vector2<double> CameraProjection::project2DTo3D(const math::Vector2<double> &inputPos, double objectHeight) {
    double u = inputPos.x;
    double v = inputPos.y;
    double xDistorted = (u - cx) / fx;
    double yDistorted = (v - cy) / fy;

    double x0 = xDistorted;
    double y0 = yDistorted;
    int iters = 10;
    for (int j = 0; j < iters; j++) { //iterative aproximate algorithm for undistorting
        double r2 = xDistorted * xDistorted + yDistorted * yDistorted;
        double icdist = 1. / (1 + r2 * (k1 + r2 * (k2 + r2 * k3)));
        double deltaX = 2 * p1 * xDistorted * yDistorted +
                        p2 * (r2 + 2 * xDistorted * xDistorted);
        double deltaY = p1 * (r2 + 2 * yDistorted * yDistorted) +
                        2 * p2 * xDistorted * yDistorted;
        xDistorted = (x0 - deltaX) * icdist;
        yDistorted = (y0 - deltaY) * icdist;
    }

    double uCorrected = fx * xDistorted + cx;
    double vCorrected = fy * yDistorted + cy;

    Vector3d cameraFrameCorrectedVector;
    cameraFrameCorrectedVector << uCorrected, vCorrected, 1;
    Vector3d a = auxMatrix * cameraFrameCorrectedVector;

    double zc = (objectHeight + b(2)) / a(2);

    math::Vector2<double> outPos;
    outPos.x = zc * a(0) - b(0);
    outPos.y = zc * a(1) - b(1);

    return outPos;
}

void CameraProjection::saveIntrinsicParams(const char *fileName, double fx, double fy, double cx, double cy, double k1,
                                           double k2, double p1, double p2, double k3) {
    std::ofstream file(fileName);
    std::locale::global(std::locale("C"));
    char str[1000];
    sprintf(str, "// cameraMatrix: fx,fy,cx,cy\n"
            "%lf,%lf,%lf,%lf\n"
            "// distortionCoeffs: k1,k2,p1,p2,k3\n"
            "%lf,%lf,%lf,%lf,%lf", fx, fy, cx, cy, k1, k2, p1, p2, k3);
    file << str;
    file.close();
}

void
CameraProjection::saveExtrinsicParams(const char *fileName, double r11, double r12, double r13, double r21, double r22,
                                      double r23, double r31, double r32, double r33, double t1, double t2, double t3) {
    std::ofstream file(fileName);
    std::locale::global(std::locale("C"));
    char str[1000];
    sprintf(str, "// rotation matrix: r11,r12,r13,r21,r22,r23,r31,r32,r33\n"
            "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n"
            "// translation vector: t1,t2,t3\n"
            "%lf,%lf,%lf", r11, r12, r13, r21, r22, r23, r31, r32, r33, t1, t2, t3);
    file << str;
    file.close();
}

// FIXME: include distortion
math::Vector2<double> CameraProjection::project3DTo2D(const math::Vector2<double> &inputPos, double objectHeight) {
    Vector3d worldPosition;
//    Vector3d pixelPosition;
    worldPosition << inputPos.x, inputPos.y, objectHeight;
    Vector3d cameraFrame = (rotationMatrix * worldPosition + translationVector);
    double xCamera = cameraFrame(0) / cameraFrame(2);
    double yCamera = cameraFrame(1) / cameraFrame(2);
    double xCamera2 = xCamera * xCamera;
    double yCamera2 = yCamera * yCamera;
    double r2 = xCamera2 + yCamera2;
    double r4 = r2 * r2;
    double r6 = r4 * r2;
    double xDistorted =
            xCamera * (1 + k1 * r2 + k2 * r4 + k3 * r6) + 2 * p1 * xCamera * yCamera +
            p2 * (r2 + 2 * xCamera2);
    double yDistorted =
            yCamera * (1 + k1 * r2 + k2 * r4 + k3 * r6) + p1 * (r2 + 2 * yCamera2) +
            2 * p2 * xCamera * yCamera;
    double u = fx * xDistorted + cx;
    double v = fy * yDistorted + cy;

    return math::Vector2<double>(u, v);
}

}
}