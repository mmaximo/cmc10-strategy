//
// Created by mmaximo on 11/11/16.
//

#ifndef ITANDROIDS_LIB_BLOB_H
#define ITANDROIDS_LIB_BLOB_H

#include "math/Vector2.h"

namespace core {
namespace modeling {

/**
 * Represents a contiguous region of pixels.
 */
class Blob {
public:
    math::Vector2<double> position; /// centroid position in image in pixels
    math::Vector2<double> worldPosition; /// centroid real world position in m
    int xi; /// x-coordinate of the left side of the bounding box
    int xf; /// x-coordinate of the right side of the bounding box
    int yi; /// y-coordinate of the top side of the bounding box
    int yf; /// y-coordinate of the bottom side of the bounding box
    int area; /// area in pixels
    bool valid; /// if the blob is valid

public:
    /**
     * Default constructor.
     */
    Blob();

    /**
     * Constructs a blob using bounding box information.
     * @param xi x-coordinate of the left side of the bounding box
     * @param xf x-coordinate of the right side of the bounding box
     * @param yi y-coordinate of the top side of the bounding box
     * @param yf y-coordinate of the right side of the bounding box
     */
    Blob(int xi, int xf, int yi, int yf);

    /**
     * Merges two blobs. After the merge, the other blob becomes invalid.
     * @param other the other blob used in this merge
     */
    void merge(Blob &other);

    /**
     * Checks if this blob overlaps with another blob.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlaps(Blob &other);

    /**
     * Checks if this blob overlaps with another blob in x axis.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlapsX(Blob &other);

    /**
     * Checks if this blob overlaps with another blob in y axis.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlapsY(Blob &other);

    /**
     * Checks if this blob overlaps with another blob considering tolerance margins both in x and y axes.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlapsWithMargin(Blob &other, int marginX, int marginY);

    /**
     * Checks if this blob overlaps with another blob in x axis considering a tolerance margin.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlapsWithMarginX(Blob &other, int marginX);

    /**
     * Checks if this blob overlaps with another blob in y axis considering a tolerance margin.
     * @param other the other blob
     * @return if the blobs overlap
     */
    bool overlapsWithMarginY(Blob &other, int marginY);
};

}
}

#endif //ITANDROIDS_LIB_BLOB_H
