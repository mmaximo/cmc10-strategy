cmake_minimum_required(VERSION 3.0)
project(agent)

set(LIB_DIR ../../../itandroids-lib/source)
set(SOURCE_DIR ../../)
set(TEST_DIR ../)

#Use following folders as header search paths
include_directories(${LIB_DIR})
include_directories(${SOURCE_DIR})
include_directories(${TEST_DIR})

file(GLOB_RECURSE SRCS *.cpp)
foreach (file_path ${SRCS})
    get_filename_component(dir_path ${file_path} PATH)
    get_filename_component(file_name_we ${file_path} NAME_WE)

    add_executable(${file_name_we} ${file_path})
    target_link_libraries(${file_name_we} core
            simulator_core
            test_source
            #itandroids-lib-library
            ${OGRE_LIBRARY_REL}
            ${OGRE_Overlay_LIBRARY_REL}
            ${OIS_LIBRARIES}
            ${ODE_LIBRARIES})

    #Add .txts files to binaries/
    file(GLOB COMP_TXTS ${dir_path}/*.txt)
    foreach (comp_txt ${COMP_TXTS})
        add_custom_command(TARGET ${file_name_we}
                POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${comp_txt} $<TARGET_FILE_DIR:${file_name_we}>)
        #message(WARNING ${comp_txt})
    endforeach ()
endforeach ()
