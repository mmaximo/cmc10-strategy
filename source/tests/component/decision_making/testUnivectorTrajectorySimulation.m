function testUnivectorTrajectorySimulation()

system('./../../../../binaries/UnivectorTrajectorySimulation_ComponentTest 0.6 0.5 0.0');
field = load('univector_field.txt');
trajectory = load('trajectory.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
plot(trajectory(:, 2), trajectory(:, 3), 'b');
for i=1:10:size(trajectory, 1)
    translation = trajectory(i, 2:3);
    rotation = trajectory(i, 4);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'b');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Univector Trajectory', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'univectory_trajectory.png');

end