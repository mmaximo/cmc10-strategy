//
// Created by mmaximo on 11/3/18.
//

#include <iostream>
#include <string>
#include <fstream>
#include <utils/simulator/DifferentialRobotKinematicSimulator.h>
#include <core/representations/Player.h>
#include <core/control/FollowLineController.h>
#include <core/math/MathUtils.h>
#include <core/decision_making/UnivectorField.h>

using core::utils::DifferentialRobotKinematicSimulator;
using core::representations::Player;
using core::representations::UnicycleSpeed;
using core::control::FollowLineController;
using core::control::FollowLineParams;
using core::math::MathUtils;
using core::representations::Player;
using core::math::Pose2D;
using core::representations::WheelSpeed;
using core::representations::UnicycleSpeed;
using core::decision_making::UnivectorField;
using core::decision_making::UnivectorFieldParams;

void testHyperbolicSpiralField(std::ofstream &file) {
    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeHyperbolicSpiralField(currentPosition, false);
            file << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }
}

void testRepulsiveField(std::ofstream &file) {
    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeRepulsiveField(currentPosition);
            file << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }
}

void testMoveToGoalField(std::ofstream &file) {
    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    Pose2D target(M_PI / 4.0, 0.3, 0.2);
    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeMoveToGoalField(target, currentPosition);
            file << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }
}

void testAvoidObstacleField(std::ofstream &file) {
    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    Vector2<double> currentVelocity(1.0, 0.0);
    Vector2<double> obstacle(0.5, 0.5);
    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeAvoidObstacleField(currentPosition, currentVelocity, obstacle);
            file << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }
}

void testComposedField(std::ofstream &file) {
    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    Pose2D target(-M_PI / 4.0, 0.0, 0.0);
    Vector2<double> currentVelocity(1.0, 0.0);
    Vector2<double> obstacle(0.5, 0.5);
    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeComposedField(target, currentPosition, currentVelocity, obstacle);
            file << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }
}

int main(int argc, char* argv[]) {
    std::ofstream fieldFile("univector_field.txt");

    int testCase = 0;

    if (argc > 1) {
        testCase = std::atoi(argv[1]);
    }

    switch (testCase) {
        case 0:
            testHyperbolicSpiralField(fieldFile);
            break;
        case 1:
            testRepulsiveField(fieldFile);
            break;
        case 2:
            testMoveToGoalField(fieldFile);
            break;
        case 3:
            testAvoidObstacleField(fieldFile);
            break;
        case 4:
            testComposedField(fieldFile);
            break;
        default:
            std::cout << "Test case not valid!" << std::endl;
            break;
    }

    fieldFile.flush();
    fieldFile.close();
}