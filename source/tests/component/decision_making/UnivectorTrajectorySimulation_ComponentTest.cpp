//
// Created by mmaximo on 11/3/18.
//

#include <iostream>
#include <string>
#include <fstream>
#include <utils/simulator/DifferentialRobotKinematicSimulator.h>
#include <core/representations/Player.h>
#include <core/control/FollowLineController.h>
#include <core/control/UnivectorController.h>
#include <core/math/MathUtils.h>
#include <core/decision_making/UnivectorField.h>

int main(int argc, char* argv[]) {
    using core::utils::DifferentialRobotKinematicSimulator;
    using core::representations::Player;
    using core::representations::UnicycleSpeed;
    using core::math::MathUtils;
    using core::representations::Player;
    using core::math::Pose2D;
    using core::representations::WheelSpeed;
    using core::representations::UnicycleSpeed;
    using core::control::UnivectorController;
    using core::decision_making::UnivectorField;
    using core::decision_making::UnivectorFieldParams;

    Pose2D initialPose(0.0, 0.0, 0.0);

    std::ofstream fieldFile("univector_field.txt");
    std::ofstream trajectoryFile("trajectory.txt");

    if (argc > 3) {
        initialPose.translation.x = std::atof(argv[1]);
        initialPose.translation.y = std::atof(argv[2]);
        initialPose.rotation = std::atof(argv[3]);
    }

    UnivectorFieldParams params = UnivectorFieldParams::getDefaultParams();
    UnivectorField univectorField(params);

    double minX = -1.0;
    double minY = -1.0;
    double maxX = 1.0;
    double maxY = 1.0;
    int nx = 30;
    int ny = 30;
    double deltaX = (maxX - minX) / (nx - 1);
    double deltaY = (maxY - minY) / (ny - 1);

    Pose2D target(-M_PI / 4.0, 0.0, 0.0);
    Vector2<double> currentVelocity(1.0, 0.0);
    Vector2<double> obstacle(0.5, 0.5);
    double x;
    double y = minY;
    for (int j = 0; j < ny; ++j) {
        x = minX;
        for (int i = 0; i < ny; ++i) {
            Vector2<double> currentPosition(x, y);
            double angle = univectorField.computeComposedField(target, currentPosition, currentVelocity, obstacle);
            fieldFile << currentPosition.x << " " << currentPosition.y << " " << angle << std::endl;
            x += deltaX;
        }
        y += deltaY;
    }

    UnivectorController univectorController(Player::MAX_WHEEL_SPEED * Player::WHEEL_RADIUS, 2.0 * M_PI * 2.0, Player::MAX_WHEEL_SPEED);

    DifferentialRobotKinematicSimulator simulator(Player::WHEEL_RADIUS, Player::DISTANCE_WHEELS,
                                                  Player::MAX_WHEEL_SPEED, initialPose);

    double dt = 1.0 / 60.0;

    int iteration = 0;
    int numIterations = 2.5 * 60; // 10 seconds
    double time = 0.0;
    double angle = 0.0;
    WheelSpeed wheelSpeed;
    UnicycleSpeed unicycleSpeed;
    trajectoryFile << time << " " << simulator.getPose().translation.x << " " << simulator.getPose().translation.y << " " << simulator.getPose().rotation << std::endl;
    while (iteration < numIterations) {
        Vector2<double> currentVelocity(unicycleSpeed.linear * cos(simulator.getPose().rotation), unicycleSpeed.linear * sin(simulator.getPose().rotation));
        double angle = univectorField.computeComposedField(target, simulator.getPose().translation, currentVelocity, obstacle);
        wheelSpeed = univectorController.control(angle, simulator.getPose().rotation);
        unicycleSpeed = UnicycleSpeed(wheelSpeed);
        simulator.step(dt, unicycleSpeed.linear, unicycleSpeed.angular);
        time += dt;
        ++iteration;
        const Pose2D &pose = simulator.getPose();
        trajectoryFile << time << " " << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
    }
    fieldFile.flush();
    trajectoryFile.flush();
    fieldFile.close();
    trajectoryFile.close();
}