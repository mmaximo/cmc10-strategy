function testUnivectorField()

testHyperbolicSpiralField();
% testRepulsiveField();
% testMoveToGoalField();
% testAvoidObstacleField();
% testComposedField();

end

function testHyperbolicSpiralField()

system('./../../../../binaries/UnivectorField_ComponentTest 0');
field = load('univector_field.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Hyperbolic Spiral Field', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'hyperbolic_spiral_field.png');
% print('-depsc2', 'hyperbolic_spiral_field.eps');

end

function testRepulsiveField()

system('./../../../../binaries/UnivectorField_ComponentTest 1');
field = load('univector_field.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Repulsive Field', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'repulsive_field.png');
% print('-depsc2', 'repulsive_field.eps');

end

function testMoveToGoalField()

system('./../../../../binaries/UnivectorField_ComponentTest 2');
field = load('univector_field.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Move to Goal Field', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'move_to_goal_field.png');
% print('-depsc2', 'move_to_goal_field.eps');

end

function testAvoidObstacleField()

system('./../../../../binaries/UnivectorField_ComponentTest 3');
field = load('univector_field.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Avoid Obstacle Field', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'avoid_obstacle_field.png');
% print('-depsc2', 'avoid_obstacle_field.eps');

end

function testComposedField()

system('./../../../../binaries/UnivectorField_ComponentTest 4');
field = load('univector_field.txt');

figure;
hold on;
numPoints = size(field, 1);
for i=1:numPoints
    translation = field(i, 1:2);
    rotation = field(i, 3);
    arrowBegin = translation;
    arrowEnd = translation + 0.05 * [cos(rotation), sin(rotation)];
    drawArrow(arrowBegin, arrowEnd, 'r');
end
axis equal;
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
grid on;
title('Composed Field', 'FontSize', 14);
set(gca, 'FontSize', 14);
print('-dpng', '-r400', 'composed_field.png');
% print('-depsc2', 'composed_field.eps');

end