//
// Created by mmaximo on 10/18/18.
//

#include <iostream>
#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "math/MathUtils.h"
#include "modeling/vision/Image.h"
#include "modeling/vision/ColorTable.h"

using core::modeling::ColorTable;
using core::modeling::Image;
using core::modeling::VSSColor;

void buildColorTable(ColorTable &colorTable) {
    int r, g, b;

    for (unsigned char y = 0; y < 255; ++y) {
        for (unsigned char u = 0; u < 255; ++u) {
            for (unsigned char v = 0; v < 255; ++v) {
                Image::yuv444ToRgb(y, u, v, &r, &g, &b);
                if (r > 200 && g > 200 && b > 200)
                    colorTable.assignColor(y, u, v, VSSColor::WHITE);
                else if (r > 200)
                    colorTable.assignColor(y, u, v, VSSColor::RED);
                else if (g > 200)
                    colorTable.assignColor(y, u, v, VSSColor::GREEN);
                else if (b > 200)
                    colorTable.assignColor(y, u, v, VSSColor::BLUE);
                else
                    colorTable.assignColor(y, u, v, VSSColor::UNDEFINED);
            }
        }
    }
}

int main() {
    using core::math::MathUtils;

    double sigma = 30.0;

    int imageWidth = 656;
    int imageHeight = 516;

    auto normalRandomGenerator = MathUtils::getNormalRandomGenerator(0.0, sigma);

    ColorTable colorTable;

    buildColorTable(colorTable);

    cv::Scalar blueColor, redColor, greenColor, whiteColor, blackColor;
    cv::Scalar colors[4];
    colors[0] = blueColor = cv::Scalar(220, 0, 0);
    colors[1] = greenColor = cv::Scalar(0, 220, 0);
    colors[2] = redColor = cv::Scalar(0, 0, 220);
    colors[3] = whiteColor = cv::Scalar(220, 220, 220);
    blackColor = cv::Scalar(0, 0, 0);

    cv::Mat image(imageHeight, imageWidth, CV_8UC3);
    image.setTo(blackColor);

    int dx = 100;
    int dy = 100;
    int radius = 30;
    int index = 0;
    int angle = 0;
    for (int x = 0; x < imageWidth; x += dx) {
        for (int y = 0; y < imageHeight; y += dy) {
            cv::RotatedRect rRect(cv::Point2f(x,y), cv::Size2f(2*radius,radius), angle);
            cv::Point2f vertices2f[4];
            cv::Point vertices[4];
            rRect.points(vertices2f);
            for (int i = 0; i < 4; i++)
                vertices[i] = vertices2f[i];
            cv::fillConvexPoly(image, vertices, 4, colors[index % 4]);
            angle += 30;
            ++index;
        }
    }

    for (int x = 0; x < imageWidth; ++x) {
        for (int y = 0; y < imageHeight; ++y) {
            int offset = 3 * (y * imageWidth + x);

            double b = MathUtils::clamp(image.data[offset] + normalRandomGenerator(), 0.0, 255.0);
            double g = MathUtils::clamp(image.data[offset + 1] + normalRandomGenerator(), 0.0, 255.0);
            double r = MathUtils::clamp(image.data[offset + 2] + normalRandomGenerator(), 0.0, 255.0);

            image.data[offset] = b;
            image.data[offset + 1] = g;
            image.data[offset + 2] = r;
        }
    }

    Image yuv422Image(imageHeight, imageWidth, core::modeling::PixelFormat::YUV422);

    Image::bgrToYuv422(image.data, yuv422Image.getData(), imageWidth, imageHeight);

    Image segmentedImage(imageWidth, imageHeight, core::modeling::PixelFormat::SEGMENTED);

    colorTable.segmentImage(yuv422Image, segmentedImage);

    cv::Mat bgrSegmentedImage(imageHeight, imageWidth, CV_8UC3);

    for (int x = 0; x < imageWidth; ++x) {
        for (int y = 0; y < imageHeight; ++y) {
            cv::Scalar bgrColor(0, 0, 0);
            unsigned char colorClass = segmentedImage.getData()[y * imageWidth + x];
            switch (colorClass) {
                case VSSColor::WHITE:
                    bgrColor = whiteColor;
                    break;
                case VSSColor::BLUE:
                    bgrColor = blueColor;
                    break;
                case VSSColor::GREEN:
                    bgrColor = greenColor;
                    break;
                case VSSColor::RED:
                    bgrColor = redColor;
                    break;
                case VSSColor::UNDEFINED:
                    bgrColor = blackColor;
                default:
                    break;
            }
            bgrSegmentedImage.data[3 * (y * imageWidth + x)] = bgrColor[0];
            bgrSegmentedImage.data[3 * (y * imageWidth + x) + 1] = bgrColor[1];
            bgrSegmentedImage.data[3 * (y * imageWidth + x) + 2] = bgrColor[2];
        }
    }

    cv::namedWindow("Noisy Image", CV_WINDOW_AUTOSIZE);
    cv::imshow("Noisy Image", image);

    cv::namedWindow("Segmented Image", CV_WINDOW_AUTOSIZE);
    cv::imshow("Segmented Image", bgrSegmentedImage);
    cv::imwrite("segmented_image.jpg", image);

    cv::waitKey(0);
}