//
// Created by mmaximo on 10/18/18.
//

#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include "modeling/vision/BlobDetector.h"
#include "modeling/vision/BlobCollection.h"
#include "modeling/vision/Blob.h"
#include "modeling/vision/Image.h"
#include "math/MathUtils.h"
#include "math/Vector2.h"
#include "utils/Clock.h"

int main() {
    using namespace core::modeling;
    using core::math::MathUtils;

    enum TestCase { SIMPLE, CIRCLE_GRID, ROTATED_RECTS, ROTATED_RECTS_NOISE };

    TestCase  testCase = SIMPLE;

    const int MAX_BLOBS = 200;

    int imageWidth = 656;
    int imageHeight = 516;

    cv::Mat image(imageHeight, imageWidth, CV_8UC3);
    if (testCase == SIMPLE) {
        cv::line(image, cv::Point(100, 100), cv::Point(300, 300), cv::Scalar(255, 255, 255), 6);
        cv::circle(image, cv::Point(500, 100), 50, cv::Scalar(0, 0, 255), -1);
        cv::rectangle(image, cv::Point(100, 350), cv::Point(200, 450), cv::Scalar(255, 0, 0), -1);
    } else if (testCase == CIRCLE_GRID) {
        cv::Scalar colors[4];
        colors[0] = cv::Scalar(255, 0, 0);
        colors[1] = cv::Scalar(0, 255, 0);
        colors[2] = cv::Scalar(0, 0, 255);
        colors[3] = cv::Scalar(255, 255, 255);
        int dx = 100;
        int dy = 100;
        int radius = 30;
        int index = 0;
        for (int x = 0; x < imageWidth; x += dx) {
            for (int y = 0; y < imageHeight; y += dy) {
                cv::circle(image, cv::Point(x, y), radius, colors[index % 4], -1);
                ++index;
            }
        }
    } else if (testCase == ROTATED_RECTS) {
        cv::Scalar colors[4];
        colors[0] = cv::Scalar(255, 0, 0);
        colors[1] = cv::Scalar(0, 255, 0);
        colors[2] = cv::Scalar(0, 0, 255);
        colors[3] = cv::Scalar(255, 255, 255);
        int dx = 100;
        int dy = 100;
        int radius = 30;
        int index = 0;
        int angle = 0;
        for (int x = 0; x < imageWidth; x += dx) {
            for (int y = 0; y < imageHeight; y += dy) {
                cv::RotatedRect rRect(cv::Point2f(x,y), cv::Size2f(2*radius,radius), angle);
                cv::Point2f vertices2f[4];
                cv::Point vertices[4];
                rRect.points(vertices2f);
                for (int i = 0; i < 4; i++)
                    vertices[i] = vertices2f[i];
                cv::fillConvexPoly(image, vertices, 4, colors[index % 4]);
                angle += 30;
                ++index;
            }
        }
    } else {
        cv::Scalar colors[4];
        colors[0] = cv::Scalar(255, 0, 0);
        colors[1] = cv::Scalar(0, 255, 0);
        colors[2] = cv::Scalar(0, 0, 255);
        colors[3] = cv::Scalar(255, 255, 255);
        int dx = 100;
        int dy = 100;
        int radius = 30;
        int index = 0;
        int angle = 0;

        auto uniformRandomGenerator = MathUtils::getUniformRandomGenerator(0.0, 1.0);
        for (int x = 0; x < imageWidth; x += dx) {
            for (int y = 0; y < imageHeight; y += dy) {
                cv::RotatedRect rRect(cv::Point2f(x,y), cv::Size2f(2*radius,radius), angle);
                cv::Point2f vertices2f[4];
                cv::Point vertices[4];
                rRect.points(vertices2f);
                for (int i = 0; i < 4; i++)
                    vertices[i] = vertices2f[i];
                cv::fillConvexPoly(image, vertices, 4, colors[index % 4]);
                angle += 30;
                ++index;
            }
            if (uniformRandomGenerator() < 0.2) {
                cv::line(image, cv::Point(x, 0), cv::Point(x, imageHeight), cv::Scalar(0, 0, 0), 2);
            }
        }

        int noiseRadius = 2;
        for (int x = 0; x < imageWidth; x += 2) {
            for (int y = 0; y < imageHeight; y += 2) {
                if (uniformRandomGenerator() < 0.001) {
                    int c = static_cast<int>(uniformRandomGenerator() * 4.0);
                    cv::circle(image, cv::Point(x, y), noiseRadius, colors[c], -1);
                }
                ++index;
            }
        }
    }

    cv::namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    cv::imshow("Original Image", image);

    enum Color { UNDEFINED = 0, RED, GREEN, BLUE, WHITE, NUM_COLORS };
    int size = imageWidth * imageHeight;
    unsigned char segmentedData[size];
    for (int y = 0; y < imageHeight; ++y) {
        for (int x = 0; x < imageWidth; ++x) {
            cv::Vec3b pixel = image.at<cv::Vec3b>(y, x);
            uchar b = pixel.val[0];
            uchar g = pixel.val[1];
            uchar r = pixel.val[2];
            int index = y * imageWidth + x;
            if (r > 200 && g > 200 && r > 200)
                segmentedData[index] = WHITE;
            else if (r > 220)
                segmentedData[index] = RED;
            else if (g > 220)
                segmentedData[index] = GREEN;
            else if (b > 220)
                segmentedData[index] = BLUE;
            else
                segmentedData[index] = UNDEFINED;
        }
    }

    Image segmentedImage(segmentedData, imageWidth, imageHeight, PixelFormat::SEGMENTED);

    BlobDetector blobDetector(imageWidth, imageHeight, 30, 3000, 5, 5);
    BlobCollection blobs(NUM_COLORS, MAX_BLOBS);
    double sumBlobs = 0.0;
    int numTrials = 100;
    core::utils::Clock clock;
    for (int i = 0; i < numTrials; ++i) {
        blobs.clear();
        double tic = clock.getTime();
        blobDetector.computeBlobs(segmentedImage, blobs);
        blobDetector.mergeBlobs(blobs);
        blobDetector.filterBlobs(blobs);
        sumBlobs += clock.getTime() - tic;
    }
    std::cout << "Mean time taken to compute blobs: " << sumBlobs / numTrials << " (" << numTrials << " trials)" <<  std::endl;

    using core::math::Vector2;
    cv::Scalar yellow(0, 255, 255);
    cv::Scalar brown(19, 69, 139);
    for (int c = 1; c < NUM_COLORS; ++c) {
        for (int b = 0; b < blobs.count[c]; ++b) {
            if (blobs.table[c][b].valid) {
                Blob& blob = blobs.table[c][b];
                cv::Point center(cv::Point(int(blob.position.x), int(blob.position.y)));

                cv::rectangle(image, cv::Point(blob.xi, blob.yi), cv::Point(blob.xf, blob.yf), yellow, 2);
                cv::circle(image, center, 3, yellow, 2);
            }
        }
    }

    cv::namedWindow("Blob Detection", CV_WINDOW_AUTOSIZE);
    cv::imshow("Blob Detection", image);
    cv::imwrite("image.jpg", image);
    cv::waitKey(0);
    return 0;
}