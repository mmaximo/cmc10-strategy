//
// Created by mmaximo on 10/19/18.
//

#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include "modeling/vision/ObjectDetector.h"
#include "modeling/vision/BlobDetector.h"
#include "modeling/vision/BlobCollection.h"
#include "modeling/vision/Blob.h"
#include "modeling/vision/Image.h"
#include "math/MathUtils.h"
#include "math/Vector2.h"
#include "utils/Clock.h"

using core::modeling::BlobCollection;
using core::modeling::Blob;
using core::modeling::ObjectDetector;
using core::math::Vector2;
using core::math::Pose2D;
using core::math::MathUtils;
using core::modeling::Image;
using core::modeling::VSSColor;
using core::modeling::VisibleTeammate;

void computeAndDrawPlayer(const Pose2D &player, cv::Mat &image, BlobCollection &blobs, VSSColor teamColor,
                          VSSColor idColor) {
    cv::Scalar bgrTeamColor;
    if (teamColor == VSSColor::BLUE)
        bgrTeamColor = cv::Scalar(255, 0, 0);
    else
        bgrTeamColor = cv::Scalar(0, 255, 255);
    cv::Scalar bgrIdColor;
    switch (idColor) {
        case VSSColor::PINK:
            bgrIdColor = cv::Scalar(148, 24, 248);
            break;
        case VSSColor::RED:
            bgrIdColor = cv::Scalar(0, 0, 255);
            break;
        case VSSColor::GREEN:
            bgrIdColor = cv::Scalar(0, 255, 0);
            break;
        default:
            bgrIdColor = cv::Scalar(0, 0, 0);
            break;
    }

    static const int robotMainPatternWidth = 60;
    static const int robotMainPatternHeight = 30;
    static const int robotSecondaryPatternWidth = 20;
    static const int robotSecondaryPatternHeight = 15;
    static const double robotSecondaryPatternDeltaX = 20 / 1000.0;
    static const double robotSecondaryPatternDeltaY = 25 / 1000.0;

    cv::Point2f vertices2f[4];
    cv::Point vertices[4];

    Pose2D secondaryMarker1;
    secondaryMarker1.translation.x = player.translation.x + robotSecondaryPatternDeltaX * cos(player.rotation) - robotSecondaryPatternDeltaY * sin(player.rotation);
    secondaryMarker1.translation.y = player.translation.y + robotSecondaryPatternDeltaX * sin(player.rotation) + robotSecondaryPatternDeltaY * cos(player.rotation);
    secondaryMarker1.rotation = player.rotation;

    Pose2D secondaryMarker2;
    secondaryMarker2.translation.x = player.translation.x + robotSecondaryPatternDeltaX * cos(player.rotation) + robotSecondaryPatternDeltaY * sin(player.rotation);
    secondaryMarker2.translation.y = player.translation.y + robotSecondaryPatternDeltaX * sin(player.rotation) - robotSecondaryPatternDeltaY * cos(player.rotation);
    secondaryMarker2.rotation = player.rotation;

    Blob blob;
    blob.valid = true;

    blob.position = player.translation * 1000.0;
    blob.worldPosition = player.translation;
    blobs.addBlob(teamColor, blob);

    blob.position = secondaryMarker1.translation * 1000.0;
    blob.worldPosition = secondaryMarker1.translation;
    blobs.addBlob(idColor, blob);

    blob.position = secondaryMarker2.translation * 1000.0;
    blob.worldPosition = secondaryMarker2.translation;
    blobs.addBlob(idColor, blob);

    cv::RotatedRect rRect(cv::Point2f(1000.0 * player.translation.x, 1000.0 * player.translation.y), cv::Size2f(robotMainPatternWidth, robotMainPatternHeight), MathUtils::radiansToDegrees(player.rotation));
    rRect.points(vertices2f);
    for (int i = 0; i < 4; i++)
        vertices[i] = vertices2f[i];
    cv::fillConvexPoly(image, vertices, 4, bgrTeamColor);

    rRect = cv::RotatedRect(cv::Point2f(1000.0 * secondaryMarker1.translation.x, 1000.0 * secondaryMarker1.translation.y), cv::Size2f(robotSecondaryPatternWidth, robotSecondaryPatternHeight), MathUtils::radiansToDegrees(secondaryMarker1.rotation));
    rRect.points(vertices2f);
    for (int i = 0; i < 4; i++)
        vertices[i] = vertices2f[i];
    cv::fillConvexPoly(image, vertices, 4, bgrIdColor);

    rRect = cv::RotatedRect(cv::Point2f(1000.0 * secondaryMarker2.translation.x, 1000.0 * secondaryMarker2.translation.y), cv::Size2f(robotSecondaryPatternWidth, robotSecondaryPatternHeight), MathUtils::radiansToDegrees(secondaryMarker2.rotation));
    rRect.points(vertices2f);
    for (int i = 0; i < 4; i++)
        vertices[i] = vertices2f[i];
    cv::fillConvexPoly(image, vertices, 4, bgrIdColor);
}



int main() {
    int ballRadius = 20;

    int imageWidth = 656;
    int imageHeight = 516;
    cv::Mat image(imageHeight, imageWidth, CV_8UC3);
    image.setTo(cv::Scalar(0, 0, 0));

    BlobCollection blobs(VSSColor::NUM_COLORS, 200);
    ObjectDetector detector(VSSColor::BLUE);

    std::vector<VSSColor> idToColorMapping = ObjectDetector::getDefaultIdToColorMapping();

    double arenaWidth = imageWidth / 1000.0;
    double arenaHeight = imageHeight / 1000.0;
    std::vector<Pose2D> teammates;
    teammates.emplace_back(Pose2D(0.0, 100.0 / 1000.0, arenaHeight / 2.0));
    teammates.emplace_back(Pose2D(MathUtils::degreesToRadians(30.0), arenaWidth / 2.0, arenaHeight / 4.0));
    teammates.emplace_back(Pose2D(MathUtils::degreesToRadians(-90.0), 3.0 * arenaWidth / 4.0, 3.0 * arenaHeight / 4.0));

    for (int i = 0; i < teammates.size(); ++i)
        computeAndDrawPlayer(teammates[i], image, blobs, VSSColor::BLUE, idToColorMapping[i]);

    std::vector<Pose2D> opponents;
    opponents.emplace_back(Pose2D(0.0, 300.0 / 1000.0, 3.0 * arenaHeight / 4.0));
    opponents.emplace_back(Pose2D(MathUtils::degreesToRadians(50.0), 100.0 / 1000.0, 3.0 * arenaHeight / 4.0));
    opponents.emplace_back(Pose2D(MathUtils::degreesToRadians(-100.0), 3.0 * arenaWidth / 4.0, arenaHeight / 4.0));

    for (int i = 0; i < teammates.size(); ++i)
        computeAndDrawPlayer(opponents[i], image, blobs, VSSColor::YELLOW, idToColorMapping[i]);

    Vector2<double> ball(arenaWidth / 2.0, arenaHeight / 2.0);
    Blob ballBlob;
    ballBlob.position = ball * 1000.0;
    ballBlob.worldPosition = ball;
    ballBlob.xi = ballBlob.position.x - ballRadius;
    ballBlob.xf = ballBlob.position.x + ballRadius;
    ballBlob.yi = ballBlob.position.y - ballRadius;
    ballBlob.yf = ballBlob.position.y + ballRadius;
    ballBlob.valid = true;
    blobs.addBlob(VSSColor::ORANGE, ballBlob);

    cv::circle(image, cv::Point(1000.0 * ball.x, 1000.0 * ball.y), ballRadius, cv::Scalar(0, 165, 255), -1);

    std::vector<VisibleTeammate> visibleTeammates;
    std::vector<Vector2<double>> visibleOpponents;
    Vector2<double> visibleBall;
    detector.detectTeammates(blobs, visibleTeammates);
    detector.detectOpponents(blobs, visibleOpponents);
    detector.detectBall(blobs, visibleBall);

    cv::Scalar yellow(0, 255, 255);
    cv::Scalar brown(19, 69, 139);
    int delta = 20;
    for (auto &visibleTeammate : visibleTeammates) {
        const Pose2D &pose = visibleTeammate.getPose();
        cv::Point center(static_cast<int>(1000.0 * pose.translation.x), static_cast<int>(1000.0 * pose.translation.y));
        cv::Point arrowEnd(static_cast<int>(1000.0 * pose.translation.x + delta * cos(pose.rotation)), static_cast<int>(1000.0 * pose.translation.y + delta * sin(pose.rotation)));

        VSSColor idColor = idToColorMapping[visibleTeammate.getId()];
        cv::Scalar bgrIdColor;
        switch (idColor) {
            case VSSColor::PINK:
                bgrIdColor = cv::Scalar(148, 24, 248);
                break;
            case VSSColor::RED:
                bgrIdColor = cv::Scalar(0, 0, 255);
                break;
            case VSSColor::GREEN:
                bgrIdColor = cv::Scalar(0, 255, 0);
                break;
            default:
                bgrIdColor = cv::Scalar(0, 0, 0);
                break;
        }

        cv::circle(image, center, 3, bgrIdColor, 2);
        cv::line(image, center, arrowEnd, bgrIdColor, 2);

        std::cout << "Teammate found: " << "[id: " << visibleTeammate.getId() << ", " << visibleTeammate.getPose() << "]" << std::endl;
    }

    for (auto &visibleOpponent : visibleOpponents) {
        cv::Point center(static_cast<int>(1000.0 * visibleOpponent.x), static_cast<int>(1000.0 * visibleOpponent.y));

        cv::circle(image, center, 3, brown, 2);

        std::cout << "Opponent found: " << "[" << visibleOpponent << "]" << std::endl;
    }

    cv::Point center(static_cast<int>(1000.0 * visibleBall.x), static_cast<int>(1000.0 * visibleBall.y));
    cv::circle(image, center, 3, brown, 2);

    cv::namedWindow("Object Detection", CV_WINDOW_AUTOSIZE);
    cv::imshow("Object Detection", image);
    cv::imwrite("image.jpg", image);
    cv::waitKey(0);
}