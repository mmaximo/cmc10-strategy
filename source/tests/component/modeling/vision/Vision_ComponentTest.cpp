//
// Created by mmaximo on 10/26/18.
//


#include <cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "modeling/vision/CameraVision.h"
#include "representations/Player.h"

int main() {
    using core::modeling::VSSColor;
    using core::math::Vector2;
    using core::representations::Player;
    using core::representations::Ball;

    core::modeling::CameraVision cameraVision;
    core::modeling::CameraProjection cameraProjection(
            "../configs/intrinsicParameters.txt",
            "../configs/extrinsicParameters.txt");

    int imageWidth = 656;
    int imageHeight = 516;
    cv::Mat rgbImage(imageHeight, imageWidth, CV_8UC3);
    cv::Mat bgrImage(imageHeight, imageWidth, CV_8UC3);
    core::modeling::VisionInterface *visionInterface;
    cv::namedWindow("Vision Test", CV_WINDOW_AUTOSIZE);
    unsigned char *rgbData;

    while (true) {
        visionInterface = cameraVision.update();

        rgbData = cameraVision.getImagePtr();

        memcpy(rgbImage.data, rgbData, 3 * imageWidth * imageHeight);

        cv::cvtColor(rgbImage, bgrImage, cv::COLOR_RGB2BGR);

        std::vector<VSSColor> idToColorMapping = core::modeling::ObjectDetector::getDefaultIdToColorMapping();

        cv::Scalar yellow(0, 255, 255);
        cv::Scalar brown(19, 69, 139);
        double delta = 0.05;
        for (auto &teammate : visionInterface->teammates) {
            if (!teammate.isSeen())
                continue;

            const Pose2D &pose = teammate.getPose();
            Vector2<double> position2D = cameraProjection.project3DTo2D(pose.translation,
                                                                        Player::TOTAL_HEIGHT);
            Vector2<double> arrowEndWorld = Vector2<double>(pose.translation.x + delta * cos(pose.rotation),
                                                            pose.translation.y + delta * sin(pose.rotation));
            Vector2<double> arrowEnd2D = cameraProjection.project3DTo2D(arrowEndWorld, Player::TOTAL_HEIGHT);
            cv::Point center(position2D.x, position2D.y);
            cv::Point arrowEnd(arrowEnd2D.x, arrowEnd2D.y);

            VSSColor idColor = idToColorMapping[teammate.getId()];
            cv::Scalar bgrIdColor;
            switch (idColor) {
                case VSSColor::PINK:
                    bgrIdColor = cv::Scalar(148, 24, 248);
                    break;
                case VSSColor::RED:
                    bgrIdColor = cv::Scalar(0, 0, 255);
                    break;
                case VSSColor::GREEN:
                    bgrIdColor = cv::Scalar(0, 255, 0);
                    break;
                default:
                    bgrIdColor = cv::Scalar(0, 0, 0);
                    break;
            }

            cv::circle(bgrImage, center, 3, bgrIdColor, 2);
            cv::line(bgrImage, center, arrowEnd, bgrIdColor, 2);

            std::cout << "Teammate found: " << "[id: " << teammate.getId() << ", " << teammate.getPose() << "]"
                      << std::endl;
        }

        for (auto &opponent : visionInterface->opponents) {
            if (!opponent.isSeen())
                continue;

            const Vector2<double> &position = opponent.getPosition();
            Vector2<double> position2D = cameraProjection.project3DTo2D(position,
                                                                        Player::TOTAL_HEIGHT);
            cv::Point center(position2D.x, position2D.y);

            cv::circle(bgrImage, center, 3, brown, 2);

            std::cout << "Opponent found: " << "[" << opponent.getPosition() << "]" << std::endl;
        }


        Vector2<double> position2D = cameraProjection.project3DTo2D(visionInterface->ball.getPosition(),
                                                                    Ball::DIAMETER);
        cv::Point center(position2D.x, position2D.y);
        cv::circle(bgrImage, center, 3, brown, 2);

        cv::imshow("Vision Test", bgrImage);

        cv::waitKey(1);
    }
}