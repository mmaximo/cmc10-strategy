//
// Created by mmaximo on 11/2/18.
//

#include <iostream>
#include <string>
#include <fstream>
#include <utils/simulator/DifferentialRobotKinematicSimulator.h>
#include <core/representations/Player.h>
#include <core/control/FollowLineController.h>
#include <core/math/MathUtils.h>

int main(int argc, char* argv[]) {
    using core::utils::DifferentialRobotKinematicSimulator;
    using core::representations::Player;
    using core::representations::UnicycleSpeed;
    using core::control::FollowLineController;
    using core::control::FollowLineParams;
    using core::math::MathUtils;
    using core::representations::Player;
    using core::math::Pose2D;
    using core::representations::WheelSpeed;
    using core::representations::UnicycleSpeed;

    std::ofstream poseFile("pose.txt");
    std::ofstream wheelSpeedFile("wheel_speed.txt");

    Pose2D initialPose(0.0, 0.0, 0.0);
    Pose2D desiredPose(0.0, 2.0, 1.0);

    if (argc > 3) {
        initialPose.rotation = std::atof(argv[1]);
        desiredPose.translation.x = std::atof(argv[2]);
        desiredPose.translation.y = std::atof(argv[3]);
        desiredPose.rotation = std::atof(argv[4]);
    }

    FollowLineParams params;
    params.kx = 2.0 * M_PI * 1.0;
    params.wn = 2.0 * M_PI * 1.0;
    params.xi = 0.7;
    params.maxAngleReference = MathUtils::degreesToRadians(80.0);
    double linearSpeedLimit = Player::MAX_WHEEL_SPEED * Player::WHEEL_RADIUS;
    params.minLinearSpeedForKh = 0.1 * linearSpeedLimit;
    params.maxLinearSpeed = 0.9 * linearSpeedLimit;
    params.maxWheelSpeed = Player::MAX_WHEEL_SPEED;
    FollowLineController followLineController(params);

    DifferentialRobotKinematicSimulator simulator(Player::WHEEL_RADIUS, Player::DISTANCE_WHEELS,
                                                  Player::MAX_WHEEL_SPEED, initialPose);

    double dt = 1.0 / 60.0;

    int iteration = 0;
    int numIterations = 10 * 60; // 10 seconds
    double time = 0.0;
    WheelSpeed wheelSpeed;
    UnicycleSpeed unicycleSpeed;
    poseFile << time << " " << simulator.getPose().translation.x << " " << simulator.getPose().translation.y << " " << simulator.getPose().rotation << std::endl;
    wheelSpeedFile << time << " " << wheelSpeed.left << " " << wheelSpeed.right << std::endl;
    while (iteration < numIterations) {
        wheelSpeed = followLineController.control(desiredPose, simulator.getPose());
        unicycleSpeed = UnicycleSpeed(wheelSpeed);
        simulator.step(dt, unicycleSpeed.linear, unicycleSpeed.angular);
        time += dt;
        ++iteration;
        const Pose2D &pose = simulator.getPose();
//        std::cout << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
        poseFile << time << " " << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
        wheelSpeedFile << time << " " << wheelSpeed.left << " " << wheelSpeed.right << std::endl;
    }
    poseFile.flush();
    wheelSpeedFile.flush();
    poseFile.close();
    wheelSpeedFile.close();
}