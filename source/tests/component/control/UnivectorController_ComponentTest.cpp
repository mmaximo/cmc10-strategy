//
// Created by mmaximo on 11/2/18.
//

#include <iostream>
#include <string>
#include <fstream>
#include <utils/simulator/DifferentialRobotKinematicSimulator.h>
#include <core/representations/Player.h>
#include <core/control/FollowLineController.h>
#include <core/control/UnivectorController.h>
#include <core/math/MathUtils.h>

int main(int argc, char* argv[]) {
    using core::utils::DifferentialRobotKinematicSimulator;
    using core::representations::Player;
    using core::representations::UnicycleSpeed;
    using core::math::MathUtils;
    using core::representations::Player;
    using core::math::Pose2D;
    using core::representations::WheelSpeed;
    using core::representations::UnicycleSpeed;
    using core::control::UnivectorController;

    Pose2D initialPose(0.0, 0.0, 0.0);

    std::ofstream poseFile("pose.txt");
    std::ofstream wheelSpeedFile("wheel_speed.txt");

    if (argc > 1) {
        initialPose.rotation = std::atof(argv[1]);
    }

    UnivectorController univectorController(Player::MAX_WHEEL_SPEED * Player::WHEEL_RADIUS, 2.0 * M_PI * 2.0, Player::MAX_WHEEL_SPEED);

    DifferentialRobotKinematicSimulator simulator(Player::WHEEL_RADIUS, Player::DISTANCE_WHEELS,
                                                  Player::MAX_WHEEL_SPEED, initialPose);

    double dt = 1.0 / 60.0;

    int iteration = 0;
    int numIterations = 10 * 60; // 10 seconds
    double time = 0.0;
    double angle = 0.0;
    WheelSpeed wheelSpeed;
    UnicycleSpeed unicycleSpeed;
    poseFile << time << " " << simulator.getPose().translation.x << " " << simulator.getPose().translation.y << " " << simulator.getPose().rotation << std::endl;
    wheelSpeedFile << time << " " << wheelSpeed.left << " " << wheelSpeed.right << std::endl;
    while (iteration < numIterations) {
        wheelSpeed = univectorController.control(angle, simulator.getPose().rotation);
        unicycleSpeed = UnicycleSpeed(wheelSpeed);
        simulator.step(dt, unicycleSpeed.linear, unicycleSpeed.angular);
        time += dt;
        ++iteration;
        const Pose2D &pose = simulator.getPose();
        angle += 2.0 * M_PI * dt / 5.0;
        poseFile << time << " " << pose.translation.x << " " << pose.translation.y << " " << pose.rotation << std::endl;
        wheelSpeedFile << time << " " << wheelSpeed.left << " " << wheelSpeed.right << std::endl;
    }
    poseFile.flush();
    wheelSpeedFile.flush();
    poseFile.close();
    wheelSpeedFile.close();
}