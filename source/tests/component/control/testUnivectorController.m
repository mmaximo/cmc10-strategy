function testUnivectorController()

testUnivectorControllerInitialAngle(0.0);
testUnivectorControllerInitialAngle(pi);

end

function testUnivectorControllerInitialAngle(initialAngle)


system(sprintf('./../../../../binaries/UnivectorController_ComponentTest %f',...
    initialAngle));
poseLog = load('pose.txt');
wheelSpeedLog = load('wheel_speed.txt');

figure;
hold on;
plot(poseLog(:, 2), poseLog(:, 3), 'LineWidth', 2);
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Univector Controller Test - XY', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
axis equal;
print('-dpng', '-r400', sprintf('univector_controller_xy_%.2f.png', initialAngle));
print('-depsc2', sprintf('univector_controller_xy_%.2f.eps', initialAngle));

figure;
hold on;
plot(poseLog(:, 1), poseLog(:, 4), 'LineWidth', 2);
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Univector Controller Test - Psi', 'FontSize', 14);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', sprintf('univector_controller_psi_%.2f.png', initialAngle));
print('-depsc2', sprintf('univector_controller_psi_%.2f.eps', initialAngle));

end