//
// Created by mmaximo on 11/2/18.
//

#include <iostream>
#include <string>
#include <fstream>
#include <utils/simulator/DifferentialRobotKinematicSimulator.h>
#include <core/representations/Player.h>
#include <core/control/AngleController.h>

int main(int argc, char* argv[]) {
    using core::utils::DifferentialRobotKinematicSimulator;
    using core::representations::Player;
    using core::representations::UnicycleSpeed;
    using core::control::AngleController;

    std::ofstream angleFile("angle.txt");
    std::ofstream angularSpeedFile("angular_speed.txt");

    double initialAngle = 0.0;
    double reference = M_PI;

    if (argc > 1) {
        initialAngle = std::atof(argv[1]);
        if (argc > 2)
            reference = std::atof(argv[2]);
    }

    core::math::Pose2D initialPose(initialAngle, 0.0, 0.0);

    AngleController angleController(2.0 * M_PI * 2.0, 2.0 * Player::MAX_WHEEL_SPEED * Player::WHEEL_RADIUS / Player::DISTANCE_WHEELS);
    DifferentialRobotKinematicSimulator simulator(Player::WHEEL_RADIUS, Player::DISTANCE_WHEELS,
                                                  Player::MAX_WHEEL_SPEED, initialPose);

    double dt = 1.0 / 60.0;

    int iteration = 0;
    int numIterations = 60; // 1 second
    double time = 0.0;
    double angularSpeed = 0.0;
    angleFile << time << " " << simulator.getPose().rotation << std::endl;
    angularSpeedFile << time << " " << angularSpeed << std::endl;
    while (iteration < numIterations) {
        angularSpeed = angleController.control(reference, simulator.getPose().rotation);
        simulator.step(dt, 0.0, angularSpeed);
        time += dt;
        ++iteration;
        angleFile << time << " " << simulator.getPose().rotation << std::endl;
        angularSpeedFile << time << " " << angularSpeed << std::endl;
    }
    angleFile.flush();
    angularSpeedFile.flush();
    angleFile.close();
    angularSpeedFile.close();
}