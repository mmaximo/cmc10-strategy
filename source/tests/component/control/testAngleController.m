function testAngleController()

testAngleControllerInitialAngle(0.0);
testAngleControllerInitialAngle(pi);

end

function testAngleControllerInitialAngle(initialAngle)

references = -(pi/2):(pi/4):(pi/2);

angleLogs = cell(length(references));
angularSpeedLogs = cell(length(references));
for i=1:length(references)
    reference = references(i);
    system(sprintf('./../../../../binaries/AngleController_ComponentTest %f %f', initialAngle, reference));
    angleLogs{i} = load('angle.txt');
    angularSpeedLogs{i} = load('angular_speed.txt');
    legs{i} = sprintf('ref = %.2f', references(i));
end

figure;
hold on;
for i=1:length(references)
    plot(angleLogs{i}(:, 1), angleLogs{i}(:, 2), 'LineWidth', 2);
end
xlabel('Time (s)', 'FontSize', 14);
ylabel('Angle (rad)', 'FontSize', 14);
title('Angle Controller Test - Angle', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', sprintf('angle_controller_angle_%.2f.png', initialAngle));
print('-depsc2', sprintf('angle_controller_angle_%.2f.eps', initialAngle));

figure;
hold on;
for i=1:length(references)
    plot(angularSpeedLogs{i}(:, 1), angularSpeedLogs{i}(:, 2), 'LineWidth', 2);
end
xlabel('Time (s)', 'FontSize', 14);
ylabel('Angular Speed (rad/s)', 'FontSize', 14);
title('Angle Controller - Angular Speed', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', sprintf('angle_controller_angular_speed_%.2f.png', initialAngle));
print('-depsc2', sprintf('angle_controller_angular_speed_%.2f.eps', initialAngle));

end