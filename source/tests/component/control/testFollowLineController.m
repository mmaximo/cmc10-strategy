function testFollowLineController()

%% Testing lines with different linear coefficients

psiInitial = 0.0;
xReference = 3.0;
yReferences = -2.0:0.5:2.0;
psiReference = 0.0;

poseLogs = cell(length(yReferences));
wheelSpeedLogs = cell(length(yReferences));
for i=1:length(yReferences)
    system(sprintf('./../../../../binaries/FollowLineController_ComponentTest %f %f %f %f',...
        psiInitial, xReference, yReferences(i), psiReference));
    poseLogs{i} = load('pose.txt');
    wheelSpeedLogs{i} = load('wheel_speed.txt');
    legs{i} = sprintf('yRef = %.1f', yReferences(i));
end

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 2), poseLogs{i}(:, 3), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - XY', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
axis equal;
print('-dpng', '-r400', 'follow_line_controller_xy.png');
print('-depsc2', 'follow_line_controller_xy.eps');

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 1), poseLogs{i}(:, 4), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - Psi', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', 'follow_line_controller_psi.png');
print('-depsc2', 'follow_line_controller_psi.eps');

%% Testing lines with different linear coefficients and making the robot move in reverse

psiInitial = 0.0;
xReference = -3.0;
yReferences = -2.0:0.5:2.0;
psiReference = 0.0;

poseLogs = cell(length(yReferences));
wheelSpeedLogs = cell(length(yReferences));
for i=1:length(yReferences)
    system(sprintf('./../../../../binaries/FollowLineController_ComponentTest %f %f %f %f',...
        psiInitial, xReference, yReferences(i), psiReference));
    poseLogs{i} = load('pose.txt');
    wheelSpeedLogs{i} = load('wheel_speed.txt');
    legs{i} = sprintf('yRef = %.1f', yReferences(i));
end

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 2), poseLogs{i}(:, 3), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - XY', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
axis equal
print('-dpng', '-r400', 'follow_line_controller_xy_reverse.png');
print('-depsc2', 'follow_line_controller_xy_reverse.eps');

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 1), poseLogs{i}(:, 4), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - Psi', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', 'follow_line_controller_psi_reverse.png');
print('-depsc2', 'follow_line_controller_psi_reverse.eps');


%% Testing lines with different angles

psiInitial = 0.0;
psiReferences = -pi:(pi/4):pi;
xReferences = cos(psiReferences);
yReferences = sin(psiReferences);

poseLogs = cell(length(yReferences));
wheelSpeedLogs = cell(length(yReferences));
for i=1:length(yReferences)
    system(sprintf('./../../../../binaries/FollowLineController_ComponentTest %f %f %f %f',...
        psiInitial, xReferences(i), yReferences(i), psiReferences(i)));
    poseLogs{i} = load('pose.txt');
    wheelSpeedLogs{i} = load('wheel_speed.txt');
    legs{i} = sprintf('psiRef = %.1f', psiReferences(i));
end

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 2), poseLogs{i}(:, 3), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - XY', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
axis equal
print('-dpng', '-r400', 'follow_line_controller_xy_star.png');
print('-depsc2', 'follow_line_controller_xy_star.eps');

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 1), poseLogs{i}(:, 4), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - Psi', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', 'follow_line_controller_psi_star.png');
print('-depsc2', 'follow_line_controller_psi_star.eps');

%% Testing lines with different angles and initializing orientation as pi

psiInitial = pi;
psiReferences = -pi:(pi/4):pi;
xReferences = cos(psiReferences);
yReferences = sin(psiReferences);

poseLogs = cell(length(yReferences));
wheelSpeedLogs = cell(length(yReferences));
for i=1:length(yReferences)
    system(sprintf('./../../../../binaries/FollowLineController_ComponentTest %f %f %f %f',...
        psiInitial, xReferences(i), yReferences(i), psiReferences(i)));
    poseLogs{i} = load('pose.txt');
    wheelSpeedLogs{i} = load('wheel_speed.txt');
    legs{i} = sprintf('psiRef = %.1f', psiReferences(i));
end

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 2), poseLogs{i}(:, 3), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - XY', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
axis equal
print('-dpng', '-r400', 'follow_line_controller_xy_star_psiInitialPi.png');
print('-depsc2', 'follow_line_controller_xy_star_psiInitialPi.eps');

figure;
hold on;
for i=1:length(yReferences)
    plot(poseLogs{i}(:, 1), poseLogs{i}(:, 4), 'LineWidth', 2);
end
xlabel('X (m)', 'FontSize', 14);
ylabel('Y (m)', 'FontSize', 14);
title('Follow Line Controller Test - Psi', 'FontSize', 14);
legend(legs);
set(gca, 'FontSize', 14);
grid on;
print('-dpng', '-r400', 'follow_line_controller_psi_star_psiInitialPi.png');
print('-depsc2', 'follow_line_controller_psi_star_psiInitialPi.eps');

end